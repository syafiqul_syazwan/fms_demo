const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('USR_DT');

var moment = require('moment');

function standardTime(el,tz) {
    try {
        if (tz) {
            tz = tz.toString();
        }
        else {
            tz = 'Asia/Singapore'
        }
        el = el.toLocaleString('en-US', { timeZone: tz });
      return el = moment(el,"ddd, DD MMM YYYY, hh:mm:ss A")._i;
        //return el = moment(el).format("ddd, DD MMM YYYY, hh:mm:ss A");
    }
    catch (e) {
        console.log('time cnversion failed ' + e);
    }
    //return moment(el).add(8, 'hours').format("ddd, DD MMM YYYY, hh:mm:ss A");
}

module.exports = function(passport){

router.post('/login',passport.authenticate('local',{
  
  failureRedirect:'/login',
  failureFlash: true

}), function (req, res) {
    if (req.user.status != "undefined" && req.user.status == "RESET") {
        res.redirect('/login');

    }
    else {

        if (req.user.status != "undefined" && req.user.status == "pwdexpired") {
            res.redirect('/login');

        }
        else {
            try{
            req.flash('success_msg', 'SUCCESSFULLY LOGIN ' + standardTime(new Date()));
            res.redirect('/dashboard');
            res.redirect('/login');
            }
            catch(e)
            {

            }
        }
    }
})
return router;

};