require('./db');

const mongoose = require('mongoose');

var fs = require('fs');
const { exception } = require('console');
const { exec } = require('child_process');

const db = mongoose.model('HEALTH_DT');

var FMSlogNm;
var cursor;
var currentDate = new Date();

var date = currentDate.getDate();
var month = currentDate.getMonth(); //Be careful! January is 0 not 1
var year = currentDate.getFullYear();
var hrs = currentDate.getHours();
var mins = currentDate.getMinutes();
var sec = currentDate.getSeconds();

var index = 0;
var flag = true;

mongoose.runCommand({ getParameter: 1, "internalQueryExecMaxBlockingSortBytes": 1 }).exec(function(err, data) {

    if (err) {
        console.log("error remove function from line 25");
    } else {
        console.log("Usage limit " + data);
    }
})

function runscript() {
    try {
        console.log("Del: File Writing to: " + FMSlogNm);
        db.createIndexes({ ts: 1 }).exec(function(err, success) {
            if (success) {
                console.log("Successfully set index");
            }
        });
        //db.createIndex({ts:1});
        db.find({ status: "recorded" }, null, { ts: { $lte: currentDate } }).sort({ ts: 1 }).exec(function(err, data) {
            if (err) {
                //console.log(err);
                console.log("Error at retrieving from db: " + err);
                fs.writeFile('log/AGV_' + FMSlogNm + '.log', "Error encountered: " + err, function(err) {


                }).catch(error => {
                    console.log("Failed to write file, ref " + FMSlogNm);
                    console.log("Error: " + error);
                    if (flag) {
                        console.log("Reattempt");
                        flag = false;
                        runscript();
                    } else {
                        console.log("Stopping reattampt");
                        return;
                    }
                })
            } else {
                fs.writeFile('log/AGV_' + FMSlogNm + '.log', JSON.stringify(data), function(err, success) {
                    if (err) {

                    } else {
                        var lastData = data[data.length - 1];
                        console.log("Last Entry for instance:");
                        console.log(lastData);
                        //   console.log("Del: Proceed to check with last date " + latestDate + " for number of entries" + data.length);

                        db.updateMany({ satus: "recorded", ts: { $lte: currentDate } }, { $set: { status: "audited" } }, (err, updated) => {
                                if (err) {
                                    console.log("Failed to Audit Update: " + err);
                                } else {
                                    console.log("Successfullly updated entries in " + FMSlogNm + " Updated numbers: " + updated.length);
                                }

                            })
                            // for (x = 0; x < data.length; x++) {
                            //     db.updateOne({ _id: data[x]._id, status: "recorded" }, { $set: { status: "audited" } }, (err, data2) => {
                            //         if (err) {
                            //             fs.writeFile('log/AGV_' + FMSlogNm + '.log', "Data Auditing unsuccessful at " + data[x]._id + ": " + err, function(err) {})
                            //             console.log("Data Auditing unsuccessful at " + data[x]._id + ": " + err);
                            //             return;
                            //         }
                            //     })
                            // }
                    }
                });
            }
        });
    } catch (e) {
        fs.writeFile('log/AGV_' + FMSlogNm + '.log', "Error encountered: " + e, function(err) {})
        console.log("DEL: Error encountered: " + e);
    }

}
runscript();
