const mongoose = require('mongoose');
const localStrategy = require('passport-local').Strategy;
const user = mongoose.model('USR_DT');
const ACT_DTS = mongoose.model('ACT_DTS');
const NOTIFY = mongoose.model('NOTIFY');
const PWD_History = mongoose.model('PWD_DT');
const MINUTES = 60000;
const DAY = 86400000;
const YEAR = DAY * 365.25;
const BLOCKED_ATTEMPT = '10';

module.exports = function (passport) {
    const err_InvalidUserWrongPwd = ">>>Failed Login<<<<br>Invalid User Name / Wrong Password";
    const err_UserNotFound = ">>>Failed Login<<<<br>Invalid User Credential";
    const err_AccountBlocked = '>>>Account Blocked<<<<br> This user has been Blocked for consecutive failed attempt of ' + BLOCKED_ATTEMPT + ' times.<br>Please seek for administrators to unblock account.';
    const success_AccountReset = `>>>Account Reset<<<<br>Your account has been recently reset.<br>Please Create a new different password`;
    const err_AccountReset = "Invalid Password.\nYour account has been recently reset, \nplease get the correct reset password from Authorised personnel";
    const success_PasswordExpired = ">>>Password Expired<<<<br>You have not changed your password in a long time, please change to a new password";
    var err_UserDupLogin = "Dupplication login session detected: User is already logged in somewhere, please log out from another device/browser first";

    passport.serializeUser(function (user, done) {
        done(null, user)
    })
    passport.deserializeUser(function (user, done) {
        done(null, user)
    })
    passport.use(new localStrategy({
        username: 'username',
        password: 'password',
        passReqToCallback: true

    }
        , (req, username, password, done) => {
            user.findOne({ Username: username, status: "recorded" }, (err, doc) => {
                if (!doc) {
                    // console.log('Username wrong')
                    return done(null, false, req.flash('error_msg', err_UserNotFound));
                }
                else {
                    var act_difftime = 0;
                    var hasACT = doc.ACT_Time == null || doc.ACT_Time == "undefined" ? false : true;
                    if (hasACT) {
                        act_difftime = Math.abs(new Date() - doc.ACT_Time) / MINUTES;
                        err_UserDupLogin = ">>>Duplicated Login<<<<br>Duplicated Login Session detected.<br>Please logout the other session before proceeding here <br>Or if you had experienced unexpected kick out just now, please login again in " + Math.ceil(15 - act_difftime) + " minutes.";
                    }
                    //below if builds have to check for reset user firstly
                    try {

                        if (doc.Reset_Status == "RESET") {

                            if (doc.comparePassword(password, doc.Reset_Pwd)) {
                                if (doc.Session == 'LOGIN' && act_difftime < 15 && act_difftime > 0) {
                                    /*
                                    a control over user login attempt if user is flagged 'Login'                                          
                                    */

                                    return done(null, false, req.flash('error_msg', err_UserDupLogin));
                                }
                                else {

                                    user.findOneAndUpdate({ _id: doc._id }, { $set: { Session: "LOGIN", Last_Login_Time: new Date(), ACT_Time: new Date() } }, (err, success) => {
                                        if (err) {
                                            console.log("Failed to update user session");
                                        }
                                    })
                                    //runs if RESET ACCOUNT CHECKED THE RIGHT PASSWORD
                                    done(null, {
                                        username: doc.Username,
                                        password: doc.Pass,
                                        role: doc.Type,
                                        id: doc._id,
                                        name: doc.Name,
                                        status: doc.Reset_Status,
                                    })
                                    var ACT_REC = new ACT_DTS();
                                    ACT_REC.User_Name = doc.Username;
                                    ACT_REC.Activity = 'User asked to Change Password due to password expired';
                                    ACT_REC.Type = 'LOGIN PAGE';
                                    ACT_REC.TS = new Date();
                                    ACT_REC.status = 'recorded';
                                    ACT_REC.save(err => {
                                        if (err) {
                                            console.log('null')
                                        } else {
                                        }
                                    })
                                }
                            }
                            else {
                                //runs if RESET ACCOUNT CHECKED THE WRONG PASSWORD
                                return done(null, false, req.flash('error_msg', err_AccountReset));
                            }
                        }
                        else {
                            /*
                            >>carry on if user is not in RESET condition and validate with the password
                            >>For ADMIN Level authorities
                            */
                            if (doc.Type == 'ADMIN') {
                                var valid1 = doc.comparePassword(password, doc.Pass)
                                if (valid1) {
                                    if (doc.Session == 'LOGIN' && act_difftime < 15 && act_difftime > 0) {
                                        /*
                                        a control over user login attempt if user is flagged 'Login'                                          
                                        */
                                        return done(null, false, req.flash('error_msg', err_UserDupLogin));
                                    }
                                    else {
                                        var pwd_difference_mins = -1;
                                        var pwd_Difference_year = -1;
                                        var lastPWDDate;
                                        var userID = doc._id;
                                        PWD_History.find({ ID: userID }, { _id: 0, Password_Date: 1 }).sort({ Password_Date: -1 }).exec((error, pwdDate) => {
                                            if (!error) {
                                                if (pwdDate.length > 0) {
                                                    var today = new Date();
                                                    lastPWDDate = pwdDate[0].Password_Date;
                                                    pwd_difference_mins = Math.abs(today - lastPWDDate) / MINUTES;
                                                    //pwd_Difference_day = Math.abs(today - lastPWDDate) / DAY;
                                                    pwd_Difference_year = Math.abs(today - lastPWDDate) / YEAR;
                                                    //if (pwd_difference_mins>= 5 || pwd_Difference_year < 0) {
                                                        if (pwd_Difference_year >= 1 || pwd_Difference_year < 0) {
                                                        doc.status = "pwdexpired";

                                                        done(null, {
                                                            status: doc.status,
                                                            username: doc.Username,
                                                            password: doc.Pass,
                                                            role: doc.Type,
                                                            id: doc._id,
                                                            name: doc.Name,
                                                        })
                                                    }
                                                    //Login with no issue
                                                    else {
                                                        doc.Login_Attempt = 0;
                                                        doc.save(err => {
                                                            if (err) {
                                                                console.log(err)
                                                            } else {
                                                                done(null, {
                                                                    username: doc.Username,
                                                                    password: doc.Pass,
                                                                    role: doc.Type,
                                                                    id: doc._id,
                                                                    name: doc.Name
                                                                })
                                                                user.findOneAndUpdate({ _id: doc._id }, { $set: { Session: "LOGIN", Last_Login_Time: new Date(), ACT_Time: new Date() } }, (err, success) => {
                                                                    if (err) {
                                                                        console.log("Failed to update user session");
                                                                    }
                                                                })
                                                                var ACT_REC = new ACT_DTS();
                                                                ACT_REC.User_Name = doc.Username;
                                                                ACT_REC.Activity = 'Successfully Login';
                                                                ACT_REC.Type = 'LOGIN PAGE';
                                                                ACT_REC.TS = new Date();
                                                                ACT_REC.status = 'recorded';
                                                                ACT_REC.save(err => {
                                                                    if (err) {
                                                                        //
                                                                    }
                                                                    else {
                                                                        //var NOTI = new NOTIFY();
                                                                        //NOTI.Type = "sign_in";
                                                                        //NOTI.Det = doc.Username + " logged in";
                                                                        //NOTI.TS = new Date();
                                                                        //NOTI.status = "recorded";
                                                                        //NOTI.save(err => {
                                                                        //    if (err) {
                                                                        //    }
                                                                        //});
                                                                    }
                                                                })

                                                            }
                                                        })
                                                    }
                                                }
                                                else {
                                                    console.log("No Password History/ PWD History Missing for this user");
                                                    if (doc.Username == "admin1" || doc.Username == "admin") {

                                                        doc.Login_Attempt = 0;
                                                        doc.save(err => {
                                                            if (err) {
                                                                console.log(err)
                                                            } else {
                                                                done(null, {
                                                                    username: doc.Username,
                                                                    password: doc.Pass,
                                                                    role: doc.Type,
                                                                    id: doc._id,
                                                                    name: doc.Name
                                                                })
                                                                user.findOneAndUpdate({ _id: doc._id }, { $set: { Session: "LOGIN", Last_Login_Time: new Date(), ACT_Time: new Date() } }, (err, success) => {
                                                                    if (err) {
                                                                        console.log("Failed to update user session");
                                                                    }
                                                                })
                                                                var ACT_REC = new ACT_DTS();
                                                                ACT_REC.User_Name = doc.Username;
                                                                ACT_REC.Activity = 'Successfully Login';
                                                                ACT_REC.Type = 'LOGIN PAGE';
                                                                ACT_REC.TS = new Date();
                                                                ACT_REC.status = 'recorded';
                                                                ACT_REC.save(err => {
                                                                    if (err) {
                                                                        //
                                                                    }
                                                                    else {
                                                                        //var NOTI = new NOTIFY();
                                                                        //NOTI.Type = "sign_in";
                                                                        //NOTI.Det = doc.Username + " logged in";
                                                                        //NOTI.TS = new Date();
                                                                        //NOTI.status = "recorded";
                                                                        //NOTI.save(err => {
                                                                        //    if (err) {
                                                                        //    }
                                                                        //});
                                                                    }
                                                                })

                                                            }
                                                        })
                                                    }
                                                    else {
                                                        return done(null, false, req.flash('error_msg', "Sorry, we couldn't find password history associated with this account."));
                                                    }

                                                }

                                            }
                                            else {
                                                console.log("Error when retrieving password record " + error)
                                                return done(null, false, req.flash('error_msg', "Error when loggin in, try later"));
                                            }


                                        });

                                    }
                                }
                                else {
                                    var ACT_REC = new ACT_DTS();
                                    ACT_REC.User_Name = username;
                                    ACT_REC.Activity = 'Failed To Login';
                                    ACT_REC.Type = 'LOGIN PAGE';
                                    ACT_REC.TS = new Date();
                                    ACT_REC.status = 'recorded';
                                    ACT_REC.save(err => {
                                        if (err) {
                                            console.log('null')
                                        } else {
                                            // console.log('masuk audit trail')
                                            //var NOTI = new NOTIFY();
                                            //NOTI.Type = "log_error";
                                            //NOTI.Det = username + " failed to login";
                                            //NOTI.TS = new Date();
                                            //NOTI.status = "recorded";
                                            //NOTI.save(err => {
                                            //    if (err) {
                                            //        console.log('null');
                                            //    }
                                            //});
                                        }
                                    })
                                    return done(null, false, req.flash('error_msg', err_InvalidUserWrongPwd));
                                }
                            }
                            //For NON-ADMIN level authorities
                            else {
                                if (doc) {
                                    var valid = doc.comparePassword(password, doc.Pass)
                                    if (valid) {
                                        /*
                                        if credential is all good to go,
                                        First: Check if user session is been Logged in , before checking on if user is been blocked
                                        Second: if user is not blocked, then check if user password is expired
                                        */
                                        if (doc.Session == 'LOGIN' && act_difftime < 15 && act_difftime > 0) {
                                            /*
                                            a control over user login attempt if user is flagged 'Login'                                          
                                            */

                                            return done(null, false, req.flash('error_msg', err_UserDupLogin));
                                        }
                                        else {
                                            /*
                                            if user account is not logged in
                                            */
                                            if (doc.Login_Status == 'BLOCKED') {
                                                var ACT_REC = new ACT_DTS();
                                                ACT_REC.User_Name = username;
                                                ACT_REC.Activity = 'Blocked user insert invalid username / wrong password';
                                                ACT_REC.Type = 'LOGIN PAGE';
                                                ACT_REC.TS = new Date();
                                                ACT_REC.status = 'recorded';
                                                ACT_REC.save(err => {
                                                    if (err) {
                                                        console.log('null')
                                                    } else {
                                                        //var NOTI = new NOTIFY();
                                                        //NOTI.Type = "log_error";
                                                        //NOTI.Det = username + " Tried to Login But Blocked";
                                                        //NOTI.TS = new Date();
                                                        //NOTI.status = "recorded";

                                                        //NOTI.save(err => {
                                                        //    if (err) {
                                                        //        console.log('null');
                                                        //    }
                                                        //});
                                                    }
                                                })

                                                return done(null, false, req.flash('error_msg', err_AccountBlocked));

                                            }
                                            else {
                                                var pwd_Difference_day = -1;
                                                var pwd_Difference_year = -1;
                                                var pwd_difference_mins = -1;

                                                var lastPWDDate;
                                                var userID = doc._id;
                                                PWD_History.find({ ID: userID }, { _id: 0, Password_Date: 1 }).sort({ Password_Date: -1 }).exec((error, pwdDate) => {
                                                    if (!error) {
                                                        if (pwdDate.length > 0) {
                                                            var today = new Date();
                                                            lastPWDDate = pwdDate[0].Password_Date;
                                                            //  pwd_Difference_day = Math.abs(today - lastPWDDate) / DAY;
                                                            pwd_Difference_year = Math.abs(today - lastPWDDate) / YEAR;
                                                            pwd_difference_mins = Math.abs(today - lastPWDDate) / MINUTES;
                                                            //if (pwd_difference_mins >= 5 || pwd_Difference_year < 0) {
                                                                if (pwd_Difference_year >= 1 || pwd_Difference_year < 0) {
                                                                done(null, {
                                                                    status: "pwdexpired",
                                                                    username: doc.Username,
                                                                    password: doc.Pass,
                                                                    role: doc.Type,
                                                                    id: doc._id,
                                                                    name: doc.Name,
                                                                })
                                                            }
                                                            //Login with no issue
                                                            else {
                                                                doc.Login_Attempt = 0;
                                                                doc.save(err => {
                                                                    if (err) {
                                                                        console.log(err)
                                                                    } else {
                                                                        done(null, {
                                                                            username: doc.Username,
                                                                            password: doc.Pass,
                                                                            role: doc.Type,
                                                                            id: doc._id,
                                                                            name: doc.Name
                                                                        })
                                                                        user.findOneAndUpdate({ _id: doc._id }, { $set: { Session: "LOGIN", Last_Login_Time: new Date(), ACT_Time: new Date() } }, (err, success) => {
                                                                            if (err) {
                                                                                console.log("Failed to update user session");
                                                                            }
                                                                        })
                                                                        var ACT_REC = new ACT_DTS();
                                                                        ACT_REC.User_Name = doc.Username;
                                                                        ACT_REC.Activity = 'Successfully Login';
                                                                        ACT_REC.Type = 'LOGIN PAGE';
                                                                        ACT_REC.TS = new Date();
                                                                        ACT_REC.status = 'recorded';
                                                                        ACT_REC.save(err => {
                                                                            if (err) {
                                                                                //
                                                                            }
                                                                            else {
                                                                                //var NOTI = new NOTIFY();
                                                                                //NOTI.Type = "sign_in";
                                                                                //NOTI.Det = doc.Username + " logged in";
                                                                                //NOTI.TS = new Date();
                                                                                //NOTI.status = "recorded";
                                                                                //NOTI.save(err => {
                                                                                //    if (err) {
                                                                                //    }
                                                                                //});
                                                                            }
                                                                        })

                                                                    }
                                                                })
                                                            }
                                                        }
                                                        else {
                                                            console.log("No Password History/ PWD History Missing" + pwdDate.length);
                                                            return done(null, false, req.flash('error_msg', "Sorry, we couldn't find password history associated with this account."));
                                                        }
                                                    }
                                                    else {
                                                        console.log("Error when retrieving password record " + error)
                                                        return done(null, false, req.flash('error_msg', "Sorry, we couldn't find password history associated with this account."));
                                                    }
                                                })
                                            }
                                        }
                                    }
                                    //if Login failed
                                    else {
                                        if (doc.Login_Attempt >= '0') {
                                            doc.Login_Attempt = doc.Login_Attempt + 1;
                                            doc.save(err => {
                                                if (err) {
                                                    console.log('err : ' + err)
                                                }
                                                else {
                                                    if (doc.Login_Attempt >= BLOCKED_ATTEMPT) {
                                                        var ACT_REC = new ACT_DTS();
                                                        ACT_REC.User_Name = username;
                                                        ACT_REC.Activity = 'This user has been blocked';
                                                        ACT_REC.Type = 'LOGIN PAGE';
                                                        ACT_REC.TS = new Date();
                                                        ACT_REC.status = 'recorded';
                                                        ACT_REC.save(err => {
                                                            if (err) {
                                                                console.log('null')
                                                            } else {
                                                                // console.log('masuk audit trail')
                                                                //var NOTI = new NOTIFY();

                                                                //NOTI.Type = "log_error";
                                                                //NOTI.Det = username + " Login Failed " + BLOCKED_ATTEMPT + " Times, Account Blocked";
                                                                //NOTI.TS = new Date();
                                                                //NOTI.status = "recorded";

                                                                //NOTI.save(err => {
                                                                //    if (err) {
                                                                //        console.log('null');
                                                                //    }
                                                                //});
                                                            }
                                                        })

                                                        doc.Login_Status = "BLOCKED";
                                                        doc.save(err => {
                                                            if (err) {
                                                                console.log('err : ' + err)
                                                            }
                                                            else {
                                                                return done(null, false, req.flash('error_msg', err_AccountBlocked));
                                                            }
                                                        })

                                                    } else {
                                                        var ACT_REC = new ACT_DTS();

                                                        ACT_REC.User_Name = doc.Username;
                                                        ACT_REC.Activity = 'Failed to login';
                                                        ACT_REC.Type = 'LOGIN PAGE';
                                                        ACT_REC.TS = new Date();
                                                        ACT_REC.status = 'recorded';
                                                        ACT_REC.save(err => {
                                                            if (err) {
                                                                console.log('null')
                                                            } else {
                                                                ////  console.log('masuk audit trail')
                                                                //var NOTI = new NOTIFY();

                                                                //NOTI.Type = "log_error";
                                                                //NOTI.Det = doc.Username + " Login Failed";
                                                                //NOTI.TS = new Date();
                                                                //NOTI.status = "recorded";

                                                                //NOTI.save(err => {
                                                                //    if (err) {
                                                                //        console.log('null');
                                                                //    }
                                                                //});
                                                            }
                                                        })
                                                        return done(null, false, req.flash('error_msg', err_InvalidUserWrongPwd));

                                                    }
                                                }
                                            })
                                        }
                                        else {
                                            return done(null, false, req.flash('error_msg', err_InvalidUserWrongPwd));
                                        }
                                    }
                                }
                                else {
                                    //console.log('masuk sini 1')
                                    done(null, false)
                                    //return done(null, false, req.flash('error_msg', 'Invalid username/Wrong Password')); 
                                }
                            }
                        }
                    }
                    catch (error) {
                        return done(null, false, req.flash('error_msg', 'We encountered the following error\n' + error + '\nWhile trying to retrieve the user information,\nPlease copy the error and inform developers for further action.'));
                    }
                }
            })

        }))

}