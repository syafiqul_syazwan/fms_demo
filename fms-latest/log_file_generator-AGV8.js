require('./db');

const mongoose = require('mongoose');

var fs = require('fs');
const { exception } = require('console');

const db = mongoose.model('HEALTH_DT');

var FMSlogNm;
var cursor;
var currentDate = new Date();

var date = currentDate.getDate();
var month = currentDate.getMonth(); //Be careful! January is 0 not 1
var year = currentDate.getFullYear();
var hrs = currentDate.getHours();
var mins = currentDate.getMinutes();
var sec = currentDate.getSeconds();

var counts = 0;
var index = 0;
var flag = true;

function runscript() {

    db.count({ status: "recorded" }, null, { ts: { $lte: currentDate } }).then(data => {
        counts = data;
        console.log("try see how many data " + counts);

        try {
            if (counts > 0) {
                FMSlogNm = date + "_" + (month + 1) + "_" + year + "_" + hrs + "_" + mins + "_" + sec + "_ver" + index;
                if (counts <= 0) {
                    console.log("Finishing task...");
                    return;
                }
                console.log("Del: File Writing to: " + FMSlogNm);
                db.find({ status: "recorded" }, null, { ts: { $lte: currentDate } }).sort({ ts: 1 }).limit(25000).exec(function(err, data) {
                    if (err) {
                        //console.log(err);
                        console.log("Error at retrieving from db: " + err);
                        fs.writeFile('log/AGV_' + FMSlogNm + '.log', "Error encountered: " + err, function(err) {}).catch(error => {
                            console.log("Failed to write file, ref " + FMSlogNm);
                            console.log("Error: " + error);
                            if (flag) {
                                console.log("Reattempt");
                                flag = false;
                                runscript();
                            } else {
                                return;
                            }
                        })
                    } else {
                        fs.writeFile('log/AGV_' + FMSlogNm + '.log', JSON.stringify(data), function(err) {
                            if (err) {


                            } else {
                                var latestDate = data[data.length - 1].TS;
                                console.log("last entry date " + latestDate);
                                console.log("last entry date " + typeof latestDate);
                                //   console.log("Del: Proceed to check with last date " + latestDate + " for number of entries" + data.length);

                                db.updateMany({ satus: "recorded", ts: { $lte: latestDate } }, { $set: { status: "audited" } }, (err, updated) => {
                                        if (err || updated.length < 1) {
                                            console.log("Failed to Audit Update: " + err);
                                        } else {
                                            console.log("Successfullly updated entries in " + FMSlogNm + " Updated numbers: " + updated.length);
                                        }

                                    })
                                    // for (x = 0; x < data.length; x++) {
                                    //     db.updateOne({ _id: data[x]._id, status: "recorded" }, { $set: { status: "audited" } }, (err, data2) => {
                                    //         if (err) {
                                    //             fs.writeFile('log/AGV_' + FMSlogNm + '.log', "Data Auditing unsuccessful at " + data[x]._id + ": " + err, function(err) {})
                                    //             console.log("Data Auditing unsuccessful at " + data[x]._id + ": " + err);
                                    //             return;
                                    //         }
                                    //     })
                                    // }
                            }
                        });
                    }
                });
                index++;
                console.log("Del: Running instance no: " + index);
                runscript();

            } else {
                return;
            }
        } catch (e) {
            fs.writeFile('log/AGV_' + FMSlogNm + '.log', "Error encountered: " + e, function(err) {})
            counts = 0;
            console.log("DEL: Error encountered: " + e);
        }

    }).catch(error => {
        console.log("Del: Error encountered " + error);
        counts = 0;
    });

}
runscript();
