/// <reference path="model/map_dt_model.js" />
require('./model/db');

const VERSION = "1.0.3";
const REMARK = "A test to see if App Switch works";
const UPDATETYPE = "FMS";
const superadmin_ID = "5e0d8a89d814f1cfc071351b";


const fs = require('fs');
const session = require('express-session');
const passport = require('passport');
const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const bodyparser = require('body-parser');
const mqtt = require('mqtt');
const agvSettingC = require('./controller/agv_dt_controller');
const loginC = require('./controller/login_controller');
const dashboardC = require('./controller/dashboard_controller');
const armControlC = require('./controller/arm_control_controller');
const diagnosticC = require('./controller/diagnostic_controller');
const operatorC = require('./controller/operator_controller');
const sosC = require('./controller/sos_controller');
const teleopC = require('./controller/teleop_controller');
const errorC = require('./controller/error_controller');
const userC = require('./controller/user_controller');
const missionC = require('./controller/mission_controller');
const reportsC = require('./controller/reports_controller');
const utilityC = require('./controller/utility_controller');
const auth = require('./auth/auth')(passport);
const flash = require('connect-flash');

require('./passport')(passport);
require('events').EventEmitter.defaultMaxListeners = 21;

var app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const mongoose = require('mongoose');
const swal = require('sweetalert2');

//modal
const HEALTH_DT = mongoose.model('HEALTH_DT');
const DIAG_DT = mongoose.model('DIAG_DT');
const SOS_DT = mongoose.model('SOS_DT');
const SOS_DT_TYPE = mongoose.model('SOS_DT_TYPE');
const SOS_DT_REC = mongoose.model('SOS_DT_REC');
const NOTIFY = mongoose.model('NOTIFY');
const NOTI_SEEN = mongoose.model('NOTI_SEEN');
const SCHED_DT = mongoose.model('SCHED_DT');
const MS_DT = mongoose.model('MS_DT');
const MAP_DT_PT = mongoose.model('MAP_DT_PT');
const MAP_DT = mongoose.model('MAP_DT');
const PRESET_DT = mongoose.model('PRESET_DT');
const ARM_DT = mongoose.model('ARM_DT');
const UPD_DT = mongoose.model('UPD_DT');
const ACT_DTS = mongoose.model('ACT_DTS');
const PWD_DT = mongoose.model('PWD_DT');


////passport
//export NODE_OPTIONS = --mac_old_space_size = 4096;

var moment = require('moment');
var Handlebars = require('handlebars');
moment.locale('sg');

app.setMaxListeners(0);
io.setMaxListeners(0);
function standardTime(el, tz) {
    try {
        if (tz) {
            tz = tz.toString();
        }
        else {
            tz = 'Asia/Singapore'
        }
        el = el.toLocaleString('en-US', { timeZone: tz });
        return el = moment(el, "ddd, DD MMM YYYY, hh:mm:ss A")._i;
    }
    catch (e) {
        //-console.log('time cnversion failed ' + e);
    }
}

const PORT = 3000;

app.use(bodyparser.urlencoded({
    extended: true
}));

app.use(bodyparser.json());
app.use(express.static(__dirname + '/public/'));
app.set('auth', path.join(__dirname, 'auth/'));
app.set('views', path.join(__dirname, 'views/'));
/*
Handlebar functions can be added here
Handlerbar functions
*/
app.engine('hbs', exphbs({
    extname: 'hbs', defaultLayout: 'mainLayout', layoutsDir: __dirname + '/views/layouts/',
    helpers: {
        rendermsg: function (text) {

            return new Handlebars.SafeString(text.toString());
        },
        decodehtml: function (text) {
            var txt = text.toString().replace(/(\n)/gm, '<br>').split('<br>').map(function (item) {
                return item.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ "/g, '&quot;').replace(/ '/g, '&# 39;');
            }).join('<br>');
            return txt;

        },
        orArray: function (v1, arr, option) {
            var cprArray = [];
            cprArray = arr.split(',');
            if (cprArray.includes(v1.toString().toUpperCase())) {
                return option.fn(this);
            }
            else {
                return option.inverse(this);
            }

        },
        ifCond: function (v1, v2, options) {
            if (v1 === v2) {
                return options.fn(this);
            }
            return options.inverse(this);
        },
        iftrue: function (v1, options) {
            if (v1) {
                return options.fn(this);
            }
            return options.inverse(this);
        },
        inc: function (v1, options) {
            return parseInt(v1) + 1;
        },
        add: function (v1, v2, options) {
            return parseFloat(v1) + parseFloat(v2);
        },
        json: function (v1) {
            return JSON.stringify(v1);
        },
        fmsTime: function (v1) {
            return standardTime(v1);
        },
        filter_user: function (lu, tu, options) {
            if (lu == superadmin_ID) {
                return options.fn(this);
            }
            else {
                if (tu == "OBSERVER") {
                    return options.fn(this);
                }
                else if (tu == "OPERATOR") {
                    return options.fn(this);
                }
                else if (lu == "ADMIN") {
                    return options.inverse(this);
                }
                else {
                    return options.inverse(this);
                }
            }
        },
        is_superadmin: function (lu, options) {
            if (lu == superadmin_ID) {
                return options.fn(this);
            }
            return options.inverse(this);
        },
    }
}));
app.set('view engine', 'hbs');

//app.set('trust proxy', 1) // trust first proxy


app.use(
    session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true,
        // cookie: { secure: true }
    })
);


// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
});

app.use('/agvSetting', agvSettingC);

app.use('/dashboard', dashboardC);

app.use('/armControl', armControlC);

app.use('/diagnostic', diagnosticC);

app.use('/mapControl', teleopC);
app.use('/mapAPI', teleopC);

app.use('/mission', missionC);

app.use('/sos', sosC);

app.use('/userSetting', userC);
app.use('/userAPI', userC);

app.use('/login', loginC);

app.use('/operation', operatorC);

app.use('/auth', auth);

app.use('/error', errorC);

app.use('/utility', utilityC);

app.use('/reports', reportsC);

io.on('connection', sosC.respond); // user socketio
io.on('connection', dashboardC.respond);
io.on('connection', agvSettingC.respond); // agv socketio
io.on('connection', missionC.respond); // agv socketio
io.on('connection', userC.respond); // user socketio
io.on('connection', operatorC.respond); // user socketio
io.on('connection', diagnosticC.respond); // user socketio
io.on('connection', utilityC.respond);
io.on('connection', teleopC.respond);
io.on('connection', reportsC.respond);
io.on('connection', armControlC.respond);

var PR = "";
var KEY = fs.readFileSync('ONH001-AGV01-key.pem');
var CERT = fs.readFileSync('ONH001-AGV01.pem');
var CAfile = [fs.readFileSync('trusted_certificates.pem')];
var mqtthost = '18.140.250.237';

var options = {
    host: mqtthost,
    port: 8883,
    protocol: 'mqtts',
    username: 'agv01',
    password: 'agv01',
    ca: CAfile,
    key: KEY,
    cert: CERT,
    secureProtocol: 'TLSv1_2_method',
    rejectUnauthorized: false
};
const client = mqtt.connect(options);


//setInterval(() => {
//    const { rss, heapTotal } = process.memoryUsage();
//    console.log('rss', rss, 'heapTotal', heapTotal);
//},5000)

//const client = mqtt.connect('mqtt://broker.mqttdashboard.com');
client.setMaxListeners(0);

client.on('connect', () => {
    //-console.log('mqtt connected');
    client.publish('test.foo.bar', "This is fms");
    client.subscribe('/a2f/ms/ht/#', { qos: 1 });
    client.subscribe('/a2f/sys/diag/#');
    client.subscribe('/a2f/nav/pt/#');
    client.subscribe('/f2a/sys/mv/#');
    client.subscribe('/f2a/arm/man/#');
    client.subscribe('/f2a/sys/man/#');//toggle
    client.subscribe('/a2f/arm/loc/#'); //arm location
    client.subscribe('/a2f/arm/fv/#'); //arm location
    client.subscribe('/a2f/arm/tray/#');//tray
    //client.subscribe('a2f.ms.sr'); //suspend resume from AGV
    client.subscribe('/a2f/sys/sos/#', { qos: 0 });
    client.subscribe('/a2f/sys/cam/#');
    client.subscribe('/a2f/sys/param/#');
    client.subscribe('/mqtt/dc', { qos: 0 });
    client.subscribe('/a2f/sys/pt/#', { qos: 2 });
})

var mqtt_time = new Date();
//const used = process.memoryUsage();
////console.log("Displaying Used/total :");
////console.log(used);
io.on('connection', socketio => {//========================================================Socket IO-1 START===========================================}}

    client.on('message', (topic, msg) => {//------------------------------------------------MQTT Subs Start-------------------------
        ////-console.log(new Date(), topic+":"+msg.toString());
        var cur_time = new Date();
        if (topic == "/mqtt/dc") {
            /*
             credit to Navein (endstruct)
             content formats:
             agv_id
             */
            var parsed = msg.toString(); //parse object "data", all nested string
            //var jdata = JSON.parse(parsed.data); //parsed all nested string in data
            console.log("AGV offffffffffline received");
            console.log(parsed);
        }
        if (cur_time.getTime() - mqtt_time.getTime() < 100) {
            topic = "ignore";
        }
        else {
            topic = "/" + topic.split('/')[1] + "/" + topic.split('/')[2] + "/" + topic.split('/')[3];
        }
        mqtt_time = cur_time;

        //-----------------GET HEALTH DATA (/a2f/ms/ht)----------------------------------//
        if (topic == '/a2f/ms/ht') {
            //console.log("Health received");
            var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
            var jdata = JSON.parse(parsed.data); //parsed all nested string in data
            //     var tray = jdata.
            jdata.TS = new Date();
            socketio.emit('agVhtOperator', jdata);
            //console.log(jdata);
            sendTrayCont(jdata);
            health_routine(msg);
            ////-console.log("health processed");
        }
        if (topic == '/mqtt/dc') {
            console.log("recived last meessage");
            var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
            var jdata = JSON.parse(parsed.data); //parsed all nested string in data

        }

        if (topic == '/a2f/sys/sos') {
            //   console.log("sos mqtt");
            sos_routine(msg.toString());
        }

        if (topic == '/a2f/sys/cam') {
            var parsed = JSON.parse(msg.toString());
            var camData = JSON.parse(parsed.data);
            //var SOSReceived = new Date();
            // //-console.log(camData);

        }
        //-----------------------AGV Config------------------------------//
        if (topic == '/a2f/sys/param') {
            var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
            var jdata = JSON.parse(parsed.data); //parsed all nested string in data
            agvconfig_routine(jdata);

            //-console.log(topic);
        }

        //-----------------------Point Request------------------------------//
        if (topic == '/a2f/sys/pt') {
            var parseds = JSON.parse(msg.toString()); //parse object "data", all nested string
            var cfdatas = JSON.parse(parseds.data); //parsed all nested string in data
            cfdatas.req = PR;
            //-console.log(cfdatas);
            pointing_routine(cfdatas);
            // socketio.emit('reqPtRes', cfdatas);
        }

        //---------------------------GET DIAGNOSIS DATA (/a2f/sys/diag)------------------------------//
        if (topic == '/a2f/sys/diag') {
            var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
            var jdata = JSON.parse(parsed.data); //parsed all nested string in data
            if (jdata.agv_ID == "AT200_2") {
                //   console.log("<<<<<<<<<<<" + new Date() + "<<<<<" + newIndex);

            }
            newIndex++;
            diagnostic_routine(jdata);
        }

        //----------------------GET NAVIGATION (/a2f/nav/pt)--------------------------------//
        if (topic == '/a2f/nav/pt') {
            //-console.log(msg.toString());
        }

        //----------------------GET ARM LOCATION DATA(/a2f/arm/loc)---------------------------------//
        if (topic == '/a2f/arm/loc') {
            var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
            var jdata = JSON.parse(parsed.data); //parsed all nested string in data
            socketio.emit('armLoc', jdata);
            ////-console.log(jsonObj.data)
        }

        //----------------------GET FINGER LOCATION DATA(/a2f/arm/loc)---------------------------------//
        if (topic == '/a2f/arm/fv') {

            var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
            var jdata = JSON.parse(parsed.data); //parsed all nested string in data

            socketio.emit('fingerLoc', jdata);
            ////-console.log(jsonObj.data)

        }

    }) //----------------------------------------------------------------------------------MQTT Subs End---------------------------
    sendTrayCont = (trayInfo) => {
        socketio.emit('trayCont', trayInfo);

    }
    diagnostic_routine = (jdata) => {

        var stringLDR = {}
        stringLDR = ({ Twodf: jdata.ldr['2df'], Twodr: jdata.ldr['2dr'], Threed: jdata.ldr['3d'] })
        jdata.lidar = stringLDR;

        ////-console.log(msg.toString());
        let doc = new DIAG_DT();

        doc.agv_ID = jdata.agv_ID
        doc.ldr = stringLDR
        doc.batt = jdata.batt
        doc.mot = jdata.mot
        doc.cam = jdata.cam
        doc.cpu = jdata.cpu
        doc.ram = jdata.ram
        doc.llc = jdata.llc
        doc.obu = jdata.obu
        doc.esw = jdata.esw
        doc.enc = jdata.enc
        doc.imu = jdata.imu
        doc.tcam = jdata.tcam
        doc.tray = jdata.tray
        doc.TS = new Date()
        doc.status = "recorded";

        doc.save((err, doc) => {
            if (err) {
                //-console.log('diag routine db error');
            }
            else {
                //-console.log('received diag');
                jdata.TS = new Date();
                socketio.emit('agVdiag', jdata);
            }
        })
    }

    agvconfig_routine = (jdata) => {
        socketio.emit('LoadConfigRes', jdata);
    }

    pointing_routine = (jdata) => {
        socketio.emit('reqPtRes', jdata);
    }

    health_routine = (msg) => {

        var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
        var jdata = JSON.parse(parsed.data); //parsed all nested string in data

        software_ver_check({ ver: jdata.ver, rem: jdata.rem, AGV: jdata.agv_ID });
        sr_noti(jdata);

        function sr_noti(jdata) {
       //     if (last_health == "") {
                HEALTH_DT.createIndexes({ AGV_ID: 1, status: 1, TS: -1 });
                HEALTH_DT.findOne({ AGV_ID: jdata.agv_ID, status: "recorded" }, {}, { sort: { TS: -1 } }).then(doc => {
                    if (doc) {
                        if (doc.AGV_ID == jdata.agv_ID) {
                            process_noti(doc, jdata);
                        }
                    }
                    else//if health empty
                    {
                        save_health(jdata);
                    }
                });
       //     }
            //else//if last health is saved in global
            //{
            //    console.log("Prev agv")
            //    console.log(last_health.AGV_ID)
            //    console.log("Cur agv")
            //    console.log(jdata.agv_ID)
            //    if (last_health.AGV_ID == jdata.agv_ID) {
            //        console.log("agv process SAME");
            //        process_noti(last_health, jdata);

            //    }
            //    else
            //    {
            //        console.log("agv process DIFF");
            //        save_health(jdata);

            //    }
    
            //}

        }

 async function process_noti(prev, cur) {
            if (prev.ESW == cur.ESW && prev.SR == cur.SR) {
                save_health(cur);
            }
            else {
                // IF ESWITCH RELEASED
                if (prev.ESW == 1 && cur.ESW == 0) {
                    console.log("esw released");
                    await save_noti(cur.agv_ID, "ESW", "E-Switch Released.", mqtt_time, cur.loc.lat, cur.loc.long);
                }
                // IF RESUME TRIGGERED
                if (prev.SR == 1 && cur.SR == 0) {
                    console.log("Resume trigerred");
                    await save_noti(cur.agv_ID, "RESUME", "RESUME Triggered.", mqtt_time, cur.loc.lat, cur.loc.long);

                }
                // IF SUSPEND TRIGGERED
                if (prev.SR == 0 && cur.SR == 1) {
                    console.log("Resume trigerred");
            
                    await save_noti(cur.agv_ID, "SUSPEND", "SUSPEND Triggered.", mqtt_time, cur.loc.lat, cur.loc.long);

                }
                await save_health(cur);
            }

        }

        function save_noti(agv, type, det, ts, lat, long) {
            let NOTI = new NOTIFY();
            NOTI.AGV_ID = agv
            NOTI.Type = type;
            NOTI.Det = det;
            NOTI.TS = ts;
            NOTI.Lat = lat;
            NOTI.Long = long;
            NOTI.status = "recorded";

            NOTI.save((err, doc) => {
                if (err) {
                    //-console.log(err);
                } else if (doc) {
                    //-console.log("Notification Saved:", doc);
                }
            });
        }

        function save_health(jdata) {
            var TS = new Date();
            let doc = new HEALTH_DT();

            var spx = parseFloat(jdata.spd.x);
            var spdx = spx.toFixed(2);
            var spz = parseFloat(jdata.spd.z);
            var spdz = spz.toFixed(2);

            doc.AGV_ID = jdata.agv_ID;
            doc.Lat = jdata.loc.lat;
            doc.Long = jdata.loc.long;
            doc.Point_X = jdata.pnt.x;
            doc.Point_Y = jdata.pnt.y;
            doc.Spd_X = spdx;
            doc.Spd_Z = spdz;
            doc.Batt = jdata.batt;
            doc.Tmp_L = jdata.tmp.l;
            doc.Tmp_R = jdata.tmp.r;
            doc.MS_ID = jdata.ms.ms_ID;
            doc.MS_SCHED_ID = jdata.ms.sched_ID;
            doc.MS_SS_ID = jdata.ms.ss_ID;
            doc.MS_ACT_SEQ = jdata.ms.act_SEQ;
            doc.MS_ACT_ID = jdata.ms.ACT_ID;
            doc.MS_point = jdata.ms.point;
            doc.MS_act = jdata.ms.act;
            doc.MS_ms_exe = jdata.ms.ms_exe;
            doc.MS_act_exe = jdata.ms.act_exe;
            doc.charge = jdata.charge;
            doc.ESW = jdata.ESW;
            doc.SR = jdata.SR;
            doc.obu_id = jdata.obu.obu_id;
            doc.obu_temp = jdata.obu.obu_temp;
            doc.obu_rainfallIntensity = jdata.obu.obu_rainfallIntensity;
            doc.obu_env_temp = jdata.obu.env_temp;
            doc.obu_humid = jdata.obu.humid;
            doc.ver = jdata.ver;
            doc.rem = jdata.rem;
            doc.state = jdata.state;
            doc.TS = TS
            doc.status = "recorded";

            doc.save((err, doc) => {
                if (err) {
                    //-console.log(err);
                }
                else {
                    jdata.TS = TS;
                    if (jdata.ms.ms_ID == "") {
                        jdata.ms_ID = "";
                        jdata.sched_ID = "";
                        socketio.emit('agVht', jdata);
                    }
                    else {
                        ms_idtofind = jdata.ms.ms_ID;
                        sc_idtofind = jdata.ms.sched_ID;
                        MS_DT.findOne({ _id: ms_idtofind }).exec((failed, doc) => {
                            if (failed || !doc) {
                                jdata.ms_ID = "";
                                jdata.sched_ID = "";
                                socketio.emit('agVht', jdata);
                            }
                            else {
                                SCHED_DT.findOne({ _id: sc_idtofind }).exec((failed, doc2) => {
                                    if (failed || !doc2) {
                                        //-console.log(err);
                                    }
                                    else {
                                        MAP_DT_PT.find({ Map_ID: doc.MAP_ID }).exec((failed, doc3) => {
                                            if (failed || !doc3) {
                                                //-console.log(err);
                                            }
                                            else {
                                                MAP_DT.findOne({ _id: doc.MAP_ID }).exec((failed, doc4) => {
                                                    if (failed || !doc4) {
                                                        //-console.log(err);
                                                    }
                                                    else {
                                                        jdata.ms_ID = doc.Name;
                                                        jdata.sched_ID = doc2.Name;
                                                        jdata.points = doc3;
                                                        jdata.home = doc4;
                                                        ////-console.log(jdata);
                                                        socketio.emit('agVht', jdata);
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })

                    }

                }
            })

        }
    }





}) //{//========================================================================================Socket IO-1 END===========================================}}
var Agv_ESW = {
};
// Amir: Process the sos data received from the AGV via topic: a2f/sys/sos
sos_routine = (data) => {
    var parsed = JSON.parse(data);
    var cur_data = JSON.parse(parsed.data);
    // console.log(cur_data.sos_ID);
    // Get the latest data from SOS_DT_REC for the current AGV
    SOS_DT_REC.findOne({ agv_ID: cur_data.agv_ID, status: "recorded" }, null, { sort: { TS: -1 } }).then(doc => {
        if (doc) {

            var prvts = new Date(doc.TS).getTime();
            var curts = new Date().getTime();

            //-console.log("prvts: " + prvts + " curts: " + curts);

            var difts = curts - prvts;
            //  console.log(difts);
            if (difts > 1000) {
                var prev_data = JSON.parse(JSON.stringify(doc));
                var json = {
                    p_list: prev_data.sosid_LIST,
                    p_ts: prev_data.TS
                }
                if (compare_array_is_not_same(json, cur_data.sos_ID)) { // Set Function compare_array_is_not_same as if argument

                    save_sos(cur_data);
                    //  console.log("SOS couted " + new Date());
                    //-console.log("Not Same");
                } else {
                    // save_sos(cur_data);
                    //   console.log("SOS not counted " + new Date());
                    //-console.log("Same");
                }

            }

        }
        else if (!doc) {
            console.log("sos no data");
            save_sos(cur_data); // in case of no data existed yet
        }
    }).catch(err => {
        console.log("soserror" + err);
    });

    compare_array_is_not_same = (prev, curr_list) => {
        var p_length = prev.p_list.length;
        var c_length = curr_list.length;

        var prev_list = prev.p_list;

        ////-console.log("prev: "+JSON.stringify(prev));

        var ctime = new Date().getTime(); // get the current time (now)
        var ptime = new Date(prev.p_ts).getTime(); // get the previous time (from db)

        var difference = (ctime - ptime); // get the duration difference

        ////-console.log("difference: "+difference);

        ////-console.log("ctime: "+ctime+" ptime: "+ptime);

        if (p_length == c_length) { // if the previous sosid_LIST array has the same array length as the current sosid_LIST array

            var same = 0;
            var count = 0;

            for (var x = 0; x < p_length; x++) { // Loop through all of the previous sosid_LIST array
                for (var y = 0; y < c_length; y++) { // Loop through all of the current sosid_LIST array

                    if (prev_list[x] == curr_list[y]) { // returns true when one of the previous sosid_LIST is the same as the current sosid_LIST array

                        same = same + 1; // increment same var

                    }

                }

                count = count + 1; // increment count var

                if (p_length == count) { // if the for loop has gone through all previous sosid_LIST then return true

                    if (p_length == same) { // if the number of array objects from the prev and curr sosid_LIST is similar then return true

                        ////-console.log("ctime: "+ctime+" ptime: "+ptime);

                        ////-console.log("difference: "+difference);


                        if (difference > 6000000) { // 10 minutes = 600 000 milliseconds
                            return true; // returns true even though the same after 10 minutes
                        } else {
                            return false; // if not same then return false
                        }
                    } else {
                        return true; // return true if not same (when the array length is the same but sosid_LIST combination is different)
                    }

                }

            }
        } else {
            return true; // return true if not same (when the array length is not the same to begin with)
        }
    }

    // Save the sos into SOS_DT_REC and NOTIFY in db
    save_sos = (c_data) => {
        ////-console.log("AGV_cur: "+AGV_cur+" Lat_cur: "+Lat_cur+" Long_cur: "+Long_cur);
        let docu = new SOS_DT_REC();
        docu.agv_ID = c_data.agv_ID;
        docu.sosid_LIST = c_data.sos_ID;
        docu.Lat = c_data.Lat;
        docu.Long = c_data.Long;
        docu.Remark = "Triggered AGV";
        docu.TS = new Date();
        docu.user_ID = "AGV";
        docu.status = "recorded";
        docu.save((err, doc1) => {
            if (err) {
                //-console.log(err);
            } else if (doc1) {
                //-console.log("New Data Saved");
                ////-console.log(doc1);

                var count = 0;
                var sos_msg_LIST = "";

                // for loop to convert the sosid_LIST from ID format to Name format
                for (x = 0; x < docu.sosid_LIST.length; x++) {
                    SOS_DT_TYPE.findOne({ _id: docu.sosid_LIST[x], status: "recorded" }).then(doc2 => {
                        if (doc2) {
                            sos_msg_LIST = sos_msg_LIST + doc2.Type + ", ";
                        }

                        count = count + 1;

                        if (count == docu.sosid_LIST.length) { // Once the loop is done thn return true
                            //-console.log(c_data);
                            let NOTI = new NOTIFY();

                            NOTI.AGV_ID = c_data.agv_ID;
                            NOTI.Type = "SOS";
                            NOTI.Det = sos_msg_LIST + " SOS Occured";
                            NOTI.TS = new Date();
                            NOTI.Lat = c_data.Lat;
                            NOTI.Long = c_data.Long;
                            NOTI.status = "recorded";

                            NOTI.save((err, doc) => {
                                if (err) {

                                } else if (doc) {
                                }
                            })

                        }
                    }).catch(err => {
                        //-console.log(err);
                    })

                }
            }
        })
    }

}

//------------------------------Included File(FMS FOLDER FOR VIEW)-----------------------------------//

client.on("packetreceive", function (mqtt) {
    //  console.log(mqtt.topic);
})
//client.on("disconnect", function () {

//    console.log("MQTT disconnect");

//})
//client.on("close", function () {
//    client.reconnect();
//    console.log("MQTT close");

//})
//client.on("reconnect", function () {
//    console.log("MQTT reconnect");

//})

//---------------------TOPIC FROM FMS TO AGV-------------------//

var index = 0;
var newIndex = 0;
io.on('connection', socket => {
    //send tests

    function sendtest(sec) {
        var now = new Date();
        setTimeout(sendtest, 2000);
        var strtest = 'sending packet at' + now + " id:" + index;
        var cvt = JSON.stringify(strtest);
        var topic = '/f2a/sys/diag';
        var data2 = {
            agv_ID: "AT200_2",
            cmd: "1",
            index: index
        };
        var data1 = {
            data: JSON.stringify(data2),
        };
        //var json = { ver: VERSION, rem: REMARK, AGV: UPDATETYPE };
        //if (index < 15) {
        //if (index < 15) {

        //    software_ver_check(json);
        //}

        client.publish(topic, JSON.stringify(data1));
        console.log(">>>>>>>>>>" + new Date() + ">>>>>" + index);
        index++;
    }
    //  sendtest(2000);

    socket.on('disconnect', function () {
        // console.log("Socket is been disconnected");

    })
    //---------------------------------Diagnostic(/f2a/sys/diag)--------------------------------//
    socket.on('sys/diag', diagdata => {
        var strings = JSON.stringify(diagdata);
        var jobj = { data: strings };
        //- console.log('send diag req to agv');
        client.publish('/f2a/sys/diag', JSON.stringify(jobj));
        console.log("Published " + JSON.stringify(jobj));
        //console.log(JSON.stringify(jobj));
    })

    //---------------------------------Toggle(/f2a/sys/man)--------------------------------//

    socket.on('sys/man', manToggle => {

        var strings = JSON.stringify(manToggle);
        var jobj = { data: strings };
        client.publish('/f2a/sys/man', JSON.stringify(jobj));
        ////-console.log(JSON.stringify(jobj));
    })

    //---------------------------------AGV MOVEMENT(/f2a/sys/mv)--------------------------------//

    socket.on('sys/mv', dmv => {
        var tempObj = {};
        tempObj.agv_ID = dmv.agv_ID;
        tempObj.x = "0";
        tempObj.z = "0";
        tempObj.cmd = "1";

        switch (dmv.agvMv) {
            case "f": tempObj.x = "0.3"; break;
            case "b": tempObj.x = "-0.3"; break;
            case "l": tempObj.z = "0.3"; break;
            case "r": tempObj.z = "-0.3"; break;
        }
        var strings = JSON.stringify(tempObj);
        var jobj = { data: strings };
        // //-console.log(jobj)
        client.publish('/f2a/sys/mv', JSON.stringify(jobj));
    })

    //--------------------------ARM MISSION (MANUAL CONTROL)(/f2a/arm/man)--------------------------------//

    socket.on('sys/arm', function (message) {
        // //-console.log(message)
        // //-console.log('MSG: ' + message.valueArmID);
        // //-console.log(message.agv)
        var tempObj = {};
        tempObj.agv_ID = message.agv;
        var armTopic = message.valueArmID;
        tempObj.x = armTopic == "xP" ? "1" : armTopic == "xN" ? "-1" : "0";
        tempObj.y = armTopic == "yP" ? "1" : armTopic == "yN" ? "-1" : "0";
        tempObj.z = armTopic == "zP" ? "1" : armTopic == "zN" ? "-1" : "0";
        tempObj.w = armTopic == "roP" ? "10" : armTopic == "roN" ? "-10" : "0";
        tempObj.p = armTopic == "yaP" ? "10" : armTopic == "yaN" ? "-10" : "0";
        tempObj.r = armTopic == "ptP" ? "10" : armTopic == "ptN" ? "-10" : "0";
        // tempObj.f = "0";
        // tempObj.s = "0";
        tempObj.f1 = armTopic == "fP" ? "10" : armTopic == "fN" ? "-10" : "0";
        tempObj.f2 = armTopic == "fP" ? "10" : armTopic == "fN" ? "-10" : "0";;

        //switch (message.valueArmID) {
        //    case "xP": tempObj.x = "1"; break; //x
        //    case "xN": tempObj.x = "-1"; break; //x
        //    case "yP": tempObj.y = "1"; break; //y
        //    case "yN": tempObj.y = "-1"; break; //y
        //    case "zP": tempObj.z = "1"; break; //z
        //    case "zN": tempObj.z = "-1"; break;//z
        //    case "roP": tempObj.w = "10"; break; //w
        //    case "roN": tempObj.w = "-10"; break; //w
        //    case "ptP": tempObj.r = "10"; break; //p
        //    case "ptN": tempObj.r = "-10"; break; //p
        //    case "yaP": tempObj.p = "10"; break; //r
        //    case "yaN": tempObj.p = "-10"; break; //r
        //    case "fP": tempObj.f1 = "10";  //fP
        //    case "fP": tempObj.f2 = "10"; break; //fP
        //    case "fN": tempObj.f1 = "-10";  //fN
        //    case "fN": tempObj.f2 = "-10"; break; //fN
        //}
        var strings = JSON.stringify(tempObj);
        //-console.log(strings)
        var jobj = { data: strings };
        client.publish('/f2a/arm/man', JSON.stringify(jobj));
    });

    //-------------------------------------------------NOTISEEN-----------------------------------------------------------//

    socket.on('notiseen', data => { // received from MainLayout
        ////-console.log(JSON.stringify(data));

        var notid = data.notid;
        var userid = data.userid;
        var notify_TS = data.nots;

        let NS = new NOTI_SEEN();

        NS.NOTI_ID = notid;
        NS.USER_ID = userid;
        NS.NOTI_TS = notify_TS;
        NS.status = "recorded";

        NS.save((err, doc) => {
            if (err) {
                //-console.log(err);
            } else {
                //-console.log(doc);
                // Trigger Global Refresh
                require('./controller/dashboard_controller.js')();

            }
        })

    })

    //-------------------------------------------------CLEAR_NOTI----------------------------------------------------------//

    socket.on('clear_noti', uid => {
        if (uid != '') {
            ////-console.log("Remove All Notification Button Clicked by User ID: "+ uid);
            clear_noti(uid);
        }
    })

    //--------------------------START SCHEDULE (/f2a/ms/st)--------------------------------//

    socket.on('getmsactSt', dataOps => {
        // if(mission1 == 'startSched') {
        // var json = {"data":"{\"agv_ID\":\"AT200_1\",\"ms_ID\":\"1\",\"fname\":{\"map\":\"OneNorth_1351434245\",\"Home_x\":\"0.1\",\"Home_y\":\"0.1\",\"Home_z\":\"0.1\",\"Home_w\":\"0.1\"},\"activity\":[{\"pt\":\"warehouse_point1\",\"x\":\"-1.51833570004\",\"y\":\"0.256689786911\",\"z\":\"0.95759344101\",\"w\":\"-0.288104772568\",\"arm\":{\"cmd\":\"pick3\",\"j1\":\"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\":\"-6088 -6088\",\"p1\": \"0.2834 0.0 0.08 0 0 90,0.0 -0.1572 0.0 0 0 0,0.0 0.0 -0.13408 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"232.24 200.18 125.30 360.22 281.63 54.82\",\"p2\": \"0.10855 0.268 0.06 0 0 0,0.03 0.3303 0.0 10 0 0,0.0 0.0 -0.19 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"190.94 189.38 124.55 354.16 253.45 -21.74\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}},{\"pt\":\"point_reverse\",\"x\":\"-0.438266992569\",\"y\":\"1.52551674843\",\"z\":\"0.937858462334\",\"w\":\"-0.346919089556\",\"arm\":\"wait\"},{\"pt\":\"warehouse_point2\",\"x\":\"-0.069412946701\",\"y\":\"5.98229503632\",\"z\":\"0.331173986197\",\"w\":\"0.943560242653\",\"arm\":\"wait\"},{\"pt\":\"warehouse_home\",\"x\":\"4.75163269043\",\"y\":\"4.53939819336\",\"z\":\"0.944541335106\",\"w\":\"-0.328045785427\",\"arm\": \"wait\"}]}"}
        var strings = JSON.stringify(dataOps);
        var jobj = { data: strings };
        client.publish('/f2a/ms/st', JSON.stringify(jobj));
        //-console.log(JSON.stringify(jobj));

        SCHED_DT.findOne({ _id: dataOps.sched_ID, status: "recorded" }).then(doc => {
            if (doc) {
                // //-console.log(dataOps.agv_ID)
                // //-console.log(doc.Name);

                var sched_nm = doc.Name;

                MS_DT.findOne({ _id: dataOps.ms_ID, status: "recorded" }).then(doc2 => {
                    if (doc2) {

                        var ms_nm = doc2.Name;

                        let NOTI = new NOTIFY();

                        NOTI.AGV_ID = doc2.agv_ID;
                        NOTI.Type = "MS_START";
                        NOTI.Det = "Schedule : " + sched_nm + ",Mission : " + ms_nm;
                        NOTI.TS = new Date();
                        NOTI.status = "recorded";

                        NOTI.save((err, doc) => {
                            if (err) {
                                //-console.log(err);
                            } else if (doc) {
                                //-console.log("Schedule : " + sched_nm + ",Mission : " + ms_nm + ", Status : Started");
                            }

                        });

                    }
                }).catch(err => {
                    //-console.log(err);
                });

            }
        }).catch(err => {
            //-console.log(err);
        })

        //    }
    });

    //--------------------SOS AGV-------------------------//

    socket.on('sos/trig', dataTrig => {
        var strings = JSON.stringify(dataTrig);
        var jobj = { data: strings };
        client.publish('/f2a/sys/sos', JSON.stringify(jobj));
        //-console.log(JSON.stringify(jobj));

    });

    //---------------------------SHUT DOWN--------------------------------------//
    socket.on('getSdReq', dataSd => {
        var strings = JSON.stringify(dataSd);
        var jobj = { data: strings };
        client.publish('/f2a/sys/off', JSON.stringify(jobj));
        //-console.log(JSON.stringify(jobj));
    });

    //-----------------------SUSPEND RESUME------------------------------//
    //logic enginnered by douzee and yunus_yst
    socket.on('reqSus', dataS => {

        var string1 = JSON.stringify(dataS);
        var jobj = { data: string1 };
        var string2 = JSON.stringify(jobj);

        client.publish('/f2a/ms/sr', string2);
    })

    //-----------------------AGV Config------------------------------//
    socket.on('agv/config/req', dataS => {
        var string1 = JSON.stringify(dataS);
        var jobj = { data: string1 };
        client.publish('/f2a/sys/paramreq', JSON.stringify(jobj));
        //-console.log(jobj);
    })

    socket.on('agv/config/save', dataS => {
        var string1 = JSON.stringify(dataS);
        var jobj = { data: string1 };
        client.publish('/f2a/sys/param', JSON.stringify(jobj));
        //-console.log(jobj);
        socket.emit('SaveConfigRes', "OK");
    })


    //-----------------------Point Request------------------------------//
    socket.on('reqPt', dataS => {
        PR = dataS.req;
        var string1 = JSON.stringify(dataS);
        var jobj = { data: string1 };
        client.publish('/f2a/sys/pt', JSON.stringify(jobj));
        //-console.log(jobj);
    })


    //--------------------------ARM MISSION (AUTOMATICALLY CONTROL) (PRESET BUTTON) (/f2a/arm/mv)--------------------------------//

    socket.on('sys/arm/mv', preset => {
        if (preset.name == 'preset1') {
            PRESET_DT.findOne({ status: 'recorded', name: 'PRESET 1' }, (err, doc) => {
                if (err) {
                    //-console.log(err);
                }
                else {
                    ARM_DT.findOne({ _id: doc.arm_ID }, (err, doc2) => {
                        if (err) {
                            //-console.log(err);
                        }
                        else {
                            var outs = {};
                            outs.agv_ID = preset.AGV;
                            outs.arm = doc2.Cmd;
                            var strings = JSON.stringify(outs);
                            var jobj = { data: strings };
                            //- console.log('check' +JSON.stringify(jobj));
                            client.publish('/f2a/arm/pr', JSON.stringify(jobj));
                        }
                    })
                }
            })
        }
        if (preset.name == 'preset2') {
            PRESET_DT.findOne({ status: 'recorded', name: 'PRESET 2' }, (err, doc) => {
                if (err) {
                    //-console.log(err);
                }
                else {
                    ARM_DT.findOne({ _id: doc.arm_ID }, (err, doc2) => {
                        if (err) {
                            //-console.log(err);
                        }
                        else {
                            var outs = {};
                            outs.agv_ID = preset.AGV;
                            outs.arm = doc2.Cmd;
                            var strings = JSON.stringify(outs);
                            var jobj = { data: strings };//-console.log(JSON.stringify(jobj));
                            client.publish('/f2a/arm/pr', JSON.stringify(jobj));
                        }
                    })
                }
            })

        }
        if (preset.name == 'preset3') {
            PRESET_DT.findOne({ status: 'recorded', name: 'PRESET 3' }, (err, doc) => {
                if (err) {
                    //-console.log(err);
                }
                else {
                    ARM_DT.findOne({ _id: doc.arm_ID }, (err, doc2) => {
                        if (err) {
                            //-console.log(err);
                        }
                        else {
                            var outs = {};
                            outs.agv_ID = preset.AGV;
                            outs.arm = doc2.Cmd;
                            var strings = JSON.stringify(outs);
                            var jobj = { data: strings };//-console.log(JSON.stringify(jobj));
                            client.publish('/f2a/arm/pr', JSON.stringify(jobj));
                        }
                    })
                }
            })

        }
        if (preset.name == 'preset4') {

            PRESET_DT.findOne({ status: 'recorded', name: 'PRESET 4' }, (err, doc) => {
                if (err) {
                    //-console.log(err);
                }
                else {
                    ARM_DT.findOne({ _id: doc.arm_ID }, (err, doc2) => {
                        if (err) {
                            //-console.log(err);
                        }
                        else {
                            var outs = {};
                            outs.agv_ID = preset.AGV;
                            outs.arm = doc2.Cmd;
                            var strings = JSON.stringify(outs);
                            var jobj = { data: strings };//-console.log(JSON.stringify(jobj));
                            client.publish('/f2a/arm/pr', JSON.stringify(jobj));
                        }
                    })
                }
            })

        }
        if (preset.name == 'preset5') { //pick3

            PRESET_DT.findOne({ status: 'recorded', name: 'PRESET 5' }, (err, doc) => {
                if (err) {
                    //-console.log(err);
                }
                else {
                    ARM_DT.findOne({ _id: doc.arm_ID }, (err, doc2) => {
                        if (err) {
                            //-console.log(err);
                        }
                        else {
                            var outs = {};
                            outs.agv_ID = preset.AGV;
                            outs.arm = doc2.Cmd;
                            var strings = JSON.stringify(outs);
                            var jobj = { data: strings };//-console.log(JSON.stringify(jobj));
                            client.publish('/f2a/arm/pr', JSON.stringify(jobj));
                        }
                    })
                }
            })

        }

        if (preset.name == 'presetHome') { //drop3
            var json = { "data": '{\"agv_ID\":\"' + preset.AGV + '\",\"arm\":{\"cmd\": \"home\",\"j1\": \"269.19 82.35 39.73 359.89 239.92 365.12\",\"fv1\": \"0 0\",\"p1\": \"0.000 0.000 0.000 0 0 0\",\"fv2\": \"0 0\",\"p2\": \"0.000 0.000 0.000 0 0 0\",\"j2\": \"269.19 82.35 39.73 359.89 239.92 365.12\",\"p3\": \"0.000 0.000 0.000 0 0 0\",\"fv3\": \"-6400 -6400\",\"p4\": \"0.000 0.000 0.000 0 0 0\",\"j3\": \"269.19 82.35 39.73 359.89 239.92 365.12\",\"fv4\": \"6000 6000\",\"j4\": \"269.19 82.35 39.73 359.89 239.92 365.12\"}}' };
            client.publish('/f2a/arm/pr', JSON.stringify(json));
            //-console.log('home' +  JSON.stringify(json));
        }

        if (preset.name == 'presetStart') {
            var json = { "data": '{\"agv_ID\":\"' + preset.AGV + '\",\"arm\":{\"cmd\": \"start\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 189.51\",\"fv1\": \"-6000 -6000\",\"p1\": \"0.000 0.000 0.000 0 0 0\",\"fv2\": \"0 0\",\r\n  \"p2\": \"0.000 0.000 0.000 0 0 0\",\"j2\": \"273.49 163.35 65.14 359.13 260.16 189.51\",\"p3\": \"0.000 0.000 0.000 0 0 0\",\"fv3\": \"0 0\",\"p4\": \"0.000 0.000 0.000 0 0 0\",\"j3\": \"273.49 163.35 65.14 359.13 260.16 189.51\",\"fv4\": \"6000 6000\",\"j4\": \"273.49 163.35 65.14 359.13 260.16 189.51\"}}' };
            client.publish('/f2a/arm/pr', JSON.stringify(json));
            //-console.log('Start' + JSON.stringify(json));
        }

    })


})


function clear_noti(uid) {

    // var allntfy;
    // var allntsn;

    NOTI_SEEN.deleteMany({ USER_ID: uid }, (err => {
        if (err) {
            //-console.log(err)
        } else {

            NOTIFY.find({}, null, { sort: { _id: -1 } }).then(doc => {
                if (doc) {

                    //allntfy = doc;
                    insert2notiseen(doc, uid);
                    ////-console.log("hello");

                }
            }).catch(err => {
                //-console.log(err);
            })

        }
    }))

}

function insert2notiseen(ntfy, uid) {

    var cnt = 0;

    for (x = 0; x < ntfy.length; x++) {

        let NS = new NOTI_SEEN();

        NS.NOTI_ID = ntfy[x]._id;
        NS.USER_ID = uid;

        NS.save((err, doc) => {
            if (err) {
                //-console.log(err);
            } else {
                ////-console.log(doc);
            }
        })
        cnt = cnt + 1;
    }

    if (cnt == ntfy.length) {

        // //-console.log("All Notifications Removed for user id: "+ uid);

    }
}

software_ver_save = (data) => {

    //data.ver == "" || data.ver == undefined ||
    if (/^.{1,}$/.test(data.ver) == false || /\s/.test(data.ver) == true) {
        console.log("not saved as Version is null");
    }
    else {
        var UPD = new UPD_DT();

        UPD.Ver = data.ver;
        UPD.Rem = data.rem;
        UPD.AGV_ID = data.AGV;
        UPD.TS = new Date();
        UPD.status = "recorded";

        UPD.save((err1, docUPD) => {
            if (err1) {
                //-console.log(err);
            } else {
                console.log("New version detected and saved");
                console.log(docUPD);
                //-console.log("Version " + docUPD.Ver + " " + docUPD.AGV_ID + " software is recorded in software version log");
                var ACT = new ACT_DTS();
                ACT.User_Name = "Backend Service";
                ACT.Activity = "Version " + docUPD.Ver + " " + docUPD.AGV_ID + " software is recorded in software version log";
                ACT.Type = "BACKEND SERVICE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err2, docAT4) => {
                    if (err2) {
                        //-console.log(err);
                    } else {

                    }
                })
            }
        })
    }

}
//Switch for software version
var flagupdateVer = false;
software_ver_check = (data) => {

    if (flagupdateVer == true) {
        //switch flagupdateVer to true when needed to update a single Version, be it remarks or anything, 
        //however DO NOT update the date as it would confuse client with disoriented orders

        UPD_DT.updateOne({ status: "recorded", AGV_ID: "FMS", Ver: "1.0.3" }, { $set: { Rem: REMARK } }).exec((error, out) => {
            if (error) {
                console.log("failed to update");
            }
            else {
                console.log("version rolled back");
            }
        })

    }

    //filter data data type FMS or AGV_ID
    //get last in UPD_DT based on data type
    //if last data is not same data.VER for data type save new record

    UPD_DT.findOne({ status: 'recorded', AGV_ID: data.AGV, Ver: data.ver }).limit(1).exec((err, out) => {
        if (err) {
            console.log("Error while checking on versions, see below for Error Phrase ");
            console.log(err);
        }
        else {
            if (out) {
                // console.log("Dupicated version determined");
            }
            else {
                software_ver_save(data);
            }
        }
    });

};


if (mongoose.connection.readyState == 2) {
    var json = { ver: VERSION, rem: REMARK, AGV: UPDATETYPE };
    software_ver_check(json);
}


http.listen(PORT, () => {
    //-console.log('listening on PORT: ' + PORT);
});