const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const MS_DT = mongoose.model('MS_DT');
const MS_DT_ACT = mongoose.model('MS_DT_ACTIVITY');
const SCHED_DT_MS = mongoose.model('SCHED_DT_MS');
const MAP_DT_PT = mongoose.model('MAP_DT_PT');
const AGV_DT = mongoose.model('AGV_DT');
const MAP_DT = mongoose.model('MAP_DT');
const USR_DT = mongoose.model('USR_DT');
const ARM_DT = mongoose.model('ARM_DT');
const SCHED_DT = mongoose.model('SCHED_DT');
const ACT_DTS = mongoose.model('ACT_DTS');
const MAP_DT_JUNC = mongoose.model('MAP_DT_JUNC');

var info = [];
const loggedin = (req, res, next) => {
    if (req.isAuthenticated()) {
        //console.log(req.user)
        if (req.user.role == 'OBSERVER') {
            if (req.path == '/') {
                return res.redirect('./error')

            }
            else {
                console.log('nothing')
            }

        }
        else if (req.user.role == 'OPERATOR') {
            if (req.path == '/') {
                return res.redirect('./error')
            }
            else {
                console.log('nothing')
            }

        }
        else if (req.user.role == 'ADMIN') {
            //console.log(req.path)

            if (req.path == '/') {
                info = [];
                var id = req.user.id;
                var name = req.user.name
                var role = req.user.role
                var pass = req.user.password
                var username = req.user.username
                info.push({ userId: id, Name: name, Role: role, Password: pass, Username: username })

            }
            else {
                console.log('nothing')
            }
        }
        else {

            return res.redirect('./error')

        }


        return next()


    }
    else {

        res.redirect('/login')

    }
}

router.get('/', loggedin, (req, res) => {
    var msjson = [];
    var interval = 1 * 10; // 10 seconds;


    USR_DT.find({ status: 'recorded' }, (err, doc9) => {
        if (err)
            console.log(err)
        else {

            AGV_DT.find({ status: 'recorded' }, (err, doc7) => {
                if (err)
                    console.log(err)
                else {
                    MAP_DT_JUNC.find({ status: 'recorded' }, (err, doc13) => {
                        if (err)
                            console.log(err)
                        else {
                            MAP_DT.find({ status: 'recorded' }, (err, doc8) => {
                                if (err)
                                    console.log(err)
                                else {
                                    MAP_DT_PT.find({ status: 'recorded' }, (err, doc11) => {
                                        if (err)
                                            console.log(err)
                                        else {
                                            ARM_DT.find({ status: 'recorded' }, (err, doc12) => {
                                                if (err)
                                                    console.log(err)
                                                else {
                                                    MS_DT.find({ status: 'recorded' }, (err, doc) => {
                                                        if (err) {
                                                            console.log(err)
                                                        }
                                                        else {
                                                            if (doc == '') {
                                                                SCHED_DT.find({ status: 'recorded' }, (err, doc10) => {

                                                                    if (err) {
                                                                        console.log(err);
                                                                    }
                                                                    else {
                                                                        if (i == doc10.length - 1) {
                                                                            console.log(doc10)
                                                                            res.render('mission/index', {
                                                                                title: "MISSION EDIT",
                                                                                datasched: doc10,
                                                                                map: doc8,
                                                                                agv: doc7,
                                                                                user: doc9,
                                                                                ms: doc,
                                                                                mapJunc: doc13,
                                                                                mapPt: doc11,
                                                                                armName: doc12,
                                                                                name: info[0].Name,
                                                                                userid: info[0].userId,
                                                                                role: info[0].Role,
                                                                                pass: info[0].Password
                                                                            })
                                                                        }

                                                                    }
                                                                })

                                                            } else {
                                                                SCHED_DT.find({ status: 'recorded' }, (err, doc3) => {
                                                                    if (err) {
                                                                        console.log(err);
                                                                    }
                                                                    else {
                                                                        if (doc3 == '') {
                                                                            var missionjson = [];
                                                                            for (var j = 0; j < doc.length; j++) {
                                                                                setTimeout(function (j) {
                                                                                    MAP_DT.findOne({ _id: doc[j].MAP_ID }, (err, doc2) => {
                                                                                        if (err) {
                                                                                            console.log(err)
                                                                                        }
                                                                                        else
                                                                                            missionjson.push({ ms_id: doc[j]._id, ms_Name: doc[j].Name, area: doc2.Name, agv: doc[j].agv_ID, TS: doc[j].TS });
                                                                                        if (j == doc.length - 1) {
                                                                                            console.log("1", doc);
                                                                                            res.render('mission/index', {
                                                                                                title: "MISSION EDIT",
                                                                                                data: missionjson,
                                                                                                datasched: doc3,
                                                                                                map: doc8,
                                                                                                agv: doc7,
                                                                                                user: doc9,
                                                                                                ms: doc,
                                                                                                mapJunc: doc13,
                                                                                                mapPt: doc11,
                                                                                                armName: doc12,
                                                                                                name: info[0].Name,
                                                                                                userid: info[0].userId,
                                                                                                role: info[0].Role,
                                                                                                pass: info[0].Password
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }, interval * j, j);
                                                                            }
                                                                        }
                                                                        else {
                                                                            for (var i = 0; i < doc.length; i++) {
                                                                                setTimeout(function (i) {
                                                                                    MAP_DT.findOne({ _id: doc[i].MAP_ID }, (err, doc2) => {
                                                                                        if (err) {
                                                                                            console.log(err)
                                                                                        }
                                                                                        else {
                                                                                            msjson.push({ ms_id: doc[i]._id, ms_Name: doc[i].Name, area: doc2.Name, agv: doc[i].agv_ID, TS: doc[i].TS })
                                                                                        }
                                                                                        if (i == doc.length - 1) {
                                                                                            res.render('mission/index', {
                                                                                                title: "MISSION EDIT",
                                                                                                data: msjson,
                                                                                                datasched: doc3,
                                                                                                map: doc8,
                                                                                                agv: doc7,
                                                                                                user: doc9,
                                                                                                ms: doc,
                                                                                                mapJunc: doc13,
                                                                                                mapPt: doc11,
                                                                                                armName: doc12,
                                                                                                name: info[0].Name,
                                                                                                userid: info[0].userId,
                                                                                                role: info[0].Role,
                                                                                                pass: info[0].Password
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }, interval * i, i);
                                                                            }
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })


})


module.exports = router;

module.exports.respond = socket => {

    socket.on('addMission', data => {
        add_mission_routine(data);
    })

    socket.on('addMissionAct', data => {
        add_mission_act_routine(data);
    })

    socket.on('EMsaving', data => {
        edit_mission_act_routine(data);
    })

    socket.on('ESsaving', data => {
        edit_schedule_act_routine(data);
    })


    socket.on('addSched', data => {
        add_sched_routine(data);
    })

    socket.on('saveSched', data => {
        save_sched_routine(data);
    })


    socket.on('deleteMission', data => {
        delete_mission_routine(data);
    })


    socket.on('deleteSchedule', data => {
        delete_schedule_routine(data);
    })


    socket.on('schedReq', data => {
        sched_req_routine(data);
    })

    var missionName = [];

    //-----TO BE CONTINUED - CLEANING------//

    socket.on('msreq', data => {
        var msarr = [];
        var armAndMapEdit = [];
        var interval = 1 * 100; // 10 seconds;
        MS_DT.findOne({ _id: data.ms_id }, (err, doc) => {
            if (!doc) {
                console.log('null')
            } else {
                // console.log(doc)
                missionName = doc.Name
                //socket.emit('EditmsID', doc);

                MAP_DT.findOne({ _id: doc.MAP_ID }, (err, doc6) => {
                    if (err) {
                        console.log('err')
                    } else {
                        //socket.emit('editMapMs',doc6)

                        MAP_DT_PT.find({ Map_ID: doc6._id }, (err, doc18) => {
                            if (err) {
                                console.log('err')
                            } else {
                                //socket.emit('EditMapData',doc18)
                                // console.log(doc18)
                                ARM_DT.find({}, (err, doc19) => {
                                    if (err) {
                                        console.log('err')
                                    } else {
                                        //socket.emit('EditArmData',doc19)
                                        AGV_DT.findOne({ agv_ID: doc.agv_ID }, (err, doc5) => {
                                            if (err)
                                                console.log('err')
                                            else {
                                                //socket.emit('editAgvMs',doc5)

                                                MS_DT_ACT.find({ MS_ID: data.ms_id, status: 'recorded' }).sort({ Seq: 1 }).exec((err, doc2) => {
                                                    if (err)
                                                        console.log('err')
                                                    else {
                                                        for (var i = 0; i < doc2.length; i++) {
                                                            setTimeout(function (i) {
                                                                MAP_DT_PT.findOne({ _id: doc2[i].PT_ID }, (err, doc3) => {
                                                                    if (err) {
                                                                        console.log('err')
                                                                    }
                                                                    else {
                                                                        //  console.log(doc3)
                                                                        ARM_DT.findOne({ _id: doc2[i].ARM_ID }, (err, doc4) => {
                                                                            if (err) {
                                                                                console.log('err')
                                                                            }
                                                                            else {

                                                                                var jsp =
                                                                                {
                                                                                    Seq: parseInt(doc2[i].Seq),
                                                                                    ACT_ID: doc2[i]._id,
                                                                                    MS_ID: doc._id,
                                                                                    ms_name: doc.Name,
                                                                                    MAP_ID: doc6._id,
                                                                                    Map_Name: doc6.Name,
                                                                                    PT_ID: doc3._id,
                                                                                    pt_name: doc3.Name,
                                                                                    ARM_ID: doc4._id,
                                                                                    arm_name: doc4.Name
                                                                                };
                                                                                msarr.push(jsp)
                                                                                if (i == doc2.length - 1) {
                                                                                    var oll =
                                                                                    {
                                                                                        ACTS: msarr,
                                                                                        MAPS: doc6,
                                                                                        ARMS: doc19,
                                                                                        AGVS: doc5,
                                                                                        MISS: doc,
                                                                                        PNTS: doc18
                                                                                    };
                                                                                    socket.emit('msres', oll);
                                                                                }
                                                                            }


                                                                        })
                                                                    }
                                                                })
                                                            }, interval * i, i);
                                                        }
                                                    }

                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }


            //socket.emit('msres',doc)
        })
    })


    socket.on('actReq', data => {

        // Soft Delete

        MS_DT_ACT.updateOne({ _id: data.act_id }, { $set: { status: "deleted" } }, (err, doc) => {

            if (err)
                socket.emit('deleteActivityS', 'error')
            else {
                socket.emit('deleteActivityS', data.act_id)
            }

        })

        // MS_DT_ACT.deleteOne({_id:data.act_id},(err,doc) => {
        //     if(err)
        //         socket.emit('deleteActivityS','error')
        //     else {

        //     socket.emit('deleteActivityS',data.act_id)
        //     // console.log(doc)

        //         }

        //     })
    })

    socket.on('delSchedMissReq', data => {

        // Soft Delete

        SCHED_DT_MS.updateOne({ MS_ID: data.ms_id, status: "recorded" }, { $set: { status: "deleted" } }, (err, doc) => {
            if (err)
                socket.emit('delSchedMissResS', 'error')
            else {
                socket.emit('delSchedMissResS', data.ms_id)
            }
        })

        // SCHED_DT_MS.deleteOne({MS_ID:data.ms_id},(err,doc) => {
        //     if(err)
        //         socket.emit('delSchedMissResS','error')
        //     else {

        //     socket.emit('delSchedMissResS',data.ms_id)
        //     // console.log(doc)

        //         }

        //     })
    })


    socket.on('AGVByMS', data => {
        var interval = 1 * 100; // 10 seconds;
        var jsonarr = [];
        AGV_DT.findOne({ _id: data.AGV_ID, status: "recorded" }, (err, doc) => {
            if (err)
                console.log(err)
            else {
                MS_DT.find({ agv_ID: doc.agv_ID, status: "recorded" }, (err, doc2) => {
                    if (err)
                        console.log(err)
                    else {
                        //console.log(doc2)
                        for (var i = 0; i < doc2.length; i++) {
                            setTimeout(function (i) {
                                MS_DT_ACT.findOne({ MS_ID: doc2[i]._id, status: "recorded" }, (err, doc3) => {
                                    if (err) {
                                        console.log(err)
                                    } else if (!doc3) {
                                        console.log('null')
                                    }
                                    else {
                                        //console.log(doc3)
                                        MS_DT.findOne({ _id: doc3.MS_ID, status: "recorded" }, (err, doc4) => {
                                            if (err) {
                                                console.log(err)
                                            }
                                            else {

                                                // console.log(doc4.Name)
                                                jsonarr = { MSNm: doc4.Name, MSID: doc4._id }
                                                socket.emit('AGVByMSRes', jsonarr);
                                            }//else...findOne(MS_DT)
                                        })

                                    } //else...MS_DT_ACT
                                })

                            }, interval * i, i);
                        }

                    }//else...MS_DT
                })

            } //else...AGV_DT
        })

    })


    socket.on('ReqEditMission', data => {
        // console.log(missionName);
        // var old_Name = {};
        MS_DT.findOne({ _id: data.MS_ID, status: "recorded" }, (err, doc) => {
            if (!doc) {
                console.log(err);
            }
            else {
                old_name = doc.Name;
                MS_DT.findOne({ Name: data.Name, status: "recorded" }, (err, doc2) => {
                    if (doc2) {
                        socket.emit('ResEditMissionS', 'error');
                        console.log('cannot same name existed')

                    } else {
                        doc.Name = data.Name;
                        //  doc.MAP_ID = data.MAP_ID;
                        // doc.agv_ID = data.agv_ID;
                        doc.TS = new Date();
                        doc.save(err => {
                            if (err) {
                                console.log('err : ' + err);
                                socket.emit('ResEditMissionS', 'error');
                            }
                            else {
                                console.log('successfully saved');
                                socket.emit('ResEditMissionS', doc);

                                var ACT = new ACT_DTS();

                                ACT.User_Name = info[0].Username;
                                ACT.Activity = 'Mission : "' + missionName + '" changed to "' + data.Name;
                                ACT.Type = "MISSION EDIT PAGE";
                                ACT.TS = new Date();
                                ACT.status = "recorded";

                                ACT.save((err, docAT4) => {
                                    if (err) {
                                        //throw err;
                                        console.log(err);
                                    } else {
                                        console.log('Mission : "' + missionName + '" changed to "' + data.Name);
                                        //socket.emit('deleteAGVs',doc);
                                    }
                                })

                            }
                        })
                    }
                })
            }
        })

    })

    //save change schedule name in edit schedule

    socket.on('saveEditSchedInfo', data => {
        SCHED_DT.findOne({ _id: data.SCHED_ID, status: "recorded" }, (err, doc) => {
            if (!doc || err) {
                console.log(err);
            }
            else {
                    
                old_name = doc.Name;
                doc.Name = data.SCHED_NAME;
                doc.USR_ID = data.OPER_ID;
                doc.TS = new Date();
                console.log(doc);
                doc.save(err => {
                    if (err) {
                        console.log('err : ' + err);
                        socket.emit('saveEditSchedInfoS', 'error');
                    }
                    else {
                        console.log('successfully saved');
                        socket.emit('saveEditSchedInfoS', doc);

                        var ACT = new ACT_DTS();

                        ACT.User_Name = info[0].Username;
                        ACT.Activity = 'Schedule : "' + old_name + '" changed to "' + data.SCHED_NAME;
                        ACT.Type = "SCHEDULE EDIT PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";

                        ACT.save((err, docAT4) => {
                            if (err) {
                                //throw err;
                                console.log(err);
                            } else {
                                console.log('Schedule : "' + old_name + '" changed to "' + data.SCHED_NAME);
                                socket.emit('ConveySuccess', 'Mission is successfully saved');
                                //socket.emit('deleteAGVs',doc);
                            }
                        })

                    }
                })
                    
            }
        })

    })

    //-------------------REQUEST POINT NAME----------------//

    socket.on('reqArmPtID', data => {
        var pushIDArmPt = {}
        ARM_DT.findOne({ _id: data.armid }, (err, doc2) => {
            if (err) {
                console.log('arm null')
            } else {

                MAP_DT_PT.findOne({ _id: data.pointid }, (err, doc) => {
                    if (err) {
                        console.log('point not found');
                    } else {

                        pushIDArmPt = ({ ArmDTid: doc2.Cmd.cmd, PtDTid: doc.Name })
                        socket.emit('resArmPtID', pushIDArmPt)
                        console.log(pushIDArmPt)
                    }

                })
            }
        })
    })

    // ---------------------------------------------------mell add on--------------------------------------------------------

    //edit map details, return map data based on ID
    socket.on('ReqMapRecbyID', data => {

        MAP_DT.findOne({ _id: data.map_id, status: "recorded" }, (err, obj) => {
            if (obj) {
                socket.emit('RetMapRecbyID', obj);
            }
        })


    })

    socket.on('ebuttDelMapRecbyID', data => {
        MAP_DT.findOne({ _id: data }, (err, doc) => {
            if (err) {
                console.log('null')
            } else {
                socket.emit('ebuttDelMapRecbyIDS', doc)
            }
        })
    })

    //----------------------DELETE MAP record-----------------------------//
    socket.on('DelMapRecbyID', data => {
        //console.log(data);
        MAP_DT.findOneAndUpdate({ _id: data }, { $set: { status: "deleted" } }, (err, doc) => {
            if (err) {
                socket.emit('DelMapRecbyIDRes', 'err')
                console.log('null');
            }
            else {
                socket.emit('DelMapRecbyIDRes', doc)

                var ACT = new ACT_DTS();

                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Map : "' + doc.Name + '" deleted successfully';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Map : "' + doc.Name + '" deleted successfully');
                        //socket.emit('deleteAGVs',doc);
                    }
                })

                MAP_DT_PT.updateMany({ Map_ID: doc._id }, { $set: { status: "deleted" } }, (err, doc2) => {
                    if (err) {
                        console.log('null')
                    } else {
                        console.log(doc2)


                        MAP_DT_JUNC.updateMany({ map_id: doc._id }, { $set: { status: "deleted" } }, (err, doc3) => {
                            if (err) {
                                console.log('null')
                            } else {
                                console.log(doc3)
                            }
                        })
                    }
                })
            }

        })

    })

    //-------------------ADD NEW MAP record----------------------------//
    socket.on('AddMapRecbyID', data => {
        //console.log(data)
        MAP_DT.findOne({ Filename: data.Filename, status: 'recorded' }, (err, doc) => {
            if (doc) {
                console.log('Filename already exist');
                socket.emit('AddMapCallRes', 'err');
            }
            else {
                var MAP = new MAP_DT();
                MAP.Name = data.Name;
                MAP.Filename = data.Filename;
                MAP.Home_x = data.Home_x;
                MAP.Home_y = data.Home_y;
                MAP.Home_z = data.Home_z;
                MAP.Home_w = data.Home_z;
                MAP.TS = new Date();
                MAP.status = "recorded";

                MAP.save((err, doc2) => {
                    if (err) {
                        console.log(err);
                    } else {
                        //console.log('Map : "'+ doc.Name + '" has been added');
                        socket.emit('AddMapCallRes', doc2);

                        var ACT = new ACT_DTS();

                        ACT.User_Name = info[0].Username;
                        ACT.Activity = 'Map : "' + doc2.Name + '" has been added ';
                        ACT.Type = "MISSION EDIT PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";

                        ACT.save((err, docAT4) => {
                            if (err) {
                                //throw err;
                                console.log(err);
                            } else {
                                console.log('Map : "' + doc2.Name + '" has been added');
                                //socket.emit('deleteAGVs',doc);
                            }
                        })

                    }
                })
            }
        })
    })


    //---------------REQ DATA EDIT MAP----------------------------//
    socket.on('ReqeMVar', data => {
        MAP_DT.findOne({ _id: data }, (err, doc) => {
            if (err) {
                console.log('map null')
            } else {
                socket.emit('ReseMVar', doc);
            }
        })


    })



    //-----------UPDATE MAP record---------------------//
    socket.on('UptMapRecbyID', data => {
        console.log(data)
        MAP_DT.findOneAndUpdate({ _id: data._id }, { $set: { Name: data.Name, Filename: data.Filename, Home_x: data.Home_x, Home_y: data.Home_y, Home_z: data.Home_z, Home_w: data.Home_w } }, (err, doc) => {
            if (err) {
                socket.emit('UptMapRecbyIDRes', 'err')
                console.log(err);
            }
            else {
                socket.emit('UptMapRecbyIDRes', doc)

                var ACT = new ACT_DTS();

                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Map : "' + doc.Name + '" has been updated ';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Map : "' + doc2.Name + '" has been updated');
                        //socket.emit('deleteAGVs',doc);
                    }
                })


            }

        })

    })

    socket.on('ReqdetailsMap', data => {
        MAP_DT.findOne({ _id: data }, (err, doc) => {
            if (err) {
                console.log('null')
                // console.log(doc)
            } else {
                MAP_DT_PT.find({ Map_ID: doc._id, status: "recorded" }, (err, doc2) => {
                    if (err) {
                        console.log('null')
                    } else {
                        // console.log("doc2" + doc2)
                        socket.emit('pointMapDetRes', doc2)
                        MAP_DT_JUNC.find({ map_id: doc._id, status: "recorded" }, (err, doc3) => {
                            if (err) {
                                console.log('null')
                            } else {
                                //console.log("doc3" + doc3)
                                socket.emit('junctionMapDetRes', doc3)
                            }
                        })
                    }
                })
            }
        })
    })

    //---------------------------ADD NEW POINT-----------------------------------//


    socket.on('addNewPointReq', data => {
        console.log(data)
        MAP_DT_PT.findOne({ Name: data.Name, status: "recorded" }, (err, doc) => {

            if (doc) {
                console.log('Already exist')
                socket.emit('addNewPointRes', 'err')

            } else {

                var PT = new MAP_DT_PT();

                PT.Name = data.Name;
                PT.Map_ID = data.Map_ID;
                PT.Loc_x = data.Loc_x;
                PT.Loc_y = data.Loc_y;
                PT.Loc_z = data.Loc_z;
                PT.Loc_w = data.Loc_w;
                PT.Lat = data.Lat;
                PT.Long = data.Long;
                PT.TS = new Date();
                PT.status = "recorded";

                PT.save((err, docAPT) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log(docAPT)
                        socket.emit('addNewPointRes', docAPT)

                        var ACT = new ACT_DTS();

                        ACT.User_Name = info[0].Username;
                        ACT.Activity = 'Map Point : "' + data.Name + '" has been added ';
                        ACT.Type = "MISSION EDIT PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";

                        ACT.save((err, docAT4) => {
                            if (err) {
                                //throw err;
                                console.log(err);
                            } else {
                                console.log('Map : "' + data.Name + '" has been added');
                                //socket.emit('deleteAGVs',doc);
                            }
                        })
                    }
                })


            }
        })
    })


    //---------------------------EDIT EXIST POINT-----------------------------------//

    socket.on('reqListMapPt', data => {
        MAP_DT_PT.findOne({ _id: data }, (err, doc) => {
            if (err) {
                console.log('null')
            } else {
                socket.emit('resListMapPt', doc)
            }
        })
    })


    socket.on('editExistPointReq', data => {
        console.log(data)
        MAP_DT_PT.findOneAndUpdate({ _id: data.id }, { $set: { Name: data.Name, Loc_x: data.Loc_x, Loc_y: data.Loc_y, Loc_z: data.Loc_z, Loc_w: data.Loc_w, Lat: data.Lat, Long: data.Long } }, (err, doc) => {

            if (err) {

                socket.emit('editExistPointRes', 'err')
            }
            else {

                socket.emit('editExistPointRes', doc.Name)

                var ACT = new ACT_DTS();

                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Map Point : "' + doc.Name + '" has been updated ';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Map : "' + doc.Name + '" has been updated');
                        //socket.emit('deleteAGVs',doc);
                    }
                })
            }
        })
    })

    //---------------------------------DELETE MAP POINT------------------------------//


    socket.on('delAddMapPoint', data => {
        MAP_DT_PT.findOneAndUpdate({ _id: data }, { $set: { status: "deleted" } }, (err, doc) => {
            if (err) {
                console.log('err')
                socket.emit('delAddMapPointS', 'err')
            } else {
                socket.emit('delAddMapPointS', doc)

                var ACT = new ACT_DTS();

                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Map Point : "' + doc.Name + '" has been deleted';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Map : "' + doc.Name + '" has been deleted');
                        //socket.emit('deleteAGVs',doc);
                    }
                })
            }
        })
    })


    //-----------------------------ADD NEW JUNCTION---------------------------------//

    socket.on('addNewJunctionReq', data => {

        var JUNCT = new MAP_DT_JUNC();

        JUNCT.map_id = data.map_id;
        JUNCT.x = data.x;
        JUNCT.y = data.y;
        JUNCT.Lat = data.Lat;
        JUNCT.Long = data.Long;
        JUNCT.TS = new Date();
        JUNCT.status = "recorded";

        JUNCT.save((err, docJ) => {
            if (err) {
                socket.emit('addNewJunctionRes', 'err')
                //  console.log(err);
            } else {
                console.log(docJ)
                socket.emit('addNewJunctionRes', docJ)

                var ACT = new ACT_DTS();

                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Map Point Junction : "' + docJ.map_id + '" has been added ';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Map : "' + docJ.map_id + '" has been added');
                        //socket.emit('deleteAGVs',doc);
                    }
                })

            }
        })

    })

    //---------------------------EDIT MAP JUNCTION--------------------------------------//

    socket.on('reqListMapJunct', data => {
        MAP_DT_JUNC.findOne({ _id: data }, (err, doc) => {
            if (err) {
                console.log('null')
            } else {
                socket.emit('resListMapJunct', doc)
            }
        })
    })


    socket.on('editExistJunctReq', data => {
        MAP_DT_JUNC.findOneAndUpdate({ _id: data.id }, { $set: { map_id: data.map_id, x: data.x, y: data.y, Lat: data.Lat, Long: data.Long } }, (err, doc) => {
            if (err) {
                console.log('null')
                socket.emit('editExistJunctRes', 'err')
            } else {
                socket.emit('editExistJunctRes', doc)

                var ACT = new ACT_DTS();

                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Map Point Junction : "' + doc.map_id + '" has been added ';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Map : "' + doc.map_id + '" has been updated');
                        //socket.emit('deleteAGVs',doc);
                    }
                })
            }
        })
    })


    //------------------------------DELETE JUNCTION--------------------------------------//

    socket.on('delAddMapJunct', data => {
        MAP_DT_JUNC.findOneAndUpdate({ _id: data }, { $set: { status: "deleted" } }, (err, doc) => {
            if (err) {
                console.log('null')
                socket.emit('delAddMapJunctS', 'err');
            } else {
                socket.emit('delAddMapJunctS', doc);

                var ACT = new ACT_DTS();

                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Map Point Junction : "' + doc.map_id + '" has been added ';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Map : "' + doc.map_id + '" has been updated');
                        //socket.emit('deleteAGVs',doc);
                    }
                })
            }
        })
    })



    //-------------------------- ROUTINE FUNCTIONS -------------------------------//

    add_mission_routine = (data) => {
        MS_DT.findOne({ Name: data.Name, status: "recorded" }, (err, doc) => {
            if (doc) {
                console.log('mission already exist!');
                socket.emit('addMissionS', 'err');
            }
            else {
                var record = new MS_DT();
                record.Name = data.Name;
                record.MAP_ID = data.MAP_ID;
                record.agv_ID = data.agv_ID;
                record.TS = new Date();
                record.status = 'recorded';
                record.save((err, doc) => {
                    if (err) {
                        console.log(err)
                    }
                    else {

                        MS_DT.findOne({ Name: data.Name, status: "recorded" }, (err, doc1) => {

                            if (err) {
                                console.log(err);
                            }
                            else {

                                MAP_DT_PT.find({ Map_ID: doc.MAP_ID }, (err, doc2) => {
                                    if (err) {
                                        console.log(err);
                                    }
                                    else {
                                        // console.log(doc2)
                                        if (doc2) {
                                            //   console.log(doc2)
                                            var mapdata = doc2
                                            ARM_DT.find({}, (err, doc3) => {
                                                if (err) {
                                                    console.log(err);
                                                }
                                                else {
                                                    if (doc3) {
                                                        //    console.log(doc3);
                                                        var armdata = doc3;
                                                        var json = { msName: doc.Name, Map: mapdata, Arm: armdata, msId: doc._id }
                                                        //  console.log(json);
                                                        socket.emit('addMissionS', json);

                                                        var ACT = new ACT_DTS();
                                                        ACT.User_Name = info[0].Username;
                                                        ACT.Activity = 'Mission  : "' + doc.Name + '" has been added';
                                                        ACT.Type = "MISSION EDIT PAGE";
                                                        ACT.TS = new Date();
                                                        ACT.status = "recorded";

                                                        ACT.save((err, docAT4) => {
                                                            if (err) {
                                                                //throw err;
                                                                console.log(err);
                                                            } else {
                                                                console.log('Mission  : "' + doc.Name + '" has been added');
                                                                //socket.emit('deleteAGVs',doc);
                                                            }
                                                        })
                                                    }
                                                }
                                            })

                                        }
                                        else {
                                            console.log('nothing matched');
                                        }
                                    }
                                });

                            }
                        })

                    }
                })
            }
        })

    }

    edit_schedule_act_routine = (all) => {
        console.log(all);
        var old = all.OLDACT;
        if (old.length > 0) {
            SCHED_DT_MS.deleteMany({ _id: old }, (err, doc1) => {
                if (err) {
                    console.log(err)
                    socket.emit('ESsaved', 'error');
                } else {
                    console.log(old)
                    console.log('are deleted old mission from SCHED_DT_MS');
                }
            });
            var data = all.NEWACT;
            for (var i = 0; i < data.length; i++) {

                var record = new SCHED_DT_MS();

                record.MS_ID = data[i].MS_ID;
                record.SCHED_ID = data[i].SCHED_ID;
                record.Loop = "1"
                record.Seq = data[i].Seq;
                record.Exe = data[i].Exe;
                record.TS = new Date();
                record.status = 'recorded';

                record.save((err, doc) => {
                    if (err) {
                        console.log(err);
                        socket.emit('ESsaved', 'error');
                    }
                    else {
                        console.log("Saved into SCHED_DT_MS table");
                        socket.emit('ESsaved', record);
                    }
                })
            }
        }

    }

    edit_mission_act_routine = (all) => {

        var old = all.OLDACT;
        if (old.length > 0) {
            MS_DT_ACT.deleteMany({ _id: old }, (err, doc1) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log('deleted ' + old.ACT_ID + ' from MS_DT_ACT');
                }
            });
            var data = all.NEWACT;
            for (var i = 0; i < data.length; i++) {

                var record = new MS_DT_ACT();

                record.MS_ID = data[i].MS_ID;
                record.MAP_ID = data[i].MAP_ID;
                record.PT_ID = data[i].PT_ID;
                record.ARM_ID = data[i].ARM_ID;
                record.Seq = data[i].Seq;
                record.Exe = data[i].Exe;
                record.TS = new Date();
                record.status = 'recorded';

                record.save((err, doc) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log("Saved into original table");
                        console.log(doc);
                    }
                })
            }
        }

        socket.emit('EMsaved', 'ok');

    }


    add_mission_act_routine = (data) => {
        //console.log("data._id: "+data._id); // Returns undefined
        for (var i = 0; i < data.length; i++) {

            var record = new MS_DT_ACT();

            record.MS_ID = data[i].MS_ID;
            record.MAP_ID = data[i].MAP_ID;
            record.PT_ID = data[i].PT_ID;
            record.ARM_ID = data[i].ARM_ID;
            record.Seq = data[i].Seq;
            record.Exe = data[i].Exe;
            record.TS = new Date();
            record.status = 'recorded';

            record.save((err, doc) => {
                if (err) {
                    console.log(err)
                    socket.emit('addMissionActS', 'error');
                }
                else {
                    console.log("Saved into original table");
                    socket.emit('addMissionActS', 'ok');
                }
            })

        }

    }


    add_sched_routine = (data) => {
        SCHED_DT.findOne({ Name: data.Name, status: 'recorded' }, (err, doc) => {
            if (err) {
                console.log(err)
            }
            else {
                if (doc) {
                    console.log('already exist');
                    socket.emit('addSchedS', 'err')
                }
                else {
                    var record = new SCHED_DT();
                    record.Name = data.Name;
                    record.USR_ID = data.USER_ID;
                    record.TS = new Date();
                    record.status = 'recorded';
                    record.save((err, doc) => {
                        if (err) {
                            console.log(err)
                        }
                        else {
                            // console.log(doc);
                            socket.emit('addSchedS', doc);
                            var ACT = new ACT_DTS();
                            ACT.User_Name = info[0].Username;
                            ACT.Activity = 'Schedule  : "' + doc.Name + '" has been added';
                            ACT.Type = "MISSION EDIT PAGE";
                            ACT.TS = new Date();
                            ACT.status = "recorded";

                            ACT.save((err, docAT4) => {
                                if (err) {
                                    //throw err;
                                    console.log(err);
                                } else {
                                    console.log('Schedule  : "' + doc.Name + '" has been added');
                                    //socket.emit('deleteAGVs',doc);
                                }
                            })
                        }
                    })
                }
            }
        })
    }


    save_sched_routine = (data) => {
        for (var i = 0; i < data.length; i++) {
            var record = new SCHED_DT_MS();
            record.MS_ID = data[i].MS_ID;
            record.SCHED_ID = data[i].SCHED_ID;
            record.Loop = data[i].Loop;
            record.Seq = data[i].Seq;
            record.Exe = data[i].Exe;
            record.TS = new Date();
            record.status = 'recorded';
            record.save((err, doc) => {
                if (err) {
                    console.log(err)
                    socket.emit('saveSchedS', 'error');
                }
                else {
                    console.log(doc);
                    socket.emit('saveSchedS', doc);

                }
            })
        }

    }


    delete_mission_routine = (data) => {
        // Soft Delete
        MS_DT.findOneAndUpdate({ _id: data.ms_id, status: "recorded" }, { $set: { status: "deleted" } }, (err, doc) => {

            if (err) {
                console.log(err)
                socket.emit('deleteMissionS', 'error');
            } else {

                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Mission  : "' + doc.Name + '" has been deleted';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Mission  : "' + doc.Name + '" has been deleted');
                        //socket.emit('deleteAGVs',doc);
                    }
                })

                console.log('deleted ' + data.ms_id + ' from ms_dt')
                MS_DT_ACT.updateMany({ MS_ID: data.ms_id, status: "recorded" }, { $set: { status: "deleted" } }, (err, doc1) => {
                    if (err) {
                        console.log(err)
                        socket.emit('deleteMissionS', 'error');
                    } else {
                        console.log('deleted ' + data.ms_id + ' from ms_dt_act');
                        SCHED_DT_MS.find({ MS_ID: data.ms_id }, (err, doc) => {
                            if (err) {
                                console.log(err)
                                socket.emit('deleteMissionS', 'error');
                            } else {
                                SCHED_DT_MS.updateMany({ MS_ID: data.ms_id, status: "recorded" }, { $set: { status: "deleted" } }, (err, doc2) => {
                                    if (err) {
                                        console.log(err)
                                        socket.emit('deleteMissionS', 'error');
                                    } else {
                                        console.log('deleted ' + data.ms_id + ' from sched_dt_ms')
                                        socket.emit('deleteMissionS', 'success');


                                    }
                                })
                            }
                        })
                    }

                })

            }
        })
    }


    delete_schedule_routine = (data) => {
        // Soft Delete
        SCHED_DT.findOneAndUpdate({ _id: data.sc_id, status: "recorded" }, { $set: { status: "deleted" } }, (err, doc) => {
            if (err) {
                console.log(err);
                socket.emit('deleteScheduleS', 'error');
            } else {
                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Schedule  : "' + doc.Name + '" has been deleted';
                ACT.Type = "MISSION EDIT PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        console.log('Mission  : "' + doc.Name + '" has been deleted');
                        //socket.emit('deleteAGVs',doc);
                    }
                })

                SCHED_DT_MS.find({ SCHED_ID: data.sc_id, status: "recorded" }, (err, doc2) => {

                    if (doc2) {
                        SCHED_DT_MS.updateMany({ SCHED_ID: data.sc_id, status: "recorded" }, { $set: { status: "deleted" } }, (err, doc) => {
                            if (err) {
                                console.log(err)
                            } else {
                                socket.emit('deleteScheduleS', doc.Name)
                            }
                        })

                    } else {
                        console.log('delete');
                        socket.emit('deleteScheduleS', doc.Name)
                    }
                })
            }
        })

    }

    sched_req_routine = (data) => {
        var schedmsarr = [];
        var allmissbyAGV = [];
        var alloperator = [];
        var interval = 1 * 100; // 10 seconds;
        SCHED_DT.findOne({ _id: data.sched_id }, (err, doc) => {
            if (err)
                console.log('null sched')
            else {
                USR_DT.findOne({ _id: doc.USR_ID }, { Username: 1, Name:1 }, (err, doc00) => {
                    if (err)
                        console.log('null user')
                    else {
                        SCHED_DT_MS.find({ SCHED_ID: doc._id, status: "recorded" }).sort({ Seq: 1 }).exec((err, doc2) => {
                            if (err)
                                console.log(err)
                            else {
                                //console.log("doc2: "+doc2);
                                for (var i = 0; i < doc2.length; i++) {
                                    setTimeout(function (i) {
                                        MS_DT.findOne({ _id: doc2[i].MS_ID, status: "recorded" }, (err, doc3) => {
                                            if (err) {
                                                console.log(err)
                                            }
                                            else {
                                                //  console.log("doc3: "+doc3);
                                                SCHED_DT.findOne({ _id: doc2[i].SCHED_ID, status: "recorded" }, (err, doc4) => {

                                                    if (err) {
                                                        console.log(err)
                                                    }
                                                    else {

                                                        // console.log("doc4: "+doc4);
                                                        USR_DT.findOne({ _id: doc4.USR_ID, status: "recorded" }, (err, doc5) => {
                                                            if (err) {
                                                                console.log(err)
                                                            }
                                                            else {


                                                                schedmsarr.push(
                                                                    {
                                                                        SCHED_MS_ID: doc2[i]._id,
                                                                        AGV_ID: doc3.agv_ID,
                                                                        MS_ID: doc3._id,
                                                                        Sched_Name: doc4.Name,
                                                                        SCHED_ID: doc4._id,
                                                                        MS_Name: doc3.Name,
                                                                        Operator: doc5 != undefined?  doc5._id : "Deleted user",
                                                                        Seq: doc2[i].Seq
                                                                    });
                                                                if (i == doc2.length - 1) {
                                                                    MS_DT.find({ agv_ID: doc3.agv_ID, status: "recorded" }, (err, doc6) => {
                                                                        if (err) {
                                                                            console.log(err)
                                                                        }
                                                                        else {
                                                                            USR_DT.find({ Type: "OPERATOR", status: "recorded" }, { Username: 1, Name:1 }, (err, doc7) => {
                                                                                if (err) {
                                                                                    console.log(err)
                                                                                }
                                                                                else {
                                                                                    var js = {
                                                                                        SMIS: schedmsarr,
                                                                                        SCHD: doc,
                                                                                        USRS: doc00,
                                                                                        MISS: doc6,
                                                                                        OPER: doc7
                                                                                    };
                                                                                    socket.emit('schedRes', js);
                                                                                }
                                                                            })
                                                                        }
                                                                    });
                                                                }

                                                            }
                                                        })//USR_DT
                                                    }
                                                })//SCHED_DT
                                            }
                                        })//MS_DT

                                    }, interval * i, i);
                                }
                            }
                        }) //sched_dt_ms
                    }
                })
            }
        })

    }


}//socket app.js
