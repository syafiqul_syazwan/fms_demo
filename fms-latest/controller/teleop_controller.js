const express = require("express");
var router = express.Router();
const mongoose = require("mongoose");
const AGV_DT = mongoose.model("AGV_DT");
const HEALTH_DT = mongoose.model("HEALTH_DT");
const ACT_DTS = mongoose.model("ACT_DTS");
const CAM_DT = mongoose.model("CAM_DT");
var NOTIFY = mongoose.model("NOTIFY");
const MS_DT_REC = mongoose.model("MS_DT_REC");
const MS_DT = mongoose.model("MS_DT");
const MAP_DT = mongoose.model("MAP_DT");
const MS_DT_ACT = mongoose.model("MS_DT_ACTIVITY");
const MAP_DT_PT = mongoose.model("MAP_DT_PT");
const SCHED_DT_MS = mongoose.model("SCHED_DT_MS");
var THISUSER = "";

var info = [];
const loggedin = (req, res, next) => {
    if (req.isAuthenticated()) {
        THISUSER = req.user;
        //console.log(req.user)
        if (req.user.role == "OBSERVER") {
            if (req.path == "/") {
                return res.redirect("./error");
            } else {
                console.log("nothing");
            }
        } else if (req.user.role == "OPERATOR") {
            if (req.path == "/") {
                info = [];
                var id = req.user.id;
                var name = req.user.name;
                var role = req.user.role;
                var pass = req.user.password;
                var username = req.user.username;
                info.push({
                    userId: id,
                    Name: name,
                    Role: role,
                    Password: pass,
                    Username: username
                });
                //return res.redirect('./error')
            } else {
                console.log("nothing");
            }
        } else if (req.user.role == "ADMIN") {
            if (req.path == "/") {
                info = [];
                var id = req.user.id;
                var name = req.user.name;
                var role = req.user.role;
                var pass = req.user.password;
                var username = req.user.username;
                info.push({
                    userId: id,
                    Name: name,
                    Role: role,
                    Password: pass,
                    Username: username
                });
            } else {
                console.log("nothing");
            }
        } else {
            return res.redirect("./error");
        }

        return next();
    } else {
        res.redirect("/login");
    }
};

router.get("/", loggedin, (req, res) => {
    // Trigger Global Refresh
    //require('./dashboard_controller')();

    AGV_DT.find({ status: "recorded" }, (err, docs) => {
        if (err) {
            console.log(err);
        } else {
            res.render("mapControl/index", {
                title: "MAP & TELEOP",
                agv: docs,
                name: info[0].Name,
                userid: info[0].userId,
                role: info[0].Role,
                pass: info[0].Password,
                Username: info[0].Username
            });
            //HEALTH_DT.find({})
            //    .limit(10)
            //    .sort({ TS: -1 })
            //    .exec((err, doc1) => {
            //        if (err) {
            //            console.log(err);
            //        } else {
            //        }
            //    });
        }
    });
});

module.exports = router;

var Map_Dt_find_recorded_maps = () => {
    return new Promise((resolve, reject) => MAP_DT.find({ status: "recorded" }).exec((failed, data) => {
        (failed || data.length < 1) ? reject(failed == null ? "No record found:" : failed) : resolve(data);
    }))
}
var Sched_Dt_Ms_find_missionidby_schedid = (id) => {
    return new Promise((resolve, reject) => id == undefined || id.length < 1 || id == null ? reject("IDnull") : SCHED_DT_MS.find({ status: "recorded", SCHED_ID: id }, { _id: 0, MS_ID: 1 }).exec((failed, data) => {
        (failed || data.length < 1) ? reject(" failed on taking missions from schedules: " + failed) : resolve(data);

    }))
}
var MSs_Dt_Act_find_PTID_by_mission_id = (arrid) => {
    return new Promise((resolve, reject) => arrid.length < 1 ? reject("no mission ID") : MS_DT_ACT.find({ status: "recorded", MS_ID: { $in: arrid } }, { _id: 0, PT_ID: 1 }).exec((failed, data) => {
        (failed || data.length < 1) ? reject(" Failed to take points from Mission Database Act: " + failed) : resolve(data);


    }))
}
var Map_Dt_Pt_find_LatLong_by_Pt_Id = (arrid) => {
    return new Promise((resolve, reject) => arrid.length < 1 ? reject("no PTID") : MAP_DT_PT.find(arrid[0].toLowerCase() == "all" ? { status: "recorded" } : { status: "recorded", _id: { $in: arrid } }, { _id: 0, Lat: 1, Long: 1, Name: 1 }).exec((failed, data) => {
        (failed || data.length < 1) ? reject(" failed to get mission points: " + failed) : resolve(data);
    }))


}
/*
 Highest Level service
 Assign ASYNC and TRY CATCh to allow wait for procedural processes
 */

async function SCHED_Dt_find_MissionsPoints_by_SCHEDID(id) {

    try {
        var PointLatLong;
        if (id != undefined || id.length > 0 || id != null) {
            if (id[0].toLowerCase() == "all") {
                PointLatLong = await Map_Dt_Pt_find_LatLong_by_Pt_Id(id);
            }
            else {
                var missionID = await Sched_Dt_Ms_find_missionidby_schedid(id);
                var MSID = [];
                for (i = 0; i < missionID.length; i++) {
                    MSID.push(missionID[i].MS_ID);
                }
                var PointID = await MSs_Dt_Act_find_PTID_by_mission_id(MSID);
                var PTID = [];
                for (i = 0; i < PointID.length; i++) {
                    PTID.push(PointID[i].PT_ID);
                }
                PointLatLong = await Map_Dt_Pt_find_LatLong_by_Pt_Id(PTID);
            }


        }
        return new Promise((resolve, reject) => {
            PointLatLong != undefined ? resolve(PointLatLong) : resolve(reject);
        })
    }
    catch (e) {
        return new Promise((resolve, reject) => {
            reject(" From SCHED_DT_find_MisisonPoints_by_Schedid: " + e);
        })

    }

}

router.post('*', function (req, res, next) {
    try {

        THISUSER = req.user;
        const accessible = ["observer", "admin", "operator"];
        var result = {
            isSuccessful: true,
            data: {},
            properties: null,
            message: "",
        }
        var url = req.url.toString();
        var arg = req.body;
        //Ajax requeswts starts here
        if (THISUSER && accessible.includes(THISUSER.role.toLowerCase())) {
            //---------------------request for getting Homepoint and/or Missionpoint to be rendered onto Map 
            if (url == '/getHomePoint') {
                var Pointreq = async (id) => {
                    var result = await SCHED_Dt_find_MissionsPoints_by_SCHEDID(id);
                    return result;
                }
                var Mapreq = async () => {
                    var result = await Map_Dt_find_recorded_maps();
                    return result;
                }
                if (arg != null && arg.isGetMissionPoint) {
                    var schedid = arg.sched_id;
                    Mapreq().then(function (response) {
                        result.data.homepoints = response;
                        result.isSuccessful = true;
                        return result;
                    }, function (exception) {
                        result.isSuccessful = false;
                        // console.log("Error got " + exception);
                        result.message += "Error in home point request " + exception;
                        return result;
                    }).then(function (result) {
                        Pointreq(schedid).then(function (response) {
                            result.data.missionpoints = response;
                            result.isSuccessful = true;
                            return result;
                        }, function (exception) {
                            result.message += "Error in mission point request " + exception + "\n";
                            result.isSuccessful = !result.isSuccessful ? false : true;
                            return result;
                        }).then(function (result) {
                            res.send(result);
                        })

                    })

                }
                else {
                    Mapreq().then(function (response) {
                        result.data.homepoints = response;
                        result.isSuccessful = true;
                        return result;
                    }, function (exception) {
                        result.isSuccessful = false;
                        result.message = "Error " + exception;
                        return result;
                    }).then(function (result) {
                        res.send(result);
                    })
                }
            }

        }
        else {
            //   console.log('This User ' + THISUSER);
            //   console.log('authentiation not right');
        }
    }
    catch (e) {
        result.isSuccessful = false;
        result.message = "Error " + e;
        //    console.log('failed at ' + req.url.toString() + ' with error: ' + e);
        res.send(JSON.stringify(result));
    }
})

module.exports.respond = socket => {
    socket.on("sys/mv1", data => {
        var ACT = new ACT_DTS();
        ACT.User_Name = info[0].Username;
        ACT.Activity =
            'User : "' +
                info[0].Username +
                '" moved "' +
                data.agv_ID +
                '" to ' + data.agvMv == "f" ? "forward" : data.agvMv == "b" ? "backward" : data.agvMv == "l" ? "left" : data.agvMv == "r" ? "right" : "unknown movement";
        ACT.Type = "MAP & TELEOP PAGE";
        ACT.TS = new Date();
        ACT.status = "recorded";

        ACT.save((err, sysmv) => {
            if (err) {
                console.log("Error on saving agv manual movement" + err);
            } else {
            }
        });

    });

    socket.on("cam/link", data => {
        CAM_DT.findOne({ AGV_ID: data }, (err, doc) => {
            //  console.log(doc)
            if (err) {
                console.log("null");
            } else {
                // let encoded = base64encode('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAADh0lEQVRYhe2YX2/TZhTGn/ePwck0u4BUJKStagMqlRACqUrTNu1kEfwZJvEJdrF9mN3AB+CCrwC0KrQOTsINIBDaoEWi0iakSSSuSFr5/bOLKhAcN7Uda+Oiz+Xr1/bP55z3nEcGTnSi/1dk1MW6605SKe8pxn5bfvDgZZ4v9hxnllF6R1J6q/rw4V+pARs3bpzXlHpEqRIo/ai1ri6urb3OA86v1eYIsAWtz4LSbShVrayvf4jbS+MW6647CUo3T5nm1GXHIcWJCQuE1Ouue2VcOM9xZgmwWbBt+7LjkFPF4hSh9Il38+aFuP1DEay77iTTesswzelLS0uGYZpQUmK72RSf2u09Rchq1nR7jjPLGfMKtj1RqlQ44xzi4ABvfD8Mu93dEFiJpnsoglTKe1Dq0ky5bBimebjGGGbKZV60rO+p1o/9Wm0uLZxfq80xzp8MwgEAP30a0/PzhlZqmil1d4gnuiCV+oVQ+s/7Z8+EFOLzOuMcFxcX+XcZ0t1Pa9GyrEE4AFBSYvfFC6EpbTOlfo3eG3tIPMeZNTjfMi3rTNwD06Q7Lq1xz2JCrJQ3Nl4lAswLcly4kYDjQuYBdyxgVsi84BIBpoXUYRjmBZcYEPjS/Qu2bUdfLIXAdqMhekEQQGt93J40UykxYB8ShHhx7aIPAABHXesefkCqkZkKsA85KpLAYc+MwqWNXGZAYHRNDipLzUUVaxaOU3Vj4w+l9U+9Tqez3Wh8NXH6kkLgre+LbrsdQOvlLHCZAROLZErQV8oEOGiZjkox4xwXKxU+rlX7bw5Jsyl6nU6mQ5Iqgv02M6rPRWuScY7SwgIvWJYFQry0Vi0xYD9yR/bAZlP0giCIOziMc5QqFV60bYukhEwEOKrmlJTYabVEt9PZo0KsCimX94PgYxxklppMZBbSztY8/eSxdivr4M8LcqRhHdeV5AF5pOXPyzKNCzkEmCdcHpBDp5hRehtan/vx2rWhVvLW98WnDLO1P7u7QRBETzdlDD9cvcqJUhNUyt+j9w4BKsZ+JoT8udNqheH+/uev3Gm1RDcI9pgQq1l+gSyurb2WQixFW5A4OMC7p09DQuk7g/Nb0ftia7DuupNcqU1eKMyUFhaM98+fj2WZBjWY7qnr1/lOqxWGvd4uo7Q6f//+34kAgW/85xEAVNbXPxClqhp4lCcccJhuDaxo4NEouBOd6FvQv4/9bFxgtiyIAAAAAElFTkSuQmCC'); // "aGV5ICB0aGVyZQ=="
                // let decoded = base64decode(encoded); // "hey there"

                //console.log(decoded)
                // socket.emit('imglah',decoded)

                socket.emit("cam/linkS", doc);

                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = '"Start teleop for "' + data + '"';
                ACT.Type = "MAP & TELEOP PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log("null");
                    } else {
                        // console.log('"Start teleop for "'+ data +'"');
                        //socket.emit('deleteAGVs',doc);
                    }
                });
            }
        });
    });

    MS_DT_REC.findOne({})
        .sort({ TS: -1 })
        .exec((err, doc) => {
            if (!doc) {
                console.log("null");
            } else {
                socket.emit("MsDtRecAct", doc.ACTVT_ACT);
            }
        });
    // var base64String ="";
    // fs.writeFile("package1.json",base64string,{encoding:'base64'},function(err){
    //     if(err){
    //             console.log('null')
    //     }else{
    //             console.log('save success')
    //     }
    // })
};