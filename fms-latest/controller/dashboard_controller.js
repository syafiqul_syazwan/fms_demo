var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var HEALTH_DT = mongoose.model('HEALTH_DT');
var MS_DT = mongoose.model('MS_DT');
var AGV_DT = mongoose.model('AGV_DT');
var ACT_DTS = mongoose.model('ACT_DTS');
var NOTIFY = mongoose.model('NOTIFY');
var NOTI_SEEN = mongoose.model('NOTI_SEEN');
var usernow;

var info = [];

const loggedin = (req, res, next) => {
    if (req.isAuthenticated()) {
        //console.log(req.user)
        if (req.user.role == 'OBSERVER') {
            if (req.path == '/') {
                info = [];
                info.push({ userId: req.user.id, Name: req.user.name, Role: req.user.role, Password: req.user.password, Username: req.user.username })
            }

            else {
                console.log('nothing')
            }

        }
        else if (req.user.role == 'OPERATOR') {
            if (req.path == '/') {
                info = [];
                info.push({ userId: req.user.id, Name: req.user.name, Role: req.user.role, Password: req.user.password, Username: req.user.username })
            }
            else {
                console.log('nothing')
            }

        }
        else if (req.user.role == 'ADMIN') {
            if (req.path == '/') {
                info = [];
                info.push({ userId: req.user.id, Name: req.user.name, Role: req.user.role, Password: req.user.password, Username: req.user.username })
            }
            else {
                console.log('nothing')
            }
        }
        else {

            return res.redirect('./error')

        }
        return next()
    }
    else {
        res.redirect('/login')
    }
}


router.get('/', loggedin, (req, res) => {

    usernow = info[0].userId;

    AGV_DT.find({ status: 'recorded' }, (err, docs) => {
        if (err) {
            console.log(err);
        }
        else {
            res.render('dashboard/index', {
                title: 'DASHBOARD',
                agv: docs,
                //name: info[0].Name == undefined ? info[0].Name : "Undefined name",
                //userid: info[0].userId == undefined ? info[0].userId : "Undefined ID",
                //role: info[0].Role == undefined ? info[0].Role : "Undefined Role",
                //pass: info[0].Password == undefined ? info[0].Password : "undefined Password",
                name: req.user.name,
                userid: req.user.id,
                role: req.user.role,
                pass: req.user.password,
            });
            info = [];
        }
    })

})

module.exports = router;

module.exports.respond = socket => {

    //----------------------REALTIME NOTIFICATION - START--------------------------//

    var noti_now = "";
    NOTIFY.createIndexes({ $sort: { _id: -1 } });

    NOTIFY.findOne({}, null, { sort: { _id: -1 } }).then(doc => {
        if (doc) {
            noti_now = doc._id;
        }
    }).catch(err => {
        console.log(err);
    });

    refreshNoti();

    function intervalFunc() {
        NOTIFY.findOne({}, null, { sort: { _id: -1 } }).then(doc2 => {
            if (doc2) {
                if (noti_now.toString() != doc2._id.toString()) {
                    refreshNoti();
                    noti_now = "";
                    noti_now = doc2._id;
                }
            }
        }).catch(err => {
            console.log(err);
        });

    }

    function refreshNoti() {

        //console.log("refreshNoti called")

        NOTIFY.find({}, null, { sort: { _id: -1 } }).limit(100).then(doc => {
            if (doc) {
                NOTI_SEEN.find({ USER_ID: usernow }).then(doc1 => {
                    if (doc1.length > 0) {

                        showNoti_ByUserId(doc, doc1);
                    } else {
                        socket.emit('refresh_noti', doc);
                    }
                }).catch(err => {
                    console.log(err);
                })

            }

        }).catch(err => {
            console.log(err);
        })
    }

    // Declare Global Refresh
    var global_refresh = function () {
        refreshNoti();
    };

    module.exports = global_refresh;
    //all notify passed down
    function showNoti_ByUserId(allnotify, allnotiseen) {

        var index2del = [];
        var count = 0;

        for (x = 0; x < allnotiseen.length; x++) {
            for (y = 0; y < allnotify.length; y++) {
                if (allnotiseen[x].NOTI_ID == allnotify[y]._id) {
                    index2del = index2del.concat(y);
                }
            }

            count = count + 1;

        }

        if (count == allnotiseen.length) {
            process_noti(index2del, allnotify);
        }
        index2del = [];


    }

    function process_noti(index, allnotify) {

        var count = 0;

        for (z = 0; z < index.length; z++) {
            allnotify.splice(index[z], 1);

            count = count + 1;

        }

        if (count == index.length) {

            socket.emit('refresh_noti', allnotify);
            index = [];

        }
    }

    setInterval(intervalFunc, 5000);

    //----------------------REALTIME NOTIFICATION - END--------------------------//


}




