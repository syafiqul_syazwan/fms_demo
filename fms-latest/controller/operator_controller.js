const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const MS_DT = mongoose.model('MS_DT');
const MS_DT_ACTIVITY = mongoose.model('MS_DT_ACTIVITY');
const AGV_DT = mongoose.model('AGV_DT');
const SCHED_DT = mongoose.model('SCHED_DT');
const MAP_DT = mongoose.model('MAP_DT');
const ARM_DT = mongoose.model('ARM_DT');
const MAP_DT_PT = mongoose.model('MAP_DT_PT');
const SCHED_DT_MS = mongoose.model('SCHED_DT_MS');
const HEALTH_DT = mongoose.model('HEALTH_DT');
const MAP_DT_JUNC = mongoose.model('MAP_DT_JUNC');
const MS_DT_REC = mongoose.model('MS_DT_REC');
var NOTIFY = mongoose.model('NOTIFY');
var SSID_DT = mongoose.model('SSID_DT');
process.setMaxListeners(Infinity);
var usernow;
var sched_myid = "";
var execId;

var sched_IDCatcher;
var ssid_catcher;

const loggedin = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.role == 'OBSERVER') {
            if (req.path == '/') {
                return res.redirect('./error')
            }
            else {
                console.log('nothing')
            }
        }
        else if (req.user.role == 'OPERATOR') {
            if (req.path == '/') {
                info = [];
                var id = req.user.id;
                var name = req.user.name
                var role = req.user.role
                var pass = req.user.password
                info.push({ userId: id, Name: name, Role: role, Password: pass })
            }
            else {
                console.log('nothing')
            }
        }
        else if (req.user.role == 'ADMIN') {
            if (req.path == '/') {
                info = [];
                var id = req.user.id;
                var name = req.user.name
                var role = req.user.role
                var pass = req.user.password
                info.push({ userId: id, Name: name, Role: role, Password: pass })
            }
            else {
                console.log('nothing')
            }
        }
        else {
            return res.redirect('./error')
        }
        return next()
    }
    else {
        res.redirect('/login')
    }
}

router.get('/', loggedin, (req, res) => {
    usernow = info[0].Name;
    AGV_DT.createIndexes({ status: 'recorded' });
    SCHED_DT.createIndexes({ status: 'recorded' });
    HEALTH_DT.createIndexes({ status: 'recorded', $sort: { TS: -1 } });
    MS_DT.createIndexes({ status: 'recorded' });

    AGV_DT.find({ status: 'recorded' }, (err, doc2) => {
        if (err) {
            console.log(err)
        }
        else {
            SCHED_DT.find({ status: 'recorded' }, (err, doc) => {
                if (err) {
                    console.log(err)
                }
                else {

                    res.render('operation/index', {
                        title: 'OPERATION',
                        sched_dt: doc,
                        agv: doc2,
                        //  health_dt: doc3,
                        name: info[0].Name,
                        userid: info[0].userId,
                        role: info[0].Role,
                        pass: info[0].Password,
                    });

                }

            })
        }
    })
})

module.exports = router;

module.exports.respond = socket => {
    function findin_agv_dt_id(match) {
        return new Promise((res, rej) => {
            AGV_DT.findOne({ agv_ID: match }, (err, doc) => {
                if (err) {
                    rej(err);
                }
                res(doc);
            })
        })
    }
    function findin_ms_dt_agvid(match) {
        return new Promise((res, rej) => {
            MS_DT.find({ agv_ID: match, status: 'recorded' }, (err, doc) => {
                if (err) {
                    rej(err);
                }
                res(doc);
            })
        })
    }
    function findin_sched_dt_msid(match) {
        return new Promise((res, rej) => {
            SCHED_DT_MS.find({ MS_ID: match, status: 'recorded' }, (err, doc) => {
                if (err) {
                    rej(err);
                }
                res(doc);
            })
        })
    }
    function findin_sched_dt_ID(match) {
        return new Promise((res, rej) => {
            SCHED_DT.findOne({ _id: match, status: 'recorded' }, (err, doc) => {
                if (err) {
                    rej(err);
                }
                res(doc);
            })
        })
    }

    function findin_sched_dt_ms_id(match) {
        return new Promise((res, rej) => {
            SCHED_DT_MS.find({ SCHED_ID: match }, (err, doc) => {
                if (err) {
                    rej(err);
                }
                res(doc);
            })
        })
    }
    function findin_ms_dt_id(match) {
        return new Promise((res, rej) => {
            MS_DT.findOne({ _id: match }, (err, doc) => {
                if (err) {
                    rej(err);
                }
                res(doc);
            })
        })
    }
    function findin_map_dt_id(match) {
        return new Promise((res, rej) => {
            MAP_DT.findOne({ _id: match }, (err, doc) => {
                if (err) {
                    rej(err);
                }
                res(doc);
            })
        })
    }

    function findin_msdtact(match) {
        return new Promise((res, rej) => {
            MS_DT_ACTIVITY.find({ MS_ID: match }, (err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function findin_mapdt(match) {
        return new Promise((res, rej) => {
            MAP_DT.findOne({ _id: match, status: "recorded" }, (err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function findin_mapdtpt(match) {
        return new Promise((res, rej) => {
            MAP_DT_PT.findOne({ _id: match, status: "recorded" }, (err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function findin_arm_dt(match) {
        return new Promise((res, rej) => {
            ARM_DT.findOne({ _id: match, status: "recorded" }, (err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function findin_mapJunc(match) {
        return new Promise((res, rej) => {
            MAP_DT_JUNC.find({ map_id: match, status: "recorded" }, (err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function sortobj(objs) {
        objs.sort(function (a, b) {
            return (a.Seq) - (b.Seq)
        });
        return objs;
    }

    function saveDBMsRec(data) {
        //purpose of creating this is when act is done or when autonomous operation mission is done
        //global variable exchanger
        //console.log(data);
        if (data.health.ms.act != "done") {
            sched_IDCatcher = data.health.ms.sched_ID;
            ssid_catcher = data.health.ms.ss_id;
        }
        let docu = new MS_DT_REC;
        docu.SS_ID = ssid_catcher;
        docu.MS_ID = data.health.ms.ms_ID;
        docu.MS_SCHED_ID = sched_IDCatcher;
        docu.ACTVT_SEQ = data.health.ms.act_Seq;
        docu.ACTVT_PT = data.health.ms.point;
        docu.ACTVT_ACT = data.health.ms.act;
        docu.Operator = usernow;
        usernow = null;
        docu.TS = new Date();
        docu.status = data.stat;
        docu.save((err, doc) => {
            if (err) {
                // console.log(err);
            }
            else {
                //processFindHealdMs();
                async function processFindHealdMs() {
                    var msID = await msDtID(msidCatcher);
                    console.log(msID);
                    if (data.status == 'done') {
                        if (msID) {
                            saveDBNotify(msID);
                        }
                    }
                }
            }
        })
    }
    function msDtID(match) {
        return new Promise((res, rej) => {
            MS_DT.findOne({ _id: match, status: "recorded" }, (err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function saveDBNotify(data) {
        let NOTI = new NOTIFY();
        NOTI.AGV_ID = data.agv_ID;
        NOTI.Type = "mission_complete";
        NOTI.Det = "Mission: " + data.Name + " > Completed";
        NOTI.TS = new Date();
        NOTI.save((err, doc) => {
            if (err) {
                console.log(err);
            }
            else {
                // console.log("Mission Activity Sent to DB");
            }
        })
    }

    function findin_latest(match) {
        return new Promise((res, rej) => {
            MS_DT_REC.find({ SS_ID: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function findin_ms_SCHEDID(match) {
        return new Promise((res, rej) => {
            SCHED_DT.find({ _id: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function findin_ms_SCHEDMSID(match) {
        return new Promise((res, rej) => {
            SCHED_DT_MS.find({ SCHED_ID: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        });
    }
    function findin_ms_dt(match) {
        return new Promise((res, rej) => {
            MS_DT.find({ _id: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            })
        })
    }
    function findin_map_dt(match) {
        return new Promise((res, rej) => {
            MAP_DT.findOne({ _id: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            })
        })
    }
    function findin_ms_act_id(match) {
        return new Promise((res, rej) => {
            MS_DT_ACTIVITY.find({ MS_ID: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            })
        })
    }
    function findin_MS_pt(match) {
        return new Promise((res, rej) => {
            MAP_DT_PT.findOne({ _id: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            })
        })
    }
    function findin_armDT(match) {
        return new Promise((res, rej) => {
            ARM_DT.findOne({ _id: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            })
        })
    }

    function findin_schedMSdt(match) {
        return new Promise((res, rej) => {
            SCHED_DT_MS.find({ SCHED_ID: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            })
        })
    }

    function findin_ssID(match) {
        return new Promise((res, rej) => {
            SSID_DT.find({ SS_ID: match }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            })
        })
    }

    function findin_delMisSSID_DT(match1, match2) {
        return new Promise((res, rej) => {
            SSID_DT.deleteOne({ SS_ID: match2, MS_ID: match1 }).exec((err, doc) => {
                if (err) rej(err);
                res(doc);
            });
        })
    }

    socket.on('reqmsAgv', data => {
        var jsonarr = [];
        processMsagv();
        async function processMsagv() {

            var msAgvID = await findin_ms_dt_agvid(data.AGV_ID);
            for (var i = 0; i < msAgvID.length; i++) {
                var scMsID = await findin_sched_dt_msid(msAgvID[i]._id);
                if (!scMsID) {
                    console.log("OperatorController Error reqmsAgv");
                }
                else {
                    //  console.log('result count: ' + scMsID.length);
                    for (var j = 0; j < scMsID.length; j++) {
                        var schedID = await findin_sched_dt_ID(scMsID[j].SCHED_ID);
                        if (schedID != null) {
                            jsonarr = { SCHED_ID: schedID._id, SCHED_NAME: schedID.Name }
                        }

                        if (j == scMsID.length - 1) {
                            socket.emit('resmsAgv', jsonarr);
                        }
                    }
                }
            }
        }
    })

    socket.on('reqmsdt', data => {
        // console.log(data.SCHED_ID);
        var jsonarr = [];
        sched_myid = data.SCHED_ID;
        processMsdt();
        async function processMsdt() {
            var scID = await findin_sched_dt_ms_id(data.SCHED_ID);
            for (var i = 0; i < scID.length; i++) {
                var msID = await findin_ms_dt_id(scID[i].MS_ID);
                var mapID = await findin_map_dt_id(msID.MAP_ID);
                var json = { Name: msID.Name, agv_ID: msID.agv_ID, MAP_ID: mapID.Name, TS: msID.TS, Loops: scID[i].Loop, fname: mapID.Filename, ms_ID: msID._id, sequ: scID[i].Seq }
                jsonarr.push(json);
                if (i == scID.length - 1) {
                    socket.emit('resmsdt', jsonarr);
                }
            }
        }
    })

    socket.on('getmsact', data => {
        //  console.log(data);
        process();
        async function process() {
            //for(var i = 0; i < data.length; i++) {  
            var agv_ID = data[0].agv_ID;
            var ms_ID = data[0].ms_ID;
            var activity = [];
            var json = {};
            var jsonMap2 = {};
            execId = new Date().valueOf();
            var fx = await findin_msdtact(data[0].ms_ID);
            var fxx = sortobj(fx);
            if (fxx[0].MAP_ID != undefined) {
                var fy = await findin_mapdt(fxx[0].MAP_ID);
            }
            var map = fy.Name;
            var Home_x = fy.Home_x;
            var Home_y = fy.Home_y;
            var Home_z = fy.Home_z;
            var Home_w = fy.Home_w;
            jsonMap2 = { map: map, Home_x: Home_x, Home_y: Home_y, Home_z: Home_z, Home_w: Home_w };
            var junc = await findin_mapJunc(fxx[0].MAP_ID);
            var junct = [];
            for (var looper = 0; looper < junc.length; looper++) {
                var jun = { "x": junc[looper].x, "y": junc[looper].y };
                junct.push(jun);
            }
            for (var j = 0; j < fxx.length; j++) {
                var fz = await findin_mapdtpt(fxx[j].PT_ID);
                var pt = fz["Name"];
                var seq = fxx[j].Seq;
                var locx = fz["Loc_x"];
                var locy = fz["Loc_y"];
                var locz = fz["Loc_z"];
                var locw = fz["Loc_w"];

                var fw = await findin_arm_dt(fxx[j].ARM_ID);
                var arm = fw.Cmd;
                var jsonMap = { Seq: seq, pt: pt, x: locx, y: locy, z: locz, w: locw, arm: arm }
                activity.push(jsonMap);
                json = { agv_ID: agv_ID, ms_ID: ms_ID, sched_ID: sched_myid, ss_id: execId, fname: jsonMap2, junc: junct, activity: activity }
                if (j == fxx.length - 1) {
                    socket.emit('getmsactS', json);
                    socket.emit('reqActList', json.activity);
                }
            }
            //}  
        }
    })

    socket.on('misRec', data => {
        saveDBMsRec(data);
    })

    //setInterval(structureSchema, 3000);

    socket.on('misStat', function (data) {
        //console.log(data);
        //initialize back to zero
        var statusStorage;
        var se = {};
        var seq;
        var PTSchem = [];
        var raw = [];
        var ARMSchem = [];
        var TSstoreage = [];
        var statusStore = [];
        var mission = [];

        structureSchema();
        async function structureSchema() {
            try {
                var misData = await findin_latest(data.ssID);
                if (misData[0] == undefined) {
                    console.log("error");
                }
                else if (statusStorage != misData[0].status) {
                    statusStorage = misData[0].status;
                    id = misData[0].SS_ID;
                    seq = misData[0].ACTVT_SEQ;
                    var misSchedID = await findin_ms_SCHEDID(misData[0].MS_SCHED_ID);
                    var misSchedMSID = await findin_ms_SCHEDMSID(misSchedID[0]._id);
                    var ms_id = await findin_ms_dt(misSchedMSID[0].MS_ID);
                    var map_id = await findin_map_dt(ms_id[0].MAP_ID);
                    var ms_act_id = await findin_ms_act_id(ms_id[0]._id);
                    for (var i = 0; i < ms_act_id.length; i++) {
                        var map_dtPt = await findin_MS_pt(ms_act_id[i].PT_ID);
                        PTSchem.push(map_dtPt);
                        if (map_dtPt.Name != misData[0].ACTVT_PT) {
                            raw.push(map_dtPt);
                        }
                        var armDT = await findin_armDT(ms_act_id[i].ARM_ID);
                        ARMSchem.push(armDT);
                    }
                    for (var i = 0; i < misData.length; i++) {
                        TSstoreage.push(misData[i].TS);
                        statusStore.push(misData[i].status);
                    }
                    mission.push({ SEQ: seq, name: ms_id, action: ARMSchem, points: PTSchem, ts: TSstoreage, status: statusStore });
                    se = { agv_id: ms_id[0].agv_ID, schedule: misSchedID, mission: mission }

                    //  console.log(se);
                    if (se == undefined) {
                        console.log("no mission");
                        socket.emit('received', "null");
                    }
                    else if (data.agvID == se.agv_id) {
                        if (se.mission[0].status[se.mission[0].status.length - 1] != "done") {
                            socket.emit('received', se);
                        }
                    }
                }
            }
            catch (e) {
                //console.log(e);

                socket.emit('received', "null");
            }
        }


    })

    socket.on('saveSSID', data => {
        processSSIDsave();
        async function processSSIDsave() {
            var schedMSdt = await findin_schedMSdt(data.schedid);
            //   console.log(schedMSdt.length);
            for (var i = 0; i < schedMSdt.length; i++) {
                let docu = new SSID_DT;
                docu.SS_ID = data.ssid;
                docu.MS_ID = schedMSdt[i].MS_ID;
                docu.save((err, doc2) => {
                    if (err) {
                        console.log(err);
                    }
                    else if (doc2) {
                        //console.log(doc2);
                    }
                })
            }
        }
    })

    socket.on('delMis', data => {
        padamMis()
        async function padamMis() {
            var deleter = await findin_delMisSSID_DT(data.delMis, data.ssid);
            //   console.log(deleter+" is deleted");
        }
    })

    socket.on('noti_done', data => {

        SCHED_DT.findOne({ _id: data.sched_id, status: "recorded" }).then(doc => {
            if (doc) {

                var sched_name = doc.Name;

                MS_DT.findOne({ _id: data.ms_id, status: "recorded" }).then(doc2 => {
                    if (doc2) {

                        var ms_name = doc2.Name;

                        let NOTI = new NOTIFY();

                        NOTI.AGV_ID = doc2.agv_ID;
                        NOTI.Type = "MS_DONE";
                        NOTI.Det = "Schedule : " + sched_name + ",Mission : " + ms_name;
                        NOTI.TS = new Date();
                        NOTI.status = "recorded";

                        NOTI.save((err, doc) => {
                            if (err) {
                                console.log(err);
                            } else if (doc) {
                                console.log("Schedule : " + sched_name + ",Mission : " + ms_name + ", Status : Completed");
                            }

                        });

                    }
                }).catch(err => {
                    console.log(err);
                });

            }
        }).catch(err => {
            console.log(err);
        })

    })



}
