const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const DIAG_DT = mongoose.model('DIAG_DT');
const AGV_DT = mongoose.model('AGV_DT');
const ACT_DTS = mongoose.model('ACT_DTS');

var info = [];
const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
	//	console.log(req.user)
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        return res.redirect('./error')  	
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                {
                    if(req.path == '/'){
                        return res.redirect('./error')  	
                        }
                        else{
                        console.log('nothing')
                        }
                                
                }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Password:pass,Role:role,Username:username})
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

        return next()   

	}
	else 
	{	
	res.redirect('/login')
	}
}


router.get('/',loggedin,(req,res) => {

    DIAG_DT.find().limit(10).sort({TS:-1}).exec((err,doc) => {
        if(err) {
            console.log(err);
        }
        else{
            AGV_DT.find({status: 'recorded'},(err,doc1) => {
                if(err) {
                    console.log(err);
                }
                else {
                    //console.log(doc[0].TS);
                    res.render('diagnostic/index', {
                        title : "DIAGNOSTIC",
                        diag_dt: doc,
                        agv_dt : doc1,
                        agv : doc1,
                        name : info[0].Name,
                        userid: info[0].userId,
                        role: info[0].Role,
                    });
                }
            })
        }
    })

})

module.exports = router;

module.exports.respond = socket => {

//-------------------- SOCKET IO FUNCTIONS ---------------------//

    socket.on('reqDiag',data => {
        req_diag_routine(data);
    })

//-------------------- ROUTINE FUNCTIONS ---------------------//


    req_diag_routine = (data) => {
        // console.log(data)
        DIAG_DT.find({agv_ID:data.agv_ID}).limit(1).sort({TS:-1}).exec((err,doc) => {
            if(err) {
                console.log(err);
            }
            else {
            // console.log(doc)
                socket.emit('resDiag',doc);

                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Request diagnostic record for "'+ data.agv_ID +'"';
                ACT.Type = "DIAGNOSTIC PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err,docAT4) => {
                    if(err){
                       console.log(err);
                    } else {
                       console.log('request diagnostic information done');
                    }
            })
            }
        });   
    }

};


