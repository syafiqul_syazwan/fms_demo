const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('USR_DT');
const AGV_DT = mongoose.model('AGV_DT');
const SCHED_DT = mongoose.model('SCHED_DT');
const ACT_DTS = mongoose.model('ACT_DTS');
const NOTIFY = mongoose.model('NOTIFY');
const PASSWORD_DT = mongoose.model('PASSWORD_DT');
const PWD_DT = mongoose.model('PWD_DT');
/*
AJAX Region
*/
var ajax = express();
ajax.use(express.static('public'));

/*
AJAX ends*/

const userC = require('./user_controller');
var UserController = express();
UserController.use('/userSetting',router);

//password History related
//the below constant(s) should be used ONLY for pasword record related

//this number represents the number of recent passwords to be check for duplication, regardless of time
const PASSWORD_HISTORY_NUMBER = 3;

//end of password History

var noti;
var count;

var info = [];
const loggedin = (req, res, next) => {
    if (req.isAuthenticated()) {
        //console.log(req.user)
        if (req.user.role == 'OBSERVER') {
            if (req.path == '/') {
                return res.redirect('./error')
            }
            else {
                console.log('nothing')
            }
        }
        else if (req.user.role == 'OPERATOR') {
            if (req.path == '/') {
                return res.redirect('./error')
            }
            else {
                console.log('nothing')
            }

        }
        else if (req.user.role == 'ADMIN') {
            if (req.path == '/') {
                info = [];
                var id = req.user.id;
                var name = req.user.name
                var role = req.user.role
                var pass = req.user.password
                var username = req.user.username
                info.push({ userId: id, Name: name, Role: role, Password: pass, Username: username })

            }
            else {
                console.log('nothing')
            }

        }
        else {

            return res.redirect('./error')

        }

        return next()

    }
    else {
        res.redirect('/login')
    }
}


router.get('/', loggedin, (req, res) => {

    //---------------- NOTIFICATION ------------------//

    NOTIFY.find({}, null, { sort: { TS: -1 } }, (err, doc) => {
        if (err)
            console.log(err);
        else {

            noti = doc;
            count = doc.length;

            User.find({ status: "recorded" }, (err, doc) => {
                if (err)
                    console.log(err);
                else {
                    AGV_DT.find({ status: "recorded" }, (err, doc1) => {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            for (var x = 0; x < doc.length; x++)
                                doc[x].loginid = info[0].userId;
                            res.render('userSetting/index', {
                                title: "USER SETTING",
                                user: doc,
                                agv: doc1,
                                name: info[0].Name,
                                userid: info[0].userId,
                                role: info[0].Role,
                                pass: info[0].Password,
                                username: info[0].Username,
                            });
                        }
                    })
                }
            })

        }
    })

});

module.exports = router;

module.exports.respond = socket => {

    socket.on('register', data => {


        var record = new User();
        // console.log(data)
        record.Name = data.Name;
        record.Username = data.Username;
        record.Pass = record.hashPassword(data.Pass);
        record.Type = data.Type;
        record.TS = new Date();
        record.Login_Attempt = "0";
        record.Login_Status = "NORMAL";
        record.status = "recorded";
        record.Reset_Status = "NORMAL";

        // console.log(record + 'record')
        record.save((err, doc) => {
            if (err) {
                console.log(err)
                socket.emit('userASave', 'errCantAdd');
            }
            else {
                var PWD = new PWD_DT();
                try {
                    PWD.ID = doc._id;
                    PWD.Password = doc.Pass;
                    PWD.Password_Date = doc.TS;

                    PWD.save((err, savedpassword) => {
                        if (!err) {

                        }
                        else
                            console.log("PWD failed to save");
                    });
                }
                catch (tryErr) {
                    console.log("Failed to save password into PWD-DT, error as below");
                    console.log(tryErr);
                }
                socket.emit('userASave', data.Username);
                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = 'New User : "' + data.Username + '" has been added';
                ACT.Type = "USER SETTING PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log("err");
                    } else {

                        var PASS = new PASSWORD_DT();
                        PASS.UserName = data.Username;
                        PASS.Password = record.hashPassword(data.Pass);
                        PASS.Stat_Pass = "NOT-EXPIRED";
                        PASS.TS = new Date();
                        PASS.status = "recorded";

                        PASS.save((err, docPass) => {
                            if (err) {
                            } else {
                            }
                        })
                    }
                })

            }
        });
    })
    socket.on('checkUN', data => {
        User.findOne({ Username: data, status:'recorded' }, (err, doc) => {
            if (!doc) {
                socket.emit('checkUNS', 'false')
            } else {
                socket.emit('checkUNS', 'true')
            }
        })

    });

    socket.on('reqEUsr', data => {
        User.findOne({ _id: data._id }, (err, doc) => {
            if (err) {
                console.log(err);
            }
            else {
                //console.log(doc);
                socket.emit('resEUsr', doc);
            }
        })
    })

    socket.on('updateUSR', (data) => {
        // console.log(data)
        User.findOne({ Username: data.Username }, (err, doc) => {
            if (err) {
                socket.emit('updateUsrs', 'err');
            } else {
                User.findOneAndUpdate({ _id: data._id }, { $set: { Name: data.Name, Type: data.Type, TS: new Date() } }, (err, doc4) => {
                    if (!doc4) {
                        socket.emit('updateUsrs', 'err');
                    }
                    else {
                        socket.emit('updateUsrs', doc4);
                        var ACT = new ACT_DTS();
                        ACT.User_Name = info[0].Username;
                        ACT.Activity = 'User : "' + data.Username + '" updated successfully';
                        ACT.Type = "USER SETTING PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";
                        ACT.save((err, docAT4) => {
                            if (err) {
                                console.log(err);
                            } else {

                            }
                        })
                    }
                })
            }
        })
    });

    socket.on('deleteUser', data => {
        var interval = 1 * 10; // 10 seconds;
        console.log("Id is " + data._id);
        User.find({ _id: data._id }, (err, doc7) => {
            if (err) {

            } else {
                for (var i = 0; i < doc7.length; i++) {
                    setTimeout(function (i) {
                        SCHED_DT.findOne({ USR_ID: doc7[i]._id }, (err, doc8) => {
                            if (!doc8) {
                                console.log('cannot find sched')
                            } else {
                                var ACT = new ACT_DTS();

                                ACT.User_Name = info[0].Username;
                                ACT.Activity = 'Schedule : "' + doc8._id + '" has been deleted';
                                ACT.Type = "USER SETTING PAGE";
                                ACT.TS = new Date();
                                ACT.status = "recorded";

                                ACT.save((err, docAT4) => {
                                    if (err) {
                                        //throw err;
                                        console.log(err);
                                    } else {
                                        // console.log("New User : "+ doc9._id);
                                        //socket.emit('deleteAGVs',doc);
                                    }
                                })
                                //console.log('sched_dt successfully deleted')
                                for (var j = 0; j < doc8.length; j++) {
                                    setTimeout(function (j) {
                                        SCHED_DT_MS.findOneAndUpdate({ SCHED_ID: doc7[j]._id }, { $set: { status: "deleted" } }, (err, doc9) => {
                                            if (!doc9) {
                                                console.log('Sched DT MS null')
                                            } else {
                                                console.log('sched_dt_ms successfully deleted')

                                            }
                                        })
                                    }, interval * j, j);
                                }

                            }
                        })

                    }, interval * i, i);
                }
                for (var z = 0; z < doc7.length; z++) {
                    setTimeout(function (z) {
                        SCHED_DT.findOneAndUpdate({ SCHED_ID: doc7[z]._id }, { $set: { status: "deleted" } }, (err, doc11) => {
                            if (!doc11) {
                                console.log('Sched null')
                            } else {
                                console.log('sched_dt successfully deleted')


                            }
                        })
                    }, interval * z, z);
                }

                User.findOneAndUpdate({ _id: data._id }, { $set: { status: "deleted" } }, (err, doc) => {
                    if (err) {
                        console.log(err)
                    }
                    else {

                        console.log('deleted successfully');
                        socket.emit('deleteUsers', doc);


                        var ACT = new ACT_DTS();
                        ACT.User_Name = info[0].Username;
                        ACT.Activity = 'User : "' + doc.Username + '" has been deleted';
                        ACT.Type = "USER SETTING PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";

                        ACT.save((err, docAT4) => {
                            if (err) {
                                //throw err;
                                console.log(err);
                            } else {
                                //   console.log("New User : "+ doc._id);
                                //socket.emit('deleteAGVs',doc);
                            }
                        })
                    }
                })
            }

        })

    })
    socket.on('unblockUser', user => {
        if (!user || user === "undefined") {
            socket.emit('conveyance', 'unblock attempt failed, user id missing');
            socket.emit('unblockUserS', 'error');
            return;
        }
        // db = User();
        User.findOneAndUpdate({ _id: user._id }, { $set: { Login_Attempt: 0, Login_Status: 'NORMAL' } }, (err, doc) => {
            if (err) {
                socket.emit('unblockUserS', 'error');
            }
            else {

                socket.emit('unblockUserS', user);
                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Admin Unblocked User. User ID : "' + user._id + '"';
                ACT.Type = "USER SETTING PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";
                ACT.save((err, docAT4) => {
                    if (err) {
                        //throw err;
                        console.log(err);
                    } else {
                        User.find({ _id: user._id }, { _id: 0, Username: 1 }).exec((err, selecteduser) => {
                            if (err) {

                            }
                            else {
                                var name = "";

                                try {
                                    name = selecteduser[0].Username;
                                }
                                catch (err) {
                                    socket.emit('conveyance', 'Error parsing string ' + err);
                                }
                                var ACT1 = new ACT_DTS();
                                ACT1.User_Name = name;
                                //  socket.emit('conveyance', 'Show User Name on ACT motion ' + ACT1.User_Name);
                                ACT1.Activity = 'User has been unblocked by ' + info[0].Username;
                                ACT1.Type = "USER SETTING PAGE";
                                ACT1.TS = new Date();
                                socket.emit('conveyancetest', 'Second Date was: ' + ACT1.TS);
                                ACT1.status = "recorded";
                                if (ACT1.User_Name != 'undefined' && ACT1.User_Name && ACT1.User_Name != null) {
                                    ACT1.save((err, actSaved) => {
                                        if (err) {
                                            socket.emit('conveyance', 'Jason Test Case X : ERROR');
                                            //`   
                                        }
                                        else {
                                            socket.emit('conveyance', 'Jason Test Case X : Success updating' + actSaved);
                                        }
                                    })
                                }
                                else {
                                    socket.emit('conveyance', 'name undefined');
                                }
                            }
                        })
                    }
                })
            }
        })
    })
    socket.on('resetPassword', datas => {
        var record = User();
        User.findOneAndUpdate({ _id: datas._id }, { $set: { Reset_Pwd: record.hashPassword(datas.AutoGen), Reset_Status: "RESET" } }, (err, doc) => {
            if (err) {
                console.log('error');
                socket.emit('resetPasswordS', 'error')
            } else {
                socket.emit('resetPasswordS', datas.AutoGen)

                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = 'Reset Password For User\'s Account that Blocked / Forgot Password';
                ACT.Type = "USER SETTING PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err, docAT4) => {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        //DO SOMETHING
                    }
                })
            }

        })

    })
    socket.on('hashedPass', data => {

        User.findOne({ _id: data.IDPass }, (err, doc) => {
            var stored_hash = doc.comparePassword(data.NewPassUsr, doc.Pass)
            if (stored_hash) {
                socket.emit('hashedPassS', stored_hash)
            }
            else {
                socket.emit('hashedPassS', stored_hash)
            }

        })
    })
    socket.on('UserUpdateSession', data=> {
        try {
            User.findOneAndUpdate({ _id: data }, { $set: { ACT_Time: new Date() } }, (err, doc) => {
                /*
                update user table's ACT_Time for activity recordings
                */

            })

        }
        catch (e) {
            console.log("Error on UserUpdateSession socket: " + e);
        }
    })

    socket.on('UserUpdateLogout', data=> {
        try {
            if (data) {

                User.findOneAndUpdate({ _id: data }, { $set: { Session: "LOGOUT", Last_LogOut_Time: new Date() } }, (err, doc) => {
                    /*
                    update user table's ACT_Time for activity recordings
                    */
                })

            }
            else
            {

            }
        }
        catch (e) {
            console.log("Error on UserUpdateSession socket: " + e);
        }
    })
    socket.on('toController', data=> {
        console.log("socket convey " + data);
    })

    socket.on('UserUpdatePassword', data => {
        var record = User();
        var flag = false;
        var actmsg = "";
        var isHistory = false;
        try {
            var userID = data.ID;
            PWD_DT.find({ ID: userID }, { _id: 0, Password: 1 }).sort({ Password_Date: -1 }).limit(PASSWORD_HISTORY_NUMBER).exec((err, pwdhistory) => {
                if (!err) {
                    // console.log("Comparing with>>> " + data.NewPassword);

                    /*
                    >>Keyword: RESETPASSWORD, RESET PASSWORD, UPDATEPASSWORD, PASSWORD, UPDATE PASSWORD
                    >>ALL BACKEND for changing current PASSWROD to this, where needed to look into the password history before proceeding                
                    */
                    if (pwdhistory.length > 0) {

                        for (i = 0; i < pwdhistory.length; i++) {
                            if (record.comparePassword(data.NewPassword, pwdhistory[i].Password)) {
                                isHistory = true;
                                socket.emit('UserUpdatePasswordReturn', 'Duplication');
                                break;
                                //>>break once a single duplication is checked on, loop goes to CHECKPOINT 5.5 if it is duplicated
                            }
                        }
                    }
                    /*<<<<CHECKPOINT_5.5>>>>
                    >>if the isHistory Flag returns false:
                    >>the new passowrd does not repeat to the latest 3 passwords
                    >>Record the activity after this
                    >>Set user back to Logout to allow re-login
                    */
                    var UserName = "UNKNOWN 1345674536442342465352643";
                    if (!isHistory) {
                        var PWD = new PWD_DT();
                        if (data.Reset == "Yes") {
                            User.findOneAndUpdate({ _id: data.ID }, { $set: { Pass: record.hashPassword(data.NewPassword), Reset_Pwd: "", Reset_Status: "NORMAL" } }, (err, doc) => {
                                if (!doc) {
                                    socket.emit('UserUpdatePasswordReturn', 'failed to update password');
                                } else {
                                    socket.emit('UserUpdatePasswordReturn', 'done');
                                    UserName = doc.Username;
                                    PWD.ID = doc._id;
                                    PWD.Password = doc.Pass;
                                    PWD.Password_Date = new Date();
                                    PWD.save((err, savedpassword) => {
                                        if (!err) {

                                        }
                                        else
                                            console.log("PWD failed to save");
                                    });
                                    flag = true;

                                    User.findOneAndUpdate({ _id: doc._id }, { $set: { Session: "LOGOUT", Last_LogOut_Time: new Date() } }, (err, success) => {
                                        if (err) {
                                            console.log("Failed to update user session");
                                        }
                                    })
                                }

                                actmsg = flag ? "Success: Unreset and Change Password for this user " : "Failed: Unreset and Change Password for this user";
                            })
                        }
                        else if (data.Reset == "No") {
                            User.findOneAndUpdate({ _id: data.ID }, { $set: { Pass: record.hashPassword(data.NewPassword) } }, (err, doc) => {
                                if (!doc) {
                                    console.log('null');
                                } else {
                                    socket.emit('UserUpdatePasswordReturn', 'done');

                                    User.findOneAndUpdate({ _id: doc._id }, { $set: { Session: "LOGOUT", Last_LogOut_Time: new Date() } }, (err, success) => {
                                        if (err) {
                                            console.log("Failed to update user session");
                                        }
                                        else {
                                            console.log("Successfully refreshed session");
                                        }
                                    })

                                    UserName = doc.Username;
                                    PWD.ID = doc._id;
                                    PWD.Password = doc.Pass;
                                    PWD.Password_Date = new Date();
                                    PWD.save((err, savedpassword) => {
                                        if (!err) {

                                        }
                                        else
                                            console.log("PWD failed to save");
                                    });
                                    flag = true;
                                }
                                actmsg = flag ? "Success: Change New Password for this user " : "Failed: Change New Password for this user";
                            })
                        }
                        var ACT = new ACT_DTS();
                        ACT.User_Name = UserName;
                        ACT.Activity = actmsg != "" ? actmsg : "MESSAGE LOST IN RESET PASS SOCKET";
                        ACT.Type = data.Reset == "Yes" ? "LOGIN PAGE" : "USER SETTING PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";
                        ACT.save((err, docAT4) => {
                            if (err) {
                                socket.emit('conveyance', "Error at saving Activity");
                            }
                            else {
                                socket.emit('conveyance', 'successfully saved Activity');
                            }
                        })
                    }

                }
                else {
                    console.log("Error occured");
                    console.log(err);
                    socket.emit('conveyance', error);
                    socket.emit('UserUpdatePasswordReturn', 'failed');
                }
            });
        }
        catch (error) {
            console.log("Error " + error);
            socket.emit('conveyance', error);
        }
    })



}
