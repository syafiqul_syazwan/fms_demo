const express = require('express');
var router = express.Router();

var info = []
const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
		//console.log(req.user)
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        info.push({userId:id,Name:name,Role:role,Password:pass})
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        info.push({userId:id,Name:name,Role:role,Password:pass})
                        }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        info.push({userId:id,Name:name,Role:role,Password:pass})
                      
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}

router.get('/',loggedin,(req,res) => {

    res.render('error/index', {
        title : "Error",
        name : info[0].Name,
        userid: info[0].userId,
        role: info[0].Role,
        pass: info[0].Password,  
    });

});


module.exports = router;

