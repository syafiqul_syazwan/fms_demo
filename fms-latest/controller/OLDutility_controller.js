const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const AGV_DT = mongoose.model('AGV_DT');
const USER_DT = mongoose.model('USR_DT');
const DIAG_DT = mongoose.model('DIAG_DT');
const HEALTH_DT = mongoose.model('HEALTH_DT');
const SCHED_DT = mongoose.model('SCHED_DT');
const SCHED_DT_MS = mongoose.model('SCHED_DT_MS');
const MS_DT = mongoose.model('MS_DT');
const MS_DT_ACTIVITY = mongoose.model('MS_DT_ACTIVITY');
const SOS_DT = mongoose.model('SOS_DT');
const SOS_DT_TYPE = mongoose.model('SOS_DT_TYPE');
const SOS_DT_RES = mongoose.model('SOS_DT_RES');
const SOS_DT_REC = mongoose.model('SOS_DT_REC');
const ACT_DTS = mongoose.model('ACT_DTS');
const UPD_DT = mongoose.model('UPD_DT');
const CAM_DT = mongoose.model('CAM_DT');
const MS_DT_REC = mongoose.model('MS_DT_REC');

const ARM_DT = mongoose.model('ARM_DT');
const MAP_DT = mongoose.model('MAP_DT');
const MAP_DT_PT = mongoose.model('MAP_DT_PT');
const MAP_DT_JUNC = mongoose.model('MAP_DT_JUNC');
var NOTIFY = mongoose.model('NOTIFY');
var NOTI_SEEN = mongoose.model('NOTI_SEEN');


var info = [];

//--------------------PERMISSION LOGIN ACCESS FUNCTIONS---------------------//

const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
		//console.log(req.user)
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        return res.redirect('./error')  	
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                            return res.redirect('./error')
                        }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}



//---------------------ROUTER FUNTIONS---------------------------//

router.get('/',loggedin,(req,res) => {

    AGV_DT.find({status: 'recorded'},(err,doc2) => {
        if(err){
            console.log(err);
        }
        else{
            USER_DT.find({status: 'recorded'},(err,doc3)=>{
                if(err){
                    console.log(err);
                }
                else{
                    SOS_DT_RES.find({status: 'recorded'}).sort({TS:-1}).exec((err, doc4) => {
                        if(err){
                            console.log(err);
                        }
                        else{
                            SOS_DT_TYPE.find({status: 'recorded'}).sort({TS:-1}).exec((err, doc5) => {
                                if(err){
                                    console.log(err);
                                }
                                else{
                                    ACT_DTS.find({status:'recorded'}).limit(100).sort({TS:-1}).exec((err, doc6) => {
                                        if(err){
                                            console.log(err);
                                        }
                                            else{
                                                UPD_DT.find({status:'recorded'}).limit(100).sort({TS:-1}).exec((err, doc7) => {
                                                    if(err){
                                                        console.log(err);
                                                    }
                                                        else{
                                                            SOS_DT.find({status:'recorded'}).limit(100).sort({TS:-1}).exec((err, doc8) => {
                                                                if(err){
                                                                    console.log(err);
                                                                }
                                                                    else{
                                                                        ARM_DT.find({status:'recorded'}).limit(100).sort({TS:-1}).exec((err, doc9) => {
                                                                            if(err){
                                                                                console.log(err);
                                                                               }
                                                                                else{
                                                                                    MAP_DT_JUNC.find({status: 'recorded'},(err,doc10) => {
                                                                                        if(err)
                                                                                        console.log(err)
                                                                                        else{
                                                                                            MAP_DT.find({status: 'recorded'},(err,doc11) => {
                                                                                                    if(err)
                                                                                                    console.log(err)
                                                                                                    else{
                                                                                                        MAP_DT_PT.find({status: 'recorded'},(err,doc12) => {
                                                                                                                if(err)
                                                                                                                console.log(err)
                                                                                                                else{
                                                                                  
                                                                                                        res.render('utility/index', {
                                                                                                            title : "UTILITY",
                                                                                                            agv :  doc2,
                                                                                                            usr : doc3, 
                                                                                                            name : info[0].Name,
                                                                                                            //name : info[0].Name,
                                                                                                            userid: info[0].userId,
                                                                                                            role: info[0].Role,
                                                                                                            pass: info[0].Password,
                                                                                                            map: doc11,
                                                                                                            mapJunc:doc10,
                                                                                                            mapPt: doc12,
                                                                                                            res:doc4,
                                                                                                            type:doc5,
                                                                                                            act_list:doc6,
                                                                                                            soft_list:doc7,
                                                                                                            sos_list:doc8,
                                                                                                            arm_list:doc9
                                                                                                        })
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                  }
                                                                            })
                                                                        }
                                                                   })
                                                               }
                                                          })
                                                      }
                                                 })
                                             }
                                        })
                                   }
                            })
                        }
                  })
              }
        })

})

module.exports = router

module.exports.respond = socket => {


//--------------SOCKET IO FUNTIONS--------------------//

//---Request Audit Trail List for User----(S1)

socket.on('AuditTRailUsrReq',data => {
    request_audit_trail_by_user_list_routine(data);   
})

//---Request Current Record Trash List----(S2)

socket.on('currentList',data =>{
    request_current_trash_record_routine(data);
})

//---Request Archive Record Trash List----(S3)

socket.on('archieveList',data =>{      
    request_archive_trash_record_list_routine(data);
})

//---Delete All Archive and Current Record Trash Function----(S4)

socket.on('ClearAllDataTrash', data =>{     
    delete_all_trash_for_current_and_archive_routine(data);
})

//---Delete One Archive and Current Record Trash Function----(S5)

socket.on('ClearRecDataTrash', data =>{
   delete_one_trash_for_current_and_archive_routine(data);
})

//---Add New Sos Type---(S6)

socket.on('SOSTypeAdd',data => {
  add_new_sos_type_routine(data);
})

//---Add New Sos Response---(S7)

socket.on('SOSResAdd',data => {
    add_new_sos_response_routine(data);
})

//----Delete Sos Type--------(S8)

socket.on('deleteSOSTypeReq',data => {     
    delete_sos_type_routine(data);
})

//----Delete Sos Response----(S9)

socket.on('deleteSOSResponseReq',data => {
    delete_sos_response_routine(data);
})

//------Edit Sos Type------(S10)

socket.on('findSosIDType',(data) => {
    request_data_sos_type_routine(data);
})

socket.on('findSosIDTypeRes',data => {
    update_sos_type_routine(data);
            
})

//-----Edit Sos Response-----(S11)

socket.on('findSosIDRes',(data) => {
    request_data_sos_response_routine(data);
})

socket.on('findSosIDResponseRes',data => { 
    update_sos_response_routine(data);
})

//-----Add New Software Update-----(S12)

socket.on('add/soft', data => {
  add_new_software_update_routine(data);
})

//---MAP & TELEOP VARIABLE EDIT TAB-------(S13)

socket.on('reqAgvCamLink',data =>{
    request_data_cam_agv_routine(data);
})

//-----Sos Record Delete------(S14)

socket.on('deleteSos', data =>{
    delete_sos_record(data);
})

//-------Update Cam Link---------(S15)
socket.on('reqCamLink',data =>{
   request_cam_link_agv_routine(data);
})

socket.on('updateCamPt', data =>{ 
    update_cam_link_agv_routine(data);
})

//------Add New Arm Without Details Data------(S16)
socket.on('ValAarmX',data =>{
    add_new_arm_X_routine(data);
})

//------Add New Arm With Details Data------(S17)

socket.on('ValAarm',data =>{  
    add_new_arm_routine(data);
})

//----List our all data of arm value for edit----(S18)

socket.on('reqArmListCmd',data =>{
   request_list_arm_data_for_edit_routine(data);
})

//-----Delete Arm Value------(S19)

socket.on('reqDelArmValD',data =>{
    delete_arm_routine(data);
})

//-----Request List Report For Batch Operation---(S20)

socket.on('reqReportsBatchOps',data =>{
    request_list_batchOps_routine(data);
})

//----Delete Software Update-----(S21)

socket.on('softDelProsReq',data =>{
    delete_software_update_routine(data); 
})

//----Request data filter by date Batch Operation---(S22)

socket.on('reqDF2',data =>{ 
    request_data_filter_batchOps_routine(data);
})

//----Request Map ID Data-----(S23)
socket.on('ReqMapRecbyID',data => {   
    request_mapDT_id_data_routine(data); 
})


socket.on('ebuttDelMapRecbyID',data => {
    request_map_list_data_routine(data);
})

//-----Delete mapDT record-----(S24)

socket.on('DelMapRecbyID',data => {
    delete_mapDT_record(data);
})

//----Add new map record--------(S25)
socket.on('AddMapRecbyID',data => {
  add_new_map_routine(data);
})

//-----Edit map record---------(S26)
socket.on('ReqeMVar',data =>{
  request_data_map_record_routine(data);
})

socket.on('UptMapRecbyID',data => {
    update_map_record_routine(data);
})

//-----Request all map Point and Junction Records-----(S27)
socket.on('ReqdetailsMap',data =>{ 
   request_map_pointAndjunc_routine(data);
})

//-------Add new Map Point-------(S28)

socket.on('addNewPointReq', data =>{
   add_new_mapPoint_routine(data); 
})

//--------Edit Map Point---------(S29)

socket.on('reqListMapPt',data =>{
   request_mapPoint_record_routine(data);
})

socket.on('editExistPointReq', data =>{
   update_mapPoint_record_routine(data);  
})

//---------Delete Map Point Record----------(S30)

socket.on('delAddMapPoint',data =>{
    delete_mapPoint_routine(data);  
})

//---------Add New Junction------------(S31)

socket.on('addNewJunctionReq',data =>{
    add_mapJunction_routine(data);
})

//-------Edit Map Junction----------------(S32)

socket.on('reqListMapJunct',data =>{
    request_mapJunct_record_routine(data);
})

socket.on('editExistJunctReq',data =>{
    update_map_junction_routine(data); 
})

//--------Delete map junction--------------(S33)

socket.on('delAddMapJunct',data =>{
    delete_map_junction_routine(data);  
})

socket.on('reqDFaud',data =>{
    request_filter_date_audit_trail_routine(data);
})

//--------Edit Sos Rec-----------------(S34) 
socket.on('editRecSosSave',data =>{
    update_sos_record_routine(data);
})

update_sos_record_routine = (data) => {
     SOS_DT.findOneAndUpdate({_id:data.sos_id},{$set:{Name:data.sos_type,Res:data.sos_response}},(err,doc) => {
            if(err){
                socket.emit('editRecSosSaveS','err');
            }else{
                socket.emit('editRecSosSaveS','save');
            }
     })
}

//---------------ROUTINE FUNCTIONS-------------------//

request_audit_trail_by_user_list_routine = (data) => {
    USER_DT.findOne({_id:data.ID}).then(doc => {
        if(!doc){
            console.log('null')
        }else{
            ACT_DTS.find({User_Name:doc.Username,status:'recorded'}).sort({TS:-1}).then(doc1 =>{
                if(!doc1){
                    console.log('null')
                }else{
                        socket.emit('AuditTRailUsrRes',doc1);

                        var ACT = new ACT_DTS();
                        ACT.User_id = info[0].userId;
                        ACT.User_Name = info[0].Username;
                        ACT.Activity = 'Request Audit Trail Record for : "'+ doc.Name + '"';
                        ACT.Type = "UTILITY PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";

                        ACT.save((err,docAT4) => {
                            if(err)
                            {
                                console.log(err);
                            }
                             else {
                               // console.log('Request Audit Trail Record for : "'+ doc.Name + '"');
                            }
                        })
                     }
                })
            }
       })

};

request_current_trash_record_routine = (data) =>{
    if(data=="AGV_DT"){
        AGV_DT.find({status:"recorded"}).sort({TS:-1}).exec((err,doc1) => {
            if(err){
                console.log('null')
            }else{
                //console.log(doc1)
                socket.emit('agvListTrash',doc1)
            }
        })
    }
    else if(data=="USR_DT"){
        USER_DT.find({status:"recorded"}).sort({TS:-1}).exec((err,doc2) => {
            if(err){
                console.log('null')
            }else{
                //console.log(doc2)
                socket.emit('userListTrash',doc2)
            }
        })
    }
    else if(data=="DIAG_DT"){
        DIAG_DT.find({status:"recorded"}).sort({TS:-1}).exec((err,doc3) => {
            if(err){
                console.log('null')
            }else{
                socket.emit('diagListTrash',doc3)
            }
        })
    }
    else if(data=="HEALTH_DT"){
        HEALTH_DT.find({status:"recorded"}).sort({TS:-1}).exec((err,doc4) => {
            if(err){
                console.log('null')
            }else{
                socket.emit('healthListTrash',doc4)
            }
        })
    }
    else if(data=="SCHED_DT"){
        SCHED_DT.find({status:"recorded"}).sort({TS:-1}).exec((err,doc5) => {
            if(err){
                console.log('null')
            }else{
                socket.emit('schedListTrash',doc5)
            }
        })
    }
    else if(data=="SCHED_DT_MS"){
        SCHED_DT_MS.find({status:"recorded"}).sort({TS:-1}).exec((err,doc6) => {
            if(err){
                console.log('null')
            }else{
                socket.emit('schedmsListTrash',doc6)
            }
        })
    }
    else if(data=="MS_DT"){
        MS_DT.find({status:"recorded"}).sort({TS:-1}).exec((err,doc7) => {
            if(err){
                console.log('null')
            }else{
                socket.emit('msListTrash',doc7)
            }
        })
    }
    else if(data=="MS_DT_ACTIVITY"){
        MS_DT_ACTIVITY.find({status:"recorded"}).sort({TS:-1}).exec((err,doc8) => {
            if(err){
                console.log('null')
            }else{
                socket.emit('msActListTrash',doc8)
            }
        })   
    }
    else if(data=="SOS_DT"){
       SOS_DT.find({status:"recorded"}).sort({TS:-1}).exec((err,doc9) => {
            if(err){
                console.log('null')
            }else{
                socket.emit('sosListTrash',doc9)
            }
        })
    }
    else if(data=="SOS_DT_REC"){
        SOS_DT_REC.find({status:"recorded"}).sort({TS:-1}).exec((err,doc10) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('sosRecListTrash',doc10);
           }

         })
    }
    else if(data=="SOS_DT_RES"){
        SOS_DT_RES.find({status:"recorded"}).sort({TS:-1}).exec((err,doc11) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('sosResListTrash',doc11);
           }

         })
    }
    else if(data=="SOS_DT_TYPE"){
        SOS_DT_TYPE.find({status:"recorded"}).sort({TS:-1}).exec((err,doc12) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('sosTypeListTrash',doc12);
           }

         })
    }
    else if(data=="UPD_DT"){
        UPD_DT.find({status:"recorded"}).sort({TS:-1}).exec((err,doc13) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('updListTrash',doc13);
           }

         })
    }else if(data=="ACT_DTS"){
        ACT_DTS.find({ $or: [ { status: "recorded" }, { status:"audited" } ] }).sort({TS:-1}).exec((err,doc14) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('actDtsListTrash',doc14);
           }

         })
    }else if(data=="NOTISEEN"){ 
        NOTI_SEEN.find({}).sort({TS:-1}).exec((err,doc15) => { 
            if(err){ 
                console.log('null') 
           }else{ 
                socket.emit('notiseenListTrash',doc15); 
                 
           } 

         }) 
    }else if(data=="NOTIFY"){ 
        NOTIFY.find({}).sort({TS:-1}).exec((err,doc16) => { 
            if(err){ 
                console.log('null') 
           }else{ 
                socket.emit('notifyListTrash',doc16); 
           } 

         }) 
    }else if(data=="ARM_DT"){ 
        ARM_DT.find({status:'recorded'}).sort({TS:-1}).exec((err,doc17) => { 
            if(err){ 
                console.log('null') 
           }else{ 
                socket.emit('armDtListTrash',doc17); 
           } 

         }) 
    }else if(data=="CAM_DT"){ 
        CAM_DT.find({status:'recorded'}).sort({TS:-1}).exec((err,doc18) => { 
            if(err){ 
                console.log('null') 
           }else{ 
                socket.emit('camListTrash',doc18); 
           } 

         }) 
    }else if(data=="MAP_DT"){ 
        MAP_DT.find({status:'recorded'}).sort({TS:-1}).exec((err,doc19) => { 
            if(err){ 
                console.log('null') 
           }else{ 
                socket.emit('mapDtListTrash',doc19); 
           } 

         }) 
    }else if(data=="MAP_DT_JUNC"){ 
        MAP_DT_JUNC.find({status:'recorded'}).sort({TS:-1}).exec((err,doc20) => { 
            if(err){ 
                console.log('null') 
           }else{ 
                socket.emit('mapDtJuncListTrash',doc20); 
           } 

         }) 
    }else if(data=="MAP_DT_PT"){ 
        MAP_DT_PT.find({status:'recorded'}).sort({TS:-1}).exec((err,doc21) => { 
            if(err){ 
                console.log('null') 
           }else{ 
                socket.emit('mapDtPtListTrash',doc21); 
           } 

         }) 
    }else if(data=="MS_DT_REC"){ 
        MS_DT_REC.find({}).sort({TS:-1}).exec((err,doc22) => { 
            if(err){ 
                console.log('null') 
           }else{ 
                socket.emit('msDtRecListTrash',doc22); 
           } 

         }) 
    }
    // else if(data=="SSID_DT"){ 
    //     SSID_DT.find({},(err,doc23) => { 
    //         if(err){ 
    //             console.log('null') 
    //        }else{ 
    //             socket.emit('ssidDtListTrash',doc23); 
    //        } 

    //      }) 
    // }
    else{
        console.log('database null/error')
    }

};

request_archive_trash_record_list_routine = (data) =>{
    if(data=="AGV_DT"){
        AGV_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc1) => {
           if(err){
                console.log('null')
           }else{
                socket.emit('agvListTrashArc',doc1);
           }
        })
    }
    else if(data=="USR_DT"){
        USER_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc2) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('userListTrashArc',doc2);
           }

         })
    }
    else if(data=="DIAG_DT"){
        DIAG_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc3) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('diagListTrashArc',doc3);
           }

         })
    }
    else if(data=="HEALTH_DT"){
        HEALTH_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc4) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('healthListTrashArc',doc4);
           }

         })
    }
    else if(data=="SCHED_DT"){
        SCHED_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc5) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('schedListTrashArc',doc5);
           }

         })
    }
    else if(data=="SCHED_DT_MS"){
        SCHED_DT_MS.find({status:"deleted"}).sort({TS:-1}).exec((err,doc6) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('schedMsListTrashArc',doc6);
           }

         })
    }
    else if(data=="MS_DT"){
        MS_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc7) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('msListTrashArc',doc7);
           }

         })
    }
    else if(data=="MS_DT_ACTIVITY"){
        MS_DT_ACTIVITY.find({status:"deleted"}).sort({TS:-1}).exec((err,doc8) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('msActListTrashArc',doc8);
           }

         })
    }
    else if(data=="SOS_DT"){
        SOS_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc9) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('sosListTrashArc',doc9);
           }

         })
    }
    else if(data=="SOS_DT_REC"){
        SOS_DT_REC.find({status:"deleted"}).sort({TS:-1}).exec((err,doc10) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('sosRecListTrashArc',doc10);
           }

         })
    }
    else if(data=="SOS_DT_RES"){
        SOS_DT_RES.find({status:"deleted"}).sort({TS:-1}).exec((err,doc11) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('sosResListTrashArc',doc11);
           }

         })
    }
    else if(data=="SOS_DT_TYPE"){
        SOS_DT_TYPE.find({status:"deleted"}).sort({TS:-1}).exec((err,doc12) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('sosTypeListTrashArc',doc12);
           }

         })
    }
    else if(data=="UPD_DT"){
        UPD_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc13) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('updListTrashArc',doc13);
           }

         })
    }else if(data=="ACT_DTS"){
        ACT_DTS.find({status:"deleted"}).sort({TS:-1}).exec((err,doc14) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('actDtsListTrashArc',doc14);
           }
         })
    }else if(data=="ARM_DT"){
        ARM_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc15) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('armDtListTrashArc',doc15);
           }
         })
    }else if(data=="CAM_DT"){
        CAM_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc16) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('camListTrashArc',doc16);
           }
         })
    }else if(data=="MAP_DT"){
        MAP_DT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc17) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('mapDtListTrashArc',doc17);
           }
         })
    }else if(data=="MAP_DT_JUNC"){
        MAP_DT_JUNC.find({status:"deleted"}).sort({TS:-1}).exec((err,doc18) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('mapDtJuncListTrashArc',doc18);
           }
         })
    }else if(data=="MAP_DT_PT"){
        MAP_DT_PT.find({status:"deleted"}).sort({TS:-1}).exec((err,doc19) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('mapDtPtListTrashArc',doc19);
           }
         })
    }else if(data=="MS_DT_REC"){
        MS_DT_REC.find({status:"deleted"}).sort({TS:-1}).exec((err,doc20) => {
            if(err){
                console.log('null')
           }else{
                socket.emit('mapDtPtListTrashArc',doc20);
           }
         })
    }
    // else if(data=="SSID_DT"){
    //     SSID_DT.find({status:"deleted"},(err,doc21) => {
    //         if(err){
    //             console.log('null')
    //        }else{
    //             socket.emit('ssidDtListTrashArc',doc21);
    //        }
    //      })
    // }
    // else if(data=="NOTISEEN"){
    //     NOTISEEN.find({status:"deleted"},(err,doc22) => {
    //         if(err){
    //             console.log('null')
    //        }else{
    //             socket.emit('notiseenListTrashArc',doc22);
    //        }
    //      })
    // }else if(data=="NOTIFY"){
    //     NOTIFY.find({status:"deleted"},(err,doc23) => {
    //         if(err){
    //             console.log('null')
    //        }else{
    //             socket.emit('notifyListTrashArc',doc23);
    //        }
    //      })
    // }
    else{
        console.log('database null/error')
    }
};

delete_all_trash_for_current_and_archive_routine = (data) =>{

    if(data.TrashID == 'current'){         //-----------------------------CURRENT-----------------------------------//

        if(data.DB == 'USR_DT'){
          USR_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc1)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'AGV_DT'){
          AGV_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc2)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'DIAG_DT'){
          DIAG_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc3)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'HEALTH_DT'){
          HEALTH_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc4)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'MS_DT'){
          MS_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc5)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'MS_DT_ACTIVITY'){
          MS_DT_ACTIVITY.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc6)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SCHED_DT'){
          SCHED_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc7)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SCHED_DT_MS'){
          SCHED_DT_MS.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc8)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SOS_DT'){
          SOS_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc9)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SOS_DT_REC'){
          SOS_DT_REC.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc10)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })  
        }else if(data.DB == 'SOS_DT_RES'){
          SOS_DT_RES.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc11)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SOS_DT_TYPE'){
          SOS_DT_TYPE.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc12)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
             
        }else if(data.DB == 'UPD_DT'){
            UPD_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc13)=>{
                  if(err){
                      socket.emit('ClearAllDataTrashSAll', 'err')
                      console.log('null')
                  }else{
                      socket.emit('ClearAllDataTrashSAll', data.DB)
                  }
            })
        }else if(data.DB == 'ACT_DTS'){
          ACT_DTS.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc14)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })  
        }else if(data.DB == 'NOTISEEN'){ 
          NOTI_SEEN.deleteMany({},(err,doc15)=>{ 
              if(err){ 
                  socket.emit('ClearAllDataTrashSAll', 'err') 
                  console.log('null') 
              }else{ 
                  socket.emit('ClearAllDataTrashSAll', data.DB) 
                  console.log(data.DB) 
              } 
        })   
        }else if(data.DB == 'NOTIFY'){ 
          NOTIFY.deleteMany({},(err,doc16)=>{ 
              if(err){ 
                  socket.emit('ClearAllDataTrashSAll', 'err') 
                  console.log('null') 
              }else{ 
                  socket.emit('ClearAllDataTrashSAll', data.DB) 
                  console.log(data.DB) 
              } 
        })   
        }else if(data.DB == 'ARM_DT'){ 
            ARM_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc17)=>{
                if(err){ 
                    socket.emit('ClearAllDataTrashSAll', 'err') 
                    console.log('null') 
                }else{ 
                    socket.emit('ClearAllDataTrashSAll', data.DB) 
                    console.log(data.DB) 
                } 
          })   
          }else if(data.DB == 'CAM_DT'){ 
            CAM_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc18)=>{
                if(err){ 
                    socket.emit('ClearAllDataTrashSAll', 'err') 
                    console.log('null') 
                }else{ 
                    socket.emit('ClearAllDataTrashSAll', data.DB) 
                    console.log(data.DB) 
                } 
          })   
          }else if(data.DB == 'MAP_DT'){ 
            MAP_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc19)=>{
                if(err){ 
                    socket.emit('ClearAllDataTrashSAll', 'err') 
                    console.log('null') 
                }else{ 
                    socket.emit('ClearAllDataTrashSAll', data.DB) 
                    console.log(data.DB) 
                } 
          })   
          }else if(data.DB == 'MAP_DT_JUNC'){ 
            MAP_DT_JUNC.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc20)=>{
                if(err){ 
                    socket.emit('ClearAllDataTrashSAll', 'err') 
                    console.log('null') 
                }else{ 
                    socket.emit('ClearAllDataTrashSAll', data.DB) 
                    console.log(data.DB) 
                } 
          })   
          }else if(data.DB == 'MAP_DT_PT'){ 
            MAP_DT_PT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc21)=>{
                if(err){ 
                    socket.emit('ClearAllDataTrashSAll', 'err') 
                    console.log('null') 
                }else{ 
                    socket.emit('ClearAllDataTrashSAll', data.DB) 
                    console.log(data.DB) 
                } 
          })   
          }else if(data.DB == 'MS_DT_REC'){ 
            MS_DT_REC.updateMany({},{"$set":{status: "deleted"}},(err,doc22)=>{
                if(err){ 
                    socket.emit('ClearAllDataTrashSAll', 'err') 
                    console.log('null') 
                }else{ 
                    socket.emit('ClearAllDataTrashSAll', data.DB) 
                    console.log(data.DB) 
                } 
          })   
          }
        //   else if(data.DB == 'SSID_DT'){ 
        //     SSID_DT.deleteMany({},(err,doc23)=>{ 
        //         if(err){ 
        //             socket.emit('ClearAllDataTrashS', 'err') 
        //             console.log('null') 
        //         }else{ 
        //             socket.emit('ClearAllDataTrashS', data.DB) 
        //             console.log(data.DB) 
        //         } 
        //   })   
        //   }
          else{
            console.log('database null')
        }
  
  }else if(data.TrashID == 'archieve'){                         //-----------------------------ACHIEVE-----------------------------------//
   
      if(data.DB == 'USR_DT'){
          USR_DT.deleteMany({status:"deleted"},(err,doc1)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'AGV_DT'){
          AGV_DT.deleteMany({status:"deleted"},(err,doc2)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'DIAG_DT'){
          DIAG_DT.deleteMany({status:"deleted"},(err,doc3)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'HEALTH_DT'){
          HEALTH_DT.deleteMany({status:"deleted"},(err,doc4)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'MS_DT'){
          MS_DT.deleteMany({status:"deleted"},(err,doc5)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'MS_DT_ACTIVITY'){
          MS_DT_ACTIVITY.deleteMany({status:"deleted"},(err,doc6)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SCHED_DT'){
          SCHED_DT.deleteMany({status:"deleted"},(err,doc7)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SCHED_DT_MS'){
          SCHED_DT_MS.deleteMany({status:"deleted"},(err,doc8)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SOS_DT'){
          SOS_DT.deleteMany({status:"deleted"},(err,doc9)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SOS_DT_REC'){
          SOS_DT_REC.deleteMany({status:"deleted"},(err,doc10)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })  
        }else if(data.DB == 'SOS_DT_RES'){
          SOS_DT_RES.deleteMany({status:"deleted"},(err,doc11)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
        }else if(data.DB == 'SOS_DT_TYPE'){
          SOS_DT_TYPE.deleteMany({status:"deleted"},(err,doc12)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })
             
        }else if(data.DB == 'UPD_DT'){
            UPD_DT.deleteMany({status:"deleted"},(err,doc13)=>{
                  if(err){
                      socket.emit('ClearAllDataTrashSAll', 'err')
                      console.log('null')
                  }else{
                      socket.emit('ClearAllDataTrashSAll', data.DB)
                  }
            })
        }else if(data.DB == 'ACT_DTS'){
          ACT_DTS.deleteMany({status:"deleted"},(err,doc14)=>{
              if(err){
                  socket.emit('ClearAllDataTrashSAll', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearAllDataTrashSAll', data.DB)
              }
        })  
        }else if(data.DB == 'ARM_DT'){
            ARM_DT.deleteMany({status:"deleted"},(err,doc15)=>{
                if(err){
                    socket.emit('ClearAllDataTrashSAll', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearAllDataTrashSAll', data.DB)
                }
          })  
          }else if(data.DB == 'CAM_DT'){
            CAM_DT.deleteMany({status:"deleted"},(err,doc16)=>{
                if(err){
                    socket.emit('ClearAllDataTrashSAll', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearAllDataTrashSAll', data.DB)
                }
          })  
          }else if(data.DB == 'MAP_DT'){
            MAP_DT.deleteMany({status:"deleted"},(err,doc17)=>{
                if(err){
                    socket.emit('ClearAllDataTrashSAll', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearAllDataTrashSAll', data.DB)
                }
          })  
          }else if(data.DB == 'MAP_DT_JUNC'){
            MAP_DT_JUNC.deleteMany({status:"deleted"},(err,doc18)=>{
                if(err){
                    socket.emit('ClearAllDataTrashSAll', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearAllDataTrashSAll', data.DB)
                }
          })  
          }else if(data.DB == 'MAP_DT_PT'){
            MAP_DT_PT.deleteMany({status:"deleted"},(err,doc19)=>{
                if(err){
                    socket.emit('ClearAllDataTrashSAll', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearAllDataTrashSAll', data.DB)
                }
          })  
          }else if(data.DB == 'MS_DT_REC'){
            MS_DT_REC.deleteMany({status:"deleted"},(err,doc20)=>{
                if(err){
                    socket.emit('ClearAllDataTrashSAll', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearAllDataTrashSAll', data.DB)
                }
          })  
          }
        //   else if(data.DB == 'SSID_DT'){
        //     SSID_DT.deleteMany({status:"deleted"},(err,doc21)=>{
        //         if(err){
        //             socket.emit('ClearAllDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearAllDataTrashS', data.DB)
        //         }
        //   })  
        //   }else if(data.DB == 'NOTISEEN'){
        //     NOTISEEN.deleteMany({status:"deleted"},(err,doc22)=>{
        //         if(err){
        //             socket.emit('ClearAllDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearAllDataTrashS', data.DB)
        //         }
        //   })  
        //   }else if(data.DB == 'NOTIFY'){
        //     NOTIFY.deleteMany({status:"deleted"},(err,doc23)=>{
        //         if(err){
        //             socket.emit('ClearAllDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearAllDataTrashS', data.DB)
        //         }
        //   })  
        //   }
          
          else{
            console.log('database null')
        }
  
  }else{
      
      socket.emit('ClearAllDataTrashSAll', 'err1')
  }


};

delete_one_trash_for_current_and_archive_routine = (data) =>{

    if(data.Rectrash == 'current'){               //-----------------------------CURRENT-----------------------------------//


        if(data.DBRec == 'USR_DT'){
          USR_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc1)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'AGV_DT'){
          AGV_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc2)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'DIAG_DT'){
          DIAG_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc3)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'HEALTH_DT'){
          HEALTH_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc4)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'MS_DT'){
          MS_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc5)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'MS_DT_ACTIVITY'){
          MS_DT_ACTIVITY.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc6)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SCHED_DT'){
          SCHED_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc7)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SCHED_DT_MS'){
          SCHED_DT_MS.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc8)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SOS_DT'){
          SOS_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc9)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SOS_DT_REC'){
          SOS_DT_REC.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc10)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })  
        }else if(data.DBRec == 'SOS_DT_RES'){
          SOS_DT_RES.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc11)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SOS_DT_TYPE'){
          SOS_DT_TYPE.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc12)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
             
        }else if(data.DBRec == 'UPD_DT'){
            UPD_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc13)=>{
                  if(err){
                      socket.emit('ClearRecDataTrashS', 'err')
                      console.log('null')
                  }else{
                      socket.emit('ClearRecDataTrashS', data.DBRec)
                  }
            })
        }else if(data.DBRec == 'ACT_DTS'){
          ACT_DTS.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc14)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })  
        }
        // else if(data.DBRec == 'NOTISEEN'){
        //     NOTISEEN.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc15)=>{
        //         if(err){
        //             socket.emit('ClearRecDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearRecDataTrashS', data.DBRec)
        //         }
        //   })  
        //   }else if(data.DBRec == 'NOTIFY'){
        //     NOTIFY.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc16)=>{
        //         if(err){
        //             socket.emit('ClearRecDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearRecDataTrashS', data.DBRec)
        //         }
        //   })  
        //   }
        else if(data.DBRec == 'NOTISEEN'){
            NOTISEEN.deleteOne({_id:data.RecordID},(err,doc15)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'NOTIFY'){
            NOTIFY.deleteOne({_id:data.RecordID},(err,doc16)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }
          else if(data.DBRec == 'ARM_DT'){
            ARM_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc17)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'CAM_DT'){
            CAM_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc18)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'MAP_DT'){
            MAP_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc19)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'MAP_DT_JUNC'){
            MAP_DT_JUNC.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc20)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'MAP_DT_PT'){
            MAP_DT_PT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc21)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'MS_DT_REC'){
            MS_DT_REC.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc22)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }
        //   else if(data.DBRec == 'SSID_DT'){
        //     SSID_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc23)=>{
        //         if(err){
        //             socket.emit('ClearRecDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearRecDataTrashS', data.DBRec)
        //         }
        //   })  
        //   }
          else{
            console.log('database null')
        }
      
      }else if(data.Rectrash == 'archieve'){                         //-----------------------------ACHIEVE-----------------------------------//
      
      if(data.DBRec == 'USR_DT'){
          USR_DT.deleteOne({_id:data.RecordID},(err,doc1)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'AGV_DT'){
          AGV_DT.deleteOne({_id:data.RecordID},(err,doc2)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'DIAG_DT'){
          DIAG_DT.deleteOne({_id:data.RecordID},(err,doc3)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'HEALTH_DT'){
          HEALTH_DT.deleteOne({_id:data.RecordID},(err,doc4)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'MS_DT'){
          MS_DT.deleteOne({_id:data.RecordID},(err,doc5)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'MS_DT_ACTIVITY'){
          MS_DT_ACTIVITY.deleteOne({_id:data.RecordID},(err,doc6)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SCHED_DT'){
          SCHED_DT.deleteOne({_id:data.RecordID},(err,doc7)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SCHED_DT_MS'){
          SCHED_DT_MS.deleteOne({_id:data.RecordID},(err,doc8)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SOS_DT'){
          SOS_DT.deleteOne({_id:data.RecordID},(err,doc9)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SOS_DT_REC'){
          SOS_DT_REC.deleteOne({_id:data.RecordID},(err,doc10)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })  
        }else if(data.DBRec == 'SOS_DT_RES'){
          SOS_DT_RES.deleteOne({_id:data.RecordID},(err,doc11)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
        }else if(data.DBRec == 'SOS_DT_TYPE'){
          SOS_DT_TYPE.deleteOne({_id:data.RecordID},(err,doc12)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })
             
        }else if(data.DBRec == 'UPD_DT'){
            UPD_DT.deleteOne({_id:data.RecordID},(err,doc13)=>{
                  if(err){
                      socket.emit('ClearRecDataTrashS', 'err')
                      console.log('null')
                  }else{
                      socket.emit('ClearRecDataTrashS', data.DBRec)
                  }
            })
        }else if(data.DBRec == 'ACT_DTS'){
          ACT_DTS.deleteOne({_id:data.RecordID},(err,doc14)=>{
              if(err){
                  socket.emit('ClearRecDataTrashS', 'err')
                  console.log('null')
              }else{
                  socket.emit('ClearRecDataTrashS', data.DBRec)
              }
        })  
        }
        // else if(data.DBRec == 'NOTISEEN'){
        //     NOTISEEN.deleteOne({_id:data.RecordID},(err,doc15)=>{
        //         if(err){
        //             socket.emit('ClearRecDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearRecDataTrashS', data.DBRec)
        //         }
        //   })  
        //   }else if(data.DBRec == 'NOTIFY'){
        //     NOTIFY.deleteOne({_id:data.RecordID},(err,doc16)=>{
        //         if(err){
        //             socket.emit('ClearRecDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearRecDataTrashS', data.DBRec)
        //         }
        //   })  
        //   }
          else if(data.DBRec == 'ARM_DT'){
            ARM_DT.deleteOne({_id:data.RecordID},(err,doc17)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'CAM_DT'){
            CAM_DT.deleteOne({_id:data.RecordID},(err,doc18)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'MAP_DT'){
            MAP_DT.deleteOne({_id:data.RecordID},(err,doc19)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'MAP_DT_JUNC'){
            MAP_DT_JUNC.deleteOne({_id:data.RecordID},(err,doc20)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'MAP_DT_PT'){
            MAP_DT_PT.deleteOne({_id:data.RecordID},(err,doc21)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }else if(data.DBRec == 'MS_DT_REC'){
            MS_DT_REC.deleteOne({_id:data.RecordID},(err,doc22)=>{
                if(err){
                    socket.emit('ClearRecDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearRecDataTrashS', data.DBRec)
                }
          })  
          }
        //   else if(data.DBRec == 'SSID_DT'){
        //     SSID_DT.deleteOne({_id:data.RecordID},(err,doc23)=>{
        //         if(err){
        //             socket.emit('ClearRecDataTrashS', 'err')
        //             console.log('null')
        //         }else{
        //             socket.emit('ClearRecDataTrashS', data.DBRec)
        //         }
        //   })  
        //   }
          else{
            console.log('database null')
        }
      
      }else{
      console.log('null')
      socket.emit('ClearRecDataTrashS', 'err1')
      }

};

add_new_sos_type_routine = (data) =>{
   if(data == ''){
    socket.emit('SOSTypeAddS','err')
   }else{
    var record = new SOS_DT_TYPE();

    record.Type = data;
    record.TS = new Date();
    record.status = 'recorded';

    record.save((err,doc) => {
    if(err){
        console.log('not save')
    }else{
        // console.log('save');
        socket.emit('SOSTypeAddS','save')

        var ACT = new ACT_DTS();
        ACT.User_Name = info[0].Username;
        ACT.Activity = "New SOS Type : '"+ data +"' has been added";
        ACT.Type = "UTILITY PAGE";
        ACT.TS = new Date();
        ACT.status = "recorded";

        ACT.save((err,docAT4) => {
            if(err){
                //throw err;
                console.log(err);
            } else {
                console.log("New SOS Type : '"+ data +"' has been added");
                //socket.emit('deleteAGVs',doc);
            }
        })
    }

    })
   }
    
};

add_new_sos_response_routine =(data) =>{

    if(data == ''){
        
        socket.emit('SOSResAddS','err')

    }else{
        var record = new SOS_DT_RES();

    record.Response = data;
    record.TS = new Date();
    record.status = 'recorded';

    record.save((err,doc) => {
     if(err){
         console.log('not save')
     }else{
     
         socket.emit('SOSResAddS','save')

         var ACT = new ACT_DTS();
        ACT.User_Name = info[0].Username;
        ACT.Activity = "New SOS Response : '"+ data +"' has been added";
        ACT.Type = "UTILITY PAGE";
        ACT.TS = new Date();
        ACT.status = "recorded";

        ACT.save((err,docAT4) => {
            if(err){
                
                console.log(err);
            } else {
               // console.log("New SOS Response : '"+ data +"' has been added");
               
               }
         })
         
      }

    })
    }

    

};

delete_sos_type_routine = (data)=>{
    SOS_DT_TYPE.findOneAndUpdate({_id:data},{$set:{status:"deleted"}},(err,doc) => {
        if(err)
            socket.emit('deleteSOSTypeReqS','error')
        else {
            socket.emit('deleteSOSTypeReqS','deleted')

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = "SOS Type : '"+ doc.Type +"' has been deleted";
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";
    
            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log("SOS Type : '"+ doc.Type +"' has been deleted");
                    //socket.emit('deleteAGVs',doc);
                }
            })

        }
    })

};

delete_sos_response_routine = (data)=>{

    SOS_DT_RES.findOneAndUpdate({_id:data},{$set:{status:"deleted"}},(err,doc) => {
        if(err)
            socket.emit('deleteSOSResponseReqS','error')
        else {
            socket.emit('deleteSOSResponseReqS','deleted')

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = "SOS Response : '"+ doc.Response +"' has been deleted";
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";
    
            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log("SOS Response : '"+ doc.Response +"' has been deleted");
                    //socket.emit('deleteAGVs',doc);
                }
            })

        }
    })
};

request_data_sos_type_routine = (data) =>{
    SOS_DT_TYPE.findOne({_id:data.sos_id_type}, (err,doc) => {
        if(doc) {
            socket.emit('findSosIDTypeReq',doc._id)
        }
        else {
            console.log(err);
        }
    })
};

update_sos_type_routine = (data) =>{
            
    SOS_DT_TYPE.findOneAndUpdate({_id:data._id},{$set:{Type:data.Type}},(err,doc) => {
        if(err){
            socket.emit('findSosIDTypeResS','error')
        }else{
           // console.log(doc)
            socket.emit('findSosIDTypeResS','save')

            SOS_DT.findOneAndUpdate({Name:doc.Type},{$set:{Name:data.Type}},(err,doc2) => {
                if(err){
                    socket.emit('findSosIDTypeResS','error')
                }else{
                     console.log(doc2)
                    var ACT = new ACT_DTS();
                    ACT.User_Name = info[0].Username;
                    ACT.Activity = "SOS Type : '"+ doc.Type +"' had changed to '"+ data.Type +"'";
                    ACT.Type = "UTILITY PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";
            
                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                          //  console.log("SOS Type : '"+ doc.Type +"' had changed to '"+ data.Type +"'");
                          
                        }
                    })
                }
            })
        }
    })
};

request_data_sos_response_routine = (data) =>{
    SOS_DT_RES.findOne({_id:data.sos_id_res}, (err,doc) => {
        if(doc) {
            //console.log("get: " + doc.Type);
            socket.emit('findSosIDResReq',doc._id)
        }
        else {
            console.log(err);
        }
    })
};

update_sos_response_routine = (data) =>{

    SOS_DT_RES.findOneAndUpdate({_id:data._id},{$set:{Response:data.Res}},(err,doc) => {
        if(err){
            socket.emit('findSosIDResponseResS','error')
        }else{
            socket.emit('findSosIDResponseResS','save')
            SOS_DT.findOneAndUpdate({Res:doc.Response},{$set:{Res:data.Res}},(err,doc2) => {
                if(err){
                    socket.emit('findSosIDTypeResS','error')
                }else{

                    var ACT = new ACT_DTS();
                    ACT.User_Name = info[0].Username;
                    ACT.Activity = "SOS Response : '"+ doc.Response +"' had changed to '"+ data.Res +"'";
                    ACT.Type = "UTILITY PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";
            
                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                           // console.log("SOS Response : '"+ doc.Response +"' had changed to '"+ data.Res +"'");
                           
                        }
                    })
                }
            })
        }

    })
};

add_new_software_update_routine = (data) =>{

             var UPD = new UPD_DT();
 
             UPD.Ver = data.Ver;
             UPD.Rem = data.Rem;
             UPD.TS = new Date();
             UPD.status = "recorded";
 
             UPD.save((err1,docUPD) => {
                 if(err1){
                     //console.log(err);
                     socket.emit('add/SoftS','err');
                 } else {
                     socket.emit('add/SoftS',docUPD);
 
                     var ACT = new ACT_DTS();
                     ACT.User_Name = info[0].Username;
                     ACT.Activity = 'New Software Update version : "'+ data.Ver +'" has been added';
                     ACT.Type = "UTILITY PAGE";
                     ACT.TS = new Date();
                     ACT.status = "recorded";
             
                     ACT.save((err2,docAT4) => {
                         if(err2){
                             console.log(err);
                         } else {
                           
                         }
                     })
                 }
             })
};

request_data_cam_agv_routine = (data) =>{
    CAM_DT.findOne({AGV_ID:data,status:"recorded"},(err,doc)=>{
        if(err){
            console.log('null cam_dt');
        }else{
            //console.log(doc)
            socket.emit('resAgvCamLink',doc)
        }
    })
};

delete_sos_record = (data) =>{
    SOS_DT.findOneAndUpdate({_id:data.sos_id, status:"recorded"},{$set:{status:"deleted"}},(err,doc) => {
        if(err) {
            console.log(err)
            socket.emit('deleteSosS','error');
        }
        else {
            
            console.log('deleted '+data.sos_id+' from sos_dt')
            socket.emit('deleteSosS','success');

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'SOS  : "'+ doc.Name + '" has been deleted';
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    //console.log('SOS  : "'+ doc.Name + '" has been deleted');
                    //socket.emit('deleteAGVs',doc);
                }
            })
        
        }
    })
};

request_cam_link_agv_routine = (data) =>{
    CAM_DT.findOne({AGV_ID:data,status:"recorded"},(err,doc) => {
        if(err){
            console.log('null');
        }else{
        // console.log(doc)
            socket.emit('resCamLink',doc)
        }
})
};

update_cam_link_agv_routine = (data) =>{
    CAM_DT.findOneAndUpdate({AGV_ID:data.AGV_ID,status:"recorded"},{$set:{Link_Cam:data.CamLink,Link_PtCld:data.PtCloud}},(err,doc) => {
        if(err){
            console.log('null');
            socket.emit('updateCamPtS','err')
        }else{
           // console.log(doc)
            socket.emit('updateCamPtS',doc)
   
            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'Camera Link : "'+ doc.Link_Cam + '" changed to "'+ data.CamLink +'" and Point Cloud Link : "'+ doc.Link_PtCld +'" changed to "'+ data.PtCloud +'"';
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docCamLink) => {
                if(err){
                    //throw err;
                    console.log('null');
                } else {
                    //console.log('Camera Link : "'+ doc.Link_Cam + '" changed to "'+ data.CamLink +'" and Point Cloud Link : "'+ doc.Link_PtCld +'" changed to "'+ data.PtCloud +'"');
                    //socket.emit('deleteAGVs',doc);
                }
            })

        }
   })
};

add_new_arm_X_routine = (data) =>{
    var VAX = new ARM_DT()

    VAX.Name = data.Name;
    VAX.Cmd = data.Cmd
    VAX.TS = new Date();
    VAX.status = "recorded";

    VAX.save((err,docArmX) => {
        if(err){
            //throw err;
            socket.emit('ValAarmSX', 'err');
            console.log("err");

        } else {
            //console.log('SOS  : "'+ doc.Name + '" has been deleted');
            socket.emit('ValAarmSX',docArmX.Name);
            console.log('ok')
            console.log(docArmX)
        }
    })
};

add_new_arm_routine = (data) =>{
    var arrArm = {};

    arrArm = ({cmd:data.cmdValue,j1:data.J1,p1:data.P1,fv1:data.FV1,j2:data.J2,p2:data.P2,fv2:data.FV2,j3:data.J3,p3:data.P3,fv3:data.FV3,j4:data.J4,p4:data.P4,fv4:data.FV4})

    var VA = new ARM_DT()

    VA.Name = data.Name;
    VA.Cmd = arrArm;
    VA.TS = new Date();
    VA.status = "recorded";

    VA.save((err,docArm) => {
        if(err){
            //throw err;
            socket.emit('ValAarmS', 'err');
            console.log("err");

        } else {
            //console.log('SOS  : "'+ doc.Name + '" has been deleted');
            socket.emit('ValAarmS',docArm.Name);
            //console.log(docArm)

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'New Arm : "'+ docArm.Name + '" has been added';
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docArmD) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                   // console.log("New User : "+ data.Username);
                    //socket.emit('deleteAGVs',doc);
                }
            })
        }
    })
};

request_list_arm_data_for_edit_routine = (data) =>{
    ARM_DT.findOne({_id:data},(err,doc) => { 
        if(err){
            console.log('err list Out all arm data');
        }else{
            socket.emit('resArmListCmd',doc);
         //   console.log(doc)
        }
   })
 };

 delete_arm_routine = (data) =>{
    ARM_DT.findOneAndUpdate({_id:data},{$set:{status:"deleted"}},(err,doc)=>{
        if(err){
            socket.emit('resDelArmValD','err')
            console.log('err delete arm val')
        }else{
            socket.emit('resDelArmValD',doc.Name)

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'Arm : "'+ doc.Name + '" has been deleted';
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docArmD) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                   // console.log("New User : "+ data.Username);
                    //socket.emit('deleteAGVs',doc);
                }
            })
        }
    })
};

request_list_batchOps_routine = (data) =>{
    if(data == 'AGV_HEALTH_STATUS'){
        HEALTH_DT.find({status:'recorded'}).limit(100000).sort({TS:-1}).exec((err, doc) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc)
                socket.emit('resReportsBatchOps',doc)
            }
        })
    }else if(data == 'AGV_DIAG_STATUS'){
        DIAG_DT.find({status:'recorded'}).limit(100000).sort({TS:-1}).exec((err, doc1) => {
            if(err){
                console.log('null')
            }else{
                //console.log(doc1)
                socket.emit('resReportsBatchOps',doc1)
            }
        })
    }else if(data == 'AUDIT_TRAIL'){
        ACT_DTS.find({status:'recorded'}).limit(100000).sort({TS:-1}).exec((err, doc2) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc2)
            }
        })
    }else if(data == 'SCHED_HISTORY'){
        MS_DT_REC.find({}).limit(100000).sort({TS:-1}).exec((err, doc3) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc3)
            }
        })
    }else if(data == 'SOS_HISTORY'){
        SOS_DT_REC.find({status:'recorded'}).limit(100000).sort({TS:-1}).exec((err, doc4) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc4)
            }
        })
    }else if(data == 'USER_HISTORY'){
        USER_DT.find({status:'recorded'}).limit(100000).sort({TS:-1}).exec((err, doc5) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc5)
            }
        })
    }else if(data == 'AGV_HISTORY'){
        AGV_DT.find({status:'recorded'}).limit(100000).sort({TS:-1}).exec((err, doc6) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc6)
            }
        })
    }else if(data == 'NOTI_HISTORY'){
        NOTIFY.find({}).limit(100000).sort({TS:-1}).exec((err, doc7) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc7)
            }
        })
    }else if(data == 'SOFTWARE_UPD_HISTORY'){
        UPD_DT.find({status:'recorded'}).limit(100000).sort({TS:-1}).exec((err, doc8) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc8)
            }
        })
    }else{
        console.log('null')
    }
};

delete_software_update_routine = (data) => {
    UPD_DT.findOneAndUpdate({_id:data},{$set:{status:'deleted'}},(err,doc)=>{
        if(err){
            console.log('null')
            socket.emit('softDelProsRes','err')
        }else{
            socket.emit('softDelProsRes',doc)
        }
    })
};

request_data_filter_batchOps_routine = (data)=>{
    if(data.batchType == 'AGV_DIAG_STATUS'){  
        DIAG_DT.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc1) => {  
            if(err){  
                console.log(err)  
            }else{   
                socket.emit('resDIAG2',doc1)  
            }  
        })  
    } 
    else if(data.batchType=='AGV_HEALTH_STATUS'){  
        HEALTH_DT.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc2) => {  
            if(err){  
                console.log(err)  
            }else{  
                socket.emit('resHT2',doc2)  
            }  
        })       
    }   
    else if(data.batchType == 'AUDIT_TRAIL'){ 
        ACT_DTS.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc3) => {  
            if(err){  
                console.log(err)  
            }else{  
                socket.emit('resAT2',doc3)  
            }  
        })  
    } 
    else if(data.batchType == 'SCHED_HISTORY'){ 
        MS_DT_REC.find({TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc4) => {  
            if(err){  
                console.log(err)  
            }else{   
                socket.emit('resSCHED',doc4) 
              //  console.log(doc4); 
            }  
        })  
    }  else if(data.batchType == 'SOS_HISTORY'){ 
        SOS_DT_REC.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc5) => {  
            if(err){  
                console.log(err)  
            }else{   
                socket.emit('resSOSHist',doc5) 
              
            }  
        })  
    }  else if(data.batchType == 'USER_HISTORY'){ 
        USER_DT.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc6) => {  
            if(err){  
                console.log(err)  
            }else{   
                socket.emit('resUSERHist',doc6) 
              
            }  
        })  
    }  else if(data.batchType == 'AGV_HISTORY'){ 
        AGV_DT.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc7) => {  
            if(err){  
                console.log(err)  
            }else{   
                socket.emit('resAGVHist',doc7) 
                
            }  
        })  
    }  else if(data.batchType == 'NOTI_HISTORY'){ 
        NOTIFY.find({TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc8) => {  
            if(err){  
                console.log(err)  
            }else{   
                socket.emit('resNOTIHist',doc8) 
               
            }  
        })  
    }  else if(data.batchType == 'SOFTWARE_UPD_HISTORY'){ 
        UPD_DT.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(10000).exec((err, doc9) => {  
            if(err){  
                console.log(err)  
            }else{   
                socket.emit('resSOFTUPDHist',doc9) 
               
            }  
        })  
    } 
};

request_mapDT_id_data_routine = (data) =>{
    MAP_DT.findOne({_id:data.map_id,status:"recorded"},(err,obj) => {
        if(obj){
            socket.emit('RetMapRecbyID',obj);
        }else{
            console.log('null')
        }
    })
};

request_map_list_data_routine = (data) =>{
    MAP_DT.findOne({_id:data},(err,doc)=>{
        if(err){
            console.log('null')
        }else{
            socket.emit('ebuttDelMapRecbyIDS',doc)
        }
    })
};

delete_mapDT_record = (data) =>{

    MAP_DT.findOneAndUpdate({_id:data},{$set:{status:"deleted"}}, (err,doc) => {
        if(err)
        {   
            socket.emit('DelMapRecbyIDRes','err')
            console.log('null');
        }
        else {
            socket.emit('DelMapRecbyIDRes',doc)

                    var ACT = new ACT_DTS();

                    ACT.User_Name = info[0].Username;
                    ACT.Activity = 'Map : "'+ doc.Name + '" deleted successfully' ;
                    ACT.Type = "MISSION EDIT PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";

                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log('Map : "'+ doc.Name + '" deleted successfully');
                            //socket.emit('deleteAGVs',doc);
                        }
                    })

                MAP_DT_PT.updateMany({Map_ID:doc._id},{$set:{status:"deleted"}}, (err,doc2) => {
                    if(err){
                        console.log('null')
                    }else{
                         console.log(doc2)

                         
                         MAP_DT_JUNC.updateMany({map_id:doc._id},{$set:{status:"deleted"}},(err,doc3)=>{
                             if(err){
                                console.log('null')
                             }else{
                                console.log(doc3)
                             }
                         })
                    }
                })
        }

    })
};

add_new_map_routine = (data) =>{
    MAP_DT.findOne({Filename:data.Filename,status:'recorded'},(err,doc) => {
        if(doc) 
        {
            console.log('Filename already exist');
            socket.emit('AddMapCallRes','err');
        }
        else
        {
            var MAP = new MAP_DT();
            MAP.Name = data.Name;
            MAP.Filename = data.Filename;
            MAP.Home_x = data.Home_x;
            MAP.Home_y = data.Home_y;
            MAP.Home_z = data.Home_z;
            MAP.Home_w = data.Home_z;
            MAP.TS = new Date();
            MAP.status = "recorded";

            MAP.save((err,doc2) => {
                if(err){
                    console.log(err);
                } else {
                    //console.log('Map : "'+ doc.Name + '" has been added');
                    socket.emit('AddMapCallRes',doc2); 

                    var ACT = new ACT_DTS();

                    ACT.User_Name = info[0].Username;
                    ACT.Activity = 'Map : "'+ doc2.Name + '" has been added ' ;
                    ACT.Type = "MISSION EDIT PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";

                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log('Map : "'+ doc2.Name + '" has been added');
                            //socket.emit('deleteAGVs',doc);
                        }
                    })

                }
            })          
        }
    })
};

request_data_map_record_routine = (data) =>{
    MAP_DT.findOne({_id:data},(err,doc) =>{
        if(err){
            console.log('map null')
        }else{
            socket.emit('ReseMVar',doc);
        }
    })
};

update_map_record_routine = (data) =>{
    MAP_DT.findOneAndUpdate({_id:data._id},{$set:{Name:data.Name,Filename:data.Filename, Home_x:data.Home_x,Home_y:data.Home_y, Home_z:data.Home_z,Home_w:data.Home_w }}, (err,doc) => {
        if(err)
        {   
            socket.emit('UptMapRecbyIDRes','err')
            console.log(err);
        }
        else {
            socket.emit('UptMapRecbyIDRes',doc)

                    var ACT = new ACT_DTS();

                    //ACT.User_Name = info[0].Username;
                    ACT.Activity = 'Map : "'+ data.Name + '" has been updated ' ;
                    ACT.Type = "UTILITY PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";

                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log('Map : "'+ data.Name + '" has been updated');
                            //socket.emit('deleteAGVs',doc);
                        }
                    })
             }

       })
};

request_map_pointAndjunc_routine = (data) =>{
    MAP_DT.findOne({_id:data},(err,doc)=>{
        if(err){
            console.log('null')
           // console.log(doc)
        }else{
            MAP_DT_PT.find({Map_ID:doc._id,status:"recorded"},(err,doc2) =>{
                if(err){
                    console.log('null')
                }else{
                    console.log("doc2" + doc2)
                    socket.emit('pointMapDetRes',doc2)
                    MAP_DT_JUNC.find({map_id:doc._id,status:"recorded"},(err,doc3)=>{
                        if(err){
                            console.log('null')
                        }else{
                            console.log("doc3" + doc3)
                            socket.emit('junctionMapDetRes',doc3)
                        }
                    })
                }
            })
        }
    })
   };

add_new_mapPoint_routine = (data) =>{
    MAP_DT_PT.findOne({Name:data.Name,status:"recorded"},(err,doc)=>{

        if(doc){
            console.log('Already exist')
            socket.emit('addNewPointRes', 'err')
           
        }else{

            var PT = new MAP_DT_PT();

            PT.Name = data.Name;
            PT.Map_ID = data.Map_ID;
            PT.Loc_x =  data.Loc_x;
            PT.Loc_y =  data.Loc_y;
            PT.Loc_z =  data.Loc_z;
            PT.Loc_w =  data.Loc_w;
            PT.Lat =  data.Lat;
            PT.Long =  data.Long;
            PT.TS = new Date();
            PT.status = "recorded";
 
            PT.save((err,docAPT) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log(docAPT)
                    socket.emit('addNewPointRes', docAPT)

                    var ACT = new ACT_DTS();

                    ACT.User_Name = info[0].Username;
                    ACT.Activity = 'Map Point : "'+ data.Name + '" has been added ' ;
                    ACT.Type = "MISSION EDIT PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";

                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log('Map : "'+ data.Name + '" has been added');
                            //socket.emit('deleteAGVs',doc);
                        }
                    })
                }
            })
            
            
        }
    })
};

request_mapPoint_record_routine = (data) =>{
    MAP_DT_PT.findOne({_id:data},(err,doc) =>{
        if(err){
            console.log('null')
        }else{
            socket.emit('resListMapPt',doc)
        }
    })
};

update_mapPoint_record_routine = (data) =>{
    MAP_DT_PT.findOneAndUpdate({_id:data.id},{$set:{Name:data.Name,Loc_x:data.Loc_x,Loc_y:data.Loc_y,Loc_z:data.Loc_z,Loc_w:data.Loc_w,Lat:data.Lat,Long:data.Long}},(err,doc)=>{
        if(err){
            socket.emit('editExistPointRes', 'err')
        }
        else{
            socket.emit('editExistPointRes', doc.Name)

                   var ACT = new ACT_DTS();
                    ACT.User_Name = info[0].Username;
                    ACT.Activity = 'Map Point : "'+ doc.Name + '" has been updated ' ;
                    ACT.Type = "MISSION EDIT PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";

                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log('Map : "'+ doc.Name + '" has been updated');
                            //socket.emit('deleteAGVs',doc);
                        }
                    })
        }
    })
};

delete_mapPoint_routine = (data) =>{
    MAP_DT_PT.findOneAndUpdate({_id:data},{$set:{status:"deleted"}},(err,doc)=>{
        if(err){
            console.log('err')
            socket.emit('delAddMapPointS','err')
        }else{
            socket.emit('delAddMapPointS',doc)

            var ACT = new ACT_DTS();
                   
                    ACT.User_Name = info[0].Username;
                    ACT.Activity = 'Map Point : "'+ doc.Name + '" has been deleted' ;
                    ACT.Type = "MISSION EDIT PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";

                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log('Map : "'+ doc.Name + '" has been deleted');
                            //socket.emit('deleteAGVs',doc);
                        }
                    })
        }
    })
};

add_mapJunction_routine = (data) =>{
   // console.log(data)
    var JUNCT = new MAP_DT_JUNC();

    JUNCT.map_id = data.map_id;
    JUNCT.x = data.x;
    JUNCT.y =  data.y;
    JUNCT.Lat =  data.Lat;
    JUNCT.Long =  data.Long;
    JUNCT.TS = new Date();
    JUNCT.status = "recorded";

    JUNCT.save((err,docJ) => {
        if(err){
            socket.emit('addNewJunctionRes', 'err')
          //  console.log(err);
        } else {
          //  console.log(docJ)
            socket.emit('addNewJunctionRes', docJ)

            var ACT = new ACT_DTS();
            
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'Map Point Junction : "'+ docJ.map_id + '" has been added ' ;
            ACT.Type = "MISSION EDIT PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log('Map : "'+ docJ.map_id + '" has been added');
                    //socket.emit('deleteAGVs',doc);
                }
            })

        }
    })
};

request_mapJunct_record_routine = (data) =>{
    MAP_DT_JUNC.findOne({_id:data},(err,doc)=>{
        if(err){
            console.log('null')
        }else{
            socket.emit('resListMapJunct',doc)
        }
    })
};

update_map_junction_routine = (data) =>{
    MAP_DT_JUNC.findOneAndUpdate({_id:data.id},{$set:{map_id:data.map_id,x:data.x,y:data.y,Lat:data.Lat,Long:data.Long}},(err,doc)=>{
        if(err){
            console.log('null')
            socket.emit('editExistJunctRes', 'err')
        }else{
            socket.emit('editExistJunctRes', doc)

            var ACT = new ACT_DTS();
           
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'Map Point Junction : "'+ doc.map_id + '" has been added ' ;
            ACT.Type = "MISSION EDIT PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log('Map : "'+ doc.map_id + '" has been updated');
                    //socket.emit('deleteAGVs',doc);
                }
            })
        }
    })
};

delete_map_junction_routine = (data) =>{
    MAP_DT_JUNC.findOneAndUpdate({_id:data},{$set:{status:"deleted"}},(err,doc)=>{
        if(err){
            console.log('null')
            socket.emit('delAddMapJunctS','err');
        }else{
            socket.emit('delAddMapJunctS',doc);

            var ACT = new ACT_DTS();
           
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'Map Point Junction : "'+ doc.map_id + '" has been added ' ;
            ACT.Type = "MISSION EDIT PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log('Map : "'+ doc.map_id + '" has been updated');
                    //socket.emit('deleteAGVs',doc);
                }
            })
        }
    })
};

request_filter_date_audit_trail_routine = (data) => {
        USER_DT.findOne({_id:data.userId},(err,doc)=>{
            if(err){
                console.log('null')
                }else{
               // console.log(data)
                ACT_DTS.find({User_Name:doc.Username, status:'recorded',TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).exec((err, doc1) => {  
                    if(err){
                        console.log('null/err on request audit trail filter by date')
                    }else{
                        socket.emit('resDFaud',doc1);
                        //console.log(doc1)
                    }
                })
            }
        })
};


socket.on('resetFactory', data =>{
    if(data == 'confirm'){
       // console.log('masuk')
        ACT_DTS.deleteMany({},(err,doc)=>{
            if(err){
                console.log('')
            }else{
                AGV_DT.deleteMany({},(err,doc1)=>{
                    if(err){
                        console.log('')
                    }else{
                        ARM_DT.deleteMany({},(err,doc2)=>{
                            if(err){
                                console.log('')
                            }else{
                                CAM_DT.deleteMany({},(err,doc3)=>{
                                    if(err){
                                        console.log('')
                                    }else{
                                        DIAG_DT.deleteMany({},(err,doc4)=>{
                                            if(err){
                                                console.log('')
                                            }else{
                                                HEALTH_DT.deleteMany({},(err,doc5)=>{
                                                    if(err){
                                                        console.log('')
                                                    }else{
                                                        MAP_DT.deleteMany({},(err,doc6)=>{
                                                            if(err){
                                                                console.log('')
                                                            }else{
                                                                MAP_DT_JUNC.deleteMany({},(err,doc7)=>{
                                                                    if(err){
                                                                        console.log('')
                                                                    }else{
                                                                        MAP_DT_PT.deleteMany({},(err,doc8)=>{
                                                                            if(err){
                                                                                console.log('')
                                                                            }else{
                                                                                MS_DT.deleteMany({},(err,doc9)=>{
                                                                                    if(err){
                                                                                        console.log('')
                                                                                    }else{
                                                                                        MS_DT_ACTIVITY.deleteMany({},(err,doc10)=>{
                                                                                            if(err){
                                                                                                console.log('')
                                                                                            }else{
                                                                                                MS_DT_REC.deleteMany({},(err,doc11)=>{
                                                                                                    if(err){
                                                                                                        console.log('')
                                                                                                    }else{
                                                                                                        NOTIFY.deleteMany({},(err,doc12)=>{
                                                                                                            if(err){
                                                                                                                console.log('')
                                                                                                            }else{
                                                                                                                NOTISEEN.deleteMany({},(err,doc13)=>{
                                                                                                                    if(err){
                                                                                                                        console.log('')
                                                                                                                    }else{
                                                                                                                        SCHED_DT.deleteMany({},(err,doc14)=>{
                                                                                                                            if(err){
                                                                                                                                console.log('')
                                                                                                                            }else{
                                                                                                                                SCHED_DT_MS.deleteMany({},(err,doc15)=>{
                                                                                                                                    if(err){
                                                                                                                                        console.log('')
                                                                                                                                    }else{
                                                                                                                                        SOS_DT.deleteMany({},(err,doc16)=>{
                                                                                                                                            if(err){
                                                                                                                                                console.log('')
                                                                                                                                            }else{
                                                                                                                                                SOS_DT_REC.deleteMany({},(err,doc17)=>{
                                                                                                                                                    if(err){
                                                                                                                                                        console.log('')
                                                                                                                                                    }else{
                                                                                                                                                        SOS_DT_TYPE.deleteMany({},(err,doc18)=>{
                                                                                                                                                            if(err){
                                                                                                                                                                console.log('')
                                                                                                                                                            }else{
                                                                                                                                                                SOS_DT_RES.deleteMany({},(err,doc19)=>{
                                                                                                                                                                    if(err){
                                                                                                                                                                        console.log('')
                                                                                                                                                                    }else{
                                                                                                                                                                        // SSID_DT.deleteMany({},(err,doc20)=>{
                                                                                                                                                                        //     if(err){
                                                                                                                                                                        //         console.log('')
                                                                                                                                                                        //     }else{
                                                                                                                                                                                
                                                                                                                                                                        //     }
                                                                                                                                                                        // })  
                                                                                                                                                                        UPD_DT.deleteMany({},(err,doc21)=>{
                                                                                                                                                                            if(err){
                                                                                                                                                                                console.log('')
                                                                                                                                                                            }else{
                                                                                                                                                                                USER_DT.deleteMany({},(err,doc22)=>{
                                                                                                                                                                                    if(err){
                                                                                                                                                                                        console.log('')
                                                                                                                                                                                    }else{
                                                                                                                                                                                       
                                                                                                                                                                                        var record = new USER_DT();
                                                                                                                                                                                        // console.log(data)
                                                                                                                                                                                         record.Name = 'Administrator';
                                                                                                                                                                                         record.Username = 'Administrator';
                                                                                                                                                                                         record.Pass = record.hashPassword('Admin321@');
                                                                                                                                                                                         record.Type = 'ADMIN';
                                                                                                                                                                                         record.TS = new Date();
                                                                                                                                                                                         record.Login_Attempt = "0";
                                                                                                                                                                                         record.Login_Status = "NORMAL";
                                                                                                                                                                                         record.status = "recorded";
                                                                                                                                                                                         
                                                                                                                                                                                        // console.log(record + 'record')
                                                                                                                                                                                         record.save((err,doc23) => {
                                                                                                                                                                                         if(err) {
                                                                                                                                                                                             console.log(err)
                                                                                                                                                                                             socket.emit('createAdmin','err');
                                                                                                                                                                                             
                                                                                                                                                                                         }
                                                                                                                                                                                         else {
                                                                                                                                                                                               //  console.log('done masuk backup');
                                                                                                                                                                                             socket.emit('createAdmin','save');
                                                                                                                                                                                         }
                                                                                                                                                                                        })
                                                                                                                                                                                    }
                                                                                                                                                                                })
                                                                                                                                                                            }
                                                                                                                                                                        })
                                                                                                                                                                    }
                                                                                                                                                                })
                                                                                                                                                            }
                                                                                                                                                        })
                                                                                                                                                    }
                                                                                                                                                }) 
                                                                                                                                            }
                                                                                                                                        })
                                                                                                                                    }
                                                                                                                                })
                                                                                                                            }
                                                                                                                        })
                                                                                                                    }
                                                                                                                })
                                                                                                            }
                                                                                                        })
                                                                                                        
                                                                                                    }
                                                                                                })
                                                                                                
                                                                                            }
                                                                                        })
                                                                                        
                                                                                    }
                                                                                })
                                                                                
                                                                            }
                                                                        })
                                                                        
                                                                    }
                                                                })
                                                                
                                                            }
                                                        })
                                                        
                                                    }
                                                })
                                                
                                            }
                                        })
                                        
                                    }
                                })
                                
                            }
                        })
                        
                    }
                })

            }
        })
    }else{

    }
})

}//--------------Socket--------------//


 

