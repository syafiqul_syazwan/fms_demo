const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const AGV_DT = mongoose.model('AGV_DT');
const MS_DT = mongoose.model('MS_DT');
const MS_DT_ACT = mongoose.model('MS_DT_ACTIVITY');
const SCHED_DT_MS = mongoose.model('SCHED_DT_MS');
const SCHED_DT = mongoose.model('SCHED_DT');
const ACT_DTS = mongoose.model('ACT_DTS');
const CAM_DT = mongoose.model('CAM_DT');



var info = [];
var socketResponse = {
    data: "",
    response: false
}
const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                       
                        return res.redirect('./error')  	
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                        return res.redirect('./error')  
                        }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        info.push({userId:id,Name:name,Role:role,Password:pass})  
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}

router.get('/',loggedin,(req,res) => {

// Trigger Global Refresh
require('./dashboard_controller')();

AGV_DT.find({status: 'recorded'},(err,docs) => { // shows only the AGV_DT with recorded status
    if(!err) {
        res.render('agvSetting/index', {
            title : "AGV SETTING",
            agv: docs,
            name : info[0].Name,
            userid: info[0].userId,
            role: info[0].Role,
            pass: info[0].Password,
        });
    }
    else {
        console.log();
    }
})

})

module.exports = router;

module.exports.respond = socket => {

//--------------------- SOCKET IO FUNCTIONS -------------------------//

    socket.on('VCsaveAGV', (data) => {
        register_agv_routine(data);
    })

    socket.on('reqEAgv',(data) => {
        req_e_agv_routine(data);
    })

    socket.on('updateAGV',(data) => {
        update_agv_routine(data);
    });

    socket.on('deleteAGV',data => { // start of deleteAGV
        delete_agv_routine(data);
    }) 


//--------------------- ROUTINE FUNCTIONS -------------------------//

    register_agv_routine = (data) => {

        AGV_DT.findOne({agv_ID: data.Name,status:"recorded"},(err,doc) => {
            if(err) {
                throw err;
            }

            else {
                if (doc) {
                    socketResponse.response = false;
                    socketResponse.data = "An AGV with ID: " + data.Name + " already existed, please try another ID"
                    socket.emit('CVsaveAGV', socketResponse);
                    return
                }
                else {
                    // save in current database
                    var AGV = new AGV_DT();
                    AGV.agv_ID = data.Name;
                    AGV.TS = data.TS;
                    AGV.status = "recorded";
                    AGV.save((err,doc) => {
                        if(err) {
                            socketResponse.response = false;
                            socketResponse.data = "AGV could not save, please try again later"
                            socket.emit('CVsaveAGV', socketResponse);
                        }
                        else {
                            var ACT = new ACT_DTS();
                            ACT.User_Name = info[0].Name;
                            ACT.Activity = "Created New AGV: "+data.Name;
                            ACT.Type = "AGV SETTING PAGE";
                            ACT.TS = data.TS;
                            ACT.status = "recorded";

                            ACT.save((err,doc2) => {
                                if(err){
                                    //throw err;
                                    console.log("Error while creating A.T. for Register AGV routine: " + err);
                                } else {
                                    socketResponse.response = true;
                                    socketResponse.data = doc.agv_ID;
                                    socket.emit('CVsaveAGV', socketResponse);
                                    var CAM = new CAM_DT();
                                    CAM.AGV_ID = data.Name;
                                    CAM.Link_Cam = "No Data";
                                    CAM.Link_PtCld = "No Data";
                                    CAM.TS = new Date();
                                    CAM.status = "recorded";
        
                                    CAM.save((err,doc3) => {
                                        if (err) {
                                            console.log('Error hwile saving DAM DT for Register AGV routine: ' + err);
                                        } else {
                                            console.log("CAM Link for Create AGV: "+doc3.AGV_ID);
                                        }
                                    })

                                }
                            })

                        }
                    })


                }
            }
        })

    }

    req_e_agv_routine = (data) => {
        AGV_DT.findOne({_id:data._id,status:"recorded"}, (err,doc) => {
            if(doc) {
                console.log("get: " + doc.agv_ID);
                socket.emit('resEAgv',doc.agv_ID)
            }
            else {
                console.log(err);
            }
        })
    }

    update_agv_routine = (data) => {
        AGV_DT.findOne({agv_ID: data.agv_ID,status:"recorded"},(err,doc4) => {
            if(err) {
                throw err;
            }
            else {
                if(doc4) {
                    socket.emit('updateAGVs','err');
                    console.log('This agv already exists!');
                    return;
                }
                else {

                    // Update data in current table
                    AGV_DT.findOneAndUpdate({_id:data._id,status:"recorded"},{$set:{agv_ID:data.agv_ID,TS:new Date()}},(err,doc) => {
                        if(!doc) {
                            console.log(err);
                        }
                        else {

                            // save action in audit trail
                            var ACT = new ACT_DTS();
                            ACT.User_Name = info[0].Name;
                            ACT.Activity = "Updated AGV: "+doc.agv_ID+" to "+data.agv_ID;
                            ACT.Type = "AGV SETTING PAGE";
                            ACT.TS = new Date();
                            ACT.status = "recorded";

                            ACT.save((err,doc3) => {
                                if(err){
                                    //throw err;
                                    console.log('null');
                                } else {
                                // console.log(doc3.Activity);
                                    socket.emit('updateAGVs',doc)

                                    CAM_DT.findOneAndUpdate({AGV_ID:doc.agv_ID,status:"recorded"},{$set:{AGV_ID:data.agv_ID}},(err,doc2) => {
                                        if(!doc2) {
                                            console.log('null');
                                        }
                                        else {
                                            //console.log(doc2)

                                        }
                                    })

                                }
                            })

                            }
                        })

                    }
                }
        })

    }

    delete_agv_routine = (data) => {
        var json = {};
        var interval = 1 * 10; // 10 seconds;
        AGV_DT.findOne({_id:data._id, status:"recorded"},(err,doc) => {
            if(err) {
                console.log(err)
            }
            else {
            
            MS_DT.find({agv_ID:doc.agv_ID, status:"recorded"},(err,doc2) => {
                if(err) {
                    console.log(err)
                }
                else {
                    console.log(doc2)
                    for(var i = 0; i < doc2.length; i++) {
                        setTimeout( function (i) {

                            MS_DT_ACT.updateMany({MS_ID:doc2[i]._id, status:"recorded"},{$set:{status:"deleted"}},(err,doc6) => {
                                if(err){
                                    console.log(err);
                                } else {
                                    //console.log(doc6);
                                    console.log('deleted mission activity successfully')

                                    // save action in audit trail
                                    var ACT = new ACT_DTS();
                                    ACT.User_Name = info[0].Name;
                                    ACT.Activity = "Deleted All Missions Associated with AGV: "+doc.agv_ID;
                                    ACT.Type = "AGV SETTING PAGE";
                                    ACT.TS = new Date();
                                    ACT.status = "recorded";

                                    ACT.save((err,docAT) => {
                                        if(err){
                                            //throw err;
                                            console.log(err);
                                        } else {
                                            console.log("Audit Trail Delete AGV 1: "+docAT);
                                        }
                                    })

                                    SCHED_DT_MS.find({MS_ID:doc2[i]._id},(err,doc8)=>{
                                        if(err){
                                            console.log(err);
                                        } else {
                                            for(var j = 0; j < doc8.length; j++) {
                                                setTimeout( function (j) {
                                                    SCHED_DT.updateMany({_id:doc8[j].SCHED_ID, status:"recorded"},{$set:{status:"deleted"}},(err,doc7) => {
                                                        if(err){
                                                            console.log(err);
                                                        } else {
                                                            console.log('deleted schedule successfully');
                                                        }
                                                    })
                                                }, interval * j, j); 
                                            }

                                                // SCHED_DT.find({_id:doc8[j].SCHED_ID, status:"deleted"}, (err,docs) => {
                                                //     if(!docs) {
                                                //         console.log('Repeated Sched Delete');
                                                //     } else {

                                                //     }
                                                // })
                                                // save action in audit trail
                                                var ACT = new ACT_DTS();
                                                ACT.User_Name = info[0].Name;
                                                ACT.Activity = "Deleted All Schedules Associated with AGV: "+doc.agv_ID;
                                                ACT.Type = "AGV SETTING PAGE";
                                                ACT.TS = new Date();
                                                ACT.status = "recorded";

                                                ACT.save((err,docAT2) => {
                                                    if(err){
                                                        //throw err;
                                                        console.log(err);
                                                    } else {
                                                        console.log("Audit Trail Delete AGV 2: "+docAT2);
                                                    }
                                                })
                                        }
                                    })

                                    SCHED_DT_MS.updateMany({MS_ID:doc2[i]._id, status:"recorded"},{$set:{status:"deleted"}},(err,doc7) => {
                                        if(err){
                                            console.log(err);
                                        } else {
                                            console.log('deleted schedule mission successfully')

                                            // save action in audit trail
                                            var ACT = new ACT_DTS();
                                            ACT.User_Name = info[0].Name;
                                            ACT.Activity = "Deleted Mission ID "+doc2[i].Name+" Associated with AGV: "+doc.agv_ID;
                                            ACT.Type = "AGV SETTING PAGE";
                                            ACT.TS = new Date();
                                            ACT.status = "recorded";

                                            ACT.save((err,docAT3) => {
                                                if(err){
                                                    //throw err;
                                                    console.log(err);
                                                } else {
                                                    console.log("Audit Trail Delete AGV 3: "+docAT3);
                                                }
                                            })
                                        }

                                    })

                                }
                            })


                        }, interval * i, i); 

                    }

                    MS_DT.updateMany({agv_ID:doc.agv_ID, status:"recorded"},{$set:{status:"deleted"}},(err,doc4)=>{
                        if(err){
                            console.log(err);
                        } else {
                            console.log('deleted mission successfully')
                            // socket.emit('deleteAGVs',doc);

                            // save action in audit trail
                            var ACT = new ACT_DTS();
                            ACT.User_Name = info[0].Name;
                            ACT.Activity = "Deleted all Missions Associated with AGV: "+doc.agv_ID;
                            ACT.Type = "AGV SETTING PAGE";
                            ACT.TS = new Date();
                            ACT.status = "recorded";

                            ACT.save((err,docAT4) => {
                                if(err){
                                    //throw err;
                                    console.log(err);
                                } else {
                                    console.log("Audit Trail Delete AGV 4: "+docAT4);
                                    //socket.emit('deleteAGVs',doc);
                                }
                            })
                        }
                    })

                    CAM_DT.updateOne({AGV_ID:doc.agv_ID, status:"recorded"},{$set:{status:"deleted"}},(err,doc11) => {
                        if(err) {
                            console.log(err);
                        } else {
                            console.log(doc11);
                        }
                    })

                    AGV_DT.updateOne({_id:data._id, status:"recorded"},{$set:{status:"deleted"}},(err,doc3) => {
                        if(err) {
                            console.log(err);
                        } else {
                            console.log('soft deleted '+ data._id +' successfully');

                            // save action in audit trail
                            var ACT = new ACT_DTS();
                            ACT.User_Name = info[0].Name;
                            ACT.Activity = "Deleted AGV: "+doc.agv_ID;
                            ACT.Type = "AGV SETTING PAGE";
                            ACT.TS = new Date();
                            ACT.status = "recorded";

                            ACT.save((err,docAT4) => {
                                if(err){
                                    //throw err;
                                    console.log(err);
                                } else {
                                    console.log("Audit Trail Delete AGV 5: "+docAT4);
                                    socket.emit('deleteAGVs',doc);
                                }
                            })

                            //socket.emit('deleteAGVs',doc);
                        }
                    })

                    }
                })
            }
        })

    } 


} // End of socket module

