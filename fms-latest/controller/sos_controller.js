const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const AGV_DT = mongoose.model('AGV_DT');
const SOS_DT = mongoose.model('SOS_DT');
const SOS_DT_REC = mongoose.model('SOS_DT_REC');
const HEALTH_DT = mongoose.model('HEALTH_DT');
const SOS_DT_RES = mongoose.model('SOS_DT_RES');
const SOS_DT_TYPE = mongoose.model('SOS_DT_TYPE');
const ACT_DTS = mongoose.model('ACT_DTS');


var info = [];
const loggedin = (req, res, next) => {
    if (req.isAuthenticated()) {
        //console.log(req.user)
        if (req.user.role == 'OBSERVER') {
            if (req.path == '/') {
                return res.redirect('./error')
            } else {
                console.log('nothing')
            }

        } else if (req.user.role == 'OPERATOR') {
            if (req.path == '/') {
                info = [];
                var id = req.user.id;
                var name = req.user.name
                var role = req.user.role
                var pass = req.user.password
                var username = req.user.username
                info.push({ userId: id, Name: name, Role: role, Password: pass, Username: username })
                //return res.redirect('./error')
            } else {
                console.log('nothing')
            }

        } else if (req.user.role == 'ADMIN') {
            if (req.path == '/') {
                info = [];
                var id = req.user.id;
                var name = req.user.name
                var role = req.user.role
                var pass = req.user.password
                var username = req.user.username
                info.push({
                    userId: id,
                    Name: name,
                    Role: role,
                    Password: pass,
                    Username: username
                })
            } else {
                console.log('nothing')
            }

        } else {

            return res.redirect('./error')

        }

        return next()

    } else {
        res.redirect('/login')
    }
}

router.get('/', loggedin, (req, res) => {
    //Render
    SOS_DT.find({
        status: 'recorded'
    }).sort({
        TS: -1
    }).exec((err, doc) => {
        if (err) {
            console.log(err);
        } else {
            AGV_DT.find({
                status: 'recorded'
            }, (err, docs) => {
                if (err) {
                    console.log(err);
                } else {
                    SOS_DT_RES.find({
                        status: 'recorded'
                    }, (err, doc1) => {
                        if (err) {
                            console.log(err);
                        } else {
                            SOS_DT_TYPE.find({
                                status: 'recorded'
                            }, (err, doc2) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    var ovdid = doc2.find(x => x.Type == 'override');
                                    SOS_DT_REC.find({
                                        status: 'recorded'
                                    },
                                    null,
                                    {
                                        sort: { _id: -1 }
                                    },
                                    (err, doc3) => {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            if (doc3.length > 0) {
                                                //console.log('DOC2 ' + doc2);
                                                for (x = 0; x < doc3.length; x++) {
                                                    var v = doc3[x].sosid_LIST;
                                                    var sn = [];
                                                    var sr = [];
                                                    for (y = 0; y < v.length; y++) {
                                                        try {
                                                            //console.log("SOS_DT_TYPE search by id",v[y])
                                                            var cd = doc2.find(xx => xx._id == v[y]); //console.log("sostype",cd);
                                                            sn.push(cd.Type);
                                                            sr.push(doc.find(xx => xx.Name == cd.Type).Res);//console.log("SOS_DT search by type", cd.Type);
                                                        }
                                                        catch (e) {
                                                            console.log('Error in sos controller Render: ' + e);
                                                        }
                                                    }
                                                    doc3[x].SosType = sn;
                                                    doc3[x].SosRes = sr;
                                                    if (x == doc3.length - 1) {
                                                        res.render('sos/index', {
                                                            title: "SOS ABNORMALITY",
                                                            agv: docs,
                                                            sos_dt: doc,
                                                            name: info[0].Name,
                                                            userid: info[0].userId,
                                                            role: info[0].Role,
                                                            pass: info[0].Password,
                                                            sos_type: doc2,
                                                            sos_res: doc1,
                                                            rec: doc3,
                                                            override_id: ovdid._id
                                                        })
                                                    }
                                                }
                                            }
                                            else {
                                                res.render('sos/index', {
                                                    title: "SOS ABNORMALITY",
                                                    agv: docs,
                                                    sos_dt: doc,
                                                    name: info[0].Name,
                                                    userid: info[0].userId,
                                                    role: info[0].Role,
                                                    pass: info[0].Password,
                                                    sos_type: doc2,
                                                    sos_res: doc1,
                                                    rec: doc3,
                                                    override_id: ovdid._id
                                                })
                                            }

                                        }
                                    }).limit(100);
                                }
                            })
                        }
                    })
                }
            })
        }
    })

})



module.exports = router;

module.exports.respond = socket => {

    socket.on('addSos', data => {
        console.log(data);
        SOS_DT.find({
            Name: data.Name,
            status: "recorded"
        }, (err, doc) => {
            //console.log(doc);
            if (err) {
                console.log(err);
            } else {
                if (!doc) {
                    socket.emit('addSosMsg', 'nullName');
                    console.log('Name NULL' + data.Name)

                } else if (doc.Name == data.Name && doc.Res == data.Res) {
                    socket.emit('addSosMsg', 'err');
                    console.log('This SOS Res already exist!');
                    return
                } else {
                    SOS_DT.find({
                        Res: data.Res,
                        status: "recorded"
                    }, (err, doc2) => {
                        if (err) {
                            console.log(err);
                        } else {
                            if (!doc2) {
                                socket.emit('addSosMsg', 'nullRes');
                                console.log('This SOS Res cannot be null!');
                            } else {
                                var SOS = new SOS_DT();
                                SOS.agv_ID = data.agv_ID;
                                SOS.Name = data.Name;
                                SOS.Res = data.Res;
                                SOS.TS = new Date();
                                SOS.status = "recorded";
                                SOS.save((err, doc) => {
                                    if (err) {
                                        throw err;
                                    } else {
                                        socket.emit('addSosMsg', doc.Name);
                                        //console.log(doc);

                                        var ACT = new ACT_DTS();
                                        ACT.User_Name = info[0].Username;
                                        ACT.Activity = 'New SOS  : "' + doc.Name + '" has been added';
                                        ACT.Type = "SOS ABNORMALITY PAGE";
                                        ACT.TS = new Date();
                                        ACT.status = "recorded";

                                        ACT.save((err, docAT4) => {
                                            if (err) {
                                                //throw err;
                                                console.log(err);
                                            } else {
                                                console.log('New SOS  : "' + doc.Name + '" has been added');
                                                //socket.emit('deleteAGVs',doc);
                                            }
                                        })

                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    })


    socket.on('sosTrigger', data => {
        console.log(data);
        // SOS_DT_REC.find({agv_ID:data.agv_ID}).then(doc =>{
        //     var sos_id = doc._id;
        HEALTH_DT.findOne({
            AGV_ID: data.agv_ID
        }).sort({
            TS: -1
        }).then(doc2 => {
            if (!doc2) {
                console.log('err')

            } else {
                var SOS_REC = new SOS_DT_REC();
                SOS_REC.agv_ID = data.agv_ID;
                SOS_REC.sosid_LIST = [data.SOSID];
                SOS_REC.Lat = "FMS";
                SOS_REC.Long = "FMS";
                SOS_REC.Remark = "From FMS:: Override - " + data.Res + ": Remark >" +  data.Remarks;
                SOS_REC.user_ID = data.Operator;
                SOS_REC.TS = new Date();
                SOS_REC.status = 'recorded';

                SOS_REC.save((err, doc3) => {
                    if (err) {
                        //throw err;
                        socket.emit('trigSosMsg', 'err');
                    } else {
                        socket.emit('trigSosMsg', data.agv_ID);
                        //console.log(doc3);

                        var ACT = new ACT_DTS();
                        ACT.User_Name = info[0].Username;
                        ACT.Activity = 'SOS  : "' + data.Res + '" has been triggered for "' + data.agv_ID + '"';
                        ACT.Type = "SOS ABNORMALITY PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";

                        ACT.save((err, docAT4) => {
                            if (err) {
                                //throw err;
                                console.log(err);
                            } else {
                               // console.log('SOS  : "' + data.Res + '" has been triggered for "' + data.agv_ID + '"');
                                //socket.emit('deleteAGVs',doc);
                            }
                        })

                    }
                })
            }

        })
    })


}