var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
var User = mongoose.model('USR_DT');
const ACT_DTS = mongoose.model('ACT_DTS');
const NOTIFY = mongoose.model('NOTIFY');

var moment = require('moment');



function standardTime(el,tz) {
    try {
        if (tz) {
            tz = tz.toString();
        }
        else {
            tz = 'Asia/Singapore'
        }
        el = el.toLocaleString('en-US', { timeZone: tz });
        return el = moment(el, "ddd, DD MMM YYYY, hh:mm:ss A")._i;
    }
    catch (e) {
        console.log('time cnversion failed ' + e);
    }
}

const loggedin = (req, res, next) => {
    try {

        if (!req.isAuthenticated()) {

            res.render('login/index', {
                title: 'LOGIN',
                layout: false

            })
        }
        else {
            id = req.user.id;
            if (req.user.status != null && req.user.status == 'RESET') {
                const success_AccountReset = `>>>Account Reset<<<<br>Your account has been recently reset.<br>Please Create a new different password`;
                res.render('login/index', {
                    title: 'LOGIN',
                    layout: false,
                    reset_success_msg: success_AccountReset,
                    user: req.user.username,
                    ID: id
                })
                req.logout();
            }
            else if (req.user.status != null && req.user.status == "pwdexpired") {
                const success_PasswordExpired = ">>>Password Expired<<<<br>You have not changed your password in a long time, please change to a new password";
                res.render('login/index', {
                    title: 'LOGIN',
                    layout: false,
                    expired_password_msg: success_PasswordExpired,
                    user: req.user.username,
                    ID: id
                })
                req.logout();
            }
            else {
                User.findOneAndUpdate({ _id: id }, { $set: { Session: "LOGOUT", Last_LogOut_Time: standardTime(new Date()) } }, (err, success) => {
                    if (err) {
                        console.log('Failed to log user ' + req.user.username + ' out at ' + standardTime(new Date()))
                        console.log(err);
                    }
                })
                var ACT_REC = new ACT_DTS();
                ACT_REC.User_Name = req.user.username;

                var userName  = req.user.username;

                ACT_REC.Activity = 'Successfully Log Out';
                ACT_REC.Type = 'LOGIN PAGE';
                ACT_REC.TS = new Date();
                ACT_REC.status = 'recorded';
                ACT_REC.save(err=> {
                    if (err) {
                    } else {
                    }
                })
                req.logout()
                req.flash('success_msg', 'Successfully Logout ' + standardTime(new Date()));
                res.redirect('/login')
            }
        }
    }
    catch (e) {
        console.log("Error at login Controller " + e);
    }
}

router.get('/', loggedin, (req, res) => {
    res.render('login/index', {
        title: 'LOGIN',
        layout: false,
    })
})



router.get('/logout', loggedin, (req, res) => {
   
    var ACT_REC = new ACT_DTS();
    ACT_REC.User_Name = req.user.username;
    ACT_REC.Activity = 'Successfully Log Out';
    ACT_REC.Type = 'LOGIN PAGE';
    ACT_REC.TS = new Date();
    ACT_REC.status = 'recorded';
    ACT_REC.save(err=> {
        if (err) {
   
        }
    })

    req.logout()
    req.flash('success_msg', 'Successfully Logout ' + standardTime(new Date()));
    res.redirect('/login')

})



module.exports = router;