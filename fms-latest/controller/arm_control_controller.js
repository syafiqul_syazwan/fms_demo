const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const AGV_DT = mongoose.model('AGV_DT');
const ACT_DTS = mongoose.model('ACT_DTS');
const PRESET_DT = mongoose.model('PRESET_DT');
const ARM_DT = mongoose.model('ARM_DT');


const CAM_DT = mongoose.model('CAM_DT');
var info = [];
var THISUSER;
const loggedin = (req, res, next) => {
    if (req.isAuthenticated()) {
        THISUSER = req.user;
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        return res.redirect('./error')  	
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                         info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                         }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}

router.get('/',loggedin,(req,res) => {

    AGV_DT.find({status: 'recorded'},(err,docs) => {
        if(err){
            console.log(err);
        }
        else{
          ARM_DT.find({status: 'recorded'},(err,doc4) => {
              if(err){
                  console.log(err);
              }
              else{
                PRESET_DT.find({status: 'recorded'},(err,doc3) => {
                    if(err){
                        console.log(err);
                    }
                    else{
                      var act = [];
                      for(var j = 0; j < doc3.length;j++) {
                        var ps = "PRESET "+ (j+1).toString()
                        for(var i = 0; i < doc3.length;i++) {
                          if(doc3[i].name==ps)
                          {
                              const result = doc4.find(obj => obj._id == doc3[i].arm_ID) ? doc4.find(obj => obj._id == doc3[i].arm_ID).Name:  "404";
                            act[j]=result;
                          }
                        }
                        if(j==doc3.length-1)
                        {
                          res.render('armControl/index', {
                            title : "ARM CONTROL",
                            agv :  docs,
                            name : info[0].Name,
                            userid: info[0].userId,
                            role: info[0].Role,
                            pass: info[0].Password,
                            p1:act[0],
                            p2:act[1],
                            p3:act[2],
                            p4:act[3],
                            p5:act[4],
                          })                      
                        }
                      }

                    }
                })
              }
          })
        }
    })

})



module.exports = router;

module.exports.respond = socket => {

//--------------------------- SOCKET IO FUNCTIONS ---------------------------------//

    socket.on('sys/arm/mv1', data =>{
        sys_arm_mv1_routine(data);
    })

    socket.on('ATArmReq', data =>{
        at_arm_req_routine(data);
    })

    socket.on('Armcam/link',data =>{
    CAM_DT.findOne({AGV_ID:data},(err,doc)=>{
      //  console.log(doc)
      if(err){
            console.log('null')
      }else{
       // let encoded = base64encode('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAADh0lEQVRYhe2YX2/TZhTGn/ePwck0u4BUJKStagMqlRACqUrTNu1kEfwZJvEJdrF9mN3AB+CCrwC0KrQOTsINIBDaoEWi0iakSSSuSFr5/bOLKhAcN7Uda+Oiz+Xr1/bP55z3nEcGTnSi/1dk1MW6605SKe8pxn5bfvDgZZ4v9hxnllF6R1J6q/rw4V+pARs3bpzXlHpEqRIo/ai1ri6urb3OA86v1eYIsAWtz4LSbShVrayvf4jbS+MW6647CUo3T5nm1GXHIcWJCQuE1Ouue2VcOM9xZgmwWbBt+7LjkFPF4hSh9Il38+aFuP1DEay77iTTesswzelLS0uGYZpQUmK72RSf2u09Rchq1nR7jjPLGfMKtj1RqlQ44xzi4ABvfD8Mu93dEFiJpnsoglTKe1Dq0ky5bBimebjGGGbKZV60rO+p1o/9Wm0uLZxfq80xzp8MwgEAP30a0/PzhlZqmil1d4gnuiCV+oVQ+s/7Z8+EFOLzOuMcFxcX+XcZ0t1Pa9GyrEE4AFBSYvfFC6EpbTOlfo3eG3tIPMeZNTjfMi3rTNwD06Q7Lq1xz2JCrJQ3Nl4lAswLcly4kYDjQuYBdyxgVsi84BIBpoXUYRjmBZcYEPjS/Qu2bUdfLIXAdqMhekEQQGt93J40UykxYB8ShHhx7aIPAABHXesefkCqkZkKsA85KpLAYc+MwqWNXGZAYHRNDipLzUUVaxaOU3Vj4w+l9U+9Tqez3Wh8NXH6kkLgre+LbrsdQOvlLHCZAROLZErQV8oEOGiZjkox4xwXKxU+rlX7bw5Jsyl6nU6mQ5Iqgv02M6rPRWuScY7SwgIvWJYFQry0Vi0xYD9yR/bAZlP0giCIOziMc5QqFV60bYukhEwEOKrmlJTYabVEt9PZo0KsCimX94PgYxxklppMZBbSztY8/eSxdivr4M8LcqRhHdeV5AF5pOXPyzKNCzkEmCdcHpBDp5hRehtan/vx2rWhVvLW98WnDLO1P7u7QRBETzdlDD9cvcqJUhNUyt+j9w4BKsZ+JoT8udNqheH+/uev3Gm1RDcI9pgQq1l+gSyurb2WQixFW5A4OMC7p09DQuk7g/Nb0ftia7DuupNcqU1eKMyUFhaM98+fj2WZBjWY7qnr1/lOqxWGvd4uo7Q6f//+34kAgW/85xEAVNbXPxClqhp4lCcccJhuDaxo4NEouBOd6FvQv4/9bFxgtiyIAAAAAElFTkSuQmCC'); // "aGV5ICB0aGVyZQ=="
       // let decoded = base64decode(encoded); // "hey there"
    
       //console.log(decoded)
       // socket.emit('imglah',decoded)

       socket.emit('Armcam/linkS',doc)
          
          var ACT = new ACT_DTS();
          ACT.User_Name = info[0].Username;
          ACT.Activity = '"Start teleop for "'+ data +'"';
          ACT.Type = "MAP & TELEOP PAGE";
          ACT.TS = new Date();
          ACT.status = "recorded";

          ACT.save((err,docAT4) => {
              if(err){
                  //throw err;
                  console.log('null');
              } else {
                 console.log('"Start Arm Camera for "'+ data +'"');
              }
          })

      }
    })
    })




//--------------------------- ROUTINE FUNCTIONS ---------------------------------//

    sys_arm_mv1_routine = (data) => {
        var ACT = new ACT_DTS();
        ACT.User_Name = info[0].Username;
        ACT.Activity = 'Arm Control "'+ data + '" has been launched';
        ACT.Type = "ARM CONTROL PAGE";
        ACT.TS = new Date();
        ACT.status = "recorded";

        ACT.save((err,docAC) => {
            if(err){
                //throw err;
                console.log(err);
            } else {
                console.log('Arm Control "'+ data + '" has been launched');
               
            }
        })
    }

    at_arm_req_routine = (data) => {
        console.log(data)
        var ACT = new ACT_DTS();
        ACT.User_Name = info[0].Username;
        ACT.Activity = 'Arm Control for AGV : "'+ data + '" has started/stopped manual control';
        ACT.Type = "ARM CONTROL PAGE";
        ACT.TS = new Date();
        ACT.status = "recorded";

        ACT.save((err,docArm) => {
            if(err){
                //throw err;
                console.log(err);
            } else {
                console.log('Arm Control for AGV : "'+ data + '" has started/stopped manual control');
            }
        })
    }

} // End of Socket Module

