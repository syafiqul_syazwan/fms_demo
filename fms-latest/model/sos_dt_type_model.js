const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sos_dt_type = new Schema ({
    Type: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('SOS_DT_TYPE', sos_dt_type, 'SOS_DT_TYPE');