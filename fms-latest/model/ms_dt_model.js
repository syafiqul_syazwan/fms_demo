const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ms_dt = new Schema ({
    Name: String,
    agv_ID: String,
    MAP_ID: String,
    TS: Date,
    status: String
});


//ms_dt.path('TS').get(function(num) {
//    return num.toLocaleString() 
//});

module.exports = mongoose.model('MS_DT', ms_dt,'MS_DT');