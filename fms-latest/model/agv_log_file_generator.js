require('./db');
//require('./model/db');
const mongoose = require('mongoose');
const db = mongoose.model('HEALTH_DT');


var ObjectId = mongoose.Types.ObjectId();
var fs = require('fs');
const { exception } = require('console');


var FMSlogNm;
var currentDate = new Date();
var moment = require('moment');
moment.locale('sg');
var date = currentDate.getDate();
var month = currentDate.getMonth(); //Be careful! January is 0 not 1
var year = currentDate.getFullYear();
var hrs = currentDate.getHours();
var mins = currentDate.getMinutes();
var sec = currentDate.getSeconds();

year = year.toString();
month = (month + 1) < 10 ? "0" + (month + 1).toString() : (month + 1).toString();
date = date < 10 ? "0" + date.toString() : date.toString();
hrs = hrs < 10 ? "0" + hrs.toString() : hrs.toString();
mins = mins < 10 ? "0" + mins.toString() : mins.toString();
var dir = "log/";
var flag = true;
var auditflag = true;

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}
fs.appendFile(dir + 'AGV_Log_Activities.log', "\n[Script runned on " + new Date + "]\n", function(err) {});
var maindir = dir;
dir += year + month + date + "/";
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

var index = 0;
var flag = true;
var SKIP = 1;
var PREV = 0;
var LASTdate;
var LASTid;

async function runscript() {

    db.createIndexes({ status: -1, TS: 1 });
    try {
        COUNTME();
        await db.find({ status: "recorded", TS: { $lte: currentDate } }).sort({ TS: 1 }).limit(5000).then(async data => {

            if (data == undefined || data.length < 1) {
                FMSlogNm = date + "_" + month + "_" + year + "-" + hrs + "_" + mins + "-Ver " + ((index - 1) < 0 ? index : (index - 1));
                await fs.appendFile(dir + 'AGV_' + FMSlogNm + '.log', "\n[Script runned " + standardTime(new Date()) + " nothing to Audit]\n", function(err) {});

                await fs.appendFile(maindir + 'AGV_Log_Activities.log', "Success\n", function(err) {});
                console.log("Finishing task AGV...Thanks for using");
                process.exit(0);
                return;
                //if data is not found or no record found upon skipping
            } else {
                FMSlogNm = date + "_" + month + "_" + year + "-" + hrs + "_" + mins + "-Ver " + index;
                PREV = SKIP;
                SKIP = PREV + data.length;
                //process data and keep the loop
                //"AGV Health no. " + (PREV) + " - " + SKIP,
                await fs.appendFile(dir + 'AGV_' + FMSlogNm + '.log', "\n[AGV Record no. " + PREV + " - " + SKIP + "]\n", function(err) {});
                await fs.appendFile(dir + 'AGV_' + FMSlogNm + '.log', "\n" + JSON.stringify(data), async function(err) {
                    if (err) {
                        console.log("Error writing file");
                        await fs.appendFile(dir + 'Error_AGV_.log', "\nError when writing log\n: " + err + "\nat " + standardTime(new Date()) + " on attempt " + index + "\n", function(err) {});
                    } else {
                        LASTdate = data[data.length - 1].TS;
                        LASTid = data[data.length - 1]._id;
                        await db.updateMany({ status: "recorded", $or: [{ _id: { $lte: convertID(LASTdate) } }, { _id: LASTid }] }, { $set: { status: "audited" } }).sort({ TS: -1 }).exec(async function(err, Audited) {
                            if (!err) {
                                console.log("Audited " + (PREV) + " - " + SKIP);
                                index++;
                                runscript();
                            } else {
                                console.log("Error " + err);
                                if (flag) {
                                    SKIP = 0;
                                    console.log("Reattempting..");
                                    flag = false;
                                    runscript();
                                } else {
                                    console.log("Give up");
                                    await fs.appendFile(dir + 'Error_AGV_.log', "\nError when auditing: \n" + dbFindRecordErr + "\nat " + standardTime(new Date()) + " on attempt " + index, function(err) {});
                                    process.exit(1);
                                    return;
                                }
                            }
                        })
                    }
                });
            }
        }).catch(dbFindRecordErr => {
            console.log("Error " + dbFindRecordErr);
            if (flag) {
                console.log("Reattempting..");
                flag = false;
                runscript();
            } else {
                console.log("Give up");
                fs.appendFile(dir + 'Error_AGV_.log', "\nError encountered: " + dbFindRecordErr + " at " + standardTime(new Date()) + " on attempt " + index + "\n", function(err) {});
                fs.appendFile(maindir + 'AGV_Log_Activities.log', "Failed\n", function(err) {});
                process.exit(1);
                return;
            }

        })
    } catch (e) {
        console.log("DEL: Error encountered: " + e);
    }
}

function COUNTME() {
    db.count({ status: "recorded" }, null, { ts: { $lte: currentDate } }).then(data => {
        var counts = data;
        console.log("Outstanding Records " + counts);
    }).catch(error => {
        console.log("Could not count the db");
        console.log(error);
    });
}
runscript();

function convertID(ts) {
    var newID = require('mongoose').Types.ObjectId.createFromTime(ts / 1000);
    return newID;
}

function standardTime(el, tz) {
    try {
        if (tz) {
            tz = tz.toString();
        } else {
            tz = 'Asia/Singapore'
        }

        el = el.toLocaleString('en-US', { timeZone: tz });
        return el = moment(el, "ddd, DD MMM YYYY, hh:mm:ss A")._i;
    } catch (e) {
        console.log('time cnversion failed ' + e);
    }
}
