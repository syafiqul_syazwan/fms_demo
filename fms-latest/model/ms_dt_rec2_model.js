const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ms_dt_rec2 = new Schema ({
    MS_ID: String,
    MS_SCHED_ID: String,
    ACTVT_SEQ: String,
    ACTVT_PT: String,
    ACTVT_ACT: String,
    Operator: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('MS_DT_REC2', ms_dt_rec2,'MS_DT_REC2');