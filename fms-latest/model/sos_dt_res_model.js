const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sos_dt_res = new Schema ({
    
    Response: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('SOS_DT_RES', sos_dt_res, 'SOS_DT_RES');