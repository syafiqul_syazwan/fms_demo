const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sched_dt_pt = new Schema ({
    MS_ID:String,
    SCHED_ID:String,
    Loop:Number,
    Seq:Number,
    Exe:String,
    TS:Date,
    status: String
});

module.exports = mongoose.model('SCHED_DT_MS', sched_dt_pt, 'SCHED_DT_MS');