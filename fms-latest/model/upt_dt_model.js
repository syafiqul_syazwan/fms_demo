const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const upt_dt = new Schema({
    AGV_ID: String,  
    Ver: String,
    Rem: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('UPT_DT', upt_dt);