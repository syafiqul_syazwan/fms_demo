const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const diag_dt = new Schema ({
    agv_ID: String,
    ldr: {
        Twodf: String,
        Twodr: String,
        Threed : String
    },
    batt: String,
    mot: {
        l : String,
        r : String
    },
    cam: String,
    cpu: String,
    ram: String,
    llc: String,
    obu: String,
    esw: String,
    enc: {
         l : String,
         r : String
    },
    imu: String,
    tcam: String,
    tray: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('DIAG_DT', diag_dt,'DIAG_DT');




// const diag_dt = new Schema ({
//     agv_ID: String,
//     ldr: String,
//     batt: String,
//     mot: String,
//     cam: String,
//     cpu: String,
//     ram: String,
//     llc: String,
//     obu: String,
//     esw: String,
//     enc: String,
//     imu: String,
//     tcam: String,
//     tray: String,
//     TS: Date,
//     status: String
// });