const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sos_dt_rec = new Schema ({
    agv_ID: String,
    sosid_LIST: Array,
    Lat: String,
    Long: String,
    Remark: String,
    user_ID: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('SOS_DT_REC', sos_dt_rec,'SOS_DT_REC');