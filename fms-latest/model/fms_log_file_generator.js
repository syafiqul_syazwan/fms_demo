require('./db');

const mongoose = require('mongoose');

var fs = require('fs');

const ACT_DTS = mongoose.model('ACT_DTS');

var FMSlogNm;

var currentDate = new Date();
var moment = require('moment');
moment.locale('sg');
var date = currentDate.getDate();
var month = currentDate.getMonth(); //Be careful! January is 0 not 1
var year = currentDate.getFullYear();
var hrs = currentDate.getHours();
var mins = currentDate.getMinutes();
var sec = currentDate.getSeconds();
year = year.toString();
month = (month + 1) < 10 ? "0" + (month + 1).toString() : (month + 1).toString();
date = date < 10 ? "0" + date.toString() : date.toString();
hrs = hrs < 10 ? "0" + hrs.toString() : hrs.toString();
var dir = "log/";
var flag = true;
var auditflag = true;

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}
fs.appendFile(dir + 'FMS_Log_Activities.log', "\n[Script runned on " +standardTime(new Date()) + "]\n", function(err) {});
dir += year + month + date + "/";
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}
var maindir = dir;

FMSlogNm = date + "_" + month + "_" + year + "-" + hrs;
async function runLog() {
    await ACT_DTS.find({ status: "recorded" }, null, { TS: { $lte: currentDate } }).exec(async function(err, data) {
        if (err || data.length < 1) {
            if (err) {
                fs.appendFile(dir + 'Error_FMS_.log', "\nError while finding records " + standardTime(new Date())  + " with error:\n" + err + '\n');
                console.log("Error while running script");
                console.log(err);
                if (flag) {
                    console.log("Reattempting");
                    flag = false;
                    runLog();
                } else {
                	                await fs.appendFile(maindir  + 'FMS_Log_Activities.log', "Failed\n", function(err) {});
                                                        process.exit(1);
                    return;
                }
            } else {
                await fs.appendFile(dir + '1_FMS_' + FMSlogNm + '.log', '[Script runned ' + standardTime(new Date())  + ' nothing to Audit]\n', function(err) {});
                                await fs.appendFile(maindir  + 'FMS_Log_Activities.log', "Success\n", function(err) {});;
                console.log("No record to be audited");
                console.log("Finishing task FMS...Thanks for using");
                                    process.exit(0);
            return;
        }

        } else {
            console.log("Running Writing Script for no. of records: " + data.length);
            await fs.appendFile(dir + '1_FMS_' + FMSlogNm + '.log', '\n[Run on ' + standardTime(new Date())  + ']\n', function(err) {
                if (err) {
                    console.log("cp1");
                } else {
                    console.log("Title ammended");
                }
            })
            await fs.appendFile(dir + '1_FMS_' + FMSlogNm + '.log', JSON.stringify(data) + "\n", async function(err) {
                if (err) {
                    await fs.appendFile(dir + 'Error_FMS_.log', "\nError while writing log " + standardTime(new Date())  + " with error:\n" + err + '\n');
                    console.log("Error while writing log");

                    console.log(err);
                    if (flag) {
                        console.log("Reattempting");
                        flag = false;
                        runLog();
                    } else {
                        console.log("Giving up writing log");
                                            process.exit(1);
                        return;
                    }
                } else {
                    console.log("Log written Successfully");
                    await auditLog();
                }
            });
        }
    });
}

async function auditLog() {
    console.log("Running Audit");
    await ACT_DTS.updateMany({ TS: { $lte: currentDate }, status: "recorded" }, { $set: { status: "audited" } }, (err, data2) => {
        if (err) {
            console.log("Error while auditing");
            console.log(err);
            if (auditflag) {
                console.log("Reattempting");
                auditflag = false;
                auditLog();
            } else {
                console.log("Giving up Auditing");
                fs.appendFile(dir + 'Error_FMS_.log', "\nError while auditing records " + standardTime(new Date())  + " with error:\n" + err + '\n');
                     fs.appendFile(maindir  + 'FMS_Log_Activities.log', "Failed on auditing\n", function(err) {});
                                         process.exit(1);
                return;
            }
        } else {
            runLog();
        }
    })

}
runLog();
function standardTime(el, tz) {
    try {
        if (tz) {
            tz = tz.toString();
        } else {
            tz = 'Asia/Singapore'
        }

        el = el.toLocaleString('en-US', { timeZone: tz });
        return el = moment(el, "ddd, DD MMM YYYY, hh:mm:ss A")._i;
    } catch (e) {
        console.log('time cnversion failed ' + e);
    }
}