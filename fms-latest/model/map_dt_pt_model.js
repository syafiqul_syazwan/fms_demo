const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const map_dt_pt = new Schema ({
    Name:String,
    Map_ID:String,
    Loc_x:String,
    Loc_y:String,
    Loc_z:String,
    Loc_w:String,
    Lat:String,
    Long:String,
    TS:Date,
    status: String

});

module.exports = mongoose.model('MAP_DT_PT', map_dt_pt,'MAP_DT_PT');