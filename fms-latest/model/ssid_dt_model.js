const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ssid_dt = new Schema ({
    SS_ID: String,
    MS_ID: String,
});

module.exports = mongoose.model('SSID_DT', ssid_dt,'SSID_DT');