const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const act_dts = new Schema ({
   User_Name:String,
   Activity:String,
   Type:String,
   TS:Date,
   status: String
});

module.exports = mongoose.model('ACT_DTS', act_dts,'ACT_DTS');