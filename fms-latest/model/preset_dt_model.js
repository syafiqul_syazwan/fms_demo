const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const preset_dt = new Schema ({
    AGV_ID: String,
    name: String,
    arm_ID: String,
    TS: String,
    status: String
});

module.exports = mongoose.model('PRESET_DT', preset_dt,'PRESET_DT');