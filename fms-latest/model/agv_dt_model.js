const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const agv_dt = new Schema ({
    agv_ID: String,
    TS: Date,
    status: String
});

agv_dt.path('TS').get(function(num) {
    return num.toLocaleString() 
});

module.exports = mongoose.model('AGV_DT', agv_dt, 'AGV_DT' );