const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var bcrypt =require('bcrypt-nodejs');


const pwd_dt = new Schema({
    ID:String,
    Password: String,
    Password_Date: Date,
});


// usr_dt.path('TS').get(function(num) {
//     return num.toLocaleString() 
// });


pwd_dt.methods.hashPassword = function(password) {
    return bcrypt.hashSync(password,bcrypt.genSaltSync(10));
}

pwd_dt.methods.comparePassword = function(password,hash) {
    return bcrypt.compareSync(password,hash);
}

module.exports = mongoose.model('PWD_DT', pwd_dt,'PWD_DT');