const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ms_dt_rec = new Schema ({
    SS_ID: String,
    MS_ID: String,
    MS_SCHED_ID: String,
    ACTVT_SEQ: String,
    ACTVT_PT: String,
    ACTVT_ACT: String,
    // MS_Start: Date,
    // MS_End: Date,
    Operator: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('MS_DT_REC', ms_dt_rec,'MS_DT_REC');