const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var bcrypt =require('bcrypt-nodejs');


const usr_dt = new Schema ({
    Username: String,
    Name: String,
    Pass: String,
    Type: String,
    TS: Date,
    UserLoginTime: Date,
    Login_Attempt: Number,
    Login_Status: String,
    status: String,
    Reset_Status: String,
    Reset_Pwd: String,
    ACT_Time: Date,
    Session: String,
    Last_Login_Time: Date,
    Last_LogOut_Time: Date
});


// usr_dt.path('TS').get(function(num) {
//     return num.toLocaleString() 
// });


usr_dt.methods.hashPassword = function(password) {
    return bcrypt.hashSync(password,bcrypt.genSaltSync(5));
}

usr_dt.methods.comparePassword = function(password,hash) {
    return bcrypt.compareSync(password,hash);
}

module.exports = mongoose.model('USR_DT', usr_dt,'USR_DT');