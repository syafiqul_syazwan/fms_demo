const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const notiseen = new Schema ({
    USER_ID: String,
    NOTI_ID:String,
    status:String
});

module.exports = mongoose.model('NOTI_SEEN', notiseen,'NOTI_SEEN');