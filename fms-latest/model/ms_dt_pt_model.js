const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ms_dt_pt = new Schema ({
    Name: String,
    Map_ID: { type: Schema.Types.ObjectId, ref: 'MAP_DT' },
    Pt_x: String,
    Pt_y: String,
    Act_ID: { type: Schema.Types.ObjectId, ref: 'MS_DT_ACT' },
    TS: Date,
    status: String
});

module.exports = mongoose.model('MS_DT_PT', ms_dt_pt,'MS_DT_PT');