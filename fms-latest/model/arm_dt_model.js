const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const arm_dt = new Schema ({
   Name:String,
   Cmd: Object,
//    Cmd:{
//       cmd: String,
//       j1: String,
//       p1 : String,
//       fv1: String,
//       j2: String,
//       p2 : String,
//       fv2: String,
//       j3: String,
//       p3 : String,
//       fv3: String,
//       j4: String,
//       p4 : String,
//       fv4: String
//   },
   TS:Date,
   status: String
});

module.exports = mongoose.model('ARM_DT', arm_dt,'ARM_DT');