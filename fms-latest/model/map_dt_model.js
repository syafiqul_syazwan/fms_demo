const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const map_dt = new Schema ({
    Name: String,
    Filename: String,
    Home_x: String,
    Home_y: String,
    Home_z: String,
    Home_w: String,
    Home_Lat: String,
    Home_Long : String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('MAP_DT', map_dt,'MAP_DT');