const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const health_dt_s = new Schema ({
    Name: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('HEALTH_DT_S', health_dt_s);
