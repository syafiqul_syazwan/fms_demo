const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sos_dt = new Schema ({
    agv_ID:String,
    Name: String,
    Res: String,
    TS: Date,
    status: String
});

module.exports = mongoose.model('SOS_DT', sos_dt, 'SOS_DT');