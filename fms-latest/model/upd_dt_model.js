const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const upd_dt = new Schema ({
    AGV_ID: String,
    Ver: String,
    Rem: String,
    TS: Date,
    status:String
});

module.exports = mongoose.model('UPD_DT', upd_dt,'UPD_DT');