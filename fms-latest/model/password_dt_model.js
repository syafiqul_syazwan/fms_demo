const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var bcrypt =require('bcrypt-nodejs');


const password_dt = new Schema ({
    UserName: String,
    Password: String,
    Stat_Pass: String,
    TS: Date,
    status: String
});


// usr_dt.path('TS').get(function(num) {
//     return num.toLocaleString() 
// });

password_dt.methods.hashPassword = function(password) {
    return bcrypt.hashSync(password,bcrypt.genSaltSync(10));
}

password_dt.methods.comparePassword = function(password,hash) {
    return bcrypt.compareSync(password,hash);
}

module.exports = mongoose.model('PASSWORD_DT', password_dt,'PASSWORD_DT');