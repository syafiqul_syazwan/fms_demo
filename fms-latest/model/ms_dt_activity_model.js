const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ms_dt_activity = new Schema ({
   MS_ID:String,
   MAP_ID:String,
   PT_ID:String,
   ARM_ID:String,
   Seq:String,
   Exe:String,
   TS:Date,
   status: String
});

module.exports = mongoose.model('MS_DT_ACTIVITY', ms_dt_activity,'MS_DT_ACTIVITY');