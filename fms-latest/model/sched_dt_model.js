const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sched_dt_pt = new Schema ({
    Name:String,
    USR_ID:String,
    TS:Date,
    status: String
});

module.exports = mongoose.model('SCHED_DT', sched_dt_pt, 'SCHED_DT');