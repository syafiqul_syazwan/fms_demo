const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cam_dt = new Schema ({
    AGV_ID: String,
    Link_Cam: String,
    Link_PtCld: String,
    TS: String,
    status: String
});

module.exports = mongoose.model('CAM_DT', cam_dt,'CAM_DT');