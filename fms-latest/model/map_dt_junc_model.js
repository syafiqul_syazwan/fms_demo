const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const map_dt_junc = new Schema({
    map_id: String,
    x: String,
    y: String,
    Long : String,
    Lat : String,
    TS : Date,
    status: String
});


module.exports = mongoose.model('MAP_DT_JUNC', map_dt_junc, 'MAP_DT_JUNC');