const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const notify = new Schema ({
    AGV_ID: String,
    Type:String,
    Det:String,
    Lat:String,
    Long:String,
    status:String,
    TS:Date
});

module.exports = mongoose.model('NOTIFY', notify,'NOTIFY');