require('./model/db');

const fs = require('fs');
const session = require('express-session');
const passport = require('passport');
const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const bodyparser = require('body-parser');
const mqtt = require('mqtt');
const agvSettingC = require('./controller/agv_dt_controller');
const loginC = require('./controller/login_controller');
const dashboardC = require('./controller/dashboard_controller');
const armControlC = require('./controller/arm_control_controller');
const diagnosticC = require('./controller/diagnostic_controller');
const operatorC = require('./controller/operator_controller');
const sosC = require('./controller/sos_controller');
const teleopC = require('./controller/teleop_controller');
const errorC = require('./controller/error_controller');
const userC = require('./controller/user_controller');
const missionC = require('./controller/mission_controller');
const reportsC = require('./controller/reports_controller');
const utilityC = require('./controller/utility_controller');
const auth = require('./auth/auth')(passport);
const flash = require('connect-flash');
//const router = express.Router()

require('./passport')(passport);
require('events').EventEmitter.defaultMaxListeners = 21;

var app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const mongoose = require('mongoose');
const swal = require('sweetalert2');

//modal
const HEALTH_DT = mongoose.model('HEALTH_DT');
const DIAG_DT = mongoose.model('DIAG_DT');
const SOS_DT = mongoose.model('SOS_DT');
const SOS_DT_REC = mongoose.model('SOS_DT_REC');
const NOTIFY = mongoose.model('NOTIFY');
const NOTI_SEEN = mongoose.model('NOTI_SEEN');
const SCHED_DT = mongoose.model('SCHED_DT');
const MS_DT = mongoose.model('MS_DT');
const MAP_DT_PT = mongoose.model('MAP_DT_PT')
const MAP_DT = mongoose.model('MAP_DT')
//passport

// const passport = require('passport');
// require('./passport')(passport);

const PORT = 3000;

app.use(bodyparser.urlencoded({
  extended: true
}));

app.use(bodyparser.json());
app.use(express.static(__dirname + '/public/'));
app.set('auth', path.join(__dirname, 'auth/'));
app.set('views', path.join(__dirname, 'views/'));
app.engine('hbs',exphbs({extname:'hbs',defaultLayout:'mainLayout', layoutsDir: __dirname + '/views/layouts/',
helpers: {
  ifCond: function(v1, v2, options) {
    if(v1 === v2) {
      return options.fn(this);
    }
    return options.inverse(this);
  },
  iftrue: function(v1,options) {
    if(v1) {
      return options.fn(this);
    }
    return options.inverse(this);
  },
  inc: function(v1,options) {
    return parseInt(v1) + 1;
  },
  add: function(v1,v2,options) {
    return parseFloat(v1) + parseFloat(v2);
  },
  json: function(v1) {
    return JSON.stringify(v1);
  },
  /*loopbat: function(array) {
    var json = new Array();
    var tryparse = array;
    for(var i = 0; i < tryparse.length;i++) {
      console.log(tryparse.length)
      json.push(tryparse[i].Batt);
      console.log(json);
    }
    return JSON.stringify(json);
  }*/
}}));
app.set('view engine','hbs');
//app.set('trust proxy', 1) // trust first proxy


app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
   // cookie: { secure: true }
  })
);


// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.use(function(req,res,next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});


var KEY = fs.readFileSync('ONH001-AGV01-key.pem');
var CERT = fs.readFileSync('ONH001-AGV01.pem');
var CAfile = [fs.readFileSync('trusted_certificates.pem')];
var mqtthost = '18.140.250.237';

var options = {
host: mqtthost,
port: 8883,
protocol: 'mqtts',
username: 'agv01',
password: 'agv01',
ca: CAfile,
key: KEY,
cert: CERT,
secureProtocol: 'TLSv1_2_method',
rejectUnauthorized:false
};

const client = mqtt.connect(options);
//const client = mqtt.connect('mqtt://broker.mqttdashboard.com');

client.on('connect', () => {
    console.log('mqtt connected');
    client.publish('test.foo.bar', "This is fms");
    client.subscribe('/a2f/ms/ht/#', {qos: 2});
    client.subscribe('/a2f/sys/diag/#');
    client.subscribe('/a2f/nav/pt/#');
    client.subscribe('/f2a/sys/mv/#');
    client.subscribe('/f2a/arm/man/#');
    client.subscribe('/f2a/sys/man/#');//toggle
    client.subscribe('/a2f/arm/loc/#'); //arm location
    client.subscribe('/a2f/arm/fv/#'); //arm location
    client.subscribe('/a2f/arm/tray/#');//tray
    //client.subscribe('a2f.ms.sr'); //suspend resume from AGV
    client.subscribe('/a2f/sys/sos/#', {qos: 2});
    client.subscribe('/a2f/sys/cam/#');
})


//---------------------TOPIC FROM AGV TO FMS-------------------//

// var Lat_cur;
// var Long_cur;
// var AGV_cur;

io.on('connection',socketio => {
     client.on('message',(topic,msg) => {
      topic = "/"+topic.split('/')[1]+"/"+topic.split('/')[2]+"/"+topic.split('/')[3];
     //console.log(topic+":"+msg.toString());


//------------------------GET HEALTH DATA (/a2f/ms/ht)----------------------------------//


if(topic == '/a2f/ms/ht') {
  var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
  var jdata = JSON.parse(parsed.data); //parsed all nested string in data
  socketio.emit('agVhtOperator',jdata);
  health_routine(msg);
}

if(topic == '/a2f/sys/sos') {
  sos_routine(msg.toString());
}

  if(topic == '/a2f/sys/cam') {
    var parsed = JSON.parse(msg.toString());
    var camData = JSON.parse(parsed.data);
    //var SOSReceived = new Date();
   // console.log(camData);
    socketio.emit('imglah',camData.agv_ID);
  }

  //------------------arm tray---------------------------
  if(topic == '/a2f/arm/tray'){
    var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
    var TRdata = JSON.parse(parsed.data); //parsed all nested string in data
   // console.log(TRdata);
    socketio.emit('trayCont',TRdata);
  }
  //------------------SUSPEND RESUME TOPIC------------------------------------------------//
  //engineered by yunus and douzee
  // if(topic == 'a2f.ms.sr'){
  //   var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
  //   var SRdata = JSON.parse(parsed.data); //parsed all nested string in data
  //   console.log(SRdata);
  //   socketio.emit('suspendResume',SRdata);
  // }

  //---------------------------GET DIAGNOSIS DATA (/a2f/sys/diag)------------------------------//


  if(topic == '/a2f/sys/diag') {


    var stringLDR = {}

       var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
       var jdata = JSON.parse(parsed.data); //parsed all nested string in data

       stringLDR = ({Twodf:jdata.ldr['2df'],Twodr:jdata.ldr['2dr'],Threed:jdata.ldr['3d']})
       jdata.lidar = stringLDR;

      //console.log(msg.toString());
      let doc = new DIAG_DT();

      doc.agv_ID = jdata.agv_ID
      doc.ldr = stringLDR
      doc.batt =jdata.batt
      doc.mot =jdata.mot
      doc.cam =jdata.cam
      doc.cpu =jdata.cpu
      doc.ram =jdata.ram
      doc.llc =jdata.llc
      doc.obu =jdata.obu
      doc.esw =jdata.esw
      doc.enc = jdata.enc
      doc.imu =jdata.imu
      doc.tcam = jdata.tcam
      doc.tray = jdata.tray
      doc.TS = new Date()
      doc.status = "recorded";

      doc.save((err,doc) => {
        if(err) {
          console.log(err);
        }
        else {
            console.log("DiagAGV:",jdata);
            socketio.emit('agVdiag',jdata);

        }
      })

  }

//----------------------GET NAVIGATION (/a2f/nav/pt)--------------------------------//

  if(topic == '/a2f/nav/pt') {
    console.log(msg.toString());
  }

//----------------------GET ARM LOCATION DATA(/a2f/arm/loc)---------------------------------//
  if(topic == '/a2f/arm/loc') {

    var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
    var jdata = JSON.parse(parsed.data); //parsed all nested string in data

   socketio.emit('armLoc',jdata);
   //console.log(jsonObj.data)


}

//----------------------GET FINGER LOCATION DATA(/a2f/arm/loc)---------------------------------//

if(topic == '/a2f/arm/fv') {

  var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
  var jdata = JSON.parse(parsed.data); //parsed all nested string in data

  socketio.emit('fingerLoc',jdata);
 //console.log(jsonObj.data)


}


})

health_routine = (msg) => {

  var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
  var jdata = JSON.parse(parsed.data); //parsed all nested string in data

  function sr_noti(jdata){
    return new Promise((res,rej) => {
      HEALTH_DT.findOne({AGV_ID: jdata.agv_ID, status: "recorded"},{},{sort:{_id: -1}}).then(doc => {
        if(doc){

        var prevts = new Date(doc.TS).getTime();
        var currts = new Date().getTime();

        var diffts = currts - prevts;

        //console.log("prevts: "+prevts+" currts: "+currts+" diffts: "+diffts);

        if(diffts > 999){

          //console.log("doc.ESW : "+doc.ESW+" jdata.ESW : "+jdata.ESW);
          // Data Not Yet Exist
          if(!doc.ESW || !doc.SR){
            res(jdata);
          }
          // ESWITCH RELEASED
          if(doc.ESW == 1 && jdata.ESW == 0){
            //console.log("E-SWITCH RELEASED for "+jdata.agv_ID);
            let NOTI = new NOTIFY();

            NOTI.AGV_ID = jdata.agv_ID;
            NOTI.Type = "ESW";
            NOTI.Det = "E-Switch Released";
            NOTI.TS = new Date();
            NOTI.Lat = jdata.loc.lat;
            NOTI.Long = jdata.loc.long;

            NOTI.status = "recorded";

            NOTI.save((err,doc) => {
              if(err){

                console.log(err);

              } else if (doc){

                console.log(doc);
                //res(jdata);

              }
            })
            res(jdata);

          }
            // ESWITCH TRIGGERED
            else if (doc.ESW == 0 && jdata.ESW == 1){

              SOS_DT_REC.findOne({agv_ID: jdata.agv_ID,status: "recorded"},null,{sort:{TS: -1}}).then(prvdt => {

                console.log("prvdt.sosid_LIST.toString(): "+prvdt.sosid_LIST.toString());

                if(prvdt.sosid_LIST.toString() == '5e0e1452119f20805c774dc6'){

                  let NOTI = new NOTIFY();

                  NOTI.AGV_ID = jdata.agv_ID;
                  NOTI.Type = "SOS";
                  NOTI.Det = "Emergency Switch Triggered. 2 ";
                  NOTI.TS = new Date();
                  NOTI.Lat = jdata.loc.lat;
                  NOTI.Long = jdata.loc.long;
                  NOTI.status = "recorded";

                  NOTI.save((err,doc) => {
                    if(err){

                      console.log(err);

                    } else if (doc){

                      console.log(doc);

                    }
                  })

                }

              }).catch(err => {
                console.log(err);
              });

            }
          // RESUME TRIGGERED
          if(doc.SR == 1 && jdata.SR == 0){
            //console.log("RESUME TRIGGERED for "+jdata.agv_ID);
            console.log("doc.SR: "+doc.SR+" jdata.SR: "+jdata.SR);
            let NOTI = new NOTIFY();

            NOTI.AGV_ID = jdata.agv_ID;
            NOTI.Type = "RESUME";
            NOTI.Det = "RESUME TRIGGERED";
            NOTI.TS = new Date();
            NOTI.Lat = jdata.loc.lat;
            NOTI.Long = jdata.loc.long;
            NOTI.status = "recorded";

            NOTI.save((err,doc) => {
              if(err){

                console.log(err);

              } else if (doc){

                console.log(doc);
                //res(jdata);

              }
            })
            res(jdata);

          }
          // SUSPEND TRIGGERED
          else if(doc.SR == 0 && jdata.SR == 1){
            //console.log("SUSPEND TRIGGERED for "+jdata.agv_ID);
            let NOTI = new NOTIFY();

            NOTI.AGV_ID = jdata.agv_ID;
            NOTI.Type = "SUSPEND";
            NOTI.Det = "SUSPEND TRIGGERED";
            NOTI.TS = new Date();
            NOTI.Lat = jdata.loc.lat;
            NOTI.Long = jdata.loc.long;
            NOTI.status = "recorded";

            NOTI.save((err,doc) => {
              if(err){

                console.log(err);

              } else if (doc){

                console.log(doc);
                //res(jdata);

              }
            })

            res(jdata);
          }
          // CURRENTLY RESUME
          else if(doc.SR == 0 && jdata.SR == 0){
            //console.log("CURRENTLY RESUME for "+jdata.agv_ID);
            res(jdata);
          }
          // CURRENTLY SUSPEND
          else if(doc.SR == 1 && jdata.SR == 1){
            //console.log("CURRENTLY SUSPEND for "+jdata.agv_ID);
            res(jdata);
          }

        }


        }

      }).catch(err => {
        console.log(err);
      })
    })
  }

  function save_health(jdata){

    let doc = new HEALTH_DT();

    var spx = parseFloat(jdata.spd.x);
    var spdx = spx.toFixed(2);
    var spz = parseFloat(jdata.spd.z);
    var spdz = spz.toFixed(2);

    // // set global variable to be attached to sos data
    // Lat_cur = jdata.loc.lat;
    // Long_cur = jdata.loc.long;
    // AGV_cur = jdata.agv_ID;

    // console.log("spdx: "+spdx)

    doc.AGV_ID = jdata.agv_ID;
    doc.Lat = jdata.loc.lat;
    doc.Long =jdata.loc.long;
    doc.Point_X =jdata.pnt.x;
    doc.Point_Y =jdata.pnt.y;
    doc.Spd_X =spdx;
    doc.Spd_Z =spdz;
    doc.Batt = jdata.batt;
    doc.Tmp_L =jdata.tmp.l;
    doc.Tmp_R =jdata.tmp.r;
    doc.MS_ID =jdata.ms.ms_ID;
    doc.MS_SCHED_ID =jdata.ms.sched_ID;
    doc.MS_ACT_ID =jdata.ms.ACT_ID;
    doc.MS_point =jdata.ms.point;
    doc.MS_act = jdata.ms.act;
    doc.MS_ms_exe = jdata.ms.ms_exe;
    doc.MS_act_exe= jdata.ms.act_exe;
    doc.charge = jdata.charge;
    doc.ESW = jdata.ESW;
    doc.SR = jdata.SR;
    //doc.sos_err = jdata.sos.err;
    // doc.sos_oth = jdata.sos.oth;
    doc.obu_id = jdata.obu.obu_id;
    doc.obu_temp = jdata.obu.obu_temp;
    doc.obu_env_temp = jdata.obu.env_temp;
    doc.obu_humid = jdata.obu.humid;
    doc.TS = new Date();
    doc.status = "recorded";

    doc.save((err,doc) => {
      if(err) {
        console.log(err);
      }
      else {

        if(jdata.ms.ms_ID == ""){
          jdata.ms_ID = "";
          jdata.sched_ID = "";
          socketio.emit('agVht',jdata);
        }
        else
        {
          ms_idtofind = jdata.ms.ms_ID;
          sc_idtofind = jdata.ms.sched_ID;
          MS_DT.findOne({_id:ms_idtofind}).then(doc =>{
            if (err) {
                console.log(err);
              }
              else {
                SCHED_DT.findOne({_id:sc_idtofind}).then(doc2 =>{
                  if (err) {
                      console.log(err);
                    }
                    else {
                      MAP_DT_PT.find({Map_ID:doc.MAP_ID}).then(doc3 =>{
                        if (err) {
                            console.log(err);
                          }
                          else {
                            MAP_DT.findOne({_id:doc.MAP_ID}).then(doc4 =>{
                              if (err) {
                                  console.log(err);
                                }
                                else {
                                  jdata.ms_ID = doc.Name;
                                  jdata.sched_ID = doc2.Name;
                                  jdata.points = doc3;
                                  jdata.home = doc4;
                                  console.log(jdata);
                                  socketio.emit('agVht',jdata);
                                }
                            }).catch(err=>{
                              console.log(err);
                            })
                          }
                      }).catch(err=>{
                        console.log(err);
                      })
                    }
                }).catch(err=>{
                  console.log(err);
                })
              }
          }).catch(err=>{
            console.log(err);
          })

        }

      }
    })


  }

  process(jdata);

  async function process(jdata){
    var json = await sr_noti(jdata);

    if(json){
      save_health(json);
    }
  }

}

}) // End of socketio


sos_routine = (data) =>
{
  var parsed = JSON.parse(data);
  var cur_data = JSON.parse(parsed.data);

  //console.log(cur_data);

  SOS_DT_REC.findOne({agv_ID: cur_data.agv_ID, status:"recorded"},null,{sort:{_id: -1}}).then(doc => {
    //console.log(doc);
    if (doc) {

    var prvts = new Date(doc.TS).getTime();
    var curts = new Date().getTime();

    console.log("prvts: "+prvts+" curts: "+curts);

    var difts = curts - prvts;

    if(difts > 1500){

      var prev_data = JSON.parse(JSON.stringify(doc));
      // console.log("Previous:",prev_data.sosid_LIST);
      // console.log("Current:",cur_data.sos_ID);

      var json = {
          p_list: prev_data.sosid_LIST,
          p_ts: prev_data.TS
      }
      if(compare_array_is_not_same(json,cur_data.sos_ID)){
        save_sos(cur_data);
        console.log("Not Same");
      } else {
        console.log("Same");
      }

    }

    }
    else if(!doc){
      save_sos(cur_data); // in case of no data existed yet
    }
  }).catch(err => {
    console.log(err);
  });

  compare_array_is_not_same = (prev,curr_list) =>
  {
      var p_length = prev.p_list.length;
      var c_length = curr_list.length;
      var prev_list = prev.p_list;

      //console.log("prev: "+JSON.stringify(prev));

      var ctime = new Date().getTime();
      var ptime = new Date(prev.p_ts).getTime();

      var difference = (ctime - ptime);

      //console.log("difference: "+difference);

      //console.log("ctime: "+ctime+" ptime: "+ptime);
    if(p_length == c_length){
        //var n = p_length;
        //var cor = 0;
        var same = 0;
        var count = 0;
        var c_count = 0;
        for(var x = 0; x<p_length; x++){
          for(var y = 0; y<c_length; y++){
            if(prev_list[x] == curr_list[y]){
              //cor = cor + 1;

              same = same + 1;

            }

          }

          count = count + 1;

          if (p_length == count) {

            if(p_length == same){

              console.log("ctime: "+ctime+" ptime: "+ptime);

              console.log("difference: "+difference);

              if(difference > 6000000){ // 10 minutes = 600 000 milliseconds
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }

          }

        }
    }else{
      return true;
    }
  }

  save_sos = (c_data) =>
  {
    //console.log("AGV_cur: "+AGV_cur+" Lat_cur: "+Lat_cur+" Long_cur: "+Long_cur);
    let docu = new SOS_DT_REC;
    docu.agv_ID = c_data.agv_ID;
    docu.sosid_LIST = c_data.sos_ID;
    docu.Lat = c_data.Lat;
    docu.Long = c_data.Long;
    docu.Remark = "Triggered AGV";
    docu.TS = new Date();
    docu.user_ID = "AGV";
    docu.status = "recorded";
    docu.save((err, doc1) => {
      if (err) {
        console.log(err);
      } else if (doc1){
        console.log("New Data Saved");
        //console.log(doc1);

        var count = 0;
        // var sos_name_LIST = [];
        // var sos_msg;
        var sos_msg_LIST = [];

        for(x=0;x<docu.sosid_LIST.length;x++){
          SOS_DT.findOne({_id: docu.sosid_LIST[x], status: "recorded"}).then(doc2=>{

            // if(doc2){
            //   sos_name_LIST = sos_name_LIST.concat(" "+doc2.Name);
            // }

            if(doc2){
              if(doc2.Name == 'junc'){
                sos_msg_LIST = sos_msg_LIST.concat("Junction Detected!");
              }
              else if(doc2.Name == 'container_pfail'){
                sos_msg_LIST = sos_msg_LIST.concat("Pick Container Activity Failed.");
              }
              else if(doc2.Name == 'container_dfail'){
                sos_msg_LIST = sos_msg_LIST.concat("Drop Container Activity Failed.");
              }
              else if(doc2.Name == 'container_perr'){
                sos_msg_LIST = sos_msg_LIST.concat("Pick Container Activity Error.");
              }
              else if(doc2.Name == 'override'){
                sos_msg_LIST = sos_msg_LIST.concat("Override Initiated.");
              }
              else if(doc2.Name == 'low_batt'){
                sos_msg_LIST = sos_msg_LIST.concat("Low Battery.");
              }
              else if(doc2.Name == 'move_err'){
                sos_msg_LIST = sos_msg_LIST.concat("Move Error.");
              }
              else if(doc2.Name == 'no_exit'){
                sos_msg_LIST = sos_msg_LIST.concat("No Exit.");
              }
              else if(doc2.Name == 'no_charger'){
                sos_msg_LIST = sos_msg_LIST.concat("No Charging Dock Detected!");
              }
              else if(doc2.Name == 'no_table'){
                sos_msg_LIST = sos_msg_LIST.concat("No Table Detected!");
              }
              else if(doc2.Name == 'container_derr'){
                sos_msg_LIST = sos_msg_LIST.concat("Drop Container Activity Error.");
              }
              else if(doc2.Name == 'eswitch'){
                sos_msg_LIST = sos_msg_LIST.concat("Emergency Switch Triggered.");
              }
              else if(doc2.Name == 'high_humidity'){
                sos_msg_LIST = sos_msg_LIST.concat("High Humidity Detected!");
              }
              else if(doc2.Name == 'rain'){
                sos_msg_LIST = sos_msg_LIST.concat("Rain Detected!");
              }
              else if(doc2.Name == 'obs_long'){
                sos_msg_LIST = sos_msg_LIST.concat("Obstacle not moving after 30s. Suspending vehicle.");
              }
              else if(doc2.Name == 'obs_short'){
                sos_msg_LIST = sos_msg_LIST.concat("Obstacle Detected!");
              }
              else if(doc2.Name == 'offline'){
                sos_msg_LIST = sos_msg_LIST.concat("Offline.");
              }
              else if(doc2.Name == 'tcam'){
                sos_msg_LIST = sos_msg_LIST.concat("Tray Camera Error.");
              }
              else if(doc2.Name == 'tray'){
                sos_msg_LIST = sos_msg_LIST.concat("Tray Error.");
              }
              else if(doc2.Name == 'imu'){
                sos_msg_LIST = sos_msg_LIST.concat("IMU Error.");
              }
              else if(doc2.Name == 'arm'){
                sos_msg_LIST = sos_msg_LIST.concat("Arm Error.");
              }
              else if(doc2.Name == 'obu'){
                sos_msg_LIST = sos_msg_LIST.concat("OBU Error.");
              }
              else if(doc2.Name == 'llc'){
                sos_msg_LIST = sos_msg_LIST.concat("Low Level Controller Error.");
              }
              else if(doc2.Name == 'ldr_2df'){
                sos_msg_LIST = sos_msg_LIST.concat("Front 2D Lidar Error.");
              }
              else if(doc2.Name == 'ldr_3d'){
                sos_msg_LIST = sos_msg_LIST.concat("3D Lidar Error.");
              }
              else if(doc2.Name == 'cam'){
                sos_msg_LIST = sos_msg_LIST.concat("Camera Error.");
              }
              else if(doc2.Name == 'ldr_2dr'){
                sos_msg_LIST = sos_msg_LIST.concat("Rear 2D Lidar Error.");
              }
            }

            count = count + 1;

            if(count == docu.sosid_LIST.length){

              let NOTI = new NOTIFY();

              NOTI.AGV_ID = c_data.agv_ID;
              NOTI.Type = "SOS";
              NOTI.Det = sos_msg_LIST.toString();
              NOTI.TS = new Date();
              NOTI.Lat = c_data.Lat_cur;
              NOTI.Long = c_data.Long;
            //   if(AGV_cur.toString() == c_data.agv_ID.toString()){
            //     NOTI.Lat = Lat_cur;
            //     NOTI.Long = Long_cur;
            //   }
              NOTI.status = "recorded";

              NOTI.save((err,doc) => {
                if(err){

                  console.log(err);

                } else if (doc){

                  console.log(doc);

                }
              })

            }
          }).catch(err=>{
            console.log(err);
          })

        }
      }
    })
  }
}

//------------------------------Included File(FMS FOLDER FOR VIEW)-----------------------------------//

app.use('/agvSetting',agvSettingC);
app.use('/dashboard',dashboardC);
app.use('/armControl',armControlC);
app.use('/diagnostic',diagnosticC);
app.use('/mapControl',teleopC);
app.use('/mission',missionC);
app.use('/sos',sosC);
app.use('/userSetting',userC);
app.use('/login',loginC);
app.use('/operation',operatorC);
app.use('/auth',auth);
app.use('/error',errorC);
app.use('/utility',utilityC);
app.use('/reports',reportsC);

io.on('connection',sosC.respond); // user socketio
io.on('connection',dashboardC.respond);
io.on('connection',agvSettingC.respond); // agv socketio
io.on('connection',missionC.respond); // agv socketio
io.on('connection',userC.respond); // user socketio
io.on('connection',operatorC.respond); // user socketio
io.on('connection',diagnosticC.respond); // user socketio
io.on('connection',utilityC.respond);
io.on('connection',teleopC.respond);
io.on('connection',reportsC.respond);
io.on('connection',armControlC.respond);


//---------------------TOPIC FROM FMS TO AGV-------------------//




io.on('connection',socket => {


  //---------------------------------Diagnostic(/f2a/sys/diag)--------------------------------//
  socket.on('sys/diag',diagdata => {

    var strings = JSON.stringify(diagdata);
    var jobj = {data:strings};
    client.publish('/f2a/sys/diag',JSON.stringify(jobj));
    console.log(JSON.stringify(jobj));
  })

  //---------------------------------Toggle(/f2a/sys/man)--------------------------------//

  socket.on('sys/man',manToggle => {

    var strings = JSON.stringify(manToggle);
    var jobj = {data:strings};
    client.publish('/f2a/sys/man',JSON.stringify(jobj));
    //console.log(JSON.stringify(jobj));
  })

  //---------------------------------AGV MOVEMENT(/f2a/sys/mv)--------------------------------//

  socket.on('sys/mv',dmv => {
    var tempObj = {};
    tempObj.agv_ID = dmv.agv_ID;
    tempObj.x = "0";
    tempObj.z = "0";
    tempObj.cmd = "1";

    switch(dmv.agvMv) {
        case "f": tempObj.x = "0.3"; break;
        case "b": tempObj.x = "-0.3"; break;
        case "l": tempObj.z = "0.3"; break;
        case "r": tempObj.z = "-0.3"; break;
    }
    var strings = JSON.stringify(tempObj);
    var jobj = {data:strings};
   // console.log(jobj)
    client.publish('/f2a/sys/mv', JSON.stringify(jobj));
  })

   //--------------------------ARM MISSION (MANUAL CONTROL)(/f2a/arm/man)--------------------------------//

   socket.on('sys/arm', function (message) {
    // console.log(message)
    // console.log('MSG: ' + message.valueArmID);
    // console.log(message.agv)
           var tempObj = {};
           tempObj.agv_ID = message.agv;
           tempObj.x = "0";
           tempObj.y = "0";
           tempObj.z = "0";
           tempObj.w = "0";
           tempObj.p = "0";
           tempObj.r = "0";
           // tempObj.f = "0";
           // tempObj.s = "0";
           tempObj.f1 = "0";
           tempObj.f2 = "0";

           switch(message.valueArmID) {
              case "xP": tempObj.x = "0.9"; break; //x
              case "xN": tempObj.x = "-0.9"; break; //x
              case "yP": tempObj.y = "0.9"; break; //y
              case "yN": tempObj.y = "-0.9"; break; //y
              case "zP": tempObj.z = "0.9"; break; //z
              case "zN": tempObj.z = "-0.9"; break;//z
              case "roP": tempObj.w = "10"; break; //w
              case "roN": tempObj.w = "-10"; break; //w
              case "ptP": tempObj.r = "10"; break; //p
              case "ptN": tempObj.r = "-10"; break; //p
              case "yaP": tempObj.p = "10"; break; //r
              case "yaN": tempObj.p = "-10"; break; //r
              case "fP": tempObj.f1 = "10";  //fP
              case "fP": tempObj.f2 = "10"; break; //fP
              case "fN": tempObj.f1 = "-10";  //fN
              case "fN": tempObj.f2 = "-10"; break; //fN
             }
           var strings = JSON.stringify(tempObj);
           var jobj = {data:strings};
           client.publish('/f2a/arm/man', JSON.stringify(jobj));
           console.log(jobj)
   });

   //-------------------------------------------------NOTISEEN-----------------------------------------------------------//

   socket.on('notiseen', data => {
    //console.log(JSON.stringify(data));

    var notid = data.notid;
    var userid = data.userid;
    var notify_TS = data.nots;

    let NS = new NOTI_SEEN();

    NS.NOTI_ID = notid;
    NS.USER_ID = userid;
    NS.NOTI_TS = notify_TS;
    NS.status = "recorded";

    NS.save((err,doc) => {
      if(err){
        console.log(err);
      } else {
        console.log(doc);
          // Trigger Global Refresh
          require('./controller/dashboard_controller.js')();

      }
    })

  })

  //-------------------------------------------------CLEAR_NOTI----------------------------------------------------------//

  socket.on('clear_noti', uid => {
    if(uid != '') {
      //console.log("Remove All Notification Button Clicked by User ID: "+ uid);
      clear_noti(uid);
    }
  })

  //--------------------------START SCHEDULE (/f2a/ms/st)--------------------------------//

  socket.on('getmsactSt', dataOps => {
    // if(mission1 == 'startSched') {
    // var json = {"data":"{\"agv_ID\":\"AT200_1\",\"ms_ID\":\"1\",\"fname\":{\"map\":\"OneNorth_1351434245\",\"Home_x\":\"0.1\",\"Home_y\":\"0.1\",\"Home_z\":\"0.1\",\"Home_w\":\"0.1\"},\"activity\":[{\"pt\":\"warehouse_point1\",\"x\":\"-1.51833570004\",\"y\":\"0.256689786911\",\"z\":\"0.95759344101\",\"w\":\"-0.288104772568\",\"arm\":{\"cmd\":\"pick3\",\"j1\":\"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\":\"-6088 -6088\",\"p1\": \"0.2834 0.0 0.08 0 0 90,0.0 -0.1572 0.0 0 0 0,0.0 0.0 -0.13408 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"232.24 200.18 125.30 360.22 281.63 54.82\",\"p2\": \"0.10855 0.268 0.06 0 0 0,0.03 0.3303 0.0 10 0 0,0.0 0.0 -0.19 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"190.94 189.38 124.55 354.16 253.45 -21.74\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}},{\"pt\":\"point_reverse\",\"x\":\"-0.438266992569\",\"y\":\"1.52551674843\",\"z\":\"0.937858462334\",\"w\":\"-0.346919089556\",\"arm\":\"wait\"},{\"pt\":\"warehouse_point2\",\"x\":\"-0.069412946701\",\"y\":\"5.98229503632\",\"z\":\"0.331173986197\",\"w\":\"0.943560242653\",\"arm\":\"wait\"},{\"pt\":\"warehouse_home\",\"x\":\"4.75163269043\",\"y\":\"4.53939819336\",\"z\":\"0.944541335106\",\"w\":\"-0.328045785427\",\"arm\": \"wait\"}]}"}
    var strings = JSON.stringify(dataOps);
    var jobj = {data:strings};
    client.publish('/f2a/ms/st',JSON.stringify(jobj));
    console.log(JSON.stringify(jobj));

        SCHED_DT.findOne({_id: dataOps.sched_ID, status: "recorded"}).then(doc => {
            if(doc){
              // console.log(dataOps.agv_ID)
              // console.log(doc.Name);

              var sched_nm = doc.Name;

              MS_DT.findOne({_id: dataOps.ms_ID, status: "recorded"}).then(doc2 => {
                if(doc2) {

                  var ms_nm = doc2.Name;

                  let NOTI = new NOTIFY();

                  NOTI.AGV_ID = doc2.agv_ID;
                  NOTI.Type = "MS_START";
                  NOTI.Det = "Schedule : "+sched_nm+",Mission : "+ms_nm;
                  NOTI.TS = new Date();
                  NOTI.status = "recorded";

                  NOTI.save((err,doc) => {
                    if(err){
                      console.log(err);
                    } else if(doc){
                        console.log("Schedule : "+sched_nm+",Mission : "+ms_nm+", Status : Started");
                    }

                    });

                }
              }).catch(err => {
                console.log(err);
              });

            }
          }).catch(err=>{
            console.log(err);
          })

    //    }
});

//--------------------SOS AGV-------------------------//

socket.on('sos/trig', dataTrig => {
    var strings = JSON.stringify(dataTrig);
    var jobj = {data:strings};
    client.publish('/f2a/sys/sos',JSON.stringify(jobj));
    console.log(JSON.stringify(jobj));

});

//---------------------------SHUT DOWN--------------------------------------//
socket.on('getSdReq', dataSd => {
var strings = JSON.stringify(dataSd);
var jobj = {data:strings};
client.publish('/f2a/sys/off',JSON.stringify(jobj));
console.log(JSON.stringify(jobj));
});

//-----------------------SUSPEND RESUME------------------------------//
//logic enginnered by douzee and yunus_yst
socket.on('reqSus',dataS =>{

  var string1 = JSON.stringify(dataS);
  var jobj = {data: string1};
  var string2 = JSON.stringify(jobj);

client.publish('/f2a/ms/sr',string2);
})

//--------------------------ARM MISSION (AUTOMATICALLY CONTROL) (PRESET BUTTON) (/f2a/arm/mv)--------------------------------//

socket.on('sys/arm/mv', preset => {
if(preset == 'preset1') {
var json = { "data": "{\"cmd\": \"start\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 189.51\",\"fv1\": \"-6000 -6000\",\"p1\": \"0.000 0.000 0.000 0 0 0\",\"fv2\": \"0 0\",\r\n  \"p2\": \"0.000 0.000 0.000 0 0 0\",\"j2\": \"273.49 163.35 65.14 359.13 260.16 189.51\",\"p3\": \"0.000 0.000 0.000 0 0 0\",\"fv3\": \"0 0\",\"p4\": \"0.000 0.000 0.000 0 0 0\",\"j3\": \"273.49 163.35 65.14 359.13 260.16 189.51\",\"fv4\": \"6000 6000\",\"j4\": \"273.49 163.35 65.14 359.13 260.16 189.51\"}"}
client.publish('/f2a/arm/pr',JSON.stringify(json));
}
if(preset == 'preset2') {

var json = {"data":"{\r\n  \"cmd\": \"pickup1\",\r\n  \"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\r\n  \"fv1\": \"-6000 -6000\",\r\n  \"p1\": \"0.2834 0.0 0.08 0 0 90,0.0 -0.1572 0.0 0 0 0,0.0 0.0 -0.16008 0 0 0\",\r\n  \"fv2\": \"6350 6350\",\r\n  \"p2\": \"0.0 0.0 0.13 0 0 0\",\r\n  \"j2\": \"170.10 195.00 109.57 360.21 273.44 169.86\",\r\n  \"p3\": \"-0.1482 -0.4708 -0.01 0 0 0,0.0 0.0 -0.10 0 0 0\",\r\n  \"fv3\": \"-6350 -6350\",\r\n  \"p4\": \"0.0 0.0 0.15 0 0 0\",\r\n  \"j3\": \"269.19 189.76 106.94 360.81 271.51 188.32\",\r\n  \"fv4\": \"6000 6000\",\r\n  \"j4\": \"269.19 84.68 39.73 359.89 239.92 365.12\"\r\n}"}
console.log(json)
client.publish('/f2a/arm/pr',JSON.stringify(json));

}
if(preset == 'preset3') {

var json = { "data": "{\"cmd\": \"kinovahome\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"-6000 -6000\",\"p1\": \"0.000 0.000 0.000 0 0 0\",\"fv2\": \"0 0\",\"p2\": \"0.000 0.000 0.000 0 0 0\",\"j2\": \"283.15 162.84 43.43 265.59 257.60 288.00\",\"p3\": \"0.000 0.000 0.000 0 0 0\",\"fv3\": \"0 0\",\"p4\": \"0.000 0.000 0.000 0 0 0\",\"j3\": \"283.15 162.84 43.43 265.59 257.60 288.00\",\"fv4\": \"6000 6000\",\"j4\": \"283.15 162.84 43.43 265.59 257.60 288.00\"}"}
client.publish('/f2a/arm/pr',JSON.stringify(json));

}
if(preset == 'preset4') {

var json = { "data": "{\"cmd\": \"attack\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"0 0\",\"p1\": \"0.000 0.000 0.000 0 0 0\",\"fv2\": \"-6000 -6000\",\"p2\": \"0.000 0.000 0.000 0 0 0\",\"j2\": \"269.12 171.66 75.59 360.08 187.31 310.33\",\"p3\": \"0.0 0.0 0.0 0 0 0\",\"fv3\": \"0 0\",\"p4\": \"0.000 0.000 0.000 0 0 0\",\"j3\": \"269.12 171.66 75.59 360.08 187.31 410.33\",\"fv4\": \"6000 6000\",\"j4\": \"269.12 171.66 75.59 360.08 187.31 410.33\"}"}
client.publish('/f2a/arm/pr',JSON.stringify(json));

}
if(preset == 'preset5') { //pick3

var json = { "data": "{\"cmd\": \"pickup\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"-6400 -6400\",\"p1\": \"0.283 0.000 0.080 0 0 90,0.000 -0.1572 0.000 0 0 0,0.000 0.000 -0.12 0 0 2\",\"fv2\": \"6250 6250\",\"p2\": \"0.000 0.000 0.000 0 0 0\",\"j2\": \"232.24 200.18 125.30 360.22 281.63 54.82\",\"p3\": \"0.000 0.000 0.000 0 0 0\",\"fv3\": \"0 0\",\"p4\": \"0.000 0.000 0.000 0 0 0\",\"j3\": \"273.49 163.35 65.14 359.13 260.16 189.51\",\"fv4\": \"0 0\",\"j4\": \"273.49 163.35 65.14 359.13 260.16 189.51\"}"}
client.publish('/f2a/arm/pr',JSON.stringify(json));

}
if(preset == 'presetHome') { //drop3
var json = { "data": "{\"cmd\": \"home\",\"j1\": \"269.19 82.35 39.73 359.89 239.92 365.12\",\"fv1\": \"0 0\",\"p1\": \"0.000 0.000 0.000 0 0 0\",\"fv2\": \"0 0\",\"p2\": \"0.000 0.000 0.000 0 0 0\",\"j2\": \"269.19 82.35 39.73 359.89 239.92 365.12\",\"p3\": \"0.000 0.000 0.000 0 0 0\",\"fv3\": \"-6400 -6400\",\"p4\": \"0.000 0.000 0.000 0 0 0\",\"j3\": \"269.19 82.35 39.73 359.89 239.92 365.12\",\"fv4\": \"6000 6000\",\"j4\": \"269.19 82.35 39.73 359.89 239.92 365.12\"}"}

client.publish('/f2a/arm/pr',JSON.stringify(json));

}
})


})


function clear_noti(uid) {

  // var allntfy;
  // var allntsn;

  NOTI_SEEN.deleteMany({USER_ID: uid}, (err => {
    if (err) {
      console.log(err)
    } else {

      NOTIFY.find({},null,{sort:{_id: -1}}).then(doc => {
        if (doc) {

          //allntfy = doc;
          insert2notiseen(doc,uid);
          //console.log("hello");

        }
      }).catch(err => {
        console.log(err);
      })

    }
  }))

}

function insert2notiseen(ntfy,uid) {

  var cnt = 0;

  for(x=0;x<ntfy.length;x++) {

    let NS = new NOTI_SEEN();

    NS.NOTI_ID = ntfy[x]._id;
    NS.USER_ID = uid;

    NS.save((err,doc) => {
      if(err){
        console.log(err);
      } else {
        //console.log(doc);
      }
    })
    cnt = cnt + 1;
  }

  if (cnt == ntfy.length) {

   // console.log("All Notifications Removed for user id: "+ uid);

  }
}

http.listen(PORT, () =>{
  console.log('listening on PORT: ' + PORT);
});