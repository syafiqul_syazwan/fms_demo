const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const health_dt = new Schema ({
    AGV_ID: String,
    Lat: String,
    Long: String,
    Point_X: String,
    Point_Y: String,
    Spd_X: String,
    Spd_Z: String,
    Batt: String,
    Tmp_L: String,
    Tmp_R: String,
    MS_ID: String,
    MS_SCHED_ID: String,
    MS_ACT_SEQ: String,
    //MS_ACT_ID: String,
    MS_point: String,
    MS_act: String,
    // MS_ms_exe: String,
    // MS_ms_act_exe: String,
    charge: String,
    SR: String,
    // sos_err: String,
    // sos_oth: String,
    obu_env_temp: String,
    obu_humid: String,
    obu_id: String,
    obu_temp: String,
    rainfallIntensity: String,
    TS: Date,
    status: String
});



module.exports = mongoose.model('HEALTH_DT', health_dt,'HEALTH_DT');