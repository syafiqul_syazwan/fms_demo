var express= require('express');
var router = express.Router();
var mongoose = require('mongoose');
var HEALTH_DT = mongoose.model('HEALTH_DT');
var MS_DT = mongoose.model('MS_DT');
var USR_DT = mongoose.model('USR_DT');
var AGV_DT = mongoose.model('AGV_DT');
var ACT_DTS = mongoose.model('ACT_DTS');
var NOTIFY = mongoose.model('NOTIFY');
var NOTI_SEEN = mongoose.model('NOTI_SEEN');

var noti;
var count;

var usernow;

var info = [];

const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
		//console.log(req.user)
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                        }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                      
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}


router.get('/',loggedin ,(req,res) => {

    usernow = info[0].userId;
    //console.log(info[0].userId)

    //---------------- NOTIFICATION ------------------//

    NOTIFY.find({},null,{sort:{TS: -1}},(err,doc)=>{
        if(err)
        console.log(err);
        else{

            noti = doc;
            count = doc.length;
            //console.log(noti);

            HEALTH_DT.find({}).limit(1).sort({TS:-1}).exec((err, doc) => {
                if(err) {
                    console.log(err);
                }else{
                    AGV_DT.find({status: 'recorded'},(err,docs) => {
                        if(err){
                        console.log(err);
                        }
                        else{
                    
                            res.render('dashboard/index', {
                            title: 'DASHBOARD',
                            health_dt : doc,
                            agv :  docs,
                            health_bat : parseInt(doc[0].Batt)/56 * 100,
                            health_tmp : (parseInt(doc[0].Tmp_R) + parseInt(doc[0].Tmp_L))/2,
                            health_spd : (parseInt(doc[0].Spd_X) + parseInt(doc[0].Spd_Z))/2,
                            name : info[0].Name,
                            userid: info[0].userId,
                            role: info[0].Role,
                            pass: info[0].Password,
                            // notify: noti,
                            // noti_count: count
                            })
        
                        // console.log(info[0].userId)
                        }
                    })
                }
            })
        }
    })

})
// router.get('/', (req,res) => {
//     HEALTH_DT.find({}).limit(10).sort({TS:-1}).exec((err, doc) => {
//         if(err) {
//             console.log(err);
//         }else{
//             //console.log(doc)
//              AGV_DT.find({},(err,doc2) =>{
//                  if(err) {
//                      console.log(err);
//                  }else{

//         res.render('dashboard/index', {
//             title: 'DASHBOARD',
//             health_dt : doc,
//             health_bat : parseInt(doc[0].Batt)/40 * 100,
//             health_tmp : (parseInt(doc[0].Tmp_R) + parseInt(doc[0].Tmp_L))/2,
//             health_spd : (parseInt(doc[0].Spd_X) + parseInt(doc[0].Spd_Z))/2,
//             agv_list : doc2
                   
//                    //  })
//                 // }  
//                // }
//                       })
//                     //   }
//                 //    })
//                  }

//             })
//         }
//     })
// })
module.exports = router; 

module.exports.respond = socket => {


//----------------------REALTIME NOTIFICATION - START--------------------------//

NOTIFY.findOne({},null,{sort:{_id: -1}}).then(doc => {
    if (doc) {
      noti_now = doc._id;
    }
  }).catch(err => {
    console.log(err);
  });

function intervalFunc() {
    // console.log(noti_now);
  
    NOTIFY.findOne({},null,{sort:{_id: -1}}).then(doc2 => {
      if (doc2) {
        if(noti_now != doc2._id) {
          //refreshNoti();
          refresh_noti_async();
          noti_now = doc2._id;
        }
      }
    }).catch(err => {
      //console.log(err);
      //noti_now = 0;
    });

  }


//------------------async this---------------------//
  
  function refreshNoti() {

    return new Promise((res,rej) => {

        console.log("refreshNoti()");

            // var notid = data.notid;
    var userid = usernow;

    var allnotify;
    var allnotiseen;



        NOTIFY.find({},null,{sort:{_id: -1}}).then(doc => {
          if(doc) {
            allnotify = doc;

            NOTI_SEEN.find({USER_ID: userid}).then(doc1 => {
                //console.log("NOTI_SEEN doc1: "+ doc1.length);
              if(doc1.length > 0) {
                allnotiseen = doc1;

                var json = {allnotify: allnotify, allnotiseen: allnotiseen};

                  //showNoti_ByUserId(json);
                  res(json);
              } else {
                  //console.log("NOTI_SEEN is empty");
                  socket.emit('refresh_noti', allnotify);
              }
            }).catch(err => {
              //console.log(err);
              rej(err);
            })

          }

        }).catch(err => {
          //console.log(err);
          rej(err);
        })

    })






    




  
    //   NOTIFY.find({},null,{sort:{TS: -1}}).then(doc3 => {
    //     if (doc3) {
    //       socket.emit('refresh_noti',doc3);
    //     }
    //   }).catch(err => {
    //     console.log(err);
    //   });
  }

  function showNoti_ByUserId(json) {

    return new Promise((res,rej) => {

        console.log("showNoti_ByUserId(json)");

        var index2del = [];

        var allnotiseen = json.allnotiseen;
        var allnotify = json.allnotify;
    
        for(x=0;x<allnotiseen.length;x++){
          for(y=0;y<allnotify.length;y++){
            if(allnotiseen[x].NOTI_ID == allnotify[y]._id){
    
              index2del = index2del.concat(y);
    
            }
          }

        
    
          if(x<allnotiseen.length){
            //process_noti();
            var json2 = {allnotify: allnotify, index: index2del};
            res(json2);
            index2del = [];
          }
        }
    
        //console.log(index2del.toString());

    })


  }

  function process_noti(json2) {

    console.log("process_noti(json2)");


        for(z=0;z<json2.index.length;z++){
            json2.allnotify.splice(json2.index[z], 1);
      
            if(z<json2.index.length){
              socket.emit('refresh_noti', json2.allnotify);
              //index = [];
            }
          }

  }

refresh_noti_async();

async function refresh_noti_async(){
    var alldata = await refreshNoti();
    var json2 = await showNoti_ByUserId(alldata);
    process_noti(json2);
    
}
  
setInterval(intervalFunc, 1500);
//setTimeout(intervalFunc, 1500);

//----------------------REALTIME NOTIFICATION - END--------------------------//
    
    socket.on('reqAgvHealth',data => {
		console.log(data);
		HEALTH_DT.find({AGV_ID:data.agv_Id}).limit(500).sort({TS:-1}).exec((err,doc) => {
			if(err) {
				console.log(err);
			}
			else {
				MS_DT.find({_id:doc.MS_ID},(err,doc2)=>{
					if(err) {
						console.log(err);
					}
					else {
						console.log(doc.MS_ID)
						socket.emit('resAgvHealth',doc);
                    
                        
                        var ACT = new ACT_DTS();
                        ACT.User_Name = info[0].Username;
                        ACT.Activity = "Health AGV Status Record has been filtered for '"+ data.agv_Id +"'";
                        ACT.Type = "DASHBOARD PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";

                        ACT.save((err,docAT4) => {
                            if(err){
                                //throw err;
                                console.log(err);
                            } else {
                                console.log("Health AGV Status Record has been filtered for '"+ data.agv_Id +"'");
                                //socket.emit('deleteAGVs',doc);
                            }
                        })
					}

				})
			}
		});
    })

    //logic engineered by yunus
    socket.on('reqHealth',data => {
        var dateF = new Date();
        var dateT = new Date();
        var FilterOpener = 0;
        var FilterCloser = 0;
        var FilterFrom = 0;
        var FilterTo = 0;
        var jsonArray=[];
        var MissController = 0;

        console.log(data);//display the message from reqDateDiag
        // Find all data in DIAG_DT
        var ACT = new ACT_DTS();
        ACT.User_Name = info[0].Username;
        ACT.Activity = "Health AGV Status Record has been filtered from '"+ data.dateFrom +"' to '" + data.dateTo + "'";
        ACT.Type = "DASHBOARD PAGE";
        ACT.TS = new Date();
        ACT.status = "recorded";

        ACT.save((err,docAT4) => {
            if(err){
                //throw err;
                console.log(err);
            } else {
                console.log("Health AGV Status Record has been filtered from '"+ data.dateFrom +"' to '" + data.dateTo + "'");
                //socket.emit('deleteAGVs',doc);
            }
        })


        HEALTH_DT.find({},(err, docs) => {
            
           if(err){
               return;
           }
           if (docs.length) {//make sure DIAG_DT is content
                //do while is for controlling for loop
                //Whenever for loop end, the for loop will start again if date hit by user not found in database
                //The for loop will restart again with adding or substracting the input date 
                do{
                    //loop for the from date hit by user
                    //loop until it found and it will break
                    for(var FilterFrom=0;FilterFrom<docs.length;FilterFrom++){
                        //store the database date to dateFTSDiag to change its format.....for e.g 15/12/1996
                        var dateFTSDiag = new Date(docs[FilterFrom].TS);
                        var json = {dateResult:((dateFTSDiag.getDate() > 9) ? dateFTSDiag.getDate() : ('0' + dateFTSDiag.getDate()))  + '/' + ((dateFTSDiag.getMonth() > 8) ? (dateFTSDiag.getMonth() + 1) : ('0' + (dateFTSDiag.getMonth() + 1))) + '/' + dateFTSDiag.getFullYear()};
                        //this selection act like a door
                        //it will only open when date from is same as date in the database
                        if(json.dateResult == data.dateFrom){
                            //FilterFrom will be initialise inside filterOpener
                            //It act like escape tool for do while loop and limiter for FilterTo
                            FilterOpener = FilterFrom;
                            //dateF will act like important variable to allow data send to another topic in view
                            dateF = json.dateResult;
                            //loop for the to date hit by user
                            for(var FilterTo=FilterOpener;FilterTo<docs.length;FilterTo++){
                                //store the database date to dateTTSDiag to change its format.....for e.g 15/12/1996
                                var dateTTSDiag = new Date(docs[FilterTo].TS);
                                var json2 = {dateResult2:((dateTTSDiag.getDate() > 9) ? dateTTSDiag.getDate() : ('0' + dateTTSDiag.getDate()))  + '/' + ((dateTTSDiag.getMonth() > 8) ? (dateTTSDiag.getMonth() + 1) : ('0' + (dateTTSDiag.getMonth() + 1))) + '/' + dateTTSDiag.getFullYear()};
                                //this selection act like a door
                                //it will only open when date to is same as date in the database
                                if(json2.dateResult2 == data.dateTo){
                                    //FilterTo will be initialise inside filterCloser
                                    //It act like a limiter for the for loop where the data will be send to topic receive
                                    //  in view 
                                    FilterCloser=FilterTo;
                                    //dateT will act like important variable to allow data send to another topic in view
                                    dateT = json2.dateResult2;
                                }
                                else{
                                    continue;//continue the loop
                                }
                            }
                            if((FilterOpener!=0) && (FilterCloser!=0)){
                                break;//out of loop
                            }
                            if(((docs.length==FilterTo) && (FilterOpener>0)) && (FilterCloser==0)){//to date substractor
                              //  console.log(data.dateTo);
                                var txt = data.dateTo;
                                var splited = txt.split("/");//split date month and year....for e.g 15/12/1996
                                var dAte = splited[0];
                                var mOnth = splited[1];
                                var yEar = splited[2];
                                //change mOnth to format needed by date
                                if(mOnth == "01"){
                                    mOnth = "Jan";
                                }
                                else  if(mOnth == "02"){
                                    mOnth = "Feb";
                                }
                                else  if(mOnth == "03"){
                                    mOnth = "Mar";
                                }
                                else  if(mOnth == "04"){
                                    mOnth = "Apr";
                                }
                                else  if(mOnth == "05"){
                                    mOnth = "May";
                                }
                                else  if(mOnth == "06"){
                                    mOnth = "June";
                                }
                                else  if(mOnth == "07"){
                                    mOnth = "July";
                                }
                                else  if(mOnth == "08"){
                                    mOnth = "Aug";
                                }
                                else  if(mOnth == "09"){
                                    mOnth = "Sept";
                                }
                                else  if(mOnth == "10"){
                                    mOnth = "Oct";
                                }
                                else  if(mOnth == "11"){
                                    mOnth = "Nov";
                                }
                                else  if(mOnth == "12"){
                                    mOnth = "Dec";
                                }
                                // console.log(dAte);
                                // console.log(mOnth);
                                // console.log(yEar);
                                var day = new Date(mOnth + ' ' + dAte + ' ' + yEar);//day according to format
                                //console.log(day); // Apr 30 2000
        
                                var nextDay = new Date(day);
                                nextDay.setDate(day.getDate() - 1);//substract the date
                              //  console.log("next day" + (( nextDay.getDate() > 9) ?  nextDay.getDate() : ('0' +  nextDay.getDate()))  + '/' + (( nextDay.getMonth() > 8) ? ( nextDay.getMonth() + 1) : ('0' + ( nextDay.getMonth() + 1))) + '/' +  nextDay.getFullYear()); // May 01 2000  
                                data.dateTo = ((( nextDay.getDate() > 9) ?  nextDay.getDate() : ('0' +  nextDay.getDate()))  + '/' + (( nextDay.getMonth() > 8) ? ( nextDay.getMonth() + 1) : ('0' + ( nextDay.getMonth() + 1))) + '/' +  nextDay.getFullYear());
                              //  console.log("true date" + data.dateTo);
                                FilterFrom = 0;//FilterFrom set to 0 to restart the for loop
                            }
                        }
                        else{
                            continue;//continue the loop
                        }
                    }    
                    if((docs.length==FilterFrom) && (FilterOpener==0)){//from date additioner
                        //console.log(data.dateFrom);
                        var txt = data.dateFrom;
                        var splited = txt.split("/");//split date month and year....for e.g 15/12/1996
                        var dAte = splited[0];
                        var mOnth = splited[1];
                        var yEar = splited[2];
                        //change mOnth to format needed by date
                        if(mOnth == "01"){
                            mOnth = "Jan";
                        }
                        else  if(mOnth == "02"){
                            mOnth = "Feb";
                        }
                        else  if(mOnth == "03"){
                            mOnth = "Mar";
                        }
                        else  if(mOnth == "04"){
                            mOnth = "Apr";
                        }
                        else  if(mOnth == "05"){
                            mOnth = "May";
                        }
                        else  if(mOnth == "06"){
                            mOnth = "June";
                        }
                        else  if(mOnth == "07"){
                            mOnth = "July";
                        }
                        else  if(mOnth == "08"){
                            mOnth = "Aug";
                        }
                        else  if(mOnth == "09"){
                            mOnth = "Sept";
                        }
                        else  if(mOnth == "10"){
                            mOnth = "Oct";
                        }
                        else  if(mOnth == "11"){
                            mOnth = "Nov";
                        }
                        else  if(mOnth == "12"){
                            mOnth = "Dec";
                        }
                        // console.log(dAte);
                        // console.log(mOnth);
                        // console.log(yEar);
                        var day = new Date(mOnth + ' ' + dAte + ' ' + yEar);//day according to format
                        //console.log(day); // Apr 30 2000

                        var nextDay = new Date(day);
                        nextDay.setDate(day.getDate() + 1);//add the date
                       // console.log("next day" + (( nextDay.getDate() > 9) ?  nextDay.getDate() : ('0' +  nextDay.getDate()))  + '/' + (( nextDay.getMonth() > 8) ? ( nextDay.getMonth() + 1) : ('0' + ( nextDay.getMonth() + 1))) + '/' +  nextDay.getFullYear()); // May 01 2000  
                        data.dateFrom = ((( nextDay.getDate() > 9) ?  nextDay.getDate() : ('0' +  nextDay.getDate()))  + '/' + (( nextDay.getMonth() > 8) ? ( nextDay.getMonth() + 1) : ('0' + ( nextDay.getMonth() + 1))) + '/' +  nextDay.getFullYear());
                       // console.log("true date" + data.dateFrom);
                        FilterFrom = 0;//FilterFrom set to 0 to restart the for loop
                    }
                    //logic still in progress
                    if(data.dateFrom==data.dateTo){
                        FilterFrom=0;
                        MissController=1;
                    }
                }while(FilterFrom==0)//loop end when FilterFrom is not equalt to 0
                
              //  console.log(FilterOpener);
              //  console.log(FilterCloser);
              //  console.log(dateF);
              //  console.log(dateT);
                //act as a door...only enter when user input and process date are the same
                if((dateF == data.dateFrom) && (dateT == data.dateTo)){
                  //  console.log("filled");
                    for(var m=FilterOpener;m<FilterCloser;m++){
                        //console.log(docs[m]);
                        if(docs[m].AGV_ID == data.agv_Id){
                            jsonArray.push(docs[m]);//push the all the data in array according to the range of date
                        }
                    }
                }
                if(MissController==1){
                    socket.emit('resDateHealth','empty');
                }
                
            } 
            else {
                console.log("empty");
                // there are no users
            }
           // console.log(jsonArray);
            socket.emit('resDateHealth',jsonArray);//send the array to resDateDiag located in view
        })

    })




    socket.on('agvDash', datas =>{
       var jsonAgv = [];
       HEALTH_DT.find({AGV_ID:datas}).limit(1).sort({TS:-1}).exec((err, doc) => {
        if(!doc) {
            console.log('null');
        }else if(doc[0].Batt == ""){
            console.log('battery null')
        }else{
        var health_bat = parseInt(doc[0].Batt)/56 * 100;
        var health_tmp = (parseInt(doc[0].Tmp_R) + parseInt(doc[0].Tmp_L))/2;
        var health_spd = (parseInt(doc[0].Spd_X) + parseInt(doc[0].Spd_Z))/2;
        jsonAgv.push({agv_ID:datas,health_bat:health_bat,health_tmp:health_tmp,health_spd:health_spd})
        console.log(jsonAgv)
        socket.emit('agvDashS',jsonAgv)


                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = "Health AGV Status Record has been filtered for '"+ datas +"'";
                ACT.Type = "DASHBOARD PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err,docAT4) => {
                    if(err){
                        //throw err;
                        console.log(err);
                    } else {
                        console.log("Health AGV Status Record has been filtered for '"+ datas +"'");
                        //socket.emit('deleteAGVs',doc);
                    }
                })
            }
        })


    })

}




