const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const MS_DT = mongoose.model('MS_DT');
const MS_DT_ACTIVITY = mongoose.model('MS_DT_ACTIVITY');
const AGV_DT = mongoose.model('AGV_DT');
const SCHED_DT = mongoose.model('SCHED_DT');
const MAP_DT = mongoose.model('MAP_DT');
const ARM_DT = mongoose.model('ARM_DT');
const MAP_DT_PT = mongoose.model('MAP_DT_PT');
const SCHED_DT_MS = mongoose.model('SCHED_DT_MS');
const HEALTH_DT = mongoose.model('HEALTH_DT');
const MAP_DT_JUNC = mongoose.model('MAP_DT_JUNC');
const MS_DT_REC = mongoose.model('MS_DT_REC');
var NOTIFY = mongoose.model('NOTIFY');

var noti;
var count;
var usernow;
var sched_myid = "";
var execId; 
var statusStorage; 
var se; 
var PTSchem = []; 
var ARMSchem = []; 
var raw = []; 
var mission = []; 
var id; 
var TSstoreage = []; 
var statusStore = []; 
var seq;


const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
		//console.log(req.user)
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        return res.redirect('./error')  	
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        info.push({userId:id,Name:name,Role:role,Password:pass})
                        }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        info.push({userId:id,Name:name,Role:role,Password:pass})
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}

router.get('/',loggedin,(req,res) => {

    //---------------- NOTIFICATION ------------------//

    usernow = info[0].Name;

    NOTIFY.find({},null,{sort:{TS: -1}},(err,doc)=>{
        if(err)
        console.log(err);
        else{

            noti = doc;
            count = doc.length;

            AGV_DT.find({status:'recorded'},(err,doc2) => {
                if(err) {
                    console.log(err)
                }
                else {
            
                    
                SCHED_DT.find({status:'recorded'},(err,doc) => {
                
                //console.log(doc);
                    if(err) {
                        console.log(err)
                    }
                    else{
                        HEALTH_DT.find({}).limit(10).sort({TS:-1}).exec((err, doc3) => {
                
                            //console.log(doc);
                            if(err) {
                                console.log(err)
                            }
                            else{
                
                            res.render('operation/index', {
                                title : 'OPERATOR',
                                sched_dt : doc,
                                agv :doc2,
                                health_dt:doc3,
                                name : info[0].Name,
                                userid: info[0].userId,
                                role: info[0].Role,
                                pass: info[0].Password,
                                // notify: noti,
                                // noti_count: count
                                })
                            }
                        })
                        }
            
                    })
                    }
                    
                
                })
            


        }
    })

})
// router.get('/',(req,res) => {


// AGV_DT.find({},(err,doc2) => {
//     if(err) {
//         console.log(err)
//     }
//     else {

        
//     SCHED_DT.find({},(err,doc) => {
     
//      //console.log(doc);
//         if(err) {
//             console.log(err)
//         }
//         else{
     
//                 res.render('operation/index', {
//                     title : 'OPERATOR',
//                     sched_dt : doc,
//                     agv_dt : doc2
//             })
//             }

//           })
//         }
        
       
//     })
    
// })







module.exports = router

module.exports.respond = socket => {

    socket.on('reqmsdt',data => {
    var jsonarr = [];
    var interval = 1 * 1000; // 10 seconds;
    

        //console.log(doc9);
    SCHED_DT_MS.find({SCHED_ID:data.SCHED_ID}).then(doc => {
        console.log(doc)
        sched_myid = data.SCHED_ID;
        
        for(var i = 0; i < doc.length;i++) {
            setTimeout( function (i) {
                MS_DT.findOne({_id:doc[i].MS_ID}).then(doc2 => {
                    
                    MAP_DT.findOne({_id:doc2.MAP_ID}).then(doc3 => {

                        //AGV_DT.findOne({_id:doc2.agv_ID}).then(doc4 => { 
                       // console.log(doc4)
                        var json = {Name:doc2.Name,agv_ID:doc2.agv_ID,MAP_ID:doc3.Name,TS:doc2.TS,Loops:doc[i].Loop,fname:doc3.Filename,ms_ID:doc2._id,sequ:doc[i].Seq}
                        jsonarr.push(json);
                        if(i == doc.length-1) {
                        socket.emit('resmsdt',jsonarr);
                        //console.log(jsonarr);
                        }
                     
                    //},err => {
                    //    console.log(err)
                   // }) 

                    },err => {
                        console.log(err)
                    }) 
                },err => {
                    console.log(err)
                },err => {
                    console.log(err)
                }) 
            }, interval * i, i);
        }
    
    },(err) => {
        if(err) {
            console.log(err)
        }

    }) 
        
})
      
        socket.on('reqmsAgv', data => {
            var interval = 1 * 10; // 10 seconds;
            var jsonarr = [];
           // console.log(data)
            AGV_DT.findOne({agv_ID:data.AGV_ID},(err,doc) =>{
                //console.log(doc.agv_ID)
                if(err){
                    console.log(err)
                }else{
                   // console.log(doc)
                  
                    MS_DT.find({agv_ID:doc.agv_ID},(err,doc2) =>{
                        if(err){
                            console.log(err)
                        }else {
                            //console.log(doc2)
                               for(var i = 0; i < doc2.length;i++) {
                                       //console.log(doc2.length)
                                     setTimeout( function (i) {
                                             SCHED_DT_MS.find({MS_ID:doc2[i]._id},(err,doc3) =>{
                                                 if(err){
                                                      console.log(err);
                                                  }else{
                                                      if(!doc3){
                                                          console.log('takda')
                                                         // return ; 
                                                      }else{

                                                        //console.log(doc3)  
                                                        for(var j = 0; j < doc3.length;j++) {
                                                          setTimeout( function (j) {
                                                            
                                                            SCHED_DT.findOne({_id:doc3[j].SCHED_ID,status:'recorded'},(err,doc4) =>{
                                                                
                                                               
                                                                  if(!doc4){
                                                                        //console.log('yoooo');
                                                                    }else{
                                                                       // console.log(doc4)
                                                                            if(doc4 == ''){
                                                                                console.log('empty')
                                                                            }else{
                                                                                   
                                                                               // var distinctNames = SCHED_DT.distinct(doc4.Name);
                                                                                //console.log(distinctNames) 
                                                                                jsonarr = {SCHED_ID:doc4._id,SCHED_NAME:doc4.Name}
                                                                               
                                                                            }
                                                       
                                                                            if(j == doc3.length-1) {
                                                                            
                                                                                            socket.emit('resmsAgv',jsonarr);
                                                                                            console.log(jsonarr)
                                                                            // return doc3;
                                                                            }
                                                                        }
                                                                    })  
                                                                }, interval * j, j);
                                                            }
                                                          }
                                                     }
                                                 })
                                      }, interval * i, i);
                            }
                       
                      }

                  })
          
                
                }

            })
   
        })
    
    function findin_msdtact(match){
        //console.log(match);
        return new Promise((res, rej) => {
            MS_DT_ACTIVITY.find({MS_ID:match},(err,doc) => {
                if (err) rej(err);
                //console.log(doc);
                res(doc);
            });
        });
    }
    function findin_mapdt(match){
        //console.log(match);
        return new Promise((res, rej) => {
            MAP_DT.findOne({_id:match},(err,doc) => {
                if (err) rej(err);
                //console.log(doc);
                res(doc);
            });
        });
    }
    function findin_mapdtpt(match){
        //console.log(match);
        return new Promise((res, rej) => {
            MAP_DT_PT.findOne({_id:match},(err,doc) => {
                if (err) rej(err);
                //console.log(doc);
                res(doc);
            });
        });
    }
    function findin_arm_dt(match){
        //console.log(match);
        return new Promise((res, rej) => {
            ARM_DT.findOne({_id:match},(err,doc) => {
                if (err) rej(err);
                //console.log(doc);
                res(doc);
            });
        });
    }

    function findin_mapJunc(match){
        //console.log(match);
        return new Promise((res, rej) => {
            MAP_DT_JUNC.find({map_id:match},(err,doc) => {
                if (err) rej(err);
                //console.log(doc);
                res(doc);
            });
        });
    }

    function sortobj(objs)
    {
        objs.sort(function(a, b){
          //return a.Seq > b.Seq;
          return (a.Seq) - (b.Seq)
        });
        return objs;
    }




    // socket.on('getmsact', function(data) {
    //     //console.log(data);
    //     var agv_ID = data[0].agv_ID;
    //     var ms_ID =  data[0].ms_ID;
    //     var activity = [];
    //     var json={};
    //     var jsonMap2 = {};
    //     //var misStore = [];
    //     process();
    //     async function process(){
    //         for(var i = 0; i < data.length; i++) {
    //             var fx = await findin_msdtact(data[i].ms_ID);
    //             var fxx = sortobj(fx);
    //             var fy = await findin_mapdt(fx[i].MAP_ID);
    //             //console.log(data.length);
    //             var map = fy.Name;
    //             var Home_x = fy.Home_x;
    //             var Home_y = fy.Home_y;
    //             var Home_z = fy.Home_z;
    //             var Home_w = fy.Home_w;
    //             jsonMap2 = {map:map,Home_x:Home_x,Home_y:Home_y,Home_z:Home_z,Home_w:Home_w};
    //             var junc = await findin_mapJunc(fx[i].MAP_ID);
    //             var junct = [];
    //             for(var looper=0;looper<junc.length;looper++){
    //                 var jun = {"x":junc[looper].x,"y":junc[looper].y};
    //                 junct.push(jun);
    //             }
    //             for(var j = 0; j < fx.length;j++) {
    //                 var fz = await findin_mapdtpt(fx[j].PT_ID);
    //                 // console.log(fz);
    //                 var pt = fz["Name"];
    //                 var seq = fx[j].Seq;
    //                 var locx = fz["Loc_x"];
    //                 var locy = fz["Loc_y"];
    //                 var locz = fz["Loc_z"];
    //                 var locw = fz["Loc_w"];
                    
    //                 var fw = await findin_arm_dt(fx[j].ARM_ID);
    //                 var arm = fw.Cmd;
    //                 var jsonMap = {Seq:seq,pt:pt,x:locx,y:locy,z:locz,w:locw,arm:arm}
    //                 activity.push(jsonMap);
    //                 json = {agv_ID:agv_ID,ms_ID:ms_ID,sched_ID:sched_myid,fname:jsonMap2,junc:junct,activity:activity}
    //                 if(j == fx.length-1) {
    //                     console.log(json);
    //                     socket.emit('getmsactS',json);
    //                     socket.emit('reqActList',json.activity);
    //                 }
    //             } 
    //         } 
            
    //     }
    // })




    // function findin_execID(IDsc){ 
    //     return new Promise((res, rej) => { 
    //         SCHED_DT.findOne({_id:IDsc}).then(doc17 => { 
    //             ACT_DTS.findOne({Activity:doc17.Name}).sort({ "TS": -1 }).limit(1).exec((err,doc22)=>{ 
    //                 res(doc22); 
    //             }) 
    //         }); 
    //     }); 
    // } 
 
 
    socket.on('getmsact', function(data) { 
        //console.log(data); 
        var agv_ID = data[0].agv_ID; 
        var ms_ID =  data[0].ms_ID; 
        var activity = []; 
        var json={}; 
        var jsonMap2 = {}; 
        //var misStore = []; 
        process(); 
        async function process(){ 
            for(var i = 0; i < data.length; i++) { 
                //var at = await findin_execID(sched_myid); 
                execId = new Date().valueOf(); 
                var fx = await findin_msdtact(data[i].ms_ID); 
                var fxx = sortobj(fx); 
                console.log("holla"+fxx);
                var fy = await findin_mapdt(fxx[i].MAP_ID); 
                //console.log(data.length); 
                var map = fy.Name; 
                var Home_x = fy.Home_x; 
                var Home_y = fy.Home_y; 
                var Home_z = fy.Home_z; 
                var Home_w = fy.Home_w; 
                jsonMap2 = {map:map,Home_x:Home_x,Home_y:Home_y,Home_z:Home_z,Home_w:Home_w}; 
                var junc = await findin_mapJunc(fxx[i].MAP_ID); 
                var junct = []; 
                for(var looper=0;looper<junc.length;looper++){ 
                    var jun = {"x":junc[looper].x,"y":junc[looper].y}; 
                    junct.push(jun); 
                } 
                for(var j = 0; j < fxx.length;j++) { 
                    var fz = await findin_mapdtpt(fxx[j].PT_ID); 
                    // console.log(fz); 
                    var pt = fz["Name"]; 
                    var seq = fxx[j].Seq; 
                    var locx = fz["Loc_x"]; 
                    var locy = fz["Loc_y"]; 
                    var locz = fz["Loc_z"]; 
                    var locw = fz["Loc_w"]; 
                     
                    var fw = await findin_arm_dt(fxx[j].ARM_ID); 
                    var arm = fw.Cmd; 
                    var jsonMap = {Seq:seq,pt:pt,x:locx,y:locy,z:locz,w:locw,arm:arm} 
                    activity.push(jsonMap); 
                    json = {agv_ID:agv_ID,ms_ID:ms_ID,sched_ID:sched_myid,ss_id:execId,fname:jsonMap2,junc:junct,activity:activity} 
                    if(j == fxx.length-1) { 
                        //console.log(json); 
                        socket.emit('getmsactS',json); 
                        socket.emit('reqActList',json.activity); 
                    } 
                }  
            }  
             
        } 
    })

    socket.on('misRec', function(data){
        let docu = new MS_DT_REC;
        console.log(data.health.agv_ID);
        docu.SS_ID = data.health.ms.ss_id;
        docu.MS_ID = data.health.ms.ms_ID;
        docu.MS_SCHED_ID = data.health.ms.sched_ID;
        docu.ACTVT_SEQ = data.health.ms.act_Seq;
        docu.ACTVT_PT = data.health.ms.point;
        docu.ACTVT_ACT = data.health.ms.act;
        docu.Operator = usernow;
        docu.TS = new Date();
        docu.status = data.stat;
        docu.save((err, doc) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log(doc);
                //socketio.emit('agVht', jdata);

                MS_DT.findOne({_id: data.health.ms.ms_ID, status: "recorded"}).then(doc2 => {

                    if (data.status == 'done') {

                        if(doc2){

                            let NOTI = new NOTIFY();
    
                            NOTI.AGV_ID = doc2.agv_ID;
                            NOTI.Type = "mission_complete";
                            NOTI.Det = "Mission: "+doc2.Name+" > Completed";
                            NOTI.TS = new Date();
            
                            NOTI.save((err,doc) => {
                                if(err){
                                console.log(err);
                                } else {
                                console.log("Mission Activity Sent to DB");
                                }

                            })
    
                            }

                    }
                    //  else {

                    //     if(doc2){

                    //         let NOTI = new NOTIFY();
    
                    //         NOTI.AGV_ID = doc2.agv_ID;
                    //         NOTI.Type = "mission_activity";
                    //         NOTI.Det = "Mission: "+doc2.Name+" > "+data.stat;
                    //         NOTI.TS = new Date();
            
                    //         NOTI.save((err,doc) => {
                    //             if(err){
                    //             console.log(err);
                    //             } else {
                    //             console.log("Mission Activity Sent to DB");
                    //             }

                    //         })
    
                    //         }

                    // }

                  }).catch(err=>{
                    console.log(err);
                  })

            }
        })

    })
    socket.on('reqSchedNMsName',data =>{
        var jsonNameReq = {}
        SCHED_DT.findOne({_id:data.NameSCHED},(err,doc)=>{
            if(err){
                //console.log('null')
            }else if(doc){
                socket.emit('resSchedName',doc.Name)
                
                MS_DT.findOne({_id:data.NameMS},(err,doc1)=>{
                    if(err){
                        //console.log('null')
                    }else if (doc1){
                        socket.emit('resMsName',doc1.Name)
                    }
                })
            }
        })
    })
}



// function findin_msdtact(match){
//     //console.log(match);
//     return new Promise((res, rej) => {
//         MS_DT_ACTIVITY.findOne({MS_ID:match},(err,doc) => {
//             if (err) rej(err);
//             //console.log(doc);
//             res(doc);
//         });
//     });
// }

// function findin_mapdt(match){
//     //console.log(match);
//     return new Promise((res, rej) => {
//         MAP_DT.findOne({_id:match},(err,doc) => {
//             if (err) rej(err);
//             //console.log(doc);
//             res(doc);
//         });
//     });
// }

// function findin_mapdtpt(match){
//     //console.log(match);
//     return new Promise((res, rej) => {
//         MAP_DT_PT.findOne({_id:match},(err,doc) => {
//             if (err) rej(err);
//             //console.log(doc);
//             res(doc);
//         });
//     });
// }

// socket.on('getmsact', function(data) {
//     var jsonMap2 = {};
//     process1();
//     async function process1(){
//     for(var loop=0;loop<data.length;loop++){

//             var fx = await findin_msdtact(data[loop].ms_ID);

//             var fy = await findin_mapdt(fx["MAP_ID"]);
//             var map = fy["Name"];
//             var Home_x = fy["Home_x"];
//             var Home_y = fy["Home_y"];
//             var Home_z = fy["Home_z"];
//             var Home_w = fy["Home_w"];
//             jsonMap= {map:map,Home_x:Home_x,Home_y:Home_y,Home_z:Home_z,Home_w:Home_w};
//             console.log(jsonMap);

//             var fz = await findin_mapdtpt(fx["PT_ID"]);
//             console.log(fz);

//         }
//     }
// })
