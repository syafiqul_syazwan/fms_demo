require('./model/db');

const fs = require('fs');
const session = require('express-session');
const passport = require('passport');
const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const bodyparser = require('body-parser');
const mqtt = require('mqtt');
const agvSettingC = require('./controller/agv_dt_controller');
const loginC = require('./controller/login_controller');
const dashboardC = require('./controller/dashboard_controller');
const armControlC = require('./controller/arm_control_controller');
const diagnosticC = require('./controller/diagnostic_controller');
const operatorC = require('./controller/operator_controller');
const sosC = require('./controller/sos_controller');
const teleopC = require('./controller/teleop_controller');
const errorC = require('./controller/error_controller');
const userC = require('./controller/user_controller');
const missionC = require('./controller/mission_controller');
const utilityC = require('./controller/utility_controller');
const auth = require('./auth/auth')(passport);
const flash = require('connect-flash');
//const router = express.Router()

require('./passport')(passport);
require('events').EventEmitter.defaultMaxListeners = 21;

var app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const mongoose = require('mongoose');
const swal = require('sweetalert2');

//modal
const HEALTH_DT = mongoose.model('HEALTH_DT');
const DIAG_DT = mongoose.model('DIAG_DT');
const SOS_DT = mongoose.model('SOS_DT');
const SOS_DT_REC = mongoose.model('SOS_DT_REC');
const NOTIFY = mongoose.model('NOTIFY');
//passport

// const passport = require('passport');
// require('./passport')(passport);

// // Global variable - for refresh_noti socket emit
// var prev_id;
// var prev_ts = "Not Set";
// var prev_type;
// var append;
// var prev_agvid;

const PORT = 3000;



app.use(bodyparser.urlencoded({
  extended: true
}));

app.use(bodyparser.json());
app.use(express.static(__dirname + '/public/'));
app.set('auth', path.join(__dirname, 'auth/'));
app.set('views', path.join(__dirname, 'views/'));
app.engine('hbs',exphbs({extname:'hbs',defaultLayout:'mainLayout', layoutsDir: __dirname + '/views/layouts/',
helpers: {
  ifCond: function(v1, v2, options) {
    if(v1 === v2) {
      return options.fn(this);
    }
    return options.inverse(this);
  },
  iftrue: function(v1,options) {
    if(v1) {
      return options.fn(this);
    }
    return options.inverse(this);
  },
  inc: function(v1,options) {
    return parseInt(v1) + 1;
  }
  ,
  add: function(v1,v2,options) {
    return parseFloat(v1) + parseFloat(v2);
  },
  json: function(v1) {
    return JSON.stringify(v1);
  },
  /*loopbat: function(array) {
    var json = new Array();
    var tryparse = array;
    for(var i = 0; i < tryparse.length;i++) {
      console.log(tryparse.length)
      json.push(tryparse[i].Batt);
      console.log(json);
    }
    return JSON.stringify(json);
  }*/
}}));
app.set('view engine','hbs');
//app.set('trust proxy', 1) // trust first proxy


app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
   // cookie: { secure: true }
  })
);


// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

// app.use(function(req,res,next) {
//   res.locals.success_msg = req.flash('success_msg');
//   res.locals.error_msg = req.flash('error_msg');
//   res.locals.error = req.flash('error');
//   next();
// });


// var KEY = fs.readFileSync('ONH001-AGV01-key.pem');
// var CERT = fs.readFileSync('ONH001-AGV01.pem');
// var CAfile = [fs.readFileSync('trusted_certificates.pem')];
// var mqtthost = '18.140.250.237';

// var options = {
// host: mqtthost,
// port: 8883,
// protocol: 'mqtts',
// username: 'agv01',
// password: 'agv01',
// ca: CAfile,
// key: KEY,
// cert: CERT,
// secureProtocol: 'TLSv1_2_method',
// rejectUnauthorized:false
// };

//const client = mqtt.connect(options);
const client = mqtt.connect('mqtt://broker.mqttdashboard.com');

client.on('connect', () => {
    console.log('mqtt connected');
    client.publish('test.foo.bar', "This is fms");
    client.subscribe('/a2f/ms/ht');
    client.subscribe('/a2f/sys/diag');
    client.subscribe('/a2f/nav/pt');
    client.subscribe('/f2a/sys/mv');
    client.subscribe('/f2a/arm/man');
    client.subscribe('/f2a/sys/man');//toggle
    client.subscribe('/a2f/arm/loc'); //arm location
    client.subscribe('/a2f/arm/fv'); //arm location
    client.subscribe('/a2f/arm/tray');//tray
    client.subscribe('/a2f/sys/sosp');
    //client.subscribe('a2f.ms.sr'); //suspend resume from AGV
})
  

//---------------------TOPIC FROM AGV TO FMS-------------------//

io.on('connection',socketio => {
     client.on('message',(topic,msg) => {
     
     //console.log(topic+":"+msg.toString()); 
//------------------------GET HEALTH DATA (/a2f/ms/ht)----------------------------------//     
 

  if(topic == '/a2f/ms/ht') {
      var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
      var jdata = JSON.parse(parsed.data); //parsed all nested string in data
      
     let doc = new HEALTH_DT();

     doc.AGV_ID = jdata.agv_ID;
     doc.Lat = jdata.loc.lat;
     doc.Long =jdata.loc.long;
     doc.Point_X =jdata.pnt.x;
     doc.Point_Y =jdata.pnt.y;
     doc.Spd_X =jdata.spd.x;
     doc.Spd_Z =jdata.spd.z;
     doc.Batt =jdata.batt;
     doc.Tmp_L =jdata.tmp.l;
     doc.Tmp_R =jdata.tmp.r;
     doc.MS_ID =jdata.ms.MS_ID;
     doc.MS_SCHED_ID =jdata.ms.SCHED_ID;
     doc.MS_ACT_ID =jdata.ms.ACT_ID;
     doc.MS_point =jdata.ms.point;
     doc.MS_act = jdata.ms.act;
     doc.MS_ms_exe = jdata.ms.ms_exe;
     doc.MS_act_exe= jdata.ms.act_exe;
     doc.charge = jdata.charge;
     doc.SR = jdata.SR;
     //doc.sos_err = jdata.sos.err;
    // doc.sos_oth = jdata.sos.oth;
    // doc.obu_env_temp = jdata.obu.env_temp;
    // doc.obu_humid = jdata.obu.humid;

     
     doc.TS = new Date();
     doc.save((err,doc) => {
       if(err) {
         console.log(err);
       }
       else {
         //console.log(doc);
         socketio.emit('agVht',jdata);

       }
     })
  }

if(topic == '/a2f/sys/sosp') {
  var parsed = JSON.parse(msg.toString());
  var sosdata = JSON.parse(parsed.data);
  var sosTS = new Date();

  // process_1();
  // async function process_1() {
  //   for(var i=0;i<sosdata.sos_ID.length;i++){
  //     var doc1 = await save
  //   }
  // }

  for(var i=0;i<sosdata.sos_ID.length;i++){
    SOS_DT.find({_id:sosdata.sos_ID[i]}).then(doc =>{
        //console.log("in");
        let docu = new SOS_DT_REC;
        docu.agv_ID = sosdata.agv_ID;
        docu.SOS_Type = doc[0].Name;
        docu.SOS_Res = doc[0].Res;
        docu.MAP = "";
        //docu.Remark = "";
        docu.ATS = sosTS;
        docu.TS = new Date;
        docu.status = "recorded";
        docu.save((err, doc1) => {
            if (err) {
                console.log(err);
            }
            else {

              console.log("server side sos topic data rcvd");

              let doc2 = new NOTIFY;
              doc2.AGV_ID = doc1.agv_ID;
              doc2.SOS_Type = doc1.SOS_Type;
              doc2.SOS_Res = doc1.SOS_Res;
              doc2.SOS_ATS = doc1.ATS;
              doc2.Det = "SOS ALERT FROM AGV: "+doc1.agv_ID;
              doc2.TS = new Date();
              doc2.save((err,doc3) => {
                if (err) {
                  console.log('null');
                } else {
                    // prev_id = doc3._id;
                    // prev_agvid = doc3.AGV_ID;
                    // prev_ts = doc3.ATS;
                    // prev_type = doc3.SOS_Type;

                    NOTIFY.find({},null,{sort:{TS: -1}}).then(dt => {
                      if(err) {
                        console.log(err);
                      } else {
                        //refresh_noti(dt);
                        socketio.emit('refresh_noti',dt);
                      }
                    })
                }
              })

              // // append if ATS is the same as the prev ATS
              // if (prev_ts == "Not Set") {

              //   let doc2 = new NOTIFY;
              //   doc2.AGV_ID = doc1.agv_ID;
              //   doc2.SOS_Type = doc1.SOS_Type;
              //   doc2.SOS_Res = doc1.SOS_Res;
              //   doc2.SOS_ATS = doc1.ATS;
              //   doc2.Det = "SOS ALERT FROM AGV: "+doc1.agv_ID;
              //   doc2.TS = new Date();
              //   doc2.save((err,doc3) => {
              //     if (err) {
              //       console.log('null');
              //     } else {
              //         prev_id = doc3._id;
              //         prev_agvid = doc3.AGV_ID;
              //         prev_ts = doc3.ATS;
              //         prev_type = doc3.SOS_Type;

              //         NOTIFY.find({},null,{sort:{TS: -1}}, (err,dt) => {
              //           if(err) {
              //             console.log(err);
              //           } else {
              //             refresh_noti(dt);
              //             //socketio.emit('refresh_noti',dt);
              //           }
              //         })
              //     }
              //   })

              // } else if (prev_ts != "" && prev_ts == doc1.ATS && prev_agvid == doc1.agv_ID) {

              //   append = prev_type.concat(', ',doc1.SOS_Type);

              //   NOTIFY.findOneAndUpdate({_id: prev_id},{$set:{SOS_Type: append, TS: new Date()}}, (err,doc) =>{
              //     if(err){
              //       console.log(err)
              //     } else {
              //       NOTIFY.find({},null,{sort:{TS: -1}}, (err,dt) => {
              //         if(err) {
              //           console.log(err);
              //         } else {
              //           refresh_noti(dt);
              //           //socketio.emit('refresh_noti',dt);
              //         }
              //       })
              //     }
              //   })

              // }



              

            }
        })
    })
  } 

  function refresh_noti(data) {
    console.log("refresh_noti() triggered");
        socketio.emit('refresh_noti',data);
  }

}






  //------------------arm tray---------------------------
  if(topic == '/a2f/arm/tray'){
    var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
    var TRdata = JSON.parse(parsed.data); //parsed all nested string in data
   // console.log(TRdata);
    socketio.emit('trayCont',TRdata);
  }
  //------------------SUSPEND RESUME TOPIC------------------------------------------------//
  //engineered by yunus and douzee
  // if(topic == 'a2f.ms.sr'){
  //   var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
  //   var SRdata = JSON.parse(parsed.data); //parsed all nested string in data
  //   console.log(SRdata);
  //   socketio.emit('suspendResume',SRdata);
  // }

  //---------------------------GET DIAGNOSIS DATA (/a2f/sys/diag)------------------------------//


  if(topic == '/a2f/sys/diag') {

       var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
       var jdata = JSON.parse(parsed.data); //parsed all nested string in data
      //console.log(msg.toString());
      let doc = new DIAG_DT();

      doc.AGV_ID = jdata.agv_ID
      doc.TwoDF = jdata.ldr['2df']
      doc.TwoDR =jdata.ldr['2dr']
      doc.ThreeD =jdata.ldr['3d']
      doc.Batt =jdata.batt
      doc.Mot_L =jdata.mot.l
      doc.Mot_R =jdata.mot.r
      doc.Cam =jdata.cam
      doc.CPU =jdata.cpu
      doc.RAM =jdata.ram
      doc.LLC =jdata.llc
      doc.OBU =jdata.obu
      doc.TOP = jdata.top
      doc.Esw =jdata.esw
      doc.Enc_L = jdata.enc.l
      doc.Enc_R =jdata.enc.r
      doc.TS = new Date()
      
      doc.save((err,doc) => {
        if(err) {
          console.log(err);
        }
        else {
            //console.log(doc);
            socketio.emit('agVdiag',jdata);
        }
      })

  }

//----------------------GET NAVIGATION (/a2f/nav/pt)--------------------------------//

  if(topic == '/a2f/nav/pt') {
    console.log(msg.toString());
  }

//----------------------GET ARM LOCATION DATA(/a2f/arm/loc)---------------------------------//
  if(topic == '/a2f/arm/loc') {
   
    var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
    var jdata = JSON.parse(parsed.data); //parsed all nested string in data

   socketio.emit('armLoc',jdata);
   //console.log(jsonObj.data)
  

} 

//----------------------GET FINGER LOCATION DATA(/a2f/arm/loc)---------------------------------//

if(topic == '/a2f/arm/fv') {
   
  var parsed = JSON.parse(msg.toString()); //parse object "data", all nested string
  var jdata = JSON.parse(parsed.data); //parsed all nested string in data

  socketio.emit('fingerLoc',jdata);
 //console.log(jsonObj.data)


} 



})


})

//------------------------------Included File(FMS FOLDER FOR VIEW)-----------------------------------//

app.use('/agvSetting',agvSettingC);
app.use('/dashboard',dashboardC);
app.use('/armControl',armControlC);
app.use('/diagnostic',diagnosticC);
app.use('/mapControl',teleopC);
app.use('/mission',missionC);
app.use('/sos',sosC);
app.use('/userSetting',userC);
app.use('/login',loginC);
app.use('/operation',operatorC);
app.use('/auth',auth);
app.use('/error',errorC);
app.use('/utility',utilityC);


io.on('connection',sosC.respond); // user socketio
io.on('connection',dashboardC.respond);
io.on('connection',agvSettingC.respond); // agv socketio
io.on('connection',missionC.respond); // agv socketio
io.on('connection',userC.respond); // user socketio
io.on('connection',operatorC.respond); // user socketio
io.on('connection',diagnosticC.respond); // user socketio
io.on('connection',utilityC.respond); 
io.on('connection',teleopC.respond); 


//---------------------TOPIC FROM FMS TO AGV-------------------//


//---------------------------------Toggle(/f2a/sys/man)--------------------------------//

io.on('connection',socket => {
  socket.on('sys/man',manToggle => {
    
    var strings = JSON.stringify(manToggle);
    var jobj = {data:strings};
    client.publish('/f2a/sys/man',JSON.stringify(jobj));
    //console.log(JSON.stringify(jobj));
  })
})


 //---------------------------------AGV MOVEMENT(/f2a/sys/mv)--------------------------------//

io.on('connection',socket => {
  socket.on('sys/mv',dmv => {
    var tempObj = {};
    tempObj.agv_ID = dmv.agv_ID;
    tempObj.x = "0";
    tempObj.z = "0";
    tempObj.cmd = "1";

    switch(dmv.agvMv) {
        case "f": tempObj.x = "0.3"; break;
        case "b": tempObj.x = "-0.3"; break;
        case "l": tempObj.z = "0.3"; break;
        case "r": tempObj.z = "-0.3"; break;
    }
    var strings = JSON.stringify(tempObj);
    var jobj = {data:strings};
   // console.log(jobj)
    client.publish('/f2a/sys/mv', JSON.stringify(jobj));
  })
})

 //--------------------------ARM MISSION (MANUAL CONTROL)(/f2a/arm/man)--------------------------------//
io.on('connection',socket => {
  socket.on('sys/arm', function (message) {
    console.log('MSG: ' + message);

          var tempObj = {};
          tempObj.agv_ID = "AT200_1";
          tempObj.x = "0";
          tempObj.y = "0";
          tempObj.z = "0";
          tempObj.w = "0";
          tempObj.p = "0";
          tempObj.r = "0";
          // tempObj.f = "0";
          // tempObj.s = "0";
          tempObj.f1 = "0";
          tempObj.f2 = "0";

          switch(message) {
              case "xP": tempObj.x = "0.9"; break; //x
              case "xN": tempObj.x = "-0.9"; break; //x
              case "yP": tempObj.y = "0.9"; break; //y
              case "yN": tempObj.y = "-0.9"; break; //y
              case "zP": tempObj.z = "0.9"; break; //z
              case "zN": tempObj.z = "-0.9"; break;//z
              case "roP": tempObj.w = "10"; break; //w
              case "roN": tempObj.w = "-10"; break; //w
              case "ptP": tempObj.p = "10"; break; //p
              case "ptN": tempObj.p = "-10"; break; //p
              case "yaP": tempObj.r = "10"; break; //r
              case "yaN": tempObj.r = "-10"; break; //r
              case "fP": tempObj.f1 = "10";  //fP
              case "fP": tempObj.f2 = "10"; break; //fP
              case "fN": tempObj.f1 = "-10";  //fN
              case "fN": tempObj.f2 = "-10"; break; //fN
            }
          var strings = JSON.stringify(tempObj);
          var jobj = {data:strings};
          client.publish('/f2a/arm/man', JSON.stringify(jobj));
  });
})





//--------------------------START SCHEDULE (TESTING ON 5 NOV FOR DANNY STE)(/f2a/ms/st)--------------------------------//
io.on('connection', socket => {


    socket.on('getmsactSt', dataOps => {
        // if(mission1 == 'startSched') {
        // var json = {"data":"{\"agv_ID\":\"AT200_1\",\"ms_ID\":\"1\",\"fname\":{\"map\":\"OneNorth_1351434245\",\"Home_x\":\"0.1\",\"Home_y\":\"0.1\",\"Home_z\":\"0.1\",\"Home_w\":\"0.1\"},\"activity\":[{\"pt\":\"warehouse_point1\",\"x\":\"-1.51833570004\",\"y\":\"0.256689786911\",\"z\":\"0.95759344101\",\"w\":\"-0.288104772568\",\"arm\":{\"cmd\":\"pick3\",\"j1\":\"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\":\"-6088 -6088\",\"p1\": \"0.2834 0.0 0.08 0 0 90,0.0 -0.1572 0.0 0 0 0,0.0 0.0 -0.13408 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"232.24 200.18 125.30 360.22 281.63 54.82\",\"p2\": \"0.10855 0.268 0.06 0 0 0,0.03 0.3303 0.0 10 0 0,0.0 0.0 -0.19 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"190.94 189.38 124.55 354.16 253.45 -21.74\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}},{\"pt\":\"point_reverse\",\"x\":\"-0.438266992569\",\"y\":\"1.52551674843\",\"z\":\"0.937858462334\",\"w\":\"-0.346919089556\",\"arm\":\"wait\"},{\"pt\":\"warehouse_point2\",\"x\":\"-0.069412946701\",\"y\":\"5.98229503632\",\"z\":\"0.331173986197\",\"w\":\"0.943560242653\",\"arm\":\"wait\"},{\"pt\":\"warehouse_home\",\"x\":\"4.75163269043\",\"y\":\"4.53939819336\",\"z\":\"0.944541335106\",\"w\":\"-0.328045785427\",\"arm\": \"wait\"}]}"}
              var strings = JSON.stringify(dataOps);
              var jobj = {data:strings};
              client.publish('/f2a/ms/st',JSON.stringify(jobj));
              console.log(JSON.stringify(jobj));
        //    }
    });

//--------------------SOS AGV-------------------------//

socket.on('sos/trig', dataTrig => {
  var strings = JSON.stringify(dataTrig);
        var jobj = {data:strings};
        client.publish('/f2a/sys/sos',JSON.stringify(jobj));
        console.log(JSON.stringify(jobj));
 
});

//---------------------------SHUT DOWN--------------------------------------//
socket.on('getSdReq', dataSd => {
  var strings = JSON.stringify(dataSd);
  var jobj = {data:strings};
  client.publish('/f2a/sys/off',JSON.stringify(jobj));
  console.log(JSON.stringify(jobj));
});

//-----------------------SUSPEND RESUME------------------------------//
//logic enginnered by douzee and yunus_yst
socket.on('reqSus',dataS =>{
  if(dataS=="suspend"){
    var schedObj = {"data":"{\"agv_ID\":\"AT200_1\",\"cmd\":\"1\"}"};
  }
  else if(dataS=="resume"){
    var schedObj = {"data":"{\"agv_ID\":\"AT200_1\",\"cmd\":\"0\"}"};
  }
  
  client.publish('/f2a/ms/sr',JSON.stringify(schedObj));
})

 //--------------------------ARM MISSION (AUTOMATICALLY CONTROL) (PRESET BUTTON) (/f2a/arm/mv)--------------------------------//

    socket.on('sys/arm/mv', preset => {
        if(preset == 'preset1') {
          var json = { "data": "{\"cmd\": \"pick1\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"-5388 -5388\",\"p1\": \"0.2834 0.0 0.08 0 0 90,0.0 -0.1572 0.0 0 0 0,0.0 0.0 -0.1408 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"232.24 200.18 125.30 360.22 281.63 54.82\",\"p2\": \"0.0 0.268 0.0 0 0 0,0.1278 0.0 0.0 0 0 0,0.0 0.179 0.0 10 0 0,0.0 0.0 -0.145 0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"167.25 198.22 120.15 361.88 280.52 -11.98\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}"}

          client.publish('/f2a/arm/pr',JSON.stringify(json));
        }
        if(preset == 'preset2') {
          var json = { "data": "{\"cmd\": \"drop1\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"-5388 -5388\",\"p1\": \"0.4206 0.0 0.100 0 0 92,0.0 0.297 0.0 0 0 0,0.0 0.0 -0.1485 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"167.25 198.22 120.15 361.88 280.52 -11.98\",\"p2\": \"-0.1482 -0.2622 0.02 0 0 0,0.0 -0.1986 0.0 0 0 0,0.0 0.0 -0.1444 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"233.67 196.68 118.52 358.03 278.43 58.85\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}"}

          client.publish('/f2a/arm/pr',JSON.stringify(json));

        }
        if(preset == 'preset3') {
          var json = { "data": "{\"cmd\": \"pick2\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"-5388 -5388\",\"p1\": \"0.2834 0.0 0.08 0 0 90,0.0 -0.1572 0.0 0 0 0,0.0 0.0 -0.1408 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"232.24 200.18 125.30 360.22 281.63 54.82\",\"p2\": \"0.0 0.268 0.0 0 0 0,-0.0831 0.0 0.0 0 0 0,0.0 0.179 0.0 10 0 0,0.0 0.0 -0.140 0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"205.70 169.53 92.29 353.51 247.71 -12.63\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}"}

          client.publish('/f2a/arm/pr',JSON.stringify(json));

        }
        if(preset == 'preset4') {
          var json = { "data": "{\"cmd\": \"drop2\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"-5388 -5388\",\"p1\": \"0.215657 0.0 0.100 0 0 90,0.0 0.291 0.0 0 0 0,0.0 0.0 -0.1535 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"158.16 163.59 77.03 361.92 265.93 -15.99\",\"p2\": \"0.0705 -0.4418 0.05 0 0 0,0.0 0.0 -0.1554 0 -5 0,0.0 0.0 0.0 0 0 0,0.0 0.0 0.0 0.0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"233.67 196.68 118.52 358.03 278.43 58.85\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}"}

          client.publish('/f2a/arm/pr',JSON.stringify(json));

        }
        if(preset == 'preset5') { //pick3
          var json = { "data": "{\"cmd\": \"pick3\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"-5388 -5388\",\"p1\": \"0.2834 0.0 0.08 0 0 90,0.0 -0.1572 0.0 0 0 0,0.0 0.0 -0.1408 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"232.24 200.18 125.30 360.22 281.63 54.82\",\"p2\": \"0.10855 0.268 0.07 0 0 0,0.04 0.3303 0.0 10 0 0,0.0 0.0 -0.20 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"190.94 189.38 124.55 354.16 253.45 -21.74\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}"}

          client.publish('/f2a/arm/pr',JSON.stringify(json));

        }
        if(preset == 'preset6') { //drop3
          var json = { "data": "{\"cmd\": \"drop3\",\"j1\": \"273.49 163.35 65.14 359.13 260.16 185.65\",\"fv1\": \"-5388 -5388\",\"p1\": \"0.4206 0.0 0.1005 0 0 0,0.0 0.4483 0.0 10 0 90,0.0 0.0 -0.15105 0 0 0,0.0 0.0 0.0 0 0 0\",\"fv2\": \"6250 6250\",\"j2\": \"155.64 203.08 142.35 359.50 294.87 -20.30\",\"p2\": \"0.0 -0.29858 -0.02 0 0 0,-0.12651 0.0 -0.02 0 0 0,0.0 -0.2648 0.0 0 0 0,0.0 0.0 -0.1658 0 0 0\",\"fv3\": \"-5394 -5394\",\"j3\": \"235.43 185.31 99.63 355.76 253.67 60.88\",\"fv4\": \"5400 5400\",\"j4\": \"269.19 84.68 39.73 359.89 239.92 354.67\"}"}

          client.publish('/f2a/arm/pr',JSON.stringify(json));

        }
   })

})

http.listen(PORT, () =>{
  console.log('listening on PORT: ' + PORT);
});

