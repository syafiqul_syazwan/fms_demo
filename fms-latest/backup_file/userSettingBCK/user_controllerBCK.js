const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('USR_DT');
const AGV_DT = mongoose.model('AGV_DT');
const SCHED_DT = mongoose.model('SCHED_DT');
const ACT_DTS = mongoose.model('ACT_DTS');
var NOTIFY = mongoose.model('NOTIFY');

var noti;
var count;

var info = [];
const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
		//console.log(req.user)
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        
                        return res.redirect('./error') 
                        	
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                       
                            return res.redirect('./error')
                      
                        }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                     
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}

router.get('/',loggedin,(req,res) => {

    //---------------- NOTIFICATION ------------------//

    NOTIFY.find({},null,{sort:{TS: -1}},(err,doc)=>{
        if(err)
        console.log(err);
        else{

            noti = doc;
            count = doc.length;
            
            User.find({status:"recorded"},(err,doc) => {
                if(err)
                    console.log(err);
                else {
                    AGV_DT.find({status:"recorded"},(err,doc1) => {
                        if(err) {
                            console.log(err);
                        }
                        else {
                            //console.log(doc[0].TS);
                            res.render('userSetting/index', {
                                title : "USER SETTING",
                                user: doc,
                                agv : doc1,
                                name : info[0].Name,
                                userid: info[0].userId,
                                role: info[0].Role,
                                pass: info[0].Password,
                                username: info[0].Username,
                                // notify: noti,
                                // noti_count: count
                            });
                        }
                    })
                }
            })

        }
    })

});

// router.get('/',(req,res) => {
//     User.find({},(err,doc) => {
//         if(err)
//             console.log(err);
//         else {
//             res.render('userSetting/index', {
//                 title : "USER SETTING",
//                 user: doc
//             });
//         }
//     })
    
// });


// router.post('/register',(req,res) => {
//     User.findOne()
// })



module.exports = router;

module.exports.respond = socket => {

    socket.on('register', data => {
       
          
                        var record = new User();
                       // console.log(data)
                        record.Name = data.Name;
                        record.Username = data.Username;
                        record.Pass = record.hashPassword(data.Pass);
                        record.Type = data.Type;
                        record.TS = new Date();
                        record.Login_Attempt = "0";
                        record.Login_Status = "NORMAL";
                        record.status = "recorded";
                        
                       // console.log(record + 'record')
                        record.save((err,doc) => {
                        if(err) {
                            console.log(err)
                            socket.emit('userASave','errCantAdd');
                            
                        }
                        else {
                                console.log('done masuk backup');
                            socket.emit('userASave',data.Username);
                               
                               
                            // var ACT = new ACT_DTS();
                            // ACT.User_Name = info[0].Username;
                            // ACT.Activity = 'New User : "'+ data.Username + '" has been added';
                            // ACT.Type = "USER SETTING PAGE";
                            // ACT.TS = new Date();
                            // ACT.status = "recorded";

                            // ACT.save((err,docAT4) => {
                            //     if(err){
                            //         //throw err;
                            //         console.log("err");
                            //     } else {
                            //        // console.log("New User : "+ data.Username);
                            //         //socket.emit('deleteAGVs',doc);
                            //     }
                            // })

                        }
                    });
                 
               
       

    })

    socket.on('checkUN', data =>{
        User.findOne({Username:data},(err,doc)=>{
            if(!doc){
                socket.emit('checkUNS','false')
            }else{
                socket.emit('checkUNS','true')
            }
        })
    
    });

    socket.on('reqEUsr',data => {
        User.findOne({_id:data._id},(err,doc) => {
            if(err) {
                console.log(err);
            }
            else {
            //console.log(doc);
                socket.emit('resEUsr',doc);
            }
        })
    })

    socket.on('updateUSR',(data) => {
     // console.log(data)
        User.findOne({_id:data._id},(err,doc) => {
            if(!doc) {
                console.log(err);
            }
            else {
                if(data.Username == data.Username){

                    User.findOneAndUpdate({_id:data._id},{$set:{Username:data.Username,Name:data.Name,Type:data.Type}},(err,doc4) => {
                        if(!doc4) {
                            console.log(err);
                        }
                        else {
                        //   console.log(doc3);
                           socket.emit('updateUsrs',doc4);
                           

                                   var ACT = new ACT_DTS();
                                   ACT.User_Name = info[0].Username;
                                   ACT.Activity = 'User : "'+ data.Username + '" updated successfully';
                                   ACT.Type = "USER SETTING PAGE";
                                   ACT.TS = new Date();
                                   ACT.status = "recorded";

                                   ACT.save((err,docAT4) => {
                                       if(err){
                                           //throw err;
                                           console.log(err);
                                       } else {
                                          // console.log("New User : "+ data.Username);
                                           //socket.emit('deleteAGVs',doc);
                                       }
                                   })

                            }
                        })



                }else{


                
                User.findOne({Username:data.Username},(err,doc2) => {
                    if(doc2) {
                        console.log('already exist');
                        socket.emit('updateUsrs','err')
                    }
                    
                    else{
 
                        var current_user_Username = doc.Username;
                        console.log(current_user_Username);
                        // console.log(data.Pass)
                        var record = User();
                        // if(data.Pass == ''){
                        //     var PasswordNot
                        // }


                         User.findOneAndUpdate({_id:data._id},{$set:{Username:data.Username,Name:data.Name,Type:data.Type}},(err,doc3) => {
                             if(!doc3) {
                                 console.log(err);
                             }
                             else {
                             //   console.log(doc3);
                                socket.emit('updateUsrs',doc3);
                                

                                        var ACT = new ACT_DTS();
                                        ACT.User_Name = info[0].Username;
                                        ACT.Activity = 'User : "'+ current_user_Username + '" updated successfully';
                                        ACT.Type = "USER SETTING PAGE";
                                        ACT.TS = new Date();
                                        ACT.status = "recorded";

                                        ACT.save((err,docAT4) => {
                                            if(err){
                                                //throw err;
                                                console.log(err);
                                            } else {
                                              //  console.log("New User : "+ data.Username);
                                                //socket.emit('deleteAGVs',doc);
                                            }
                                        })

                                 }
                             })
                         }
                    })
                     }
                }
            })
    });

    socket.on('deleteUser',data => {
        var interval = 1 * 10; // 10 seconds;
        User.find({_id:data._id},(err,doc7)=>{
            if(err){

            }else{
                for(var i = 0; i < doc7.length; i++) {
                    setTimeout( function (i) {
                        SCHED_DT.findOne({USR_ID:doc7[i]._id},(err,doc8)=>{
                            if(!doc8){
                                console.log('cannot find sched')
                            }else{
                                                var ACT = new ACT_DTS();

                                                ACT.User_Name = info[0].Username;
                                                ACT.Activity = 'Schedule : "'+ doc8._id + '" has been deleted';
                                                ACT.Type = "USER SETTING PAGE";
                                                ACT.TS = new Date();
                                                ACT.status = "recorded";
                    
                                                ACT.save((err,docAT4) => {
                                                    if(err){
                                                        //throw err;
                                                        console.log(err);
                                                    } else {
                                                       // console.log("New User : "+ doc9._id);
                                                        //socket.emit('deleteAGVs',doc);
                                                    }
                                                })
                                //console.log('sched_dt successfully deleted')
                                    for(var j = 0; j < doc8.length; j++) {
                                        setTimeout( function (j) {
                                           SCHED_DT_MS.findOneAndUpdate({SCHED_ID:doc7[j]._id},{$set:{status:"deleted"}},(err,doc9)=>{
                                            if(!doc9){
                                                console.log('Sched DT MS null')
                                            }else{
                                                console.log('sched_dt_ms successfully deleted')
                                                
                                            }
                                        })
                                    }, interval * j, j); 
                                }
                                
                            }
                        })
                   
                    }, interval * i, i); 
                }
                for(var z = 0; z < doc7.length; z++) {
                    setTimeout( function (z) {
                SCHED_DT.findOneAndUpdate({SCHED_ID:doc7[z]._id},{$set:{status:"deleted"}},(err,doc11)=>{
                    if(!doc11){
                        console.log('Sched null')
                    }else{
                        console.log('sched_dt successfully deleted')
                        

                        }
                    })
                }, interval * z, z); 
            }

            User.findOneAndUpdate({_id:data._id},{$set:{status:"deleted"}},(err,doc) => {
                if(err) {
                    console.log(err)
                }
                else {
                    
                    console.log('deleted successfully');
                    socket.emit('deleteUsers',doc);


                            var ACT = new ACT_DTS();
                            ACT.User_Name = info[0].Username;
                            ACT.Activity = 'User : "'+ doc.Username + '" has been deleted';
                            ACT.Type = "USER SETTING PAGE";
                            ACT.TS = new Date();
                            ACT.status = "recorded";

                            ACT.save((err,docAT4) => {
                                if(err){
                                    //throw err;
                                    console.log(err);
                                } else {
                                 //   console.log("New User : "+ doc._id);
                                    //socket.emit('deleteAGVs',doc);
                                }
                            })
                }
            })
            }

        })
        
    })

socket.on('resetPassword',datas =>{
    var record = User();
    console.log(datas)
    User.findOneAndUpdate({_id:datas._id},{$set:{Login_Attempt:0,Login_Status:'NORMAL',Pass:record.hashPassword(datas.AutoGen)}},(err,doc) => {
        if(err){
            console.log('error');
            socket.emit('resetPasswordS','error')
        }else{
            console.log('done')
            socket.emit('resetPasswordS',datas.AutoGen)

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'User Password : "'+ datas._id + '" has been updated';
            ACT.Type = "USER SETTING PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                   // console.log("New User : "+ doc._id);
                    //socket.emit('deleteAGVs',doc);
                }
            })
        }

    })

})
socket.on('hashedPass',data =>{
    
    User.findOne({_id:data.IDPass},(err,doc)=>{
        var stored_hash = doc.comparePassword(data.NewPassUsr,doc.Pass)
        if(stored_hash){
            socket.emit('hashedPassS', stored_hash)
         }
         else{
            socket.emit('hashedPassS', stored_hash)
         }
       
    })
})

socket.on('resetPass',data =>{
    var record = User();
    User.findOneAndUpdate({_id:data.ID},{$set:{Pass:record.hashPassword(data.NewPassword)}},(err,doc)=>{
        if(!doc){
            console.log('null');
        }else{
            console.log('done')
            socket.emit('resetPassS','done')
            
            // var ACT = new ACT_DTS();
            // ACT.User_Name = info[0].Username;
            // ACT.Activity = 'User Password : "'+ data.ID + '" has been reset';
            // ACT.Type = "USER SETTING PAGE";
            // ACT.TS = new Date();
            // ACT.status = "recorded";

            // ACT.save((err,docAT4) => {
            //     if(err){
            //         //throw err;
            //         console.log(err);
            //     } else {
            //        // console.log("New User : "+ doc._id);
            //         //socket.emit('deleteAGVs',doc);
            //     }
            // })
        }  

    })
})



}