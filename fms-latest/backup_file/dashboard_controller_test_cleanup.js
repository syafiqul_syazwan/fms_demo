var express= require('express');
var router = express.Router();
var mongoose = require('mongoose');
var HEALTH_DT = mongoose.model('HEALTH_DT');
var MS_DT = mongoose.model('MS_DT');
var USR_DT = mongoose.model('USR_DT');
var AGV_DT = mongoose.model('AGV_DT');
var ACT_DTS = mongoose.model('ACT_DTS');
var NOTIFY = mongoose.model('NOTIFY');
var NOTI_SEEN = mongoose.model('NOTI_SEEN');

var noti;
var count;

var usernow;

var info = [];

const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
		//console.log(req.user)
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                        }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                      
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}


router.get('/',loggedin ,(req,res) => {

    usernow = info[0].userId;
    //console.log(info[0].userId)

    HEALTH_DT.find({status:'recorded'}).limit(10).sort({TS:-1}).exec((err, doc12) => {
        if(err) {
            console.log(err);
        }else{
            AGV_DT.find({status: 'recorded'},(err,docs) => {
                if(err){
                console.log(err);
                }
                else{
            
                    res.render('dashboard/index', {
                    title: 'DASHBOARD',
                    health_dt : doc12,
                    agv :  docs,
                    health_bat : parseInt(doc12[0].Batt)/56 * 100,
                    health_tmp : (parseInt(doc12[0].Tmp_R) + parseInt(doc12[0].Tmp_L))/2,
                    health_spd : (parseInt(doc12[0].Spd_X) + parseInt(doc12[0].Spd_Z))/2,
                    name : info[0].Name,
                    userid: info[0].userId,
                    role: info[0].Role,
                    pass: info[0].Password,
                    Lat: doc12[0].Lat,
                    Long: doc12[0].Long

                    })
                }
            })
        }
    })

})

module.exports = router; 

module.exports.respond = socket => {

//---------REALTIME NOTIFICATION SOCKET---------//
trig_noti: trigger_notification = (data) => {
    socket.emit('refresh_noti', data);
}
    
    socket.on('reqAgvHealth',data => {
		console.log(data);
		HEALTH_DT.find({AGV_ID:data.agv_Id}).limit(500).sort({TS:-1}).exec((err,doc) => {
			if(err) {
				console.log(err);
			}
			else {
				MS_DT.find({_id:doc.MS_ID},(err,doc2)=>{
					if(err) {
						console.log(err);
					}
					else {
						console.log(doc.MS_ID)
						socket.emit('resAgvHealth',doc);
                    
                        
                        var ACT = new ACT_DTS();
                        ACT.User_Name = info[0].Username;
                        ACT.Activity = "Health AGV Status Record has been filtered for '"+ data.agv_Id +"'";
                        ACT.Type = "DASHBOARD PAGE";
                        ACT.TS = new Date();
                        ACT.status = "recorded";

                        ACT.save((err,docAT4) => {
                            if(err){
                                //throw err;
                                console.log(err);
                            } else {
                                console.log("Health AGV Status Record has been filtered for '"+ data.agv_Id +"'");
                                //socket.emit('deleteAGVs',doc);
                            }
                        })
					}

				})
			}
		});
    })


    socket.on('agvDash', datas =>{
       var jsonAgv = [];
       HEALTH_DT.find({AGV_ID:datas, status:'recorded'}).limit(1).sort({TS:-1}).exec((err, doc) => {
        if(!doc) {
            console.log('null');
        }else{
           // console.log(doc[0].Batt)
        var health_bat = parseInt(doc[0].Batt)/56 * 100;
        var health_tmp = (parseInt(doc[0].Tmp_R) + parseInt(doc[0].Tmp_L))/2;
        // var health_spd = (parseInt(doc[0].Spd_X) + parseInt(doc[0].Spd_Z))/2;
        var health_spd = parseInt(doc[0].Spd_X);
        jsonAgv.push({agv_ID:datas,health_bat:health_bat,health_tmp:health_tmp,health_spd:health_spd})
       
        socket.emit('agvDashS',jsonAgv)


                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = "Health AGV Status Record has been filtered for '"+ datas +"'";
                ACT.Type = "DASHBOARD PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";

                ACT.save((err,docAT4) => {
                    if(err){
                        //throw err;
                        console.log(err);
                    } else {
                        console.log("Health AGV Status Record has been filtered for '"+ datas +"'");
                        //socket.emit('deleteAGVs',doc);
                    }
                })
            }
        })


    })

    
}

var dashboard_c = require('./dashboard_controller_test_cleanup.js/index.js')

//----------------------REALTIME NOTIFICATION - START--------------------------//

NOTIFY.findOne({},null,{sort:{_id: -1}}).then(doc => {
    if (doc) {
      noti_now = doc._id;
    }
  }).catch(err => {
    console.log(err);
  });

intervalFunc();

function intervalFunc() {

    NOTIFY.findOne({},null,{sort:{_id: -1}}).then(doc2 => {
      if (doc2) {
        if(noti_now != doc2._id) {
          refreshNoti();
          noti_now = doc2._id;
        }
      }
    }).catch(err => {
        console.log(err);
    });

  }

var userid = usernow;

var allnotify;
var allnotiseen;

function refreshNoti() {

    NOTIFY.find({},null,{sort:{_id: -1}}).then(doc => {
        if(doc) {
        allnotify = doc;
        notify_total = doc.length;

        NOTI_SEEN.find({USER_ID: userid}).then(doc1 => {

            if(doc1.length > 0) {
            allnotiseen = doc1;

                showNoti_ByUserId(allnotify, allnotiseen);
            } else {
                //console.log("NOTI_SEEN is empty");
                //socket.emit('refresh_noti', allnotify);
                //trigger_notification(allnotify);
                module.exports.trig_noti(allnotify);
            }
        }).catch(err => {
            console.log(err);
        })

        }

    }).catch(err => {
        console.log(err);
    })
  }

  function showNoti_ByUserId(allnotify, allnotiseen) {

    var index2del = [];

    var count = 0;

    for(x=0;x<allnotiseen.length;x++){
      for(y=0;y<allnotify.length;y++){
        if(allnotiseen[x].NOTI_ID == allnotify[y]._id){

          index2del = index2del.concat(y);

        }
      }

      count = count + 1;

    }

    if(count == allnotiseen.length){
        process_noti(index2del);
        index2del = [];
      }

  }

  function process_noti(index) {

    var count = 0;

    for(z=0;z<index.length;z++){
      allnotify.splice(index[z], 1);

      count = count + 1;

    }

    if(count == index.length){

            //socket.emit('refresh_noti', allnotify);
            //trigger_notification(allnotify);
            module.exports.trig_noti(allnotify);
            index = [];

      }
  }
  
setInterval(intervalFunc, 1500);
//setTimeout(intervalFunc, 1500);

//----------------------REALTIME NOTIFICATION - END--------------------------//


