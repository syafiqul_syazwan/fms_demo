const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const AGV_DT = mongoose.model('AGV_DT');
const USER_DT = mongoose.model('USR_DT');
const DIAG_DT = mongoose.model('DIAG_DT');
const HEALTH_DT = mongoose.model('HEALTH_DT');
const SCHED_DT = mongoose.model('SCHED_DT');
const SCHED_DT_MS = mongoose.model('SCHED_DT_MS');
const MS_DT = mongoose.model('MS_DT');
const MS_DT_ACTIVITY = mongoose.model('MS_DT_ACTIVITY');
const SOS_DT = mongoose.model('SOS_DT');
const SOS_DT_TYPE = mongoose.model('SOS_DT_TYPE');
const SOS_DT_RES = mongoose.model('SOS_DT_RES');
const SOS_DT_REC = mongoose.model('SOS_DT_REC');
const ACT_DTS = mongoose.model('ACT_DTS');
const UPD_DT = mongoose.model('UPD_DT');
const CAM_DT = mongoose.model('CAM_DT');
const MS_DT_REC2 = mongoose.model('MS_DT_REC2');

const ARM_DT = mongoose.model('ARM_DT');
const MAP_DT = mongoose.model('MAP_DT');
const MAP_DT_PT = mongoose.model('MAP_DT_PT');
var NOTIFY = mongoose.model('NOTIFY');
var NOTI_SEEN = mongoose.model('NOTI_SEEN');
var noti;
var count;

var info = [];
const loggedin = (req,res,next) => {
	if(req.isAuthenticated())
	{
		//console.log(req.user)
		if(req.user.role == 'OBSERVER')
                {
                    if(req.path == '/'){
                        return res.redirect('./error')  	
                    }
                
                    else{
                        console.log('nothing')
                    }
                            
                }
		else if(req.user.role == 'OPERATOR')
                 {
                    if(req.path == '/'){
                            return res.redirect('./error')
                        }
                        else{
                        console.log('nothing')
                        }
                                
                 }
		else if(req.user.role == 'ADMIN') 
                {
                    if(req.path == '/'){
                        info = [];
                        var id = req.user.id;
                        var name = req.user.name
                        var role = req.user.role
                        var pass = req.user.password
                        var username = req.user.username
                        info.push({userId:id,Name:name,Role:role,Password:pass,Username:username})
                        }
                        else{
                        console.log('nothing')
                        }
                        
                }
		else{

			return res.redirect('./error')

		}

	 	return next()
		 
	}
	else 
	{	
	res.redirect('/login')
	}
}

router.get('/',loggedin,(req,res) => {

//---------------- NOTIFICATION ------------------//

    NOTIFY.find({},null,{sort:{TS: -1}},(err,doc)=>{
        if(err)
        console.log(err);
        else{

            noti = doc;
            count = doc.length;

            AGV_DT.find({status: 'recorded'},(err,doc2) => {
                if(err){
                    console.log(err);
                }
                else{
                    USER_DT.find({status: 'recorded'},(err,doc3)=>{
                        if(err){
                            console.log(err);
                        }
                        else{
                            SOS_DT_RES.find({status: 'recorded'},(err,doc4)=>{
                                if(err){
                                    console.log(err);
                                }
                                else{
                                    SOS_DT_TYPE.find({status: 'recorded'},(err,doc5)=>{
                                        if(err){
                                            console.log(err);
                                        }
                                        else{
                                            ACT_DTS.find({status:'recorded'}).limit(100).sort({TS:-1}).exec((err, doc6) => {
                                                if(err){
                                                    console.log(err);
                                                }
                                                    else{
                                                        UPD_DT.find({status:'recorded'}).limit(100).sort({TS:-1}).exec((err, doc7) => {
                                                            if(err){
                                                                console.log(err);
                                                            }
                                                                else{
                                                                    SOS_DT.find({status:'recorded'}).limit(100).sort({TS:-1}).exec((err, doc8) => {
                                                                        if(err){
                                                                            console.log(err);
                                                                        }
                                                                            else{
                                                                                ARM_DT.find({status:'recorded'}).limit(100).sort({TS:-1}).exec((err, doc9) => {
                                                                                    if(err){
                                                                                        console.log(err);
                                                                                    }
                                                                                        else{
                                                                                            // ACT_DTS.find({status:'recorded'}).sort({TS:-1}).exec((err, doc7) => {
                                                                                            //     if(err){
                                                                                            //         console.log(err);
                                                                                            //     }
                                                                                            //         else{
                                                                                            //             console.log(doc7)
                                                                                            res.render('utility/index', {
                                                                                                title : "UTILITIES",
                                                                                                agv :  doc2,
                                                                                                usr : doc3, 
                                                                                                name : info[0].Name,
                                                                                                userid: info[0].userId,
                                                                                                role: info[0].Role,
                                                                                                pass: info[0].Password,
                                                                                                // notify: noti,
                                                                                                // noti_count: count,
                                                                                                res:doc4,
                                                                                                type:doc5,
                                                                                                act_list:doc6,
                                                                                                soft_list:doc7,
                                                                                                sos_list:doc8,
                                                                                                arm_list:doc9
                                                                                               })
                                                                                            }
                                                                                        })
                                                                                     }
                                                                                  })
                                                                               }
                                                                            })
                                                                          }
                                                                        })
                                                                     }
                                                                 })
                                                            }
                                                    })
                                                }
                                            })
                                        }
                                    })

        }
    })

})

module.exports = router

module.exports.respond = socket => {

    socket.on('AuditTRailUsrReq',data => {
        //console.log(data);
        USER_DT.findOne({_id:data.ID}).then(doc => {
            if(!doc){
                console.log('null')
            }else{
                 //console.log(doc);
             ACT_DTS.find({User_Name:doc.Username}).sort({TS:-1}).then(doc1 =>{
                if(!doc1){
                    console.log('null')
                }else{
                    //console.log(doc1)
                 socket.emit('AuditTRailUsrRes',doc1);

                            var ACT = new ACT_DTS();
                            ACT.User_Name = info[0].Username;
                            ACT.Activity = 'Request Audit Trail Record for : "'+ doc.Name + '"';
                            ACT.Type = "UTILITY PAGE";
                            ACT.TS = new Date();
                            ACT.status = "recorded";

                            ACT.save((err,docAT4) => {
                                if(err){
                                    //throw err;
                                    console.log(err);
                                } else {
                                    console.log('Request Audit Trail Record for : "'+ doc.Name + '"');
                                    //socket.emit('deleteAGVs',doc);
                                }
                            })

                }
             })
            }
          
        })
    })


//---------------------CURRENT REQUEST LIST-------------------------//

    socket.on('currentList',data =>{
        if(data=="AGV_DT"){
            AGV_DT.find({status:"recorded"},(err,doc1) => {
                if(err){
                    console.log('null')
                }else{
                    //console.log(doc1)
                    socket.emit('agvListTrash',doc1)
                }
            })
        }
        else if(data=="USR_DT"){
            USER_DT.find({status:"recorded"},(err,doc2) => {
                if(err){
                    console.log('null')
                }else{
                    //console.log(doc2)
                    socket.emit('userListTrash',doc2)
                }
            })
        }
        else if(data=="DIAG_DT"){
            DIAG_DT.find({status:"recorded"},(err,doc3) => {
                if(err){
                    console.log('null')
                }else{
                    socket.emit('diagListTrash',doc3)
                }
            })
        }
        else if(data=="HEALTH_DT"){
            HEALTH_DT.find({status:"recorded"},(err,doc4) => {
                if(err){
                    console.log('null')
                }else{
                    socket.emit('healthListTrash',doc4)
                }
            })
        }
        else if(data=="SCHED_DT"){
            SCHED_DT.find({status:"recorded"},(err,doc5) => {
                if(err){
                    console.log('null')
                }else{
                    socket.emit('schedListTrash',doc5)
                }
            })
        }
        else if(data=="SCHED_DT_MS"){
            SCHED_DT_MS.find({status:"recorded"},(err,doc6) => {
                if(err){
                    console.log('null')
                }else{
                    socket.emit('schedmsListTrash',doc6)
                }
            })
        }
        else if(data=="MS_DT"){
            MS_DT.find({status:"recorded"},(err,doc7) => {
                if(err){
                    console.log('null')
                }else{
                    socket.emit('msListTrash',doc7)
                }
            })
        }
        else if(data=="MS_DT_ACTIVITY"){
            MS_DT_ACTIVITY.find({status:"recorded"},(err,doc8) => {
                if(err){
                    console.log('null')
                }else{
                    socket.emit('msActListTrash',doc8)
                }
            })   
        }
        else if(data=="SOS_DT"){
           SOS_DT.find({status:"recorded"},(err,doc9) => {
                if(err){
                    console.log('null')
                }else{
                    socket.emit('sosListTrash',doc9)
                }
            })
        }
        else if(data=="SOS_DT_REC"){
            SOS_DT_REC.find({status:"recorded"},(err,doc10) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('sosRecListTrash',doc10);
               }
 
             })
        }
        else if(data=="SOS_DT_RES"){
            SOS_DT_RES.find({status:"recorded"},(err,doc11) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('sosResListTrash',doc11);
               }
 
             })
        }
        else if(data=="SOS_DT_TYPE"){
            SOS_DT_TYPE.find({status:"recorded"},(err,doc12) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('sosTypeListTrash',doc12);
               }
 
             })
        }
        else if(data=="UPD_DT"){
            UPD_DT.find({status:"recorded"},(err,doc13) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('updListTrash',doc13);
               }
 
             })
        }else if(data=="ACT_DTS"){
            ACT_DTS.find({ $or: [ { status: "recorded" }, { status:"audited" } ] },(err,doc14) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('actDtsListTrash',doc14);
               }
 
             })
        }else if(data=="NOTISEEN"){ 
            NOTI_SEEN.find({},(err,doc15) => { 
                if(err){ 
                    console.log('null') 
               }else{ 
                    socket.emit('actDtsListTrash',doc15); 
                     
               } 
  
             }) 
        }else if(data=="NOTIFY"){ 
            NOTIFY.find({},(err,doc16) => { 
                if(err){ 
                    console.log('null') 
               }else{ 
                    socket.emit('actDtsListTrash',doc16); 
               } 
  
             }) 
        }else{
            console.log('database null/error')
        }
    })


//---------------------ACHIEVE REQUEST LIST-------------------------//
    socket.on('archieveList',data =>{
        if(data=="AGV_DT"){
           
            AGV_DT.find({status:"deleted"},(err,doc1) => {
               if(err){
                    console.log('null')
               }else{
                    socket.emit('agvListTrashArc',doc1);
               }
            })
        }
        else if(data=="USR_DT"){
            USER_DT.find({status:"deleted"},(err,doc2) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('userListTrashArc',doc2);
               }
 
             })
        }
        else if(data=="DIAG_DT"){
            DIAG_DT.find({status:"deleted"},(err,doc3) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('diagListTrashArc',doc3);
               }
 
             })
        }
        else if(data=="HEALTH_DT"){
            HEALTH_DT.find({status:"deleted"},(err,doc4) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('healthListTrashArc',doc4);
               }
 
             })
        }
        else if(data=="SCHED_DT"){
            SCHED_DT.find({status:"deleted"},(err,doc5) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('schedListTrashArc',doc5);
               }
 
             })
        }
        else if(data=="SCHED_DT_MS"){
            SCHED_DT_MS.find({status:"deleted"},(err,doc6) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('schedMsListTrashArc',doc6);
               }
 
             })
        }
        else if(data=="MS_DT"){
            MS_DT.find({status:"deleted"},(err,doc7) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('msListTrashArc',doc7);
               }
 
             })
        }
        else if(data=="MS_DT_ACTIVITY"){
            MS_DT_ACTIVITY.find({status:"deleted"},(err,doc8) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('msActListTrashArc',doc8);
               }
 
             })
        }
        else if(data=="SOS_DT"){
            SOS_DT.find({status:"deleted"},(err,doc9) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('sosListTrashArc',doc9);
               }
 
             })
        }
        else if(data=="SOS_DT_REC"){
            SOS_DT_REC.find({status:"deleted"},(err,doc10) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('sosRecListTrashArc',doc10);
               }
 
             })
        }
        else if(data=="SOS_DT_RES"){
            SOS_DT_RES.find({status:"deleted"},(err,doc11) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('sosResListTrashArc',doc11);
               }
 
             })
        }
        else if(data=="SOS_DT_TYPE"){
            SOS_DT_TYPE.find({status:"deleted"},(err,doc12) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('sosTypeListTrashArc',doc12);
               }
 
             })
        }
        else if(data=="UPD_DT"){
            UPD_DT.find({status:"deleted"},(err,doc13) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('updListTrashArc',doc13);
               }
 
             })
        }else if(data=="ACT_DTS"){
            ACT_DTS.find({status:"deleted"},(err,doc14) => {
                if(err){
                    console.log('null')
               }else{
                    socket.emit('actDtsListTrashArc',doc14);
               }
 
             })
        }else{
            console.log('database null/error')
        }
    })


//--------------------TRASH CLEAR ALL DATA---------------------//

socket.on('ClearAllDataTrash', data =>{
       // console.log()
if(data.TrashID == 'current'){                                   //-----------------------------CURRENT-----------------------------------//


      if(data.DB == 'USR_DT'){
        USR_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc1)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'AGV_DT'){
        AGV_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc2)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'DIAG_DT'){
        DIAG_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc3)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'HEALTH_DT'){
        HEALTH_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc4)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'MS_DT'){
        MS_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc5)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'MS_DT_ACTIVITY'){
        MS_DT_ACTIVITY.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc6)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SCHED_DT'){
        SCHED_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc7)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SCHED_DT_MS'){
        SCHED_DT_MS.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc8)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SOS_DT'){
        SOS_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc9)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SOS_DT_REC'){
        SOS_DT_REC.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc10)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })  
      }else if(data.DB == 'SOS_DT_RES'){
        SOS_DT_RES.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc11)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SOS_DT_TYPE'){
        SOS_DT_TYPE.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc12)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
           
      }else if(data.DB == 'UPD_DT'){
          UPD_DT.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc13)=>{
                if(err){
                    socket.emit('ClearAllDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearAllDataTrashS', data.DB)
                }
          })
      }else if(data.DB == 'ACT_DTS'){
        ACT_DTS.updateMany({status:"recorded"},{"$set":{status: "deleted"}},(err,doc14)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })  
      }else if(data.DB == 'NOTISEEN'){ 
        NOTI_SEEN.deleteMany({},(err,doc15)=>{ 
            if(err){ 
                socket.emit('ClearAllDataTrashS', 'err') 
                console.log('null') 
            }else{ 
                socket.emit('ClearAllDataTrashS', data.DB) 
                console.log(data.DB) 
            } 
      })   
      }else if(data.DB == 'NOTIFY'){ 
        NOTIFY.deleteMany({},(err,doc16)=>{ 
            if(err){ 
                socket.emit('ClearAllDataTrashS', 'err') 
                console.log('null') 
            }else{ 
                socket.emit('ClearAllDataTrashS', data.DB) 
                console.log(data.DB) 
            } 
      })   
      }else{
          console.log('database null')
      }

}else if(data.TrashID == 'archieve'){                         //-----------------------------ACHIEVE-----------------------------------//
 
    if(data.DB == 'USR_DT'){
        USR_DT.deleteMany({status:"deleted"},(err,doc1)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'AGV_DT'){
        AGV_DT.deleteMany({status:"deleted"},(err,doc2)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'DIAG_DT'){
        DIAG_DT.deleteMany({status:"deleted"},(err,doc3)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'HEALTH_DT'){
        HEALTH_DT.deleteMany({status:"deleted"},(err,doc4)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'MS_DT'){
        MS_DT.deleteMany({status:"deleted"},(err,doc5)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'MS_DT_ACTIVITY'){
        MS_DT_ACTIVITY.deleteMany({status:"deleted"},(err,doc6)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SCHED_DT'){
        SCHED_DT.deleteMany({status:"deleted"},(err,doc7)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SCHED_DT_MS'){
        SCHED_DT_MS.deleteMany({status:"deleted"},(err,doc8)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SOS_DT'){
        SOS_DT.deleteMany({status:"deleted"},(err,doc9)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SOS_DT_REC'){
        SOS_DT_REC.deleteMany({status:"deleted"},(err,doc10)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })  
      }else if(data.DB == 'SOS_DT_RES'){
        SOS_DT_RES.deleteMany({status:"deleted"},(err,doc11)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
      }else if(data.DB == 'SOS_DT_TYPE'){
        SOS_DT_TYPE.deleteMany({status:"deleted"},(err,doc12)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })
           
      }else if(data.DB == 'UPD_DT'){
          UPD_DT.deleteMany({status:"deleted"},(err,doc13)=>{
                if(err){
                    socket.emit('ClearAllDataTrashS', 'err')
                    console.log('null')
                }else{
                    socket.emit('ClearAllDataTrashS', data.DB)
                }
          })
      }else if(data.DB == 'ACT_DTS'){
        ACT_DTS.deleteMany({status:"deleted"},(err,doc14)=>{
            if(err){
                socket.emit('ClearAllDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearAllDataTrashS', data.DB)
            }
      })  
      }else{
          console.log('database null')
      }

}else{
    console.log('null')
}



})


//--------------------TRASH CLEAR RECORDS FROM TABLE---------------------//

socket.on('ClearRecDataTrash', data =>{
    console.log(data)
if(data.Rectrash == 'current'){                                   //-----------------------------CURRENT-----------------------------------//


  if(data.DBRec == 'USR_DT'){
    USR_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc1)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'AGV_DT'){
    AGV_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc2)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'DIAG_DT'){
    DIAG_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc3)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'HEALTH_DT'){
    HEALTH_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc4)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'MS_DT'){
    MS_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc5)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'MS_DT_ACTIVITY'){
    MS_DT_ACTIVITY.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc6)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SCHED_DT'){
    SCHED_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc7)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SCHED_DT_MS'){
    SCHED_DT_MS.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc8)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SOS_DT'){
    SOS_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc9)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SOS_DT_REC'){
    SOS_DT_REC.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc10)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })  
  }else if(data.DBRec == 'SOS_DT_RES'){
    SOS_DT_RES.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc11)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SOS_DT_TYPE'){
    SOS_DT_TYPE.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc12)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
       
  }else if(data.DBRec == 'UPD_DT'){
      UPD_DT.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc13)=>{
            if(err){
                socket.emit('ClearRecDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearRecDataTrashS', data.DBRec)
            }
      })
  }else if(data.DBRec == 'ACT_DTS'){
    ACT_DTS.findOneAndUpdate({_id:data.RecordID},{"$set":{status: "deleted"}},(err,doc14)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })  
  }else{
      console.log('database null')
  }

}else if(data.Rectrash == 'archieve'){                         //-----------------------------ACHIEVE-----------------------------------//

if(data.DBRec == 'USR_DT'){
    USR_DT.deleteOne({_id:data.RecordID},(err,doc1)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'AGV_DT'){
    AGV_DT.deleteOne({_id:data.RecordID},(err,doc2)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'DIAG_DT'){
    DIAG_DT.deleteOne({_id:data.RecordID},(err,doc3)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'HEALTH_DT'){
    HEALTH_DT.deleteOne({_id:data.RecordID},(err,doc4)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'MS_DT'){
    MS_DT.deleteOne({_id:data.RecordID},(err,doc5)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'MS_DT_ACTIVITY'){
    MS_DT_ACTIVITY.deleteOne({_id:data.RecordID},(err,doc6)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SCHED_DT'){
    SCHED_DT.deleteOne({_id:data.RecordID},(err,doc7)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SCHED_DT_MS'){
    SCHED_DT_MS.deleteOne({_id:data.RecordID},(err,doc8)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SOS_DT'){
    SOS_DT.deleteOne({_id:data.RecordID},(err,doc9)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SOS_DT_REC'){
    SOS_DT_REC.deleteOne({_id:data.RecordID},(err,doc10)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })  
  }else if(data.DBRec == 'SOS_DT_RES'){
    SOS_DT_RES.deleteOne({_id:data.RecordID},(err,doc11)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
  }else if(data.DBRec == 'SOS_DT_TYPE'){
    SOS_DT_TYPE.deleteOne({_id:data.RecordID},(err,doc12)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })
       
  }else if(data.DBRec == 'UPD_DT'){
      UPD_DT.deleteOne({_id:data.RecordID},(err,doc13)=>{
            if(err){
                socket.emit('ClearRecDataTrashS', 'err')
                console.log('null')
            }else{
                socket.emit('ClearRecDataTrashS', data.DBRec)
            }
      })
  }else if(data.DBRec == 'ACT_DTS'){
    ACT_DTS.deleteOne({_id:data.RecordID},(err,doc14)=>{
        if(err){
            socket.emit('ClearRecDataTrashS', 'err')
            console.log('null')
        }else{
            socket.emit('ClearRecDataTrashS', data.DBRec)
        }
  })  
  }else{
      console.log('database null')
  }

}else{
console.log('null')
}



})

//-----------------ADD NEW SOS TYPE---------------------------//
    socket.on('SOSTypeAdd',data => {
       
       var record = new SOS_DT_TYPE();

       record.Type = data;
       record.TS = new Date();
       record.status = 'recorded';

       record.save((err,doc) => {
        if(err){
            console.log('not save')
        }else{
           // console.log('save');
            socket.emit('SOSTypeAddS','save')

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = "New SOS Type : '"+ data +"' has been added";
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";
    
            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log("New SOS Type : '"+ data +"' has been added");
                    //socket.emit('deleteAGVs',doc);
                }
            })
        }

       })

    })

//-----------------ADD NEW SOS RESPONSE----------------------------//
    socket.on('SOSResAdd',data => {
       
        var record = new SOS_DT_RES();
 
        record.Response = data;
        record.TS = new Date();
        record.status = 'recorded';
 
        record.save((err,doc) => {
         if(err){
             console.log('not save')
         }else{
            // console.log('save');
             socket.emit('SOSResAddS','save')

             var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = "New SOS Response : '"+ data +"' has been added";
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";
    
            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log("New SOS Response : '"+ data +"' has been added");
                    //socket.emit('deleteAGVs',doc);
                }
            })
             
         }
 
        })
 
     })

//-----------------------DELETE SOS TYPE----------------------------//

     socket.on('deleteSOSTypeReq',data => {
            // console.log(data)
             SOS_DT_TYPE.findOneAndUpdate({_id:data.sos_id_type},{$set:{status:"deleted"}},(err,doc) => {
                if(err)
                    socket.emit('deleteSOSTypeReqS','error')
                else {
                    socket.emit('deleteSOSTypeReqS','deleted')

                    var ACT = new ACT_DTS();
                    ACT.User_Name = info[0].Username;
                    ACT.Activity = "SOS Type : '"+ doc.Type +"' has been deleted";
                    ACT.Type = "UTILITY PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";
            
                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log("SOS Type : '"+ doc.Type +"' has been deleted");
                            //socket.emit('deleteAGVs',doc);
                        }
                    })

                }
            })


     })

//--------------------DELETE SOS RESPONSE---------------------------//

     socket.on('deleteSOSResponseReq',data => {
        //console.log(data)
        SOS_DT_RES.findOneAndUpdate({_id:data.sos_id_res},{$set:{status:"deleted"}},(err,doc) => {
            if(err)
                socket.emit('deleteSOSResponseReqS','error')
            else {
                socket.emit('deleteSOSResponseReqS','deleted')

                var ACT = new ACT_DTS();
                ACT.User_Name = info[0].Username;
                ACT.Activity = "SOS Response : '"+ doc.Response +"' has been deleted";
                ACT.Type = "UTILITY PAGE";
                ACT.TS = new Date();
                ACT.status = "recorded";
        
                ACT.save((err,docAT4) => {
                    if(err){
                        //throw err;
                        console.log(err);
                    } else {
                        console.log("SOS Response : '"+ doc.Response +"' has been deleted");
                        //socket.emit('deleteAGVs',doc);
                    }
                })

            }
        })


         })

//----------------------EDIT SOS TYPE------------------------------//

         socket.on('findSosIDType',(data) => {
            SOS_DT_TYPE.findOne({_id:data.sos_id_type}, (err,doc) => {
                if(doc) {
                    //console.log("get: " + doc.Type);
                    socket.emit('findSosIDTypeReq',doc._id)
                }
                else {
                    console.log(err);
                }
            })
        })

        socket.on('findSosIDTypeRes',data => {
           // console.log(data)
            SOS_DT_TYPE.findOneAndUpdate({_id:data._id},{$set:{Type:data.Type}},(err,doc) => {
                if(err){
                    socket.emit('findSosIDTypeResS','error')
                }else{
                   // console.log(doc)
                    socket.emit('findSosIDTypeResS','save')

                    SOS_DT.findOneAndUpdate({Name:doc.Type},{$set:{Name:data.Type}},(err,doc2) => {
                        if(err){
                            socket.emit('findSosIDTypeResS','error')
                        }else{
                             
                            var ACT = new ACT_DTS();
                            ACT.User_Name = info[0].Username;
                            ACT.Activity = "SOS Type : '"+ doc.Type +"' had changed to '"+ data.Type +"'";
                            ACT.Type = "UTILITY PAGE";
                            ACT.TS = new Date();
                            ACT.status = "recorded";
                    
                            ACT.save((err,docAT4) => {
                                if(err){
                                    //throw err;
                                    console.log(err);
                                } else {
                                    console.log("SOS Type : '"+ doc.Type +"' had changed to '"+ data.Type +"'");
                                    //socket.emit('deleteAGVs',doc);
                                }
                            })
                        }
                    })
                }

            })
        
        })

//----------------------------EDIT SOS RESPONSE---------------------------------------//

socket.on('findSosIDRes',(data) => {
    SOS_DT_RES.findOne({_id:data.sos_id_res}, (err,doc) => {
        if(doc) {
            //console.log("get: " + doc.Type);
            socket.emit('findSosIDResReq',doc._id)
        }
        else {
            console.log(err);
        }
    })
})

socket.on('findSosIDResponseRes',data => {
   // console.log(data)
    SOS_DT_RES.findOneAndUpdate({_id:data._id},{$set:{Response:data.Res}},(err,doc) => {
        if(err){
            socket.emit('findSosIDResponseResS','error')
        }else{
           // console.log(doc)
            socket.emit('findSosIDResponseResS','save')
            SOS_DT.findOneAndUpdate({Res:doc.Response},{$set:{Res:data.Res}},(err,doc2) => {
                if(err){
                    socket.emit('findSosIDTypeResS','error')
                }else{

                    var ACT = new ACT_DTS();
                    ACT.User_Name = info[0].Username;
                    ACT.Activity = "SOS Response : '"+ doc.Response +"' had changed to '"+ data.Res +"'";
                    ACT.Type = "UTILITY PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";
            
                    ACT.save((err,docAT4) => {
                        if(err){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log("SOS Response : '"+ doc.Response +"' had changed to '"+ data.Res +"'");
                            //socket.emit('deleteAGVs',doc);
                        }
                    })
                }
            })
        }

    })

})


socket.on('add/soft', data => {
  
   // console.log(data.agv_ID)
   UPD_DT.findOne({Ver:data.Ver},(err,doc) =>{
       if(doc){
           console.log('sama')
           socket.emit('add/SoftS','err');
       }else{
           console.log('save')

            var UPD = new UPD_DT();

            UPD.AGV_ID = data.agv_ID;
            UPD.Ver = data.Ver;
            UPD.Rem = data.Rem;
            UPD.TS = new Date();
            UPD.status = "recorded";

            UPD.save((err1,docUPD) => {
                if(err1){
                    //throw err;
                    console.log(err);
                } else {
                   // console.log(docUPD);
                    socket.emit('add/SoftS',docUPD);

                    var ACT = new ACT_DTS();
                    ACT.User_Name = info[0].Username;
                    ACT.Activity = 'New Software Update version : "'+ data.Ver +'" has been added';
                    ACT.Type = "UTILITY PAGE";
                    ACT.TS = new Date();
                    ACT.status = "recorded";
            
                    ACT.save((err2,docAT4) => {
                        if(err2){
                            //throw err;
                            console.log(err);
                        } else {
                            console.log('New Software Update version : "'+ data.Ver +'" has been added');
                            //socket.emit('deleteAGVs',doc);
                        }
                    })
                }
            })

       }
   })
})


//--------------------------------------MAP & TELEOP VARIABLE EDIT TAB-------------------------------------//

socket.on('reqAgvCamLink',data =>{

    CAM_DT.findOne({AGV_ID:data,status:"recorded"},(err,doc)=>{
        if(err){
            console.log('null cam_dt');
        }else{
            //console.log(doc)
            socket.emit('resAgvCamLink',doc)
        }
    })
})


//-----------------------------SOS ABNORMALITIES DELETE REC---------------------------------------------//

socket.on('deleteSos', data =>{
    SOS_DT.findOneAndUpdate({_id:data.sos_id, status:"recorded"},{$set:{status:"deleted"}},(err,doc) => {
        if(err) {
            console.log(err)
            socket.emit('deleteSosS','error');
        }
        else {
            
            console.log('deleted '+data.sos_id+' from sos_dt')
            socket.emit('deleteSosS','success');

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'SOS  : "'+ doc.Name + '" has been deleted';
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docAT4) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    //console.log('SOS  : "'+ doc.Name + '" has been deleted');
                    //socket.emit('deleteAGVs',doc);
                }
            })
        
        }
    })
})


socket.on('reqCamLink',data =>{
    console.log(data)
    CAM_DT.findOne({AGV_ID:data,status:"recorded"},(err,doc) => {
            if(err){
                console.log('null');
            }else{
               // console.log(doc)
                socket.emit('resCamLink',doc)
            }
    })
})

socket.on('updateCamPt', data =>{
    
    CAM_DT.findOneAndUpdate({AGV_ID:data.AGV_ID,status:"recorded"},{$set:{Link_Cam:data.CamLink,Link_PtCld:data.PtCloud}},(err,doc) => {
        if(err){
            console.log('null');
            socket.emit('updateCamPtS','err')
        }else{
           // console.log(doc)
            socket.emit('updateCamPtS',doc)
   
            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'Camera Link : "'+ doc.Link_Cam + '" changed to "'+ data.CamLink +'" and Point Cloud Link : "'+ doc.Link_PtCld +'" changed to "'+ data.PtCloud +'"';
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docCamLink) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                    console.log('Camera Link : "'+ doc.Link_Cam + '" changed to "'+ data.CamLink +'" and Point Cloud Link : "'+ doc.Link_PtCld +'" changed to "'+ data.PtCloud +'"');
                    //socket.emit('deleteAGVs',doc);
                }
            })

        }
})
})
socket.on('ValAarmX',data =>{
    
    var VAX = new ARM_DT()

    VAX.Name = data.Name;
    VAX.Cmd = data.Cmd
    VAX.TS = new Date();
    VAX.status = "recorded";

    VAX.save((err,docArmX) => {
        if(err){
            //throw err;
            socket.emit('ValAarmSX', 'err');
            console.log("err");

        } else {
            //console.log('SOS  : "'+ doc.Name + '" has been deleted');
            socket.emit('ValAarmSX',docArmX.Name);
            console.log('ok')
            console.log(docArmX)
        }
    })
})

socket.on('ValAarm',data =>{
    
    var arrArm = {};

    arrArm = ({cmd:data.cmdValue,j1:data.J1,p1:data.P1,fv1:data.FV1,j2:data.J2,p2:data.P2,fv2:data.FV2,j3:data.J3,p3:data.P3,fv3:data.FV3,j4:data.J4,p4:data.P4,fv4:data.FV4})

    var VA = new ARM_DT()

    VA.Name = data.Name;
    VA.Cmd = arrArm;
    VA.TS = new Date();
    VA.status = "recorded";

    VA.save((err,docArm) => {
        if(err){
            //throw err;
            socket.emit('ValAarmS', 'err');
            console.log("err");

        } else {
            //console.log('SOS  : "'+ doc.Name + '" has been deleted');
            socket.emit('ValAarmS',docArm.Name);
            //console.log(docArm)

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'New Arm : "'+ docArm.Name + '" has been added';
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docArmD) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                   // console.log("New User : "+ data.Username);
                    //socket.emit('deleteAGVs',doc);
                }
            })
        }
    })
})

//---------------------EDIT ARM VALUE------------------------------------//

socket.on('reqArmListCmd',data =>{
   ARM_DT.findOne({_id:data},(err,doc) => { 
        if(err){
            console.log('err list Out all arm data');
        }else{
            socket.emit('resArmListCmd',doc);
         //   console.log(doc)
        }
   })

})



//--------------------DELETE ARM VALUE-------------------------------------//

socket.on('reqDelArmValD',data =>{

    ARM_DT.findOneAndUpdate({_id:data},{$set:{status:"deleted"}},(err,doc)=>{
        if(err){
            socket.emit('resDelArmValD','err')
            console.log('err delete arm val')
        }else{
            socket.emit('resDelArmValD',doc.Name)

            var ACT = new ACT_DTS();
            ACT.User_Name = info[0].Username;
            ACT.Activity = 'Arm : "'+ doc.Name + '" has been deleted';
            ACT.Type = "UTILITY PAGE";
            ACT.TS = new Date();
            ACT.status = "recorded";

            ACT.save((err,docArmD) => {
                if(err){
                    //throw err;
                    console.log(err);
                } else {
                   // console.log("New User : "+ data.Username);
                    //socket.emit('deleteAGVs',doc);
                }
            })
        }
    })
})


socket.on('reqReportsBatchOps',data =>{
  //  console.log(data)
    if(data == 'AGV_HEALTH_STATUS'){
        HEALTH_DT.find({status:'recorded'}).limit(10).sort({TS:-1}).exec((err, doc) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc)
                socket.emit('resReportsBatchOps',doc)
            }
        })
    }else if(data == 'AGV_DIAG_STATUS'){
        DIAG_DT.find({status:'recorded'}).limit(10).sort({TS:-1}).exec((err, doc1) => {
            if(err){
                console.log('null')
            }else{
                //console.log(doc1)
                socket.emit('resReportsBatchOps',doc1)
            }
        })
    }else if(data == 'AUDIT_TRAIL'){
        ACT_DTS.find({status:'recorded'}).limit(10).sort({TS:-1}).exec((err, doc2) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc2)
            }
        })
    }else if(data == 'SCHED_HISTORY'){
        MS_DT_REC2.find({}).limit(10).sort({TS:-1}).exec((err, doc3) => {
            if(err){
                console.log('null')
            }else{
               // console.log(doc2)
                socket.emit('resReportsBatchOps',doc3)
            }
        })
    }else{
        console.log('null')
    }

})



socket.on('softDelProsReq',data =>{
  //  console.log(data)
    UPD_DT.findOneAndUpdate({_id:data},{$set:{status:'deleted'}},(err,doc)=>{
        if(err){
            console.log('null')
            socket.emit('softDelProsRes','err')
        }else{
            socket.emit('softDelProsRes',doc)
        }
    })
})


//logic engineered by yunus
socket.on('reqReportFilter',data => {

    if(data.titleDateFilter == 'AGV_HEALTH_STATUS'){
                                var dateF = new Date();
                                var dateT = new Date();
                                var FilterOpener = 0;
                                var FilterCloser = 0;
                                var FilterFrom = 0;
                                var FilterTo = 0;
                                var jsonArray=[];
                                var MissController = 0;
    
                              //  console.log(data);//display the message from reqDateDiag
                                // Find all data in DIAG_DT
                                var ACT = new ACT_DTS();
                                ACT.User_Name = info[0].Username;
                                ACT.Activity = "Health AGV Status Record has been filtered from '"+ data.dateFrom +"' to '" + data.dateTo + "'";
                                ACT.Type = "DASHBOARD PAGE";
                                ACT.TS = new Date();
                                ACT.status = "recorded";
    
                                ACT.save((err,docAT4) => {
                                    if(err){
                                        //throw err;
                                        console.log(err);
                                    } else {
                                       // console.log("Health AGV Status Record has been filtered from '"+ data.dateFrom +"' to '" + data.dateTo + "'");
                                        //socket.emit('deleteAGVs',doc);
                                    }
                                })
    
                                //console.log('masuk')
                                HEALTH_DT.find({status:'recorded'},(err, docs) => {
                                    
                                if(err){
                                   
                                    return;
                                    
                                }
                                if (docs.length) {//make sure DIAG_DT is content
                                        //do while is for controlling for loop
                                        //Whenever for loop end, the for loop will start again if date hit by user not found in database
                                        //The for loop will restart again with adding or substracting the input date 
                                       
                                        do{
                                            //loop for the from date hit by user
                                            //loop until it found and it will break
                                            for(var FilterFrom=0;FilterFrom<docs.length;FilterFrom++){
                                                //store the database date to dateFTSDiag to change its format.....for e.g 15/12/1996
                                                var dateFTSDiag = new Date(docs[FilterFrom].TS);
                                                var json = {dateResult:((dateFTSDiag.getDate() > 9) ? dateFTSDiag.getDate() : ('0' + dateFTSDiag.getDate()))  + '/' + ((dateFTSDiag.getMonth() > 8) ? (dateFTSDiag.getMonth() + 1) : ('0' + (dateFTSDiag.getMonth() + 1))) + '/' + dateFTSDiag.getFullYear()};
                                                //this selection act like a door
                                                //it will only open when date from is same as date in the database
                                                if(json.dateResult == data.dateFrom){
                                                    //FilterFrom will be initialise inside filterOpener
                                                    //It act like escape tool for do while loop and limiter for FilterTo
                                                    FilterOpener = FilterFrom;
                                                    //dateF will act like important variable to allow data send to another topic in view
                                                    dateF = json.dateResult;
                                                    //loop for the to date hit by user
                                                    for(var FilterTo=FilterOpener;FilterTo<docs.length;FilterTo++){
                                                        //store the database date to dateTTSDiag to change its format.....for e.g 15/12/1996
                                                        var dateTTSDiag = new Date(docs[FilterTo].TS);
                                                        var json2 = {dateResult2:((dateTTSDiag.getDate() > 9) ? dateTTSDiag.getDate() : ('0' + dateTTSDiag.getDate()))  + '/' + ((dateTTSDiag.getMonth() > 8) ? (dateTTSDiag.getMonth() + 1) : ('0' + (dateTTSDiag.getMonth() + 1))) + '/' + dateTTSDiag.getFullYear()};
                                                        //this selection act like a door
                                                        //it will only open when date to is same as date in the database
                                                        if(json2.dateResult2 == data.dateTo){
                                                            //FilterTo will be initialise inside filterCloser
                                                            //It act like a limiter for the for loop where the data will be send to topic receive
                                                            //  in view 
                                                            FilterCloser=FilterTo;
                                                            //dateT will act like important variable to allow data send to another topic in view
                                                            dateT = json2.dateResult2;
                                                        }
                                                        else{
                                                            continue;//continue the loop
                                                        }
                                                    }
                                                    if((FilterOpener!=0) && (FilterCloser!=0)){
                                                        break;//out of loop
                                                    }
                                                    if(((docs.length==FilterTo) && (FilterOpener>0)) && (FilterCloser==0)){//to date substractor
                                                    //  console.log(data.dateTo);
                                                        var txt = data.dateTo;
                                                        var splited = txt.split("/");//split date month and year....for e.g 15/12/1996
                                                        var dAte = splited[0];
                                                        var mOnth = splited[1];
                                                        var yEar = splited[2];
                                                        //change mOnth to format needed by date
                                                        if(mOnth == "01"){
                                                            mOnth = "Jan";
                                                        }
                                                        else  if(mOnth == "02"){
                                                            mOnth = "Feb";
                                                        }
                                                        else  if(mOnth == "03"){
                                                            mOnth = "Mar";
                                                        }
                                                        else  if(mOnth == "04"){
                                                            mOnth = "Apr";
                                                        }
                                                        else  if(mOnth == "05"){
                                                            mOnth = "May";
                                                        }
                                                        else  if(mOnth == "06"){
                                                            mOnth = "June";
                                                        }
                                                        else  if(mOnth == "07"){
                                                            mOnth = "July";
                                                        }
                                                        else  if(mOnth == "08"){
                                                            mOnth = "Aug";
                                                        }
                                                        else  if(mOnth == "09"){
                                                            mOnth = "Sept";
                                                        }
                                                        else  if(mOnth == "10"){
                                                            mOnth = "Oct";
                                                        }
                                                        else  if(mOnth == "11"){
                                                            mOnth = "Nov";
                                                        }
                                                        else  if(mOnth == "12"){
                                                            mOnth = "Dec";
                                                        }
                                                        // console.log(dAte);
                                                        // console.log(mOnth);
                                                        // console.log(yEar);
                                                        var day = new Date(mOnth + ' ' + dAte + ' ' + yEar);//day according to format
                                                        //console.log(day); // Apr 30 2000
                                
                                                        var nextDay = new Date(day);
                                                        nextDay.setDate(day.getDate() - 1);//substract the date
                                                    //  console.log("next day" + (( nextDay.getDate() > 9) ?  nextDay.getDate() : ('0' +  nextDay.getDate()))  + '/' + (( nextDay.getMonth() > 8) ? ( nextDay.getMonth() + 1) : ('0' + ( nextDay.getMonth() + 1))) + '/' +  nextDay.getFullYear()); // May 01 2000  
                                                        data.dateTo = ((( nextDay.getDate() > 9) ?  nextDay.getDate() : ('0' +  nextDay.getDate()))  + '/' + (( nextDay.getMonth() > 8) ? ( nextDay.getMonth() + 1) : ('0' + ( nextDay.getMonth() + 1))) + '/' +  nextDay.getFullYear());
                                                    //  console.log("true date" + data.dateTo);
                                                        FilterFrom = 0;//FilterFrom set to 0 to restart the for loop
                                                    }
                                                }
                                                else{
                                                    continue;//continue the loop
                                                }
                                            }    
                                            if((docs.length==FilterFrom) && (FilterOpener==0)){//from date additioner
                                                //console.log(data.dateFrom);
                                                var txt = data.dateFrom;
                                                var splited = txt.split("/");//split date month and year....for e.g 15/12/1996
                                                var dAte = splited[0];
                                                var mOnth = splited[1];
                                                var yEar = splited[2];
                                                //change mOnth to format needed by date
                                                if(mOnth == "01"){
                                                    mOnth = "Jan";
                                                }
                                                else  if(mOnth == "02"){
                                                    mOnth = "Feb";
                                                }
                                                else  if(mOnth == "03"){
                                                    mOnth = "Mar";
                                                }
                                                else  if(mOnth == "04"){
                                                    mOnth = "Apr";
                                                }
                                                else  if(mOnth == "05"){
                                                    mOnth = "May";
                                                }
                                                else  if(mOnth == "06"){
                                                    mOnth = "June";
                                                }
                                                else  if(mOnth == "07"){
                                                    mOnth = "July";
                                                }
                                                else  if(mOnth == "08"){
                                                    mOnth = "Aug";
                                                }
                                                else  if(mOnth == "09"){
                                                    mOnth = "Sept";
                                                }
                                                else  if(mOnth == "10"){
                                                    mOnth = "Oct";
                                                }
                                                else  if(mOnth == "11"){
                                                    mOnth = "Nov";
                                                }
                                                else  if(mOnth == "12"){
                                                    mOnth = "Dec";
                                                }
                                                // console.log(dAte);
                                                // console.log(mOnth);
                                                // console.log(yEar);
                                                var day = new Date(mOnth + ' ' + dAte + ' ' + yEar);//day according to format
                                                //console.log(day); // Apr 30 2000
    
                                                var nextDay = new Date(day);
                                                nextDay.setDate(day.getDate() + 1);//add the date
                                            // console.log("next day" + (( nextDay.getDate() > 9) ?  nextDay.getDate() : ('0' +  nextDay.getDate()))  + '/' + (( nextDay.getMonth() > 8) ? ( nextDay.getMonth() + 1) : ('0' + ( nextDay.getMonth() + 1))) + '/' +  nextDay.getFullYear()); // May 01 2000  
                                                data.dateFrom = ((( nextDay.getDate() > 9) ?  nextDay.getDate() : ('0' +  nextDay.getDate()))  + '/' + (( nextDay.getMonth() > 8) ? ( nextDay.getMonth() + 1) : ('0' + ( nextDay.getMonth() + 1))) + '/' +  nextDay.getFullYear());
                                            // console.log("true date" + data.dateFrom);
                                                FilterFrom = 0;//FilterFrom set to 0 to restart the for loop
                                            }
                                            //logic still in progress
                                            if(data.dateFrom==data.dateTo){
                                                FilterFrom=0;
                                                MissController=1;
                                            }
                                        }while(FilterFrom==0)//loop end when FilterFrom is not equalt to 0
                                        
                                      console.log(FilterOpener);
                                      console.log(FilterCloser);
                                      console.log(dateF);
                                      console.log(dateT);
                                        //act as a door...only enter when user input and process date are the same
                                        if((dateF == data.dateFrom) && (dateT == data.dateTo)){
                                        //  console.log("filled");
                                            for(var m=FilterOpener;m<FilterCloser;m++){
                                                //console.log(docs[m]);
                                               // if(docs[m].AGV_ID == data.agv_Id){
                                                    jsonArray.push(docs[m]);//push the all the data in array according to the range of date
                                               // }
                                            }
                                        }
                                        if(MissController==1){
                                            socket.emit('resDateHealth','empty');
                                        }
                                        
                                    } 
                                    else {
                                        console.log("empty");
                                        // there are no users
                                    }
                                    console.log(jsonArray);
                                    socket.emit('resDateHealth',jsonArray);//send the array to resDateDiag located in view
                                })
    
    }else if(data.titleDateFilter == 'AGV_DIAG_STATUS'){    // AGV_DIAG_STATUS
    
    
    
    
    }else if(data.titleDateFilter == 'AUDIT_TRAIL'){   //AUDIT TRAIL
    
    
    
    }else if(data.titleDateFilter == 'SCHED_HISTORY'){     //SCHEDULE HISTORY
    
    
    }else{
        console.log('nothing')
    }
    
    })

socket.on('reqDF2',data =>{  
        console.log(data);  
        if(data.batchType == 'AGV_DIAG_STATUS'){  
            DIAG_DT.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(100).exec((err, doc1) => {  
                if(err){  
                    console.log(err)  
                }else{   
                    socket.emit('resDIAG2',doc1)  
                }  
            })  
        } 
        else if(data.batchType=='AGV_HEALTH_STATUS'){  
            HEALTH_DT.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(100).exec((err, doc2) => {  
                if(err){  
                    console.log(err)  
                }else{  
                    socket.emit('resHT2',doc2)  
                }  
            })       
        }   
        else if(data.batchType == 'AUDIT_TRAIL'){ 
            ACT_DTS.find({status: "recorded",TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).limit(100).exec((err, doc3) => {  
                if(err){  
                    console.log(err)  
                }else{  
                    socket.emit('resAT2',doc3)  
                }  
            })  
        } 
        else if(data.batchType == 'SCHED_HISTORY'){ 
            MS_DT_REC2.find({TS: {$gte: data.dateFrom ,$lte: data.dateTo}}).exec((err, doc4) => {  
                if(err){  
                    console.log(err)  
                }else{   
                    socket.emit('resSCHED',doc4) 
                    console.log(doc4); 
                }  
            })  
        } 
    })

}//--------------Socket--------------//


 