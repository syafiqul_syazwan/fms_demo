var timeoutId = 0;

$('.cartesian-btn').on('mousedown', function() {
    var id = $(this).attr('id');
    timeoutId = setInterval(function() {
        socket.emit('sys/arm', id);
    }, 10);
}).on('mouseup mouseleave', function() {
    clearInterval(timeoutId);
});
