﻿//--Please categorize using classm, mfs_Global aims to be acceptable in any project, try not to specify elem id
//--or identities in here, we can move this class to nay project in future

//set global constant using Const declaration
//use static declaration for security to prevent code tamper
const SECONDS = 1000;
const MINUTES = 60 * SECONDS;
const HOURS = 60 * MINUTES;
const HTML_Btn_Loading = `<i class="fa fa-spinner fa-spin" style="font-size:1vw"></i> Loading`;
const Lang = 'en-US';
class fms_util {

    static shared_util_origin() {
        console.log("Hello World");
    }
    //appears in Views/usersetting/index.js
}

class Utils {

    static Datetime(el, tz) {
        try {
            if (tz) {
                tz = tz.toString();
            }
            else {
                tz = 'Asia/Singapore'
            }
                   el = el.toString();
            const formats = {timezone:tz, weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' }
            const rtndate = typeof el == "object" ? el.toLocaleString(Lang, formats).toString() : new Date(Date.parse(el)).toLocaleString(Lang, formats).toString();
            return rtndate;
        }
        catch (e) {
            console.log('SU time cnversion failed ' + e);
        }
    }


    static shakeElem(elem) {
        try {
            $(elem).effect('shake');

        }
        catch (er) {
            console.log(er);
        }
    }


}

function globalfx() {
    console.log("lets see if this succeeds");
    //appears in Views/usersetting/index.js
}
class FMS_check {
    //This class contains multiple Regex checks for strings

    static checkIM8(text) {
        //this instance check if a string contains 12 characters of at least 1 numeric number, 1 lower and upper alphabet and 1 special character
        //expect a True if the text meets the above criteria
        return /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{12,}$/.test(text);
    }

    static checkIfGotSpace(text) {
        //this instanct check if a string contains any blankspace( space or tabs)
        //exopect a True if the text meets the above criteria
        return /\s/.test(text);
    }

    static checkIfnotNull(text) {
        return /^.{1,}$/.test(text);
    }

}

class login_page {
    /*
    this class is specially for login page with speficied Id, as to make this script unrendered to inspect element for security
    */
    static usr_lot() {
        console.log("logout");
        var id = $("button#btn_savepwd").data('userid');
        socket.emit('UserUpdateLogout', id);

    }

}
