var timeoutId = 0;

$('.move').on('mousedown', function() {
    var id = $(this).attr('id');
    timeoutId = setInterval(function() {
        socket.emit('sys/mv', id);
    }, 10);
}).on('mouseup mouseleave', function() {
    clearInterval(timeoutId);
});
