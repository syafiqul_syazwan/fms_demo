#!/usr/bin/env python

from flask import Flask
import rospy


rospy.init_node("flask_node")
app = Flask(__name__,
            static_url_path='',
            static_folder='/root/catkin_ws/src/atlas200/src/static')

if __name__ == "__main__":
    app.debug = True
    app.run(host = '0.0.0.0',port=8080)
